package de.l3s.icrawl.online.time;

import org.joda.time.DateTime;

/**
 * Computes a relevance score based on a time difference.
 */
public interface TimeDifferenceRelevance {

    double computeRelevance(DateTime target, DateTime reference);

    double defaultValue();

}
