package de.l3s.icrawl.online.time;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Period;

/**
 * Compute a time difference relevance using the Weibull function.
 *
 * This function has the parameters <em>half decay</em> and <em>shape</em> that
 * determine how fast the values drop. The value is symmetric.
 */
public class WeibullTimeDifferenceRelevance implements TimeDifferenceRelevance {
    private static final double LOG_OF_2 = Math.log(2);
    private final double halfDecay;
    private final double shape;

    public WeibullTimeDifferenceRelevance() {
        this(Period.weeks(1), 1);
    }

    public WeibullTimeDifferenceRelevance(double halfDecay, double shape) {
        this.halfDecay = halfDecay;
        this.shape = shape;
    }

    public WeibullTimeDifferenceRelevance(Period halfDecayPeriod, double shape) {
        this(halfDecayPeriod.toStandardDuration().getMillis(), shape);
    }

    @Override
    public double computeRelevance(DateTime target, DateTime reference) {
        long millis = new Duration(target, reference).getMillis();
        return millis > 0 ? weibull(millis) : weibull(-millis);
    }


    private double weibull(long t) {
        // exp( -(t/L)^k * log(2) ), with L = half decay and k = shape
        double scaled = t / halfDecay;
        double shaped = -Math.pow(scaled, shape);
        return Math.exp(shaped * LOG_OF_2);
    }

    @Override
    public double defaultValue() {
        return 0.5;
    }

}
