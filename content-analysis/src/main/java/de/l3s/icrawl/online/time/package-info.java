/**
 * Compute a relevance score based on time (e.g. page freshness)
 */
package de.l3s.icrawl.online.time;