package de.l3s.icrawl.online;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.storage.WebPage;
import org.apache.nutch.util.TableUtil;
import org.openimaj.text.nlp.language.LanguageDetector;
import org.openimaj.text.nlp.language.LanguageDetector.WeightedLocale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Maps;

import de.l3s.icrawl.FieldNames;
import de.l3s.icrawl.domain.specification.CrawlSpecification;
import de.l3s.icrawl.domain.specification.CrawlUrl;
import de.l3s.icrawl.domain.specification.NamedEntity;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate;
import de.l3s.icrawl.util.net.Document;
import de.l3s.icrawl.util.net.SimpleDownloader;

import static de.l3s.icrawl.domain.support.ByteBufferUtils.asString;
import static java.util.function.Function.identity;

public final class DocumentVectorUtils {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final Logger logger = LoggerFactory.getLogger(DocumentVectorUtils.class);

    private DocumentVectorUtils() {
        // static methods only, forbid instantiation
    }

    /** Create a reference vector from the crawled seed documents. */
    public static DocumentVectorSimilarity createCrawlReferenceVector(
            CrawlDataRepositoryTemplate template, long campaignId, CrawlSpecification spec,
            boolean useKeywords, int maxTerms, boolean useDF) throws IOException {
        Map<String, Locale> referenceDocuments = getSeedDocuments(template, campaignId, spec);
        Set<String> keywords;
        Set<NamedEntity> entities;
        if (useKeywords) {
            keywords = spec.getKeywords();
            entities = spec.getEntities();
        } else {
            keywords = Collections.emptySet();
            entities = Collections.emptySet();
        }
        return new DocumentVectorSimilarity(referenceDocuments, keywords, entities, maxTerms, useDF);
    }

    /**
     * Create a reference vector by downloading the reference documents from the
     * live web.
     */
    public static DocumentVectorSimilarity createWebReferenceVector(CrawlSpecification spec)
            throws IOException {
        LanguageDetector languageDetector = new LanguageDetector();
        try (SimpleDownloader downloader = new SimpleDownloader()) {
            Collection<Document> fetchedDocuments = downloader.fetch(spec.getReferenceDocuments());
            Map<String, Locale> referenceDocuments = fetchedDocuments.stream()
                .map(Document::getContent)
                .collect(
                    Collectors.toMap(identity(), s -> languageDetector.classify(s).getLocale()));
            return new DocumentVectorSimilarity(referenceDocuments, spec.getKeywords(),
                spec.getEntities(), -1, false);
        }
    }

    static Map<String, Locale> getSeedDocuments(CrawlDataRepositoryTemplate template,
            long campaignId, CrawlSpecification spec) throws IOException {
        Set<CrawlUrl> seeds = spec.getSeeds();
        Map<String, Locale> referenceDocuments = Maps.newHashMapWithExpectedSize(seeds.size());
        LanguageDetector languageClassifier = new LanguageDetector();
        for (CrawlUrl crawlUrl : seeds) {
            try {
                WebPage webPage = template.getWebPage(campaignId, TableUtil.reverseUrl(crawlUrl.getUrl()));
                if (webPage != null && webPage.getText() != null) {
                    String text = webPage.getText().toString();
                    Locale locale = getOrDetectPageLanguage(webPage, crawlUrl, text, languageClassifier);
                    referenceDocuments.put(text, locale);
                } else {
                    logger.info("No record found for seed URL {} in campaign {}",
                        crawlUrl.getUrl(), campaignId);
                }
            } catch (IOException | RuntimeException e) {
                logger.info("Exception while processing seed {}, skipping", crawlUrl.getUrl(), e);
            }
        }
        return referenceDocuments;
    }

    private static Locale getOrDetectPageLanguage(WebPage webPage, CrawlUrl crawlUrl, String text,
            LanguageDetector languageClassifier) {
        String language = asString(webPage.getMetadata().get(FieldNames.LANGUAGE));
        Locale locale = Locale.forLanguageTag(language);
        if (locale == null) {
            WeightedLocale weightedLocale = languageClassifier.classify(text);
            logger.debug("estimated locale {}", weightedLocale.asMap());
            locale = weightedLocale.getLocale();
        }
        if (locale == null || locale.toString().trim().isEmpty()) {
            logger.info("could not detect language from text '{}'", text);
            locale = Locale.ENGLISH;
        }
        logger.debug("Got language {} ({}) for document {}", locale, language,
            crawlUrl.getUrl());
        return locale;
    }

    public static DocumentVectorSimilarity readFromConfiguration(Configuration configuration)
            throws IOException {
        String json = configuration.get(FieldNames.SIMILARITY);
        if (json == null || json.isEmpty()) {
            return null;
        }
        return OBJECT_MAPPER.readValue(json, DocumentVectorSimilarity.class);
    }

    public static void storeToConfiguration(Configuration conf, DocumentVectorSimilarity similarity)
            throws IOException {
        conf.set(FieldNames.SIMILARITY, OBJECT_MAPPER.writeValueAsString(similarity));
    }

}
