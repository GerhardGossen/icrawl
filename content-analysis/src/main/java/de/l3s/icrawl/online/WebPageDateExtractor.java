package de.l3s.icrawl.online;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.mapreduce.Mapper;
import org.apache.nutch.storage.WebPage;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.w3c.dom.traversal.DocumentTraversal;
import org.w3c.dom.traversal.NodeFilter;
import org.w3c.dom.traversal.TreeWalker;
import org.xml.sax.SAXException;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.io.LineProcessor;
import com.google.common.io.Resources;

import de.l3s.icrawl.util.DateUtils;
import de.l3s.icrawl.util.HtmlParseException;
import de.l3s.icrawl.util.WebPageUtils;

import static de.l3s.icrawl.util.DateUtils.isValidDate;
import static de.l3s.icrawl.util.HadoopUtils.incrementCount;
import static de.l3s.icrawl.util.WebPageUtils.fetchAndParse;
import static de.l3s.icrawl.util.WebPageUtils.parseHtml;

public class WebPageDateExtractor {
    private static final Logger logger = LoggerFactory.getLogger(WebPageDateExtractor.class);

    public enum DateSource {
        /** date is part of Web page URL */
        URL,
        /** date is contained in a {@code <time />} element */
        TIME,
        /** date is contained in a {@code <meta />} element */
        META,
        /** date was found in a paragraph containing a trigger word */
        TRIGGER_WORD,
        /** date was found in normal text content */
        TEXT_DATE,
        /** date was found in a HTTP header */
        HEADER,
        /** Modification date is unknown */
        NOT_FOUND
    }

    public static final class WebPageDate {
        public final DateTime date;
        public final DateSource dateSource;

        public WebPageDate(DateTime date, DateSource dateSource) {
            this.date = date;
            this.dateSource = dateSource;
        }

        @Override
        public String toString() {
            return String.format("%s [%s]", date, dateSource);
        }
    }

    private static final class DateIsValidPredicate implements Predicate<WebPageDate> {
        @Override
        public boolean apply(WebPageDate wpd) {
            return DateUtils.isValidDate(wpd.date);
        }
    }

    private static final class SkipElementsByNameFilter implements NodeFilter {
        @Override
        public short acceptNode(Node n) {
            if (n.getNodeName() == null) {
                return FILTER_ACCEPT;
            }
            if (SKIPPED_ELEMENTS.matcher(n.getNodeName()).matches()) {
                return FILTER_REJECT;
            }
            return FILTER_ACCEPT;
        }
    }

    public static class ExtractionException extends RuntimeException {
        private static final long serialVersionUID = 1L;

        public ExtractionException(String message, Throwable cause) {
            super(message, cause);
        }

        public ExtractionException(String message) {
            super(message);
        }

        public ExtractionException(Throwable cause) {
            super(cause);
        }

    }

    private static final Pattern DATE_TRIGGERS = Pattern.compile(
        "created?|updated?|modified|last modifi|änder", Pattern.CASE_INSENSITIVE);
    private static final Pattern SKIPPED_ELEMENTS = Pattern.compile("script|style",
        Pattern.CASE_INSENSITIVE);
    private static final Map<String, Integer> NAMES_TO_MONTH = namesMap();
    static final List<Pattern> DATE_PATTERNS = buildDatePattern(NAMES_TO_MONTH);

    /**
     * Retrieve the document and extract its likely modification date.
     *
     * @param documentUrl
     *            a resolvable URL
     * @return the modification date or null
     */
    public WebPageDate extractModifiedDateFromUrl(String documentUrl) {
        try {
            return extractModifiedDate(fetchAndParse(documentUrl));
        } catch (HtmlParseException e) {
            throw new ExtractionException("Could not parse document " + documentUrl, e);
        }
    }

    /**
     * Parse the document and extract its likely modification date.
     *
     * @param documentHtmlIS
     *            HTML source code of the document
     * @return the modification date or null
     */
    public static WebPageDate extractModifiedDateFromHtml(InputStream documentHtmlIS, String url) {
        try {
            return extractModifiedDate(parseHtml(documentHtmlIS, url));
        } catch (HtmlParseException e) {
            throw new ExtractionException("Could not parse document " + url, e);
        }
    }

    private static List<Pattern> buildDatePattern(Map<String, Integer> namesToMonth) {
        ImmutableList.Builder<Pattern> patterns = ImmutableList.builder();
        // YYYY-mm-dd
        patterns.add(Pattern.compile("(?<year>\\d{4})-(?<month>\\d{2})-(?<day>\\d{2})"))
        // dd. mm. yyyy
            .add(Pattern.compile("(?<day>\\d{1,2})\\.\\s*(?<month>\\d{1,2})\\.\\s*(?<year>\\d{4})"))
            // dd. mmm. yyyy
            .add(Pattern.compile("(?<day>\\d{1,2})\\.?\\s*(?<month>\\w+)\\.?\\s+(?<year>\\d{4})"))
        // mmm. dd.,? yyyy
            .add(Pattern.compile("(?<month>\\w+)\\s+(?<day>\\d{1,2})[\\.,]\\s*(?<year>\\d{4})"))
        // mm/dd/yyyy
            .add(Pattern.compile("(?<month>\\d{1,2})/(?<day>\\d{1,2})/(?<year>\\d{4})"))
        // dd/mm/yyyy
            .add(Pattern.compile("(?<day>\\d{1,2})/(?<month>\\d{1,2})/(?<year>\\d{4})"));
        return patterns.build();
    }

    private static Map<String, Integer> namesMap() {
        try {
            URL mappingsResource = Resources.getResource("de/l3s/icrawl/month_mappings.tsv");
            return Resources.readLines(mappingsResource, StandardCharsets.UTF_8,
                new LineProcessor<Map<String, Integer>>() {
                    private final ImmutableMap.Builder<String, Integer> namesBuilder = ImmutableMap.builder();

                    @Override
                    public boolean processLine(String line) throws IOException {
                        String[] split = line.split("\t", 2);
                        String key = split[0];
                        int value = Integer.parseInt(split[1]);
                        namesBuilder.put(key, value);
                        return true;
                    }

                    @Override
                    public Map<String, Integer> getResult() {
                        return namesBuilder.build();
                    }
                });
        } catch (IOException e) {
            logger.warn("Cannot initialize date extractor: ", e);
            return Collections.emptyMap();
        }
    }

    /**
     * Extract the likely modification date from a parsed document.
     *
     * @param dom
     *            DOM tree of an HTML document
     * @return the modification date or null
     */
    public static WebPageDate extractModifiedDate(Document dom) {
        Preconditions.checkNotNull(dom);
        Map<Element, WebPageDate> candidates = findCandidateElements(dom);
        logger.trace("Found {} candidates: {}", candidates.size(), candidates);
        candidates = Maps.filterValues(candidates, new DateIsValidPredicate());
        if (candidates.isEmpty()) {
            candidates = findElementsWithDate(dom);
        }
        candidates = Maps.filterValues(candidates, new DateIsValidPredicate());
        return candidates.isEmpty() ? null : candidates.values().iterator().next();
    }

    private static Map<Element, WebPageDate> findElementsWithDate(Document dom) {
        Map<Element, WebPageDate> candidates = new LinkedHashMap<>();
        TreeWalker iterator = ((DocumentTraversal) dom).createTreeWalker(findDomRoot(dom),
            NodeFilter.SHOW_TEXT | NodeFilter.SHOW_ELEMENT, new SkipElementsByNameFilter(), false);
        Node n;
        while ((n = iterator.nextNode()) != null) {
            if (n instanceof Text) {
                DateTime dateTime = findDateMatch(n.getTextContent());
                if (dateTime != null) {
                    Element element = WebPageUtils.findParagraphParent(n, -1);
                    candidates.put(element, new WebPageDate(dateTime, DateSource.TEXT_DATE));
                }
            }
        }
        return candidates;
    }

    private static Map<Element, WebPageDate> findCandidateElements(Document dom) {
        Map<Element, WebPageDate> candidates = new LinkedHashMap<>();
        NodeList timeElements = dom.getElementsByTagName("time");
        for (int i = 0; i < timeElements.getLength(); i++) {
            Element element = (Element) timeElements.item(i);
            DateTime date = getTimeElementDate(element);
            if (date != null) {
                candidates.put(element, new WebPageDate(date, DateSource.TIME));
            }
        }
        NodeList metaElements = dom.getElementsByTagName("meta");
        for (int i = 0; i < metaElements.getLength(); i++) {
            Element element = (Element) metaElements.item(i);
            DateTime date = getMetaElementDate(element);
            if (date != null) {
                candidates.put(element, new WebPageDate(date, DateSource.META));
            }

        }
        try {
        TreeWalker iterator = ((DocumentTraversal) dom).createTreeWalker(findDomRoot(dom),
            NodeFilter.SHOW_TEXT | NodeFilter.SHOW_ELEMENT,
            new SkipElementsByNameFilter(), false);
        Node n;
        while ((n = iterator.nextNode()) != null) {
            if (n instanceof Text && DATE_TRIGGERS.matcher(n.getTextContent()).find()) {
                extractDateFromTextNode(n, candidates);
            }
        }
        } catch (DOMException e) {
            logger.debug("Exception while iterating: ", e);
        }
        return candidates;
    }

    private static void extractDateFromTextNode(Node n, Map<Element, WebPageDate> candidates) {
        Element element = WebPageUtils.findParagraphParent(n, -1);
        DateTime dateTime = findDateMatch(element.getTextContent());
        if (dateTime != null) {
                candidates.put(element, new WebPageDate(dateTime, DateSource.TRIGGER_WORD));
        }
    }

    private static Node findDomRoot(Document dom) {
        NodeList bodyList = dom.getElementsByTagName("body");
        if (bodyList.getLength() > 0) {
            return bodyList.item(0);
        } else {
            return dom;
        }
    }

    private static DateTime getMetaElementDate(Element element) {
        for (String name : DateUtils.META_ATTRIBUTE_NAMES) {
            String value = element.getAttribute(name);
            if (value != null && DateUtils.dateMetaKey(value)) {
                return DateUtils.liberalParseDate(element.getAttribute("content"));
            }
        }
        return null;
    }

    /** Get date from a <time> element. */
    private static DateTime getTimeElementDate(Element element) {
        if (element.hasAttribute("datetime")) {
            return DateUtils.liberalParseDate(element.getAttribute("datetime"));
        } else {
            logger.trace("Expected attribte 'datetime' on element '{}'", element);
            return null;
        }
    }

    public static WebPageDate getModifiedDate(String url, WebPage page,
            Mapper<?, ?, ?, ?>.Context context) throws IOException, SAXException {
        if (page.getContent() == null) {
            return null;
        }
        return getModifiedDate(url, parseHtml(page.getContent(), url), page.getModifiedTime(),
            context);
    }

    public static WebPageDate getModifiedDate(String url, Document document, Long httpModifiedTime,
            Mapper<?, ?, ?, ?>.Context context) {
        LocalDate urlDate = DateUtils.extractDateFromUrl(url);
        if (urlDate != null && isValidDate(urlDate.toDateTimeAtStartOfDay())) {
            incrementCount(context, DateSource.URL);
            return new WebPageDate(urlDate.toDateTimeAtStartOfDay(), DateSource.URL);
        }

        WebPageDate contentDate = extractModifiedDate(document);
        if (contentDate != null && isValidDate(contentDate.date)) {
            incrementCount(context, contentDate.dateSource);
            return contentDate;
        }

        if (httpModifiedTime != null) {
            DateTime httpDateTime = new DateTime(httpModifiedTime);
            if (isValidDate(httpDateTime)) {
                incrementCount(context, DateSource.HEADER);
                return new WebPageDate(httpDateTime, DateSource.HEADER);
            }
        }
        logger.debug("No date found for URL {}", url);
        return null;
    }

    static DateTime findDateMatch(String s) {
        for (Pattern pattern : DATE_PATTERNS) {
            Matcher matcher = pattern.matcher(s);
            if (matcher.find()) {
                int year = Integer.parseInt(matcher.group("year"));
                String rawMonth = matcher.group("month");
                int month = -1;
                Integer monthLookup = NAMES_TO_MONTH.get(rawMonth.toLowerCase(Locale.ENGLISH));
                if (monthLookup != null) {
                    month = monthLookup.intValue();
                } else if (rawMonth.matches("\\d+")) {
                    month = Integer.parseInt(rawMonth);
                } else {
                    continue;
                }
                int day = Integer.parseInt(matcher.group("day"));
                try {
                    return new LocalDate(year, month, day).toDateTimeAtStartOfDay();
                } catch (IllegalArgumentException e) {
                    logger.trace("Could not use as a date: {}-{}-{}: ", year, month, day, e);
                }
            }
        }
        return null;
    }
}
