package de.l3s.icrawl.online;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.concurrent.ThreadSafe;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.de.GermanAnalyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.fr.FrenchAnalyzer;
import org.apache.lucene.analysis.it.ItalianAnalyzer;
import org.apache.lucene.analysis.shingle.ShingleAnalyzerWrapper;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.util.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Joiner;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Multiset;
import com.google.common.collect.Sets;

import de.l3s.icrawl.domain.specification.NamedEntity;
import de.l3s.icrawl.domain.specification.NamedEntity.Label;

import static com.google.common.collect.Iterables.concat;

/**
 * Class to compute the distance of a document to a document collection.
 */
@ThreadSafe
public class DocumentVectorSimilarity implements Serializable {

    @SuppressWarnings("deprecation")
    private static final Version LUCENE_VERSION = Version.LUCENE_4_9;

    public static class KeywordMatcher implements Serializable {
        private static final long serialVersionUID = 1L;
        public static final double FULL_MATCH_WEIGHT = 2.0;
        public static final double PARTIAL_MATCH_WEIGHT = 1.5;
        public static final double NO_MATCH_WEIGHT = 1.0;

        enum Match {
            MATCHES_FULL, MATCHES_PARTIAL, NO_MATCH
        }

        private static final Joiner TOKEN_JOINER = Joiner.on(TOKEN_SEPARATOR);
        @JsonProperty
        private final Set<String> singleTokenKeywords;
        @JsonProperty
        private final Set<String> multiTokenKeywords;
        @JsonProperty
        private final int ngramSize;

        @JsonCreator
        protected KeywordMatcher(
                @JsonProperty("singleTokenKeywords") Set<String> singleTokenKeywords,
                @JsonProperty("multiTokenKeywords") Set<String> multiTokenKeywords,
                @JsonProperty("ngramSize") int ngramSize) {
            this.singleTokenKeywords = singleTokenKeywords;
            this.multiTokenKeywords = multiTokenKeywords;
            this.ngramSize = ngramSize;
        }

        public KeywordMatcher(Collection<String> languageKeywords,
                Collection<String> allLanguageKeywords, String lang, Analyzer wrappingAnalyzer,
                int ngramSize) {
            this.ngramSize = ngramSize;
            ImmutableSet.Builder<String> singleTokenKeywordsBuilder = ImmutableSet.builder();
            ImmutableSet.Builder<String> multiTokenKeywordsBuilder = ImmutableSet.builder();
            Analyzer analyzer = wrappingAnalyzer;
            if (wrappingAnalyzer instanceof ShingleAnalyzerWrapper) {
                analyzer = ((ShingleAnalyzerWrapper) wrappingAnalyzer).getWrappedAnalyzer("text");
            }
            for (String keyword : concat(languageKeywords, allLanguageKeywords)) {
                List<String> tokens = analyzeDocument(keyword, analyzer, new ArrayList<String>());
                if (tokens.isEmpty()) {
                    logger.debug("empty tokens list for keywords '{}'", keyword);
                } else if (tokens.size() == 1) {
                    singleTokenKeywordsBuilder.add(tokens.get(0));
                } else {
                    multiTokenKeywordsBuilder.addAll(ngrams(tokens, ngramSize));
                }
            }
            this.singleTokenKeywords = singleTokenKeywordsBuilder.build();
            this.multiTokenKeywords = multiTokenKeywordsBuilder.build();
        }

        private Set<String> ngrams(List<String> tokens, int ngramSize) {
            Set<String> ngrams = Sets.newHashSetWithExpectedSize(tokens.size());
            for (int i = 0; i < tokens.size() - ngramSize + 1; i++) {
                ngrams.add(TOKEN_JOINER.join(tokens.subList(i, i + ngramSize)));
            }
            return ngrams;
        }

        public Match match(List<String> tokens) {
            for (String ngram : ngrams(tokens, ngramSize)) {
                if (multiTokenKeywords.contains(ngram)) {
                    return Match.MATCHES_FULL;
                }
            }
            for (String token : tokens) {
                if (singleTokenKeywords.contains(token)) {
                    return Match.MATCHES_PARTIAL;
                }
            }
            return Match.NO_MATCH;
        }

        @VisibleForTesting
        Set<String> getSingleTokenKeywords() {
            return singleTokenKeywords;
        }

        @VisibleForTesting
        Set<String> getMultiTokenKeywords() {
            return multiTokenKeywords;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                .add("single", singleTokenKeywords)
                .add("multi", multiTokenKeywords)
                .toString();
        }
    }

    static final String TOKEN_SEPARATOR = " ";
    static final int DEFAULT_NGRAM_SIZE = 2;
    private static final Locale DEFAULT_LANGUAGE = Locale.ENGLISH;
    private static final Logger logger = LoggerFactory.getLogger(DocumentVectorSimilarity.class);
    private static final int EXPECTED_DOCUMENT_VOCABULARY_SIZE = 1024;
    private static final Map<Locale, Class<? extends Analyzer>> ANALYZERS;
    private static final long serialVersionUID = 2L;
    private static final FieldType TEXT_FIELD_TYPE;
    static {
        ANALYZERS = new HashMap<>();
        ANALYZERS.put(Locale.ENGLISH, EnglishAnalyzer.class);
        ANALYZERS.put(Locale.GERMAN, GermanAnalyzer.class);
        ANALYZERS.put(Locale.FRENCH, FrenchAnalyzer.class);
        ANALYZERS.put(Locale.ITALIAN, ItalianAnalyzer.class);

        TEXT_FIELD_TYPE = new FieldType();
        TEXT_FIELD_TYPE.setIndexed(true);
        TEXT_FIELD_TYPE.setTokenized(true);
        TEXT_FIELD_TYPE.setStored(false);
        TEXT_FIELD_TYPE.setStoreTermVectors(true);
        TEXT_FIELD_TYPE.setStoreTermVectorPositions(false);
        TEXT_FIELD_TYPE.freeze();
    }

    @JsonProperty
    private final Map<Locale, DocumentVector> referenceVectors;
    @JsonProperty
    private final Map<Locale, KeywordMatcher> matchers;

    /**
     * Create a SpecSimilarity Object that is serializable and stores a
     * reference to the input's document collection.
     *
     * @param referenceDocumentsToLanguage
     *            the document collection (each mapping to its language)
     */
    public DocumentVectorSimilarity(Map<String, Locale> referenceDocumentsToLanguage) {
        this(referenceDocumentsToLanguage, Collections.<String> emptySet(),
            Collections.<NamedEntity> emptySet(), -1, false);
    }

    public DocumentVectorSimilarity(Map<String, Locale> referenceDocumentsToLanguage,
            Set<String> keywords, Set<NamedEntity> entities, int maxTerms, boolean useDF) {
        Multimap<Locale, String> documents = Multimaps.invertFrom(
            Multimaps.forMap(referenceDocumentsToLanguage),
            ArrayListMultimap.<Locale, String> create());
        Set<String> allLanguageKeywords = new HashSet<>(keywords);
        Multimap<String, String> keywordsByLanguage = HashMultimap.create();
        for (NamedEntity entity : entities) {
            for (Label label : entity.getLabels()) {
                if (label.getLanguage() != null) {
                    keywordsByLanguage.put(label.getLanguage(), label.getName());
                } else {
                    allLanguageKeywords.add(label.getName());
                }
            }
        }
        ImmutableMap.Builder<Locale, DocumentVector> vectors = ImmutableMap.builder();
        ImmutableMap.Builder<Locale, KeywordMatcher> matchersBuilder = ImmutableMap.builder();
        for (Map.Entry<Locale, Collection<String>> languageDocuments : documents.asMap().entrySet()) {
            Collection<DocumentVector> languageVectors = Lists.newArrayListWithExpectedSize(languageDocuments.getValue().size());
            Locale lang = languageDocuments.getKey();
            Analyzer analyzer = getAnalyzerForLanguage(lang);
            if (analyzer == null) {
                logger.debug("Language {} of this document is not supported", lang);
                continue;
            }
            KeywordMatcher keywordMatcher = new KeywordMatcher(keywordsByLanguage.get(lang.getLanguage()),
                allLanguageKeywords, lang.getLanguage(), analyzer, DEFAULT_NGRAM_SIZE);

            for (String document : languageDocuments.getValue()) {
                languageVectors.add(documentVectorForDocument(document, analyzer, keywordMatcher));
            }
            logger.debug("Got doc vectors for language {}: {}", lang.getLanguage(), languageVectors);
            DocumentVector vector = DocumentVector.merge(languageVectors, useDF);
            if (maxTerms > 0) {
                vector = vector.topN(maxTerms);
            }
            final DocumentVector referenceVector = vector;
            double avgSimilarity = languageVectors.stream()
                .mapToDouble(dv -> dv.cosineSimilarity(referenceVector))
                .average()
                .orElse(1.0);
            if (Math.abs(avgSimilarity) < 0.0001) {
                logger.info("Got avgSimilarity of {} for reference vectors, using 1.0 instead", avgSimilarity);
                avgSimilarity = 1.0;
            }
            vector = vector.multiplyComponentsBy(1 / avgSimilarity);
            vectors.put(lang, vector);
            matchersBuilder.put(lang, keywordMatcher);
        }
        this.referenceVectors = vectors.build();
        this.matchers = matchersBuilder.build();
        if (logger.isDebugEnabled()) {
            for (Entry<Locale, DocumentVector> entry : referenceVectors.entrySet()) {
                logger.debug("Reference vector for language '{}': {}...", entry.getKey(),
                    entry.getValue().topComponents(10));
            }
        }
    }

    public static DocumentVectorSimilarity fromVectors(Map<Locale, DocumentVector> referenceVectors,
            Map<Locale, Set<String>> keywords) {
        Set<String> allLanguageKeywords = new HashSet<>();
        keywords.values().forEach(allLanguageKeywords::addAll);
        ImmutableMap.Builder<Locale, KeywordMatcher> matchersBuilder = ImmutableMap.builder();
        for (Entry<Locale, Set<String>> language : keywords.entrySet()) {
            KeywordMatcher keywordMatcher = new KeywordMatcher(language.getValue(), allLanguageKeywords,
                language.getKey().getLanguage(), getAnalyzerForLanguage(language.getKey()), DEFAULT_NGRAM_SIZE);
            matchersBuilder.put(language.getKey(), keywordMatcher);
        }

        return new DocumentVectorSimilarity(referenceVectors, matchersBuilder.build());
    }

    @JsonCreator
    protected DocumentVectorSimilarity(
            @JsonProperty("referenceVectors") Map<Locale, DocumentVector> referenceVectors,
            @JsonProperty("matchers") Map<Locale, KeywordMatcher> matchers) {
        this.referenceVectors = referenceVectors;
        this.matchers = matchers;
    }

    /**
     * calculate the cosine-similarity of the doc to the specification
     *
     * @param doc
     *            the text of the document
     * @param language
     *            the language of this document
     * @return the cosine-similarity of the doc to the specification
     */
    public double getSimilarity(String doc, Locale language) {
        Preconditions.checkArgument(!doc.isEmpty(), "Document must have length > 0.");
        Analyzer analyzer = getAnalyzerForLanguage(language);
        DocumentVector reference = getReferenceVectorForLanguage(language);
        if (reference == null) {
            return 0.5;
        }
        KeywordMatcher keywordMatcher = getKeywordMatcherForLanguage(language);
        if (keywordMatcher == null) {
            logger.debug("Available keywords matchers: {}", matchers.keySet());
        }
        DocumentVector documentVector = documentVectorForDocument(doc, analyzer, keywordMatcher);
        double documentSimilarity = reference.cosineSimilarity(documentVector);
        logger.trace("result: {}", documentSimilarity);
        if (!Double.isFinite(documentSimilarity)) {
            logger.debug("Got NaN similarity for input '{}'@{}: {}", doc, language,
                documentSimilarity);
            return 0.0;
        }
        return documentSimilarity;
    }

    @Override
    public String toString() {
        Map<Locale, List<Entry<String, Double>>> topComponents = Maps.transformValues(referenceVectors,  dv -> dv.topComponents(10));
        StringBuilder sb = new StringBuilder();
        sb.append("DocumentVectorSimilarity[");
        Joiner.on(", ").withKeyValueSeparator(" => ").appendTo(sb, topComponents);
        return sb.append("]").toString();
    }

    KeywordMatcher getKeywordMatcherForLanguage(Locale language) {
        KeywordMatcher keywordMatcher = matchers.get(language);
        if (keywordMatcher == null) {
            logger.debug("No keyword matcher for language '{}', falling back to default", language);
            keywordMatcher = matchers.get(DEFAULT_LANGUAGE);
        }
        return keywordMatcher;
    }

    DocumentVector getReferenceVectorForLanguage(Locale language) {
        DocumentVector reference = referenceVectors.get(language);
        if (reference == null) {
            logger.debug("No reference vector for language '{}', falling back to default", language);
            reference = referenceVectors.get(DEFAULT_LANGUAGE);
        }
        return reference;
    }

    private DocumentVector documentVectorForDocument(String document, Analyzer analyzer,
            KeywordMatcher keywordMatcher) {
        Multiset<String> tokens = analyzeDocument(document, analyzer,
            HashMultiset.<String> create(EXPECTED_DOCUMENT_VOCABULARY_SIZE));
        Map<String, Double> dv = Maps.newHashMapWithExpectedSize(tokens.elementSet().size());

        double size = tokens.size();
        Splitter tokenizer = Splitter.on(TOKEN_SEPARATOR);
        for (Multiset.Entry<String> entry : tokens.entrySet()) {
            double weight;
            String multiToken = entry.getElement();
            List<String> tokenList = tokenizer.splitToList(multiToken);
            if (keywordMatcher != null) {
                switch (keywordMatcher.match(tokenList)) {
                case MATCHES_FULL:
                    weight = KeywordMatcher.FULL_MATCH_WEIGHT;
                    break;
                case MATCHES_PARTIAL:
                    weight = KeywordMatcher.PARTIAL_MATCH_WEIGHT;
                    break;
                case NO_MATCH:
                default:
                    weight = KeywordMatcher.NO_MATCH_WEIGHT;
                }
            } else {
                weight = KeywordMatcher.NO_MATCH_WEIGHT;
            }
            dv.put(multiToken, weight * entry.getCount() / size);
        }
        return new DocumentVector(dv);
    }

    public void writeToFile(File outputFile) throws IOException {
        new ObjectMapper().writeValue(outputFile, this);
    }

    private static Analyzer asNgramAnalyzer(Analyzer inner, int ngramSize) {
        return new ShingleAnalyzerWrapper(inner, ngramSize, ngramSize, TOKEN_SEPARATOR, true, true,
            "_");
    }

    static Analyzer getAnalyzerForLanguage(Locale lang) {
        try {
            Class<? extends Analyzer> analyzerClass = ANALYZERS.get(lang);
            if (analyzerClass == null) {
                logger.debug("Unsupported language '{}', falling back to default", lang);
                analyzerClass = ANALYZERS.get(DEFAULT_LANGUAGE);
            }
            Analyzer languageSpecificAnalyzer = analyzerClass.getConstructor(Version.class)
                .newInstance(LUCENE_VERSION);
            return asNgramAnalyzer(languageSpecificAnalyzer, DEFAULT_NGRAM_SIZE);
        } catch (ReflectiveOperationException | IllegalArgumentException | SecurityException e) {
            throw new IllegalStateException("Failed to create analyzer for language "
                    + lang.getLanguage(), e);
        }
    }

    static <T extends Collection<String>> T analyzeDocument(String document, Analyzer analyzer,
            T tokenConsumer) {
        try (TokenStream ts = analyzer.tokenStream("text", document)) {
            ts.reset();
            CharTermAttribute textAttribute = ts.addAttribute(CharTermAttribute.class);
            while (ts.incrementToken()) {
                tokenConsumer.add(textAttribute.toString());
            }
            ts.end();
        } catch (IOException e) {
            throw new AssertionError("Unexpected exception while analysing string", e);
        }
        return tokenConsumer;
    }

    public static DocumentVectorSimilarity readFromFile(File inputFile) throws IOException {
        return new ObjectMapper().readValue(inputFile, DocumentVectorSimilarity.class);
    }

    @VisibleForTesting
    public Map<Locale, DocumentVector> getReferenceVectors() {
        return Collections.unmodifiableMap(referenceVectors);
    }

    @VisibleForTesting
    public Map<Locale, KeywordMatcher> getMatchers() {
        return matchers;
    }
}
