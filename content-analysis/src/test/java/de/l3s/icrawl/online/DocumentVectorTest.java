package de.l3s.icrawl.online;

import java.io.IOException;
import java.util.List;
import java.util.Map.Entry;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

public class DocumentVectorTest {
    private static final Logger logger = LoggerFactory.getLogger(DocumentVectorTest.class);

    @Test
    public void testSerialization() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        DocumentVector dv = new DocumentVector(ImmutableList.of("foo", "foo", "bar"));
        String json = mapper.writeValueAsString(dv);
        logger.trace("serialized: {}", json);
        DocumentVector dv2 = mapper.readValue(json, DocumentVector.class);
        logger.trace("deserialized:  {}", dv2);
        assertThat(dv2.cosineSimilarity(dv), is(1.0));
    }

    @Test
    public void testTopComponents() throws Exception {
        DocumentVector dv = new DocumentVector(ImmutableList.of("foo", "foo", "bar"));
        List<Entry<String, Double>> topComponents = dv.topComponents(1);
        assertThat(topComponents, hasSize(1));
        assertThat(topComponents.get(0).getKey(), is("foo"));
    }
}
