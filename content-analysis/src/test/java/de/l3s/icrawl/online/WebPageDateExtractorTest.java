package de.l3s.icrawl.online;

import java.net.URL;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.io.Resources;

import de.l3s.icrawl.online.WebPageDateExtractor;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class WebPageDateExtractorTest {
    private static final Logger logger = LoggerFactory.getLogger(WebPageDateExtractorTest.class);
    private final static Map<String, DateTime> RESOURCES;
    static {
        ImmutableMap.Builder<String, DateTime> resourcesBuilder = ImmutableMap.builder();
        // TODO find actual modification dates
        resourcesBuilder.put("modification/cnn.com.html", new DateTime(2014, 10, 22, 12, 00, 00))
            .put("modification/de.wikipedia.org.html", new DateTime(2014, 10, 22, 12, 00, 00))
            .put("modification/en.wikipedia.org.html", new DateTime(2014, 10, 22, 12, 00, 00))
            .put("modification/nbcnews.com.html", new DateTime(2014, 10, 22, 12, 00, 00))
            .put("modification/news.yahoo.com.html", new DateTime(2014, 10, 22, 12, 00, 00))
            .put("modification/nytimes.com.html", new DateTime(2014, 10, 22, 12, 00, 00))
            .put("modification/reuters.com.html", new DateTime(2014, 10, 22, 12, 00, 00))
            .put("modification/scmp.com.html", new DateTime(2014, 10, 22, 12, 00, 00))
            .put("modification/spiegel.de.html", new DateTime(2014, 10, 22, 12, 00, 00))
            .put("modification/theguardian.com.html", new DateTime(2014, 10, 22, 12, 00, 00))
            .put("modification/twitter.com.html", new DateTime(2014, 10, 22, 12, 00, 00));
//            .put("modification/kyivpost.com.html", new DateTime(2014, 11, 6, 9, 21, 00)); TODO
        RESOURCES = resourcesBuilder.build();
    }

    @Test
    public void testExtractModifiedDateString() {
        WebPageDateExtractor extractor = new WebPageDateExtractor();
        for (String resource : RESOURCES.keySet()) {
            logger.debug("Extracting dates from {}", resource);
            URL url = Resources.getResource(resource);
            assertThat(resource, extractor.extractModifiedDateFromUrl(url.toString()), is(notNullValue()));
        }
        //        fail("Not yet implemented");
    }

    private static final DateTime DATE_TO_EXTRACT = new LocalDate(2014, 2, 13).toDateTimeAtStartOfDay();
    private static final Set<String> DATES;
    static {
        ImmutableSet.Builder<String> testStrings = ImmutableSet.builder();
        testStrings.add("2014-02-13")
            .add("13.02.2014")
            .add("13. 02. 2014")
            .add("13 February 2014")
            .add("13. Februar 2014")
            .add("February 13, 2014")
            .add("02/13/2014")
            .add("13/02/2014");
        DATES = testStrings.build();
    }
    @Test
    public void testRegex() throws Exception {
        for (String date : DATES) {
            assertThat(date, WebPageDateExtractor.findDateMatch(date), is(equalTo(DATE_TO_EXTRACT)));
        }
    }
}
