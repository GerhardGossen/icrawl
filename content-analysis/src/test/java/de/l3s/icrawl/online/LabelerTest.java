package de.l3s.icrawl.online;

import java.util.Collection;

import org.junit.Test;
import org.openimaj.text.nlp.language.LanguageDetector.WeightedLocale;

import com.google.common.collect.Iterables;

import de.l3s.icrawl.domain.specification.NamedEntity;
import de.l3s.icrawl.domain.specification.NamedEntity.Label;
import de.l3s.icrawl.domain.specification.NamedEntity.Type;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

public class LabelerTest {
    private static final String EN_TEST_SENTENCE = "Barack Obama is the president of the United States of America.";
    private static final String DE_TEST_SENTENCE = "Angela Merkel ist die Kanzlerin der Bundesrepublik Deutschland.";
    private final LabelerFactory labelerFactory = LabelerFactory.defaultFactory();

    @Test
    public void testEN() {
        Labeler labeler = labelerFactory.get(new WeightedLocale("en", 1.0));
        Collection<NamedEntity> entities = labeler.extractEntities(EN_TEST_SENTENCE);
        System.out.println(entities);

        assertThat(entities, hasSize(2));

        NamedEntity obama = Iterables.get(entities, 0);
        Label obamaLabel = obama.getLabels().iterator().next();
        assertThat(obama.getType(), is(Type.PERSON));
        assertThat(obamaLabel.getName(), is("Barack Obama"));

        NamedEntity usa = Iterables.get(entities, 1);
        Label usaLabel = usa.getLabels().iterator().next();
        assertThat(usa.getType(), is(Type.LOCATION));
        assertThat(usaLabel.getName(), is("United States of America"));
    }

    @Test
    public void testDE() throws Exception {
        Labeler labeler = labelerFactory.get(new WeightedLocale("de", 1.0));
        Collection<NamedEntity> entities = labeler.extractEntities(DE_TEST_SENTENCE);
        System.out.println(entities);

        assertThat(entities, hasSize(2));

        NamedEntity obama = Iterables.get(entities, 0);
        Label obamaLabel = obama.getLabels().iterator().next();
        assertThat(obama.getType(), is(Type.PERSON));
        assertThat(obamaLabel.getName(), is("Angela Merkel"));

        NamedEntity usa = Iterables.get(entities, 1);
        Label usaLabel = usa.getLabels().iterator().next();
        assertThat(usa.getType(), is(Type.LOCATION));
        assertThat(usaLabel.getName(), is("Bundesrepublik Deutschland"));
    }
}
