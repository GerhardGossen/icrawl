package de.l3s.icrawl.online.time;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class WeibullTimeDifferenceRelevanceTest {

    @Test
    public void testComputeTimeRelevance() {
        TimeDifferenceRelevance tdr = new WeibullTimeDifferenceRelevance(Period.weeks(1), 1);
        DateTime reference = new DateTime(2014, 11, 28, 0, 0, 0, 0);
        assertThat(tdr.computeRelevance(reference.plusWeeks(1), reference), is(closeTo(0.5, .01)));
        assertThat(tdr.computeRelevance(reference.minusWeeks(1), reference), is(closeTo(0.5, .01)));

        assertThat(tdr.computeRelevance(reference.plusMonths(1), reference), is(lessThan(.1)));
        assertThat(tdr.computeRelevance(reference.minusMonths(1), reference), is(lessThan(.1)));
    }

    @Test
    public void testShape() {
        TimeDifferenceRelevance shape1 = new WeibullTimeDifferenceRelevance(Period.weeks(1), 1);
        TimeDifferenceRelevance shape2 = new WeibullTimeDifferenceRelevance(Period.weeks(1), 2);
        DateTime reference = new DateTime(2014, 11, 28, 0, 0, 0, 0);
        for (int i = 1; i < 7; i++) {
            DateTime target = reference.plusDays(i);
            assertTrue("slower decay at " + i + " days", 
                shape1.computeRelevance(target, reference) < shape2.computeRelevance(target, reference));
        }

        DateTime targetHalf = reference.plusWeeks(1);
        assertTrue("equal decay at halfDuration",
            shape1.computeRelevance(targetHalf, reference) == shape2.computeRelevance(targetHalf, reference));

        for (int i = 2; i <= 10; i++) {
            DateTime target = reference.plusWeeks(i);
            assertTrue("faster decay at " + i + " weeks", 
                shape1.computeRelevance(target, reference) > shape2.computeRelevance(target, reference));
        }

    }

}
