package de.l3s.icrawl.online;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.junit.Test;
import org.openimaj.text.nlp.language.LanguageDetector;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multiset;
import com.google.common.io.Resources;

import de.l3s.icrawl.online.ContentAnalyser.Counts;
import de.l3s.icrawl.util.WebPageUtils;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class ContentAnalyzerTest {

    @Test
    public void testExtractKeywords() throws SAXException, IOException {
        ContentAnalyser contentAnalyzer = new ContentAnalyser(new LanguageDetector(), null);
        Multiset<String> keywords = contentAnalyzer.analyze(
            loadDocument("l3s.de.html", "http://www.l3s.de/"),
            ImmutableSet.of("l3s", "hannover", "universität")).getKeywords();
        System.out.println(keywords);
        assertThat(keywords, hasItem("l3s"));
    }

    private DocumentFragment loadDocument(String resourceName, String origUrl) throws SAXException, IOException {
        URL url = Resources.getResource(getClass(), resourceName);
        try (InputStream is = url.openStream()) {
            Document doc = WebPageUtils.parseHtml(is, origUrl);
            DocumentFragment docFragment = doc.createDocumentFragment();
            NodeList body = doc.getElementsByTagName("BODY");
            for (int i = 0; i < body.getLength(); i++) {
                docFragment.appendChild(body.item(i));
            }
            return docFragment;
        }
    }

    @Test
    public void testSecondPage() throws Exception {
        DocumentFragment doc = loadDocument("example.html", "http://www.iicm.tugraz.at/maurer");
        ContentAnalyser contentAnalyzer = new ContentAnalyser(new LanguageDetector(), null);
        Counts keywords = contentAnalyzer.analyze(doc, ImmutableSet.of("institut"));
        assertThat(keywords.getKeywords(), not(empty()));
    }

}
