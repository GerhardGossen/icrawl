package de.l3s.icrawl.online;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.io.Resources;

import de.l3s.icrawl.domain.specification.NamedEntity;
import de.l3s.icrawl.online.DocumentVectorSimilarity.KeywordMatcher;
import de.l3s.icrawl.online.DocumentVectorSimilarity.KeywordMatcher.Match;

import static com.google.common.io.Resources.getResource;
import static de.l3s.icrawl.online.DocumentVectorSimilarity.DEFAULT_NGRAM_SIZE;
import static de.l3s.icrawl.online.DocumentVectorSimilarity.getAnalyzerForLanguage;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.emptySet;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class DocumentVectorSimilarityTest {

    DocumentVectorSimilarity createReferenceVector() throws IOException {
        Set<String> keywords = ImmutableSet.of();
        Set<NamedEntity> entities = ImmutableSet.of();
        return createReferenceVector(keywords, entities);
    }

    DocumentVectorSimilarity createReferenceVector(Set<String> keywords, Set<NamedEntity> entities)
            throws IOException {
        Map<String, Locale> input = new HashMap<String, Locale>();
        for (int i = 1; i <= 3; i++) {
            String text = readTextFile("SpecSimilarity/reference" + i + ".txt");
            input.put(text, Locale.ENGLISH);
        }
        return new DocumentVectorSimilarity(input, keywords, entities, -1, false);
    }

    @Test
    public void testHighSimilarity() throws IOException {
        DocumentVectorSimilarity spec = createReferenceVector();
        String s = readTextFile("SpecSimilarity/document1.txt");
        double similarity = spec.getSimilarity(s, Locale.ENGLISH);
        assertThat(similarity, is(greaterThan(0.4)));
        assertThat(similarity, is(lessThanOrEqualTo(1.0)));
    }

    @Test
    public void testLowSimilarity() throws IOException {
        DocumentVectorSimilarity spec = createReferenceVector();
        String s = readTextFile("SpecSimilarity/document2.txt");
        double similarity = spec.getSimilarity(s, Locale.ENGLISH);
        assertThat(similarity, is(greaterThanOrEqualTo(0.0)));
        assertThat(similarity, is(lessThan(0.25)));
    }

    @Test
    public void testSerialization() throws Exception {
        DocumentVectorSimilarity spec = createReferenceVector();
        File outFile = File.createTempFile("similarity", ".bin");
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(outFile))) {
            oos.writeObject(spec);
        }
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(outFile))) {
            DocumentVectorSimilarity specSim2 = (DocumentVectorSimilarity) ois.readObject();
            String s = readTextFile("SpecSimilarity/document1.txt");
            assertEquals(spec.getSimilarity(s, Locale.ENGLISH),
                specSim2.getSimilarity(s, Locale.ENGLISH), 0.001);
        }
        outFile.delete();
    }

    @Test
    public void testSelfSimilarity() throws Exception {
        String text = readTextFile("SpecSimilarity/document1.txt");
        Map<String, Locale> referenceDocuments = Collections.singletonMap(text, Locale.ENGLISH);
        DocumentVectorSimilarity spec = new DocumentVectorSimilarity(referenceDocuments);
        double similarity = spec.getSimilarity(text, Locale.ENGLISH);
        assertThat(similarity, is(greaterThan(0.99)));
        assertThat(similarity, is(lessThanOrEqualTo(1.0)));
    }

    @Test
    public void testKeywordMatching() throws Exception {
        String text = readTextFile("SpecSimilarity/document1.txt");
        Set<String> keywords = ImmutableSet.of("liberia", "maternal care clinic");
        Set<NamedEntity> entities = ImmutableSet.of();
        DocumentVectorSimilarity specWithKeywords = createReferenceVector(keywords, entities);
        DocumentVectorSimilarity specNoKeywords = createReferenceVector();
        double similarityNoKeywords = specNoKeywords.getSimilarity(text, Locale.ENGLISH);
        double similarityWithKeywords = specWithKeywords.getSimilarity(text, Locale.ENGLISH);
        assertThat(similarityWithKeywords, is(greaterThan(similarityNoKeywords)));
        assertThat(similarityWithKeywords, is(lessThanOrEqualTo(1.0)));
    }

    @Test
    public void testKeywordMatcher() throws Exception {
        KeywordMatcher matcher = new KeywordMatcher(emptySet(),
            ImmutableSet.of("liberia", "maternal care clinic"), "en",
            getAnalyzerForLanguage(Locale.ENGLISH),
            DEFAULT_NGRAM_SIZE);
        assertThat(matcher.match(ImmutableList.of("back", "to", "liberia")),
            is(Match.MATCHES_PARTIAL));
        assertThat(matcher.match(ImmutableList.of("matern", "care")),
            is(Match.MATCHES_FULL));
    }

    @Test
    public void testKeywordMatcherSerialization() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        KeywordMatcher matcher = new KeywordMatcher(emptySet(), ImmutableSet.of(
            "liberia", "maternal care clinic"), "en", getAnalyzerForLanguage(Locale.ENGLISH),
            DEFAULT_NGRAM_SIZE);
        String json = mapper.writeValueAsString(matcher);
        KeywordMatcher reconstructedMatcher = mapper.readValue(json, KeywordMatcher.class);
        assertThat(reconstructedMatcher, is(notNullValue()));
        assertThat(reconstructedMatcher.getSingleTokenKeywords(), is(matcher.getSingleTokenKeywords()));
        assertThat(reconstructedMatcher.getMultiTokenKeywords(), is(matcher.getMultiTokenKeywords()));
    }

    private static String readTextFile(String name) throws IOException {
        return Resources.toString(getResource(name), UTF_8);
    }
}
