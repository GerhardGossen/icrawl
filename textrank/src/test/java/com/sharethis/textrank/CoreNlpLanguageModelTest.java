package com.sharethis.textrank;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.Matchers.arrayWithSize;
import static org.junit.Assert.assertThat;

public class CoreNlpLanguageModelTest {
    private static final Logger logger = LoggerFactory.getLogger(CoreNlpLanguageModelTest.class);

    @Test
    public void testTagTokens() {
        String text = "Januar 2015 23:59 Uhr Schlüsse gegen Schlüsse sind aber in Ordnung) Scherz beiseite: ich möchte nur verlauten lassen  dass ich mich zutiefst schäme für meine Stadt Dresden.";

        LanguageModel model = CoreNlpLanguageModel.germanModel();
        Sentence sentence = model.parseSentence(text);
        if (logger.isDebugEnabled()) {
            for (int i = 0; i < sentence.token_list.length; i++) {
                logger.debug("{}/{}", sentence.token_list[i], sentence.tag_list[i]);
            }
        }
        assertThat(sentence.tag_list, arrayWithSize(sentence.token_list.length));
    }

}
