package de.l3s.icrawl.online;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.io.Resources;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;

public class TextRankWrapperTest {
    private static TextRankWrapper wrapper;

    @BeforeClass
    public static void initialize() {
        wrapper = new TextRankWrapper();
    }

    @Test
    public void testTextRank_en() throws IOException {
        String text = Resources.toString(Resources.getResource("text_en.txt"), UTF_8);
        List<String> keywords = wrapper.rank(text, Locale.ENGLISH, 5);
        System.out.println(keywords);

        assertThat(keywords, contains("airliner", "reports", "news", "Organization", "path"));
    }

    @Test
    public void testTextRank_de() throws IOException {
        String text = Resources.toString(Resources.getResource("text_de.txt"), UTF_8);
        List<String> keywords = wrapper.rank(text, Locale.GERMAN, 5);

        System.out.println(keywords);
        assertThat(keywords,
            contains("NATO-Generalsekretär", "Parteiführer", "Gericht", "Sicherheitskräften", "Gelegenheit"));
    }

}
