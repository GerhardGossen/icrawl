package de.l3s.icrawl.online;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.base.Splitter;
import com.google.common.io.Resources;

import de.l3s.icrawl.online.singlerank.SingleRankKeywordExtractor;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertThat;

public class SingleRankTest {

    private static SingleRankKeywordExtractor ranker;

    @BeforeClass
    public static void initialize() {
        ranker = new SingleRankKeywordExtractor();
    }

    @Test
    public void testSingleRank_en() throws IOException {
        String text = Resources.toString(Resources.getResource("text_en.txt"), UTF_8);
        List<String> paragraphs = Splitter.on("\n\n").splitToList(text);
        List<String> keywords = ranker.extractKeywords(paragraphs, Locale.ENGLISH, 5);

        System.out.println(keywords);

        assertThat(
            keywords,
            hasItems("Malaysia Transport Minister Liow Tiong Lai",
                "Kuala Lumpur International Airport today",
                "long-range air defense missile systems", "Malaysia Transport Minister Liow Tiong",
                "Malaysian Prime Minister Najib Razak"));
    }

    @Test
    public void testSingleRank_de() throws IOException {
        String text = Resources.toString(Resources.getResource("text_de.txt"), UTF_8);
        List<String> paragraphs = Splitter.on("\n\n").splitToList(text);
        List<String> keywords = ranker.extractKeywords(paragraphs, Locale.GERMAN, 5);

        System.out.println(keywords);
        assertThat(
            keywords,
            hasItems("Protestführer Imran Khan", "Richter Nasir ul-Mulk",
                "Pakistans Nachbarland Afghanistan", "Premierminister Nawaz Sharif", "Imran Khan"));
    }

}
