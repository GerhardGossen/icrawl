package de.l3s.icrawl.online.singlerank;

import java.util.List;
import java.util.Objects;

import com.google.common.primitives.Doubles;

import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;

class Span implements Comparable<Span> {
    private final Annotation source;
    private int start = -1;
    private final int end;
    private double weight = -1;
    private String text = null;

    public Span(Annotation source, int end) {
        this.source = source;
        this.end = end;
    }

    @Override
    public int compareTo(Span o) {
        return Doubles.compare(getWeight(), o.getWeight());
    }

    public String getText() {
        if (text == null) {
            List<CoreLabel> allTokens = source.get(TokensAnnotation.class);
            List<CoreLabel> coveredTokens = allTokens.subList(start, end);
            StringBuilder sb = new StringBuilder();
            for (CoreLabel token : coveredTokens) {
                sb.append(token.getString(TextAnnotation.class)).append(" ");
            }
            text = sb.substring(0, sb.length() - 1);
        }
        return text;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getText());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Span other = (Span) obj;
        return Objects.equals(getText(), other.getText());
    }

    public double getWeight() {
        return weight / length();
    }

    public int length() {
        return end - start;
    }

    public void addWeight(double score) {
        this.weight += score;
    }

    public void setStart(int start) {
        this.start = start;
    }
}