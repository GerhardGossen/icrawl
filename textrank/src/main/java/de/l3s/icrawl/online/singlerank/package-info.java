/**
 * Implementation of the <i>SingleRank</i> algorithm for keyword extraction.
 */
package de.l3s.icrawl.online.singlerank;