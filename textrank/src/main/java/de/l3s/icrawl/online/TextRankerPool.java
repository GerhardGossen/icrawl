package de.l3s.icrawl.online;

import java.util.LinkedList;
import java.util.Queue;

public class TextRankerPool {
    private final Queue<TextRankWrapper> pool = new LinkedList<>();

    public TextRankWrapper acquire() {
        TextRankWrapper instance;
        if ((instance = pool.poll()) != null) {
            return instance;
        } else {
            return createInstance();
        }
    }

    protected TextRankWrapper createInstance() {
        return new TextRankWrapper();
    }

    public void release(TextRankWrapper textRank) {
        if (textRank != null) {
            pool.offer(textRank);
        }
    }
}
