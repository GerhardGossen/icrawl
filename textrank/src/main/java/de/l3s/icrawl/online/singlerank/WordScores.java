package de.l3s.icrawl.online.singlerank;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;
import com.google.common.collect.Table;

import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreLabel;

class WordScores {
    private static final Logger LOG = LoggerFactory.getLogger(WordScores.class);
    private final Map<String, Double> scores;

    private WordScores(Map<String, Double> scores) {
        this.scores = scores;
    }

    public static WordScores compute(Table<String, String, Double> affinity,
            Set<String> dictionary, double dampingFactor, double maxError, int maxIterations) {
        double normalization = (1 - dampingFactor) / dictionary.size();
        Map<String, Double> wordScores = createInitialWordScores(dictionary);
        double error = 0.0;
        int iterations = 0;
        do {
            error = 0.0;
            LOG.trace("Starting iteration {}", iterations);
            Map<String, Double> updatedWordScores = Maps.newHashMapWithExpectedSize(dictionary.size());
            for (Entry<String, Double> wordScore : wordScores.entrySet()) {
                String word = wordScore.getKey();
                Set<Entry<String, Double>> relatedWords = affinity.row(word).entrySet();
                double newWordScore = updateWordScore(wordScores, relatedWords, dampingFactor,
                    normalization);
                updatedWordScores.put(word, newWordScore);
                error += Math.abs(wordScore.getValue() - newWordScore);
            }
            wordScores = updatedWordScores;
            LOG.trace("Finised iteration {}, error is {}", iterations, error);
        } while (error > maxError && ++iterations < maxIterations);
        return new WordScores(wordScores);
    }

    private static double updateWordScore(Map<String, Double> wordScores,
            Set<Entry<String, Double>> relatedWords, double dampingFactor, double normalization) {
        double newWordScore = 0.0;
        for (Entry<String, Double> relatedWord : relatedWords) {
            if (relatedWord.getValue() != null) {
                newWordScore += wordScores.get(relatedWord.getKey()) * relatedWord.getValue();
            }
        }
        newWordScore *= dampingFactor;
        newWordScore += normalization;
        return newWordScore;
    }

    private static Map<String, Double> createInitialWordScores(Set<String> dictionary) {
        Map<String, Double> vector = Maps.newHashMapWithExpectedSize(dictionary.size());
        for (String word : dictionary) {
            vector.put(word, 1.0);
        }
        return vector;
    }

    public double getScore(LanguageModel languageModel, CoreLabel token) {
        String tokenText = token.get(TextAnnotation.class);
        String stemToken = languageModel.stemToken(tokenText);
        Double score = scores.get(stemToken);
        if (score == null) {
            LOG.debug("No score defined for {}, contains={}", stemToken,
                scores.containsKey(stemToken));
        }
        return score != null ? score.doubleValue() : 0.0;
    }

}
