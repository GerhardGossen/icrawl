package de.l3s.icrawl.online.singlerank;

import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tartarus.snowball.SnowballProgram;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.POSTaggerAnnotator;
import edu.stanford.nlp.pipeline.TokenizerAnnotator;
import edu.stanford.nlp.pipeline.WordsToSentencesAnnotator;

class LanguageModel implements Function<String, Annotation> {
    private static final Logger LOG = LoggerFactory.getLogger(LanguageModel.class);
    private final WordsToSentencesAnnotator splitter;
    private final TokenizerAnnotator tokenizer;
    private final POSTaggerAnnotator tagger;
    private final SnowballProgram stemmer;
    private final Locale locale;

    public LanguageModel(WordsToSentencesAnnotator splitter, TokenizerAnnotator tokenizer,
            POSTaggerAnnotator tagger, SnowballProgram stemmer, Locale locale) {
        this.splitter = splitter;
        this.tokenizer = tokenizer;
        this.tagger = tagger;
        this.stemmer = stemmer;
        this.locale = locale;
    }

    @Override
    public Annotation apply(String text) {
        Annotation document = new Annotation(text);
        tokenizer.annotate(document);
        splitter.annotate(document);
        tagger.annotate(document);
        return document;
    }

    public String stemToken(String token) {
        stemmer.setCurrent(token);
        stemmer.stem();

        return stemmer.getCurrent().toLowerCase(locale);
    }

    public boolean isInterestingToken(CoreLabel token) {
        String pos = token.get(PartOfSpeechAnnotation.class);
        return isNoun(pos) || isAdjective(pos);
    }

    public boolean isNoun(String pos) {
        return pos.startsWith("N");
    }

    public boolean isAdjective(String pos) {
        return pos.startsWith("JJ");
    }

    public List<Annotation> analyse(List<String> paragraphs) {
        LOG.debug("Starting to analyse text");
        List<Annotation> results = Lists.transform(paragraphs, this);
        LOG.debug("Finished analysing text");
        return results;
    }
}