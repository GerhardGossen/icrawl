package de.l3s.icrawl.online.singlerank;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tartarus.snowball.SnowballProgram;
import org.tartarus.snowball.ext.EnglishStemmer;
import org.tartarus.snowball.ext.GermanStemmer;

import com.google.common.base.Function;
import com.google.common.collect.ArrayTable;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.collect.Table;

import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.POSTaggerAnnotator;
import edu.stanford.nlp.pipeline.TokenizerAnnotator;
import edu.stanford.nlp.pipeline.WordsToSentencesAnnotator;

public class SingleRankKeywordExtractor {
    private final class SpanToText implements Function<Span, String> {
        @Override
        public String apply(Span s) {
            return s.getText();
        }
    }

    public static final double DEFAULT_DAMPING_FACTOR = 0.85;
    public static final int DEFAULT_WINDOW_SIZE = 10;
    private static final double MAX_ERROR = 0.0001;
    private static final int MAX_ITERATIONS = 100;

    private static final Logger LOG = LoggerFactory.getLogger(SingleRankKeywordExtractor.class);

    private final Map<Locale, LanguageModel> models;

    public SingleRankKeywordExtractor() {
        ImmutableMap.Builder<Locale, LanguageModel> modelsBuilder = ImmutableMap.builder();
        WordsToSentencesAnnotator splitter = new WordsToSentencesAnnotator(false);
        TokenizerAnnotator tokenizer = new TokenizerAnnotator(false);

        String enPosModel = "models/english-left3words-distsim.tagger";
        POSTaggerAnnotator enTagger = new POSTaggerAnnotator(enPosModel, false, 100, 1);
        SnowballProgram enStemmer = new EnglishStemmer();
        modelsBuilder.put(Locale.ENGLISH, new LanguageModel(splitter, tokenizer, enTagger, enStemmer, Locale.ENGLISH));
        String dePosModel = "models/german-fast.tagger";
        POSTaggerAnnotator deTagger = new POSTaggerAnnotator(dePosModel, false, 100, 1);
        SnowballProgram deStemmer = new GermanStemmer();

        modelsBuilder.put(Locale.GERMAN, new LanguageModel(splitter, tokenizer, deTagger, deStemmer, Locale.GERMAN));

        this.models = modelsBuilder.build();
    }

    public List<String> extractKeywords(List<String> paragraphs, Locale locale, int numKeywords) {
        return extractKeywords(paragraphs, locale, numKeywords, DEFAULT_WINDOW_SIZE,
            DEFAULT_DAMPING_FACTOR);
    }

    public List<String> extractKeywords(List<String> paragraphs, Locale locale, int numKeywords,
            int windowSize, double dampingFactor) {
        LanguageModel languageModel = models.get(locale);

        if (languageModel != null) {
            return extract(languageModel.analyse(paragraphs), languageModel, numKeywords,
                windowSize, dampingFactor);
        } else {
            throw new IllegalArgumentException("Unsupported language: " + locale);
        }
    }

    private List<String> extract(List<Annotation> analysedParagraphs, LanguageModel languageModel,
            int numKeywords, int windowSize, double dampingFactor) {
        LOG.debug("starting extraction for {} paragraphs", analysedParagraphs.size());
        Table<String, String, Integer> cooccurrences = createCoocurrenceMatrix(analysedParagraphs,
            languageModel, windowSize);

        Set<String> dictionary = cooccurrences.rowKeySet();
        Table<String, String, Double> affinity = createNormalizedMatrix(cooccurrences, dictionary);

        LOG.debug("Created affinity matrix");

        WordScores wordScores = WordScores.compute(affinity, dictionary, dampingFactor, MAX_ERROR,
            MAX_ITERATIONS);

        LOG.debug("Computed word scores");

        return findHighestScoringPhrases(analysedParagraphs, languageModel, wordScores, numKeywords);
    }

    private Table<String, String, Integer> createCoocurrenceMatrix(
            List<Annotation> analysedParagraphs, LanguageModel languageModel, int windowSize) {
        BoundedHistory<String> history = new BoundedHistory<>(windowSize);
        Table<String, String, Integer> cooccurrences = HashBasedTable.create();
        long tokens = 0;
        for (Annotation annotation : analysedParagraphs) {
            for (CoreLabel token : annotation.get(TokensAnnotation.class)) {
                tokens++;
                if (tokens % 1000 == 0) {
                    LOG.debug("Processed {} tokens", tokens);
                }

                if (languageModel.isInterestingToken(token)) {
                    String tokenText = languageModel.stemToken(token.getString(TextAnnotation.class));
                    addCooccurrences(cooccurrences, tokenText, history);
                    history.add(tokenText);
                } else {
                    history.add(null);
                }
            }
        }
        LOG.debug("Created cooccurrence matrix");
        return cooccurrences;
    }

    private void addCooccurrences(Table<String, String, Integer> cooccurrences, String tokenText,
            BoundedHistory<String> history) {
        for (String cooccurrence : history) {
            increment(cooccurrences, tokenText, cooccurrence);
            increment(cooccurrences, cooccurrence, tokenText);
        }
    }

    private List<String> findHighestScoringPhrases(List<Annotation> analysedParagraphs,
            LanguageModel languageModel, WordScores wordScores, int numKeywords) {
        Set<Span> candidates = new HashSet<>();
        for (Annotation paragraph : analysedParagraphs) {
            candidates.addAll(findParagraphPhrases(wordScores, languageModel, paragraph));
        }

        List<Span> bestPhrases = Ordering.natural().greatestOf(candidates, numKeywords);
        return Lists.transform(bestPhrases, new SpanToText());
    }

    private Set<Span> findParagraphPhrases(WordScores wordScores, LanguageModel languageModel,
            Annotation paragraph) {
        Set<Span> candidates = new HashSet<>();
        Collection<Span> currentSpans = new ArrayList<>();
        List<CoreLabel> tokens = paragraph.get(TokensAnnotation.class);
        // reverse order because keyphrases have to end with nouns
        for (int i = tokens.size() - 1; i >= 0; i--) {
            CoreLabel token = tokens.get(i);
            String pos = token.get(PartOfSpeechAnnotation.class);
            // nouns are at the end of phrases
            if (languageModel.isNoun(pos)) {
                currentSpans.add(new Span(paragraph, i + 1));
                incrementSpanWeights(currentSpans, wordScores.getScore(languageModel, token));
            } else
            // adjective continue phrases
            if (languageModel.isAdjective(pos)) {
                incrementSpanWeights(currentSpans, wordScores.getScore(languageModel, token));
            } else {
                // everything else ends phrases
                for (Span span : currentSpans) {
                    span.setStart(i + 1);
                    candidates.add(span);
                }
                currentSpans.clear();
            }
        }
        for (Span span : currentSpans) {
            span.setStart(0);
            candidates.add(span);
        }
        return candidates;
    }

    private void incrementSpanWeights(Collection<Span> spans, double score) {
        for (Span span : spans) {
            span.addWeight(score);
        }
    }

    private Table<String, String, Double> createNormalizedMatrix(
            Table<String, String, Integer> cooccurrences, Set<String> dictionary) {
        Table<String, String, Double> affinity = ArrayTable.create(dictionary, dictionary);
        for (Entry<String, Map<String, Integer>> row : cooccurrences.rowMap().entrySet()) {
            double sum = sum(row.getValue().values());
            for (Entry<String, Integer> cell : row.getValue().entrySet()) {
                affinity.put(row.getKey(), cell.getKey(), cell.getValue() / sum);
            }
        }
        return affinity;
    }

    private static double sum(Collection<Integer> values) {
        double sum = 0.0;
        for (Integer value : values) {
            sum += value;
        }
        return sum;
    }

    private static <R, C> void increment(Table<R, C, Integer> table, R rowKey, C colKey) {
        Integer count = table.get(rowKey, colKey);
        if (count == null) {
            count = 1;
        } else {
            count++;
        }
        table.put(rowKey, colKey, count);
    }
}
