package de.l3s.icrawl.online.singlerank;

import java.util.Arrays;
import java.util.Iterator;

import com.google.common.collect.AbstractIterator;

class BoundedHistory<T> implements Iterable<T> {
    private final T[] elements;
    private int max;
    private int head;

    @SuppressWarnings("unchecked")
    public BoundedHistory(int size) {
        elements = (T[]) new Object[size];
        max = 0;
        head = -1;
    }

    public void add(T elem) {
        head = (head + 1) % elements.length;
        if (max <= head) {
            max = head + 1;
        }
        elements[head] = elem;
    }

    @Override
    public Iterator<T> iterator() {
        return new AbstractIterator<T>() {
            int pos = 0;
            @Override
            protected T computeNext() {
                while (pos < max && elements[pos] == null) {
                    pos++;
                }
                if (pos >= max) {
                    return endOfData();
                } else {
                    return elements[pos++];
                }
            }
        };
    }

    @Override
    public String toString() {
        return Arrays.toString(elements);
    }

}