package de.l3s.icrawl.export.warc;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.ByteBuffer;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.gora.mapreduce.GoraMapper;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.TaskID;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.http.ProtocolVersion;
import org.apache.http.impl.EnglishReasonPhraseCatalog;
import org.apache.http.message.BasicStatusLine;
import org.apache.nutch.crawl.CrawlStatus;
import org.apache.nutch.storage.WebPage;
import org.apache.nutch.util.TableUtil;
import org.archive.format.warc.WARCConstants.WARCRecordType;
import org.archive.io.warc.WARCRecordInfo;
import org.archive.io.warc.WARCWriter;
import org.archive.io.warc.WARCWriterPoolSettingsData;
import org.archive.uid.UUIDGenerator;
import org.archive.util.binsearch.ByteBufferInputStream;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.l3s.icrawl.util.WebPageUtils;

import static java.nio.charset.StandardCharsets.UTF_8;

class WarcExporterMapper extends GoraMapper<String, WebPage, Text, Text> {
    private static final Logger logger = LoggerFactory.getLogger(WarcExporterMapper.class);
    private static final DateTimeFormatter DATE14_FORMAT = DateTimeFormat.forPattern("yyyyMMddHHmmss");
    private int warcFileCounter = 0;
    private final AtomicLong writtenRecords = new AtomicLong();
    private WARCWriterPoolSettingsData settings;
    private WARCWriter writer;

    private enum Counters {
        PROCESSED, EMITTED, UNFETCHED, FAILED
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        final Path path = FileOutputFormat.getWorkOutputPath(context);
        logger.info("setting up WARC exporter, using path {}", path);

        // Make sure the output path exists
        path.getFileSystem(context.getConfiguration()).mkdirs(path);

        createNewWriter(context);
    }

    private void createNewWriter(Context context) throws IOException, InterruptedException {
        final Path path = FileOutputFormat.getWorkOutputPath(context);

        // Get the task identifier
        TaskID taskId = context.getTaskAttemptID().getTaskID();

        final Path outputFile = new Path(path, taskId + "-" + warcFileCounter + ".warc.gz");

        final FSDataOutputStream out = path.getFileSystem(context.getConfiguration()).create(
            outputFile);

        final AtomicInteger serialNo = new AtomicInteger(taskId.getId());
        settings = new WARCWriterPoolSettingsData(null, null, Long.MAX_VALUE, true, null, null,
            new UUIDGenerator());
        writer = new WARCWriter(serialNo, out, new File(path.toString()), settings);

        warcFileCounter++;

    }

    @Override
    protected void map(String key, WebPage page, Context context) throws IOException,
            InterruptedException {
        context.getCounter(Counters.PROCESSED).increment(1L);
        try {
            if (CrawlStatus.STATUS_FETCHED != page.getStatus().byteValue()) {
                context.getCounter(Counters.UNFETCHED).increment(1L);
                return;
            }
            logger.debug("Exporting record for {}, status={}", key,
                CrawlStatus.getName(page.getStatus().byteValue()));
            WARCRecordInfo record = new WARCRecordInfo();
            record.setType(WARCRecordType.resource);
            String url = TableUtil.unreverseUrl(key);
            record.setUrl(url);

            String mime = page.getContentType().toString();

            if (mime == null) {
                logger.info("Mime was null for " + key);

                mime = "application/http; msgtype=response";
            }
            record.setMimetype(mime);
            String crawlDate = DATE14_FORMAT.print(page.getFetchTime());
            record.setCreate14DigitDate(crawlDate);

            StringBuilder headers = new StringBuilder(1024);
            int status = WebPageUtils.mapHttpStatus(page.getProtocolStatus());
            final ProtocolVersion proto = new ProtocolVersion("HTTP", 1, 1);
            String reason = EnglishReasonPhraseCatalog.INSTANCE.getReason(status, Locale.ENGLISH);
            final BasicStatusLine statusLine = new BasicStatusLine(proto, status, reason);
            headers.append(statusLine.toString()).append("\r\n");
            for (Entry<CharSequence, CharSequence> header : page.getHeaders().entrySet()) {
                headers.append(header.getKey())
                    .append(": ")
                    .append(header.getValue())
                    .append("\r\n");
            }
            headers.append("\r\n\r\n");
            byte[] headerBytes = headers.toString().getBytes(UTF_8);
            ByteBuffer allBuffer = ByteBuffer.allocate(headerBytes.length
                    + page.getContent().limit());
            allBuffer.put(headerBytes);
            allBuffer.put(page.getContent());
            page.getContent().flip();
            allBuffer.flip();
            record.setContentLength(allBuffer.limit());
            record.setContentStream(new ByteBufferInputStream(allBuffer));

            record.setRecordId(new URI(url));
            writer.writeRecord(record);
            writtenRecords.incrementAndGet();
            context.getCounter(Counters.EMITTED).increment(1L);
        } catch (Exception e) {
            logger.debug("Exception while processing page {}", key, e);
            context.getCounter(Counters.FAILED).increment(1L);
        }

    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        writer.close();
        logger.info("WARC export finished, wrote {} records", writtenRecords.get());
    }
}