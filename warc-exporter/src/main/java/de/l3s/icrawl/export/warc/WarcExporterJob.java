package de.l3s.icrawl.export.warc;

import java.io.IOException;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.l3s.icrawl.crawlmanager.TaskExecution;
import de.l3s.icrawl.crawlmanager.TaskExecutionContext;
import de.l3s.icrawl.domain.crawl.WarcExporterRecord;
import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.domain.status.ExportStatus;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate.JobSetup;

public class WarcExporterJob implements TaskExecution {
    private static class WarcExporterReducer extends Reducer<Text, Text, Text, Text> {}

    private static final Logger logger = LoggerFactory.getLogger(WarcExporterJob.class);
    private static final String[] FIELDS = { "baseUrl", "status", "fetchTime", "modifiedTime",
            "protocolStatus", "content", "contentType", "parseStatus", "score", "reprUrl",
            "headers", "outlinks", "inlinks", "markers", "metadata", };

    private final CrawlDataRepositoryTemplate template;
    private final Campaign campaign;
    private final String user;
    private final String path;

    private State state = State.STARTING;
    private org.apache.hadoop.mapreduce.Job job;
    private Exception exception;

    public WarcExporterJob(CrawlDataRepositoryTemplate template, WarcExporterRecord exportRecord) {
        this.template = template;
        this.campaign = exportRecord.getCampaign();
        this.user = exportRecord.getConfiguration().getUser();
        this.path = exportRecord.getConfiguration().getPath();
    }

    @Override
    public void execute(TaskExecutionContext context) {
        try {
            logger.info("Starting Exporter job for crawl {}", campaign);
            job = template.jobBuilder(campaign, "WARC export")
                .setFields(FIELDS)
                .setMapper(WarcExporterMapper.class, Text.class, Text.class)
                .setReducer(WarcExporterReducer.class, Text.class, Text.class)
                .setNumReducers(0)
                .setOutputFormat(TextOutputFormat.class)
                .setupJob(new JobSetup() {

                    @Override
                    public void setup(org.apache.hadoop.mapreduce.Job job) {
                        FileOutputFormat.setOutputPath(job, new Path(path));
                    }
                })
                .execute();
            logger.info("Exporter job started for crawl {}", campaign);

            this.state = State.RUNNING;
            boolean success = job.waitForCompletion(false);
            logger.info("Exporter job finished for crawl {}, success={}", campaign, success);
            this.state = success ? State.FINISHED : State.FAILED;
        } catch (IOException | ClassNotFoundException e) {
            this.state = State.FAILED;
            this.exception = e;
            logger.info("Exception while running export", e);
        } catch (InterruptedException e) {
            this.state = State.STOPPED;
            logger.info("Interrupted while running export job", e);
        } catch (Exception e) {
            logger.warn("Uncaught exception", e);
            throw e;
        } finally {
            logger.info("Exiting Exporter job for crawl {}", campaign);
        }
    }

    @Override
    public ExportStatus getStatus() {
        switch (state) {
        case STARTING:
            return ExportStatus.starting(campaign.getId(), user);
        case RUNNING:
            return ExportStatus.running(campaign.getId(), getProgress());
        case FINISHED:
            return ExportStatus.finished(campaign.getId());
        case STOPPING:
        case KILLING:
            return ExportStatus.stopping(campaign.getId(), user, getProgress());
        case STOPPED:
        case KILLED:
            return ExportStatus.aborted(campaign.getId(), user, getProgress());
        case FAILED:
            String error = exception != null ? exception.getMessage() : "error";
            return ExportStatus.failed(campaign.getId(), error, getProgress());
        default:
            throw new IllegalStateException(state.name());
        }
    }

    @Override
    public void stop() {
        this.state = State.STOPPING;
        innerStop();
        this.state = State.STOPPED;
    }

    @Override
    public void kill() {
        this.state = State.KILLING;
        innerStop();
        this.state = State.KILLED;
    }

    private void innerStop() {
        if (job != null) {
            try {
                job.killJob();
            } catch (IOException e) {
                logger.warn("Exception while stopping job", e);
                this.exception = e;
            }
        }
    }


    @Override
    public double getProgress() {
        if (job == null) {
            return -1;
        } else {
            try {
                return job.mapProgress();
            } catch (IOException e) {
                logger.info("Exception while getting job progress", e);
                return -1;
            }
        }
    }

}
