package de.l3s.icrawl.core;

/**
 * Entity mention 
 * @author giangbinhtran
 *
 */
public class iEntity {
	String _mention;
	String _type="MISC";
	public iEntity (String mention) {
		_mention = mention;
	}
	public iEntity (String type, String mention) {
		_type = type;
		_mention = mention;
	}
	
	public String getMention() {
		return _mention;
	}
	
	public String getType() {
		return _type;
	}
	
	public String toString() {
		return _mention + ":" + _type;
	}
}
