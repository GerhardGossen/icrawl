package de.l3s.icrawl.onlineAnalysis;


import java.util.ArrayList;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.extractors.ArticleExtractor;
import de.l3s.icrawl.core.iEntity;

import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.*;
import edu.stanford.nlp.io.IOUtils;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.Triple;

import java.util.List;
import java.io.IOException;


import java.util.List;
import java.io.IOException;


/**
 * This class preprocess the output from crawler (warc files)
 * e.g. extract html data (or other types of metadata)
 * @author giangbinhtran
 */
public class NERHandler {
	String serializedClassifier = "resources/english.conll.4class.distsim.crf.ser.gz";
	AbstractSequenceClassifier<CoreLabel> classifier; 
	
	public NERHandler() {
		classifier = CRFClassifier.getClassifierNoExceptions(serializedClassifier);
	}
	
	
	/**
	 * get content text from source
	 * @param source
	 */
	public String getContent(String source) throws Exception {
		return ArticleExtractor.INSTANCE.getText(source);
	}

	/**
	 * get entity mentions from the NL text
	 * @param content
	 */
	public ArrayList<iEntity> getEntityMentions(String content) {
		ArrayList<iEntity> mentions = new ArrayList<iEntity> ();

        List<Triple<String,Integer,Integer>> out = classifier.classifyToCharacterOffsets(content);
        for (Triple<String, Integer, Integer> triple: out) {
        	String type = triple.first();
        	String mention = content.substring(triple.second(), triple.third());
        	iEntity e = new iEntity(type, mention);
        	mentions.add(e);
        }
		return mentions;
	}
	
	public static void main(String[] args) {
		NERHandler ner = new NERHandler();
		ArrayList<iEntity> ents = ner.getEntityMentions("I go to school at the L3S Research Center in Hannover.");
		for (iEntity e: ents) {
			System.out.println(e.toString());
		}
		
	}
}
