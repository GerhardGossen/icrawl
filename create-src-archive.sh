#!/bin/sh
VERSION=${1:-HEAD}

echo Exporting version ${VERSION} ...

git archive -o ${VERSION}-src.zip --prefix=${VERSION}/ ${VERSION}
git archive -o ${VERSION}-src.tar.gz --prefix=${VERSION}/ ${VERSION}
