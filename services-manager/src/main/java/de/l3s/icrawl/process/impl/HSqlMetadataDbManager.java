package de.l3s.icrawl.process.impl;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;

import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.hsqldb.server.Server;
import org.hsqldb.server.ServerConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import de.l3s.icrawl.process.MetadataDbManager;

@Service
@Profile("services")
public class HSqlMetadataDbManager implements MetadataDbManager {
    static final class Slf4jLoggingWriter extends PrintWriter {
        static final class LogOutputStream extends Writer {
            private final Logger delegateLogger;
            private final StringBuilder buffer;

            public LogOutputStream(Class<?> contextClass) {
                this.delegateLogger = LoggerFactory.getLogger(contextClass);
                this.buffer = new StringBuilder(1024);
            }

            @Override
            public void flush() throws IOException {
                delegateLogger.trace("Flushing LogOutputStream");
            }

            @Override
            public void close() throws IOException {
                delegateLogger.trace("Closing LogOutputStream");
            }

            @Override
            public void write(char[] cbuf, int off, int len) throws IOException {
                for (int i = 0; i < len; i++) {
                    char ch = cbuf[off + i];
                    if (ch == '\n' && this.buffer.length() > 0) {
                        this.delegateLogger.debug(this.buffer.toString());
                        this.buffer.setLength(0);
                    } else {
                        this.buffer.append(ch);
                    }
                }
            }
        }

        public Slf4jLoggingWriter(Class<?> contextClass) {
            super(new LogOutputStream(contextClass));
        }
    }

    private static final Logger logger = LoggerFactory.getLogger(HSqlMetadataDbManager.class);
    private final Server server;

    @Inject
    public HSqlMetadataDbManager(@Value("${icrawl.data.directory}") File dataDirectory,
            @Value("${icrawl.db.port}") int port) {
        server = new Server();
        server.setLogWriter(new Slf4jLoggingWriter(Server.class));
        server.setPort(port);
        server.setDatabaseName(0, "metadata");
        String dbFileName = new File(dataDirectory, "metadata.db").toString();
        server.setDatabasePath(0, dbFileName);
        server.setNoSystemExit(true);
        server.setSilent(true);
        logger.info("Using metadata DB in {}", dbFileName);
        start();
    }

    @Override
    public void start() {
        logger.info("Starting metadata DB ...");
        server.start();
        logger.info("Starting metadata DB finished.");
    }

    @Override
    @PreDestroy
    public void stop() {
        logger.info("Stopping metadata DB ...");
        server.stop();
        logger.info("Stopping metadata DB finished.");
    }

    @Override
    public boolean isRunning() {
        return server.getState() == ServerConstants.SERVER_STATE_ONLINE;
    }

    @Override
    public String getStatus() {
        return server.getStateDescriptor();
    }
}
