package de.l3s.icrawl.process.impl;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.apache.activemq.apollo.broker.Broker;
import org.apache.activemq.apollo.dto.AcceptingConnectorDTO;
import org.apache.activemq.apollo.dto.AuthenticationDTO;
import org.apache.activemq.apollo.dto.BrokerDTO;
import org.apache.activemq.apollo.dto.NullStoreDTO;
import org.apache.activemq.apollo.dto.VirtualHostDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import de.l3s.icrawl.process.MessageBusManager;

@Service
public class ApolloMessageBusManager implements MessageBusManager {
    private static final Logger logger = LoggerFactory.getLogger(ApolloMessageBusManager.class);
    private final Broker broker;

    @Inject
    public ApolloMessageBusManager(@Value("${icrawl.data.directory}") File appDir,
            @Value("${icrawl.messages.tcpPort}") int tcpPort,
            @Value("${icrawl.messages.wsPort}") int wsPort) throws IOException {
        this.broker = createBroker(appDir, tcpPort, wsPort);
        start();
    }

    static Broker createBroker(File appDir, int tcpPort, int wsPort) {
        Broker broker = new Broker();
        broker.setTmp(new File(appDir, "apollo"));
        broker.setConfig(createConfig(tcpPort, wsPort));
        return broker;
    }

    static BrokerDTO createConfig(int tcpPort, int wsPort) {
        BrokerDTO broker = new BrokerDTO();

        VirtualHostDTO host = new VirtualHostDTO();
        host.id = "default";
        host.host_names.add("localhost");
        host.host_names.add("127.0.0.1");
        AuthenticationDTO authentication = new AuthenticationDTO();
        authentication.enabled = false;
        host.authentication = authentication;

        // No persistent storage needed
        NullStoreDTO store = new NullStoreDTO();
        host.store = store;

        broker.virtual_hosts.add(host);

        host.auto_create_destinations = true;

        AcceptingConnectorDTO connector = new AcceptingConnectorDTO();
        connector.id = "tcp";
        connector.bind = "tcp://0.0.0.0:" + tcpPort;
        connector.connection_limit = 50;
        broker.connectors.add(connector);

        AcceptingConnectorDTO connectorWS = new AcceptingConnectorDTO();
        connectorWS.id = "ws";
        connectorWS.bind = "ws://0.0.0.0:" + wsPort;
        connectorWS.connection_limit = 50;
        broker.connectors.add(connectorWS);

        return broker;
    }

    @Override
    public void start() throws IOException {
        final CountDownLatch latch = new CountDownLatch(1);
        broker.start(new Runnable() {
            @Override
            public void run() {
                logger.info("Started message bus broker");
                latch.countDown();
            }
        });
        try {
            latch.await();
        } catch (InterruptedException e) {
            logger.warn("Interrupted while starting broker, may not be running yet");
        }
    }

    @Override
    @PreDestroy
    public void stop() {
        final CountDownLatch latch = new CountDownLatch(1);
        broker.stop(new Runnable() {
            @Override
            public void run() {
                logger.info("Stopped message bus broker");
                latch.countDown();
            }
        });
        try {
            boolean successful = latch.await(1, TimeUnit.MINUTES);
            if (!successful) {
                logger.warn("Message bus broker did not stop in alloted time, make sure it's really stopped before continuing");
            }
        } catch (InterruptedException e) {
            logger.warn("Interrupted while waiting for message bus broker to shut down, may be in inconsistent state");
        }
    }

    @Override
    public boolean isRunning() {
        return broker.service_state().is_starting_or_started();
    }

    @Override
    public String getStatus() {
        return broker.service_state().toString();
    }

}
