package de.l3s.icrawl.process.impl;

import java.io.IOException;

import org.apache.accumulo.core.client.AccumuloException;
import org.apache.accumulo.core.client.AccumuloSecurityException;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.Instance;
import org.apache.accumulo.core.client.ZooKeeperInstance;
import org.apache.accumulo.core.client.security.tokens.PasswordToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.l3s.icrawl.process.CrawlDbManager;

import static java.util.stream.Collectors.joining;

public class AccumuloCrawlDbManager implements CrawlDbManager {
    private static final Logger logger = LoggerFactory.getLogger(AccumuloCrawlDbManager.class);
    private final Instance instance;
    private Connector conn;

    public AccumuloCrawlDbManager(String instanceName, String zooKeepers) throws IOException {
        instance = new ZooKeeperInstance(instanceName, zooKeepers);
        try {
            conn = instance.getConnector("root", new PasswordToken("password"));
        } catch (AccumuloException | AccumuloSecurityException e) {
            throw new IOException("Could not connect to Accumulo", e);
        }
    }

    @Override
    public void start() throws IOException {
        logger.info("External Accumulo is running? {}", isRunning());
    }

    @Override
    public void stop() {
        logger.info("Skipping stop of external Accumulo");
    }

    @Override
    public boolean isRunning() {
        return !conn.instanceOperations().getTabletServers().isEmpty();
    }

    @Override
    public String getStatus() {
        return conn.instanceOperations()
            .getTabletServers()
            .stream()
            .collect(joining(", ", "Servers: [", "]"));
    }

}
