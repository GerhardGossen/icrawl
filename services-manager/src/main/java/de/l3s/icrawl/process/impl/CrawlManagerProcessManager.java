package de.l3s.icrawl.process.impl;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.io.Files;
import com.google.common.io.PatternFilenameFilter;

import de.l3s.icrawl.process.CrawlManagerManager;

@Service
public class CrawlManagerProcessManager implements CrawlManagerManager {
    private static final Logger logger = LoggerFactory.getLogger(CrawlManagerProcessManager.class);

    private final File appDirectory;

    private final CrawlManagerManager delegate;

    private final String version;

    private Process process;

    @Inject
    public CrawlManagerProcessManager(@Value("${icrawl.directory}") File appDirectory,
            @Qualifier("remoteCrawlManager") CrawlManagerManager delegate,
            @Value("${icrawl.version}") String version) throws IOException {
        this.appDirectory = appDirectory;
        this.delegate = delegate;
        this.version = version;
        start();
    }

    private Collection<File> createNutchClassPath() {
        List<File> classPath = new ArrayList<>();
        logger.info("Using appDirectory={}", appDirectory);
        classPath.add(appDirectory);
        addIfExists(classPath, new File(appDirectory, "conf"));
        File javaLib = new File(System.getProperty("java.home"), "lib");
        addIfExists(classPath, new File(javaLib, "tools.jar"));
        FilenameFilter jarFilter = new PatternFilenameFilter(".*\\.jar");
        for (File libJar : new File(appDirectory, "lib").listFiles(jarFilter)) {
            // exclude logging jars, otherwise delegation loop Log4J -> SLF4J -> Log4J, and servlet jars
            if (!libJar.getName().contains("slf4j") && !libJar.getName().contains("servlet")
                    && !libJar.getName().contains("jaxb-")
                    && !libJar.getName().matches("^nutch-\\d\\.\\d-icrawl.*")) {
                classPath.add(libJar);
            }
        }
        Collections.reverse(classPath);
        return classPath;
    }

    private void addIfExists(Collection<File> classPath, File classPathEntry) {
        if (classPathEntry.exists()) {
            classPath.add(classPathEntry);
        }
    }

    @Override
    public void start() throws IOException {
        logger.info("Starting CrawlManager in separate JVM ...");
        String separator = System.getProperty("path.separator");
        Set<File> classPathEntries = new LinkedHashSet<>();
        classPathEntries.add(new File(appDirectory, "conf"));
        classPathEntries.add(getCrawlManagerJar());
        File cmClassPath = new File(getCrawlManagerDirectory(), "classpath.txt");
        File nutchClassPath = new File(appDirectory, "nutch-classpath");
        classPathEntries.addAll(getClassPath(nutchClassPath, new File(appDirectory, "lib")));
        classPathEntries.addAll(getClassPath(cmClassPath, appDirectory));

        classPathEntries.addAll(createNutchClassPath());
        String classPath = Joiner.on(separator).join(classPathEntries);
        logger.info("Using classpath for crawl manager: {}", classPath);
        logger.info("|classpath|= {}", classPathEntries.size());
        ProcessBuilder pb = new ProcessBuilder("java", "-cp", classPath,
            "-Dicrawl.directory=" + appDirectory.getAbsolutePath(),
            "de.l3s.icrawl.crawlmanager.CrawlManagerService");
        File logsDir = new File(appDirectory, "logs");
        pb.redirectOutput(new File(logsDir, "crawl-manager.out"));
        pb.redirectError(new File(logsDir, "crawl-manager.err"));
        process = pb.start();
        logger.info("CrawlManager successfully started");
    }

    private File getCrawlManagerJar() {
        File directory = getCrawlManagerDirectory();
        File jarFile = new File(directory, "crawl-manager-" + version + ".jar");
        if (!jarFile.exists()) {
            throw new IllegalStateException("No crawl manager .jar, expected file " + jarFile);
        } else {
            return jarFile;
        }
    }

    private Collection<File> getClassPath(File classPathFile, File baseDir)
            throws IOException {
        Set<File> entries = new LinkedHashSet<>();
        String classPath = Files.toString(classPathFile, StandardCharsets.UTF_8);
        for (String lib : Splitter.on(':').split(classPath)) {
            addIfExists(entries, new File(baseDir, lib));
        }
        return entries;
    }

    private File getCrawlManagerDirectory() {
        return new File(appDirectory, "lib");
    }

    @Override
    @PreDestroy
    public void stop() {
        if (process == null) {
            logger.info("CrawlManager is not running, no need for stop");
            return;
        }

        logger.info("Stopping CrawlManager ...");
        delegate.stop();
        try {
            int retcode = process.waitFor();
            logger.debug("CrawlManager stopped with code {}", retcode);
        } catch (InterruptedException e) {
            logger.info("Got interrupt while waiting for CrawlManager to stop, killing it");
            process.destroy();
            logger.info("CrawlManager killed");
        }
        process = null;
        logger.info("Stopping CrawlManager finished.");
    }

    @Override
    public boolean isRunning() {
        return process == null || delegate.isRunning();
    }

    @Override
    public String getStatus() {
        return process == null ? "Not started" : delegate.getStatus();
    }

}
