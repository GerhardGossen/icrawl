package de.l3s.icrawl.process;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafAutoConfiguration;
import org.springframework.boot.autoconfigure.velocity.VelocityAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jms.listener.SimpleMessageListenerContainer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;

import de.l3s.icrawl.process.impl.AccumuloCrawlDbManager;
import de.l3s.icrawl.ui.UiConfiguration;
import de.l3s.icrawl.util.HadoopUtils;

import static de.l3s.icrawl.process.JmsConfig.*;
import static de.l3s.icrawl.util.HadoopUtils.defaultDataDirectory;

@Configuration
@EnableAutoConfiguration(exclude = { ThymeleafAutoConfiguration.class,
        SecurityAutoConfiguration.class, HibernateJpaAutoConfiguration.class,
        DataSourceAutoConfiguration.class, VelocityAutoConfiguration.class })
@PropertySource({ "application.properties", "gora.properties" })
@Profile("services")
@ComponentScan(basePackageClasses = { ProcessManager.class })
public class Bootstrap {
    private static final Logger logger = LoggerFactory.getLogger(Bootstrap.class);
    @Value("${icrawl.directory}")
    File appDirectory;

    @Value("${icrawl.data.directory}")
    String dataDirectory;

    @Value("${gora.datastore.accumulo.instance}")
    String instanceName;

    @Value("${gora.datastore.accumulo.zookeepers}")
    String zooKeepers;

    @Inject
    JmsConfig jmsConfig;

    @Inject
    @Qualifier("crawlManagerProcessManager")
    CrawlManagerManager crawlManagerManager;


    @Bean
    org.apache.hadoop.conf.Configuration conf() {
        return HadoopUtils.createConf(appDirectory);
    }

    @Bean
    @Profile("services")
    CrawlDbManager crawlDbManager() throws IOException {
        return new AccumuloCrawlDbManager(instanceName, zooKeepers);
    }

    @Bean
    SimpleMessageListenerContainer crawlDbManagerExporter() throws IOException {
        return jmsConfig.createServiceExporter(CrawlDbManager.class, crawlDbManager(),
            CRAWL_DB_MANAGER_QUEUE);
    }

    @Bean
    SimpleMessageListenerContainer metadataDbManagerExporter(MetadataDbManager metadataDbManager) {
        return jmsConfig.createServiceExporter(MetadataDbManager.class, metadataDbManager,
            METADATA_DB_MANAGER_QUEUE);
    }

    @Bean
    @DependsOn({ "messageBusManager", "crawlDbManager" })
    CrawlManagerManager remoteCrawlManager() {
        return jmsConfig.createProxy(CRAWL_MANAGER_MANAGER_QUEUE, CrawlManagerManager.class, true);
    }

    @Bean
    SimpleMessageListenerContainer messageBusManager(MessageBusManager messageBusManager) {
        return jmsConfig.createServiceExporter(MessageBusManager.class, messageBusManager,
            MESSAGE_BUS_MANAGER_QUEUE);
    }

    @Bean
    ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    public static void main(String[] args) {
        ImmutableMap<String, Object> defaultProperties = createDefaultProperties();

        startServiceManager(args, defaultProperties);

        startWebUI(args, defaultProperties);
    }

    private static ImmutableMap<String, Object> createDefaultProperties() {
        ImmutableMap.Builder<String, Object> propsBuilder = ImmutableMap.builder();
        String appDirectory = System.getProperty("app.home");
        if (appDirectory != null) {
            logger.info("In Distribution version");
            propsBuilder.put("icrawl.directory", appDirectory);
            propsBuilder.put("icrawl.data.directory", defaultDataDirectory(appDirectory));
        }
        propsBuilder.put("spring.config.name", "icrawl").put("app.name", "iCrawl");
        return propsBuilder.build();
    }

    private static void startServiceManager(String[] args,
            ImmutableMap<String, Object> defaultProperties) {
        SpringApplication app = new SpringApplication(Bootstrap.class);
        app.setAdditionalProfiles("services");
        app.setShowBanner(false);
        app.setDefaultProperties(defaultProperties);
        app.setWebEnvironment(false);
        ConfigurableApplicationContext context = app.run(args);
        logger.info("appDirectory={}", context.getEnvironment().getProperty("icrawl.directory"));
        context.registerShutdownHook();
    }

    private static void startWebUI(String[] args, ImmutableMap<String, Object> defaultProperties) {
        SpringApplication webApp = new SpringApplication(UiConfiguration.class);
        webApp.setMainApplicationClass(UiConfiguration.class);
        webApp.setAdditionalProfiles("ui");
        webApp.setShowBanner(false);
        webApp.setDefaultProperties(defaultProperties);
        logger.info("About to start Web App");
        ConfigurableApplicationContext webCtx = webApp.run(args);
        webCtx.registerShutdownHook();
    }
}
