iCrawl: The Integrated Focused Crawling Toolbox
===============================================

iCrawl is a Web Crawling toolbox developed at [L3S Research Center][l3s].
It provides **integrated** and **focused** crawling of Web and Social Media data, i.e. the combined crawling of the Web and Social Media for a specified topic.
It allows researchers, journalists and other domain experts to easily create and run web crawls to collect, analyze and archive web documents for their specific needs.

You can [download the system](download.html) and give it a go.


The system is described in the following published and several forthcoming papers:

- [What Do You Want to Collect from the Web?](http://www.l3s.de/~gossen/publications/risse_et_al_bwow_2014.pdf) (2014)<br/>
  Thomas Risse, Elena Demidova, and Gerhard Gossen. Proc. of the Building Web Observatories Workshop BWOW 2014, page 1-7.
- [The iCrawl Wizard – Supporting Interactive Focused Crawl Specification](http://www.l3s.de/~gossen/publications/gossen_et_al_ecir_2015.pdf) (2015)<br/>
  Gerhard Gossen, Elena Demidova, and Thomas Risse. Proc. of the 37th European Conference on Information Retrieval ECIR'15.
- [iCrawl: Improving the Freshness of Web Collections by Integrating Social Web and Focused Web Crawling](http://www.l3s.de/~gossen/publications/gossen_et_al_jcdl_2015.pdf) (2015)<br/>
  Gerhard Gossen, Elena Demidova, and Thomas Risse. Proceedings of the Joint Conference on Digital Libraries 2015.

  [l3s]: https://www.l3s.de/
