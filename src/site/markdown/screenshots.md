Screen shots
============

Crawl statistics view
---------------------

![Crawl Statistics](crawl_statistics.png)

Crawl specification Wizard
--------------------------

![Crawl specification wizard](wizard.png)

QA: Individual resource
-----------------------

![Resource view](qa_individual_resource.png)

Wayback Machine Integration
---------------------------

![Wayback Machine Integration](wayback_machine.png)


