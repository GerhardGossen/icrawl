package de.l3s.icrawl.apifetcher;

import java.io.IOException;
import java.util.Map;

import org.apache.nutch.util.NutchConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.l3s.icrawl.api.ApiFetcherContext;
import de.l3s.icrawl.api.OAuthCredentialsProvider;
import de.l3s.icrawl.domain.api.ApiFetcherDocument;
import de.l3s.icrawl.domain.api.ApiRequest;
import de.l3s.icrawl.domain.status.ApiStatus;

public class TwitterApiFetcherTest {
    private static final class LoggingContext implements ApiFetcherContext {
        @Override
        public void writeRedirect(String fromUri, String toUri, Map<String, String> metadata) {
            logger.debug("writeRedirect {} -> {}, {}", fromUri, toUri, metadata);
        }

        @Override
        public void writeOutlink(String uri, Map<String, String> metadata) {
            logger.debug("writeOutlink {}, {}", uri, metadata);
        }

        @Override
        public void writeDocument(ApiFetcherDocument doc) {
            logger.debug("writeDocument {}", doc);
        }

        @Override
        public void reportError(ApiStatus.ErrorType type, ApiStatus.ErrorBehavior behavior, String message, Exception e) {
            logger.debug("reportError {}, {}, {}, {}", type, behavior, message, e);
        }

        @Override
        public boolean equals(Object obj) {
            return this == obj;
        }
    }

    private static final Logger logger = LoggerFactory.getLogger(TwitterApiFetcherTest.class);

    public static void followKeyword(String keyword) throws InterruptedException, IOException {
        OAuthCredentialsProvider credentialsProvider = new InMemoryCredentialsProvider(
            OAuthCredentialUtils.fromProperties("twitter4j.properties"));
        try (TwitterApiFetcher twitterApiFetcher = new TwitterApiFetcher(credentialsProvider)) {
            twitterApiFetcher.setConfiguration(NutchConfiguration.create());
            ApiRequest request = new ApiRequest("twitter", "");
            request.getQueries().add(keyword);
            LoggingContext context = new LoggingContext();
            twitterApiFetcher.addRequest(request, context);
            System.out.print("Press any key to stop ...");
            System.in.read();
            twitterApiFetcher.removeRequest(request, context);
        }
    }

    public static void main(String[] args) {
        try {
            followKeyword(args.length > 0 ? args[0] : "#MH17");
        } catch (InterruptedException | IOException e) {
            logger.warn("Exception: ", e);
        }
    }

}
