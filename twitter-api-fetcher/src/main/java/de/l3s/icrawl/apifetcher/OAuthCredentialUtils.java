package de.l3s.icrawl.apifetcher;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.google.common.base.MoreObjects;
import com.google.common.io.Resources;

import de.l3s.icrawl.domain.api.OAuthCredentials;

public class OAuthCredentialUtils {
    private static class UnstoredOAuthCredentials implements OAuthCredentials {

        private final String consumerKey;
        private final String consumerSecret;
        private final String accessToken;
        private final String tokenSecret;

        public UnstoredOAuthCredentials(String consumerKey, String consumerSecret,
                String accessToken, String tokenSecret) {
            this.consumerKey = consumerKey;
            this.consumerSecret = consumerSecret;
            this.accessToken = accessToken;
            this.tokenSecret = tokenSecret;
        }

        @Override
        public String getConsumerKey() {
            return consumerKey;
        }

        @Override
        public String getConsumerSecret() {
            return consumerSecret;
        }

        @Override
        public String getAccessToken() {
            return accessToken;
        }

        @Override
        public String getAccessTokenSecret() {
            return tokenSecret;
        }
        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                .add("consumerKey", consumerKey)
                .add("consumerSecret", consumerSecret)
                .add("accessToken", accessToken)
                .add("accessTokenSecret", tokenSecret)
                .toString();
        }

    }

    public static OAuthCredentials fromProperties(String fileName) throws IOException {
        try (InputStream is = Resources.getResource(fileName).openStream()) {
            return fromProperties(is);
        }
    }

    public static OAuthCredentials fromProperties(InputStream is) throws IOException {
        Properties props = new Properties();
        props.load(is);

        String consumerKey = props.getProperty("oauth.consumerKey");
        String consumerSecret = props.getProperty("oauth.consumerSecret");
        String accessToken = props.getProperty("oauth.accessToken");
        String tokenSecret = props.getProperty("oauth.accessTokenSecret");

        return new UnstoredOAuthCredentials(consumerKey, consumerSecret, accessToken, tokenSecret);
    }
}