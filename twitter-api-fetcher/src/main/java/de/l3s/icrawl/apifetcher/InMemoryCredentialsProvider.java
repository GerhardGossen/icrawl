package de.l3s.icrawl.apifetcher;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;

import de.l3s.icrawl.api.OAuthCredentialsProvider;
import de.l3s.icrawl.domain.api.OAuthCredentials;

public class InMemoryCredentialsProvider implements OAuthCredentialsProvider {

    private final Queue<OAuthCredentials> elements = new LinkedList<>();

    public InMemoryCredentialsProvider(OAuthCredentials... credentials) {
        this(Arrays.asList(credentials));
    }
    public InMemoryCredentialsProvider(Collection<OAuthCredentials> credentials) {
        elements.addAll(credentials);
    }

    @Override
    public OAuthCredentials get() {
        return elements.poll();
    }

    @Override
    public void release(OAuthCredentials credentials) {
        elements.offer(credentials);
    }
}
