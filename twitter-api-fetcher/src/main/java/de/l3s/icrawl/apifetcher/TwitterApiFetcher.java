package de.l3s.icrawl.apifetcher;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.hadoop.conf.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.FilterQuery;
import twitter4j.ResponseList;
import twitter4j.StallWarning;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterObjectFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.URLEntity;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;

import com.google.common.base.Preconditions;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import de.l3s.icrawl.api.AbstractApiFetcher;
import de.l3s.icrawl.api.OAuthCredentialsProvider;
import de.l3s.icrawl.domain.api.ApiFetcherDocument;
import de.l3s.icrawl.domain.api.ApiFetcherException;
import de.l3s.icrawl.domain.api.ApiRequest;
import de.l3s.icrawl.domain.api.OAuthCredentials;
import de.l3s.icrawl.domain.status.ApiStatus;

public class TwitterApiFetcher extends AbstractApiFetcher {
    public static final String TWITTER_MIME_TYPE = "application/twitter+json";
    private final class StatusStreamListener implements StatusListener {
        private final ApiRequest request;

        private StatusStreamListener(ApiRequest request) {
            this.request = request;
        }

        @Override
        public void onException(Exception ex) {
            reportError(request, ApiStatus.ErrorType.SERVER, ApiStatus.ErrorBehavior.RETRY, null,
                ex);
        }

        @Override
        public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
            reportError(request, ApiStatus.ErrorType.CLIENT, ApiStatus.ErrorBehavior.RETRY,
                "Some results are omitted from stream because they exceed limitations", null);
        }

        @Override
        public void onStallWarning(StallWarning warning) {
            reportError(request, ApiStatus.ErrorType.CLIENT, ApiStatus.ErrorBehavior.RETRY,
                warning.toString(), null);
        }

        @Override
        public void onScrubGeo(long userId, long upToStatusId) {
            logger.debug("scrubGeo({},{}", userId, upToStatusId);
        }

        @Override
        public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
            logger.debug("deletionNotice({})", statusDeletionNotice);
        }

        @Override
        public void onStatus(twitter4j.Status status) {
            logger.trace("Got status, text is '{}'", status.getText());
            URLEntity[] urls = status.getURLEntities();
            for (URLEntity url : urls) {
                if (!url.getURL().equals(url.getExpandedURL())) {
                    writeRedirect(request, url.getURL(), url.getExpandedURL(),
                        ImmutableMap.of("source", request.toString()));
                }
            }
            String tweetUrl = String.format("https://twitter.com/%s/status/%s",
                status.getUser().getScreenName(),
                status.getId());
            Date createdAt = status.getCreatedAt();
            ApiFetcherDocument doc = ApiFetcherDocument.builder()
                .setUrl(tweetUrl)
                .setContentType(TWITTER_MIME_TYPE)
                .setFetchTime(createdAt.getTime())
                .setModifiedTime(createdAt.getTime())
                .setSourceUrl(request.toString())
                .setContent(TwitterObjectFactory.getRawJSON(status))
                .setStatusCode(200)
                .setHeaders(new HashMap<String, String>())
                .build();
            logger.debug("Storing under tweet URL {} with source {}", tweetUrl, request);
            writeDocument(request, doc);
        }
    }

    private static final Logger logger = LoggerFactory.getLogger(TwitterApiFetcher.class);

    private final Map<ApiRequest, TwitterStream> streamsByRequest = new HashMap<>();

    private final Map<TwitterStream, OAuthCredentials> credentialsByStream = new HashMap<>();

    private final Twitter twitter;

    private final OAuthCredentialsProvider credentialsProvider;


    public TwitterApiFetcher(OAuthCredentialsProvider credentialsProvider) {
        this.credentialsProvider = credentialsProvider;

        twitter = new TwitterFactory(new ConfigurationBuilder().build()).getInstance();
    }

    @Override
    public void setConfiguration(Configuration conf) {
        super.setConfiguration(conf);
    }

    @Override
    public String getUriScheme() {
        return "twitter:";
    }

    @Override
    protected void startFollowing(final ApiRequest request) {
        Preconditions.checkState(!streamsByRequest.containsKey(request), "Already following  %s",
            request);
        FilterQuery query = new FilterQuery();
        Set<String> keywordQueries = request.getQueries();
        if (!keywordQueries.isEmpty()) {
            query = setKeywordQuery(query, keywordQueries);
        }
        if (!request.getUsers().isEmpty()) {
            query = setUserQuery(query, request.getUsers());
        }
        // TODO locations
        TwitterStream stream = createStream();
        stream.addListener(new StatusStreamListener(request));
        stream.filter(query);
        streamsByRequest.put(request, stream);
    }

    private TwitterStream createStream() {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setJSONStoreEnabled(true);

        OAuthCredentials credentials = credentialsProvider.get();
        if (credentials == null) {
            throw new ApiFetcherException("No twitter credentials are available");
        }
        logger.info("Using twitter credentials {}", credentials);

        cb.setOAuthConsumerKey(credentials.getConsumerKey());
        cb.setOAuthConsumerSecret(credentials.getConsumerSecret());
        cb.setOAuthAccessToken(credentials.getAccessToken());
        cb.setOAuthAccessTokenSecret(credentials.getAccessTokenSecret());

        twitter4j.conf.Configuration twitterConfig = cb.build();
        TwitterStreamFactory streamFactory = new TwitterStreamFactory(twitterConfig);
        TwitterStream stream = streamFactory.getInstance();
        credentialsByStream.put(stream, credentials);
        return stream;
    }

    protected FilterQuery setKeywordQuery(FilterQuery query, Set<String> keywordQueries) {
        String[] keywords = keywordQueries.toArray(new String[keywordQueries.size()]);
        return query.track(keywords);
    }

    protected FilterQuery setUserQuery(FilterQuery query, Set<String> users) {
        Collection<Long> userIds = lookupUsers(users);
        long[] userIdSpec = new long[userIds.size()];
        int i = 0;
        for (Long id : userIds) {
            if (id != null) {
                userIdSpec[i] = id;
                i++;
            }
        }
        if (i < userIdSpec.length) {
            userIdSpec = Arrays.copyOf(userIdSpec, i);
        }
        return query.follow(userIdSpec);
    }


    /**
     * Look up the numerical Twitter user IDs for the passed in screen names.
     *
     * @param usernamesOrIds
     *            screen names or user IDs
     * @return the user IDs in arbitrary order
     */
    Collection<Long> lookupUsers(Collection<String> usernamesOrIds) {
        List<Long> ids = Lists.newArrayListWithExpectedSize(usernamesOrIds.size());
        for (List<String> userBatch : Iterables.partition(usernamesOrIds, 100)) {
            String[] names = userBatch.toArray(new String[userBatch.size()]);
            ResponseList<User> responseList;
            try {
                responseList = twitter.lookupUsers(names);
            } catch (TwitterException e) {
                throw Throwables.propagate(e);
            }
            for (User user : responseList) {
                ids.add(user.getId());
            }
        }
        return ids;
    }

    @Override
    protected void stopFollowing(ApiRequest request) {
        TwitterStream removedStream = streamsByRequest.remove(request);
        Preconditions.checkState(removedStream != null, "Not following URI " + request);
        credentialsProvider.release(credentialsByStream.remove(removedStream));
        removedStream.cleanUp();
    }

    @Override
    public void close() throws IOException {
        super.close();
        for (OAuthCredentials credential : credentialsByStream.values()) {
            credentialsProvider.release(credential);
        }
        TwitterStreamFactory.getSingleton().shutdown();
    }

}
