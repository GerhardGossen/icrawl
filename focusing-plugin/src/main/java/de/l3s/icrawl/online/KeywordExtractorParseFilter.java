package de.l3s.icrawl.online;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.avro.util.Utf8;
import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.metadata.Nutch;
import org.apache.nutch.net.URLNormalizers;
import org.apache.nutch.net.protocols.HttpDateFormat;
import org.apache.nutch.parse.HTMLMetaTags;
import org.apache.nutch.parse.Outlink;
import org.apache.nutch.parse.Parse;
import org.apache.nutch.parse.ParseFilter;
import org.apache.nutch.storage.WebPage;
import org.apache.nutch.storage.WebPage.Field;
import org.joda.time.DateTime;
import org.openimaj.text.nlp.language.LanguageDetector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

import de.l3s.icrawl.FieldNames;
import de.l3s.icrawl.domain.event.URLCrawlEvent;
import de.l3s.icrawl.domain.specification.CrawlSpecification;
import de.l3s.icrawl.domain.specification.CrawlSpecificationUtils;
import de.l3s.icrawl.domain.support.CountedSet;
import de.l3s.icrawl.online.WebPageDateExtractor.WebPageDate;
import de.l3s.icrawl.online.time.TimeDifferenceRelevance;
import de.l3s.icrawl.online.time.WeibullTimeDifferenceRelevance;
import de.l3s.icrawl.util.TextExtractor;
import de.l3s.icrawl.util.WebPageUtils;

import static de.l3s.icrawl.domain.support.ByteBufferUtils.asByteBuffer;
import static de.l3s.icrawl.domain.support.ByteBufferUtils.stringify;
import static java.util.stream.Collectors.toSet;

public class KeywordExtractorParseFilter implements ParseFilter {

    private static final Logger logger = LoggerFactory.getLogger(KeywordExtractorParseFilter.class);
    private static final Collection<Field> FIELDS = ImmutableSet.of(Field.METADATA);
    public static final double DOC_SCORE_WEIGHT = 0.2;
    public static final double ANCHOR_SCORE_WEIGHT = 0.4;
    public static final double PARAGRAPH_SCORE_WEIGHT = 0.4;
    private LanguageDetector languageDetector;
    //    private EventSender eventSender;
    private Configuration conf;
    private CrawlSpecification crawlSpec;
    private Set<String> keywords;
    private ContentAnalyser contentAnalyzer;
    private DocumentVectorSimilarity similarity;
    private TimeDifferenceRelevance timeRelevance;
    private URLNormalizers urlNormalizer;

    @Override
    public Collection<Field> getFields() {
        return FIELDS;
    }

    @Override
    public void setConf(Configuration conf) {
        this.conf = conf;
        setCrawlSpecification(CrawlSpecificationUtils.get(conf));
        boolean extractEntities = conf.getBoolean(CrawlSpecificationUtils.EXTRACT_ENTITIES_KEY, false);
        try {
            languageDetector = new LanguageDetector();
            contentAnalyzer = new ContentAnalyser(languageDetector,
                extractEntities ? LabelerFactory.defaultFactory() : null);
            //            this.eventSender = new EventSender(conf);
            similarity = DocumentVectorUtils.readFromConfiguration(conf);
            timeRelevance = new WeibullTimeDifferenceRelevance();
        } catch (IOException /*| JMSException*/e) {
            logger.info("Exception while configuring KeywordExtractorParseFilter", e);
            throw new IllegalArgumentException("Exception while configuring KeywordExtractorParseFilter", e);
        }
        this.urlNormalizer = new URLNormalizers(conf, URLNormalizers.SCOPE_DEFAULT);
    }

    public void setCrawlSpecification(CrawlSpecification crawlSpecification) {
        this.crawlSpec = crawlSpecification;
        this.keywords = Sets.newHashSetWithExpectedSize(crawlSpec.getKeywords().size());
        for (String keyword : crawlSpec.getKeywords()) {
            keywords.add(keyword.toLowerCase().trim());
        }
        logger.debug("Using crawl specification: {}", crawlSpec);
    }

    @Override
    public Configuration getConf() {
        return conf;
    }

    @Override
    public Parse filter(String url, WebPage page, Parse parse, HTMLMetaTags metaTags,
            DocumentFragment doc) {
        URL baseUrl = metaTags.getBaseHref();
        if (baseUrl == null) {
            try {
                baseUrl = new URL(url);
            } catch (MalformedURLException e) {
                logger.debug("No valid URL for Web page, got '{}'", url);
                baseUrl = null;
            }
        }

        ContentAnalyser.Counts counts = contentAnalyzer.analyze(doc, keywords);
        logger.debug("extracted from URL {} keywords {}", url, counts.getKeywords());
        page.getMetadata().put(FieldNames.KEYWORDS, stringify(counts.getKeywords()));
        page.getMetadata().put(FieldNames.DOC_LENGTH, asByteBuffer(counts.getDocumentLength()));
        page.getMetadata().put(FieldNames.ENTITIES, stringify(counts.getEntities()));
        page.getMetadata().put(FieldNames.DETECTED_KEYWORDS, stringify(counts.getDetectedKeywords()));
        page.getMetadata().put(FieldNames.LANGUAGE, asByteBuffer(counts.getLanguage().getLanguage()));
        Long modifiedTime = null;
        CharSequence lastModified = page.getHeaders().get(new Utf8("Last-Modified"));
        if (lastModified != null) {
            try {
                modifiedTime = HttpDateFormat.toLong(lastModified.toString());
            } catch (ParseException e) {
                logger.debug("Could not parse as modified date header: '{}'", lastModified, e);
            }
        }
        WebPageDate modifiedDate = WebPageDateExtractor.getModifiedDate(url, doc.getOwnerDocument(), modifiedTime, null);
        final double timeRelevanceScore;
        if (modifiedDate != null && modifiedDate.date != null) {
            timeRelevanceScore = timeRelevance.computeRelevance(modifiedDate.date, DateTime.now());
            page.getMetadata().put(FieldNames.MODIFIED_DATE, asByteBuffer(modifiedDate.date.getMillis()));
        } else {
            timeRelevanceScore = timeRelevance.defaultValue();
        }
        Map<String, Double> dvOutlinkScores = computeOutlinkScores(doc, parse.getOutlinks(), baseUrl);
        page.getMetadata().put(FieldNames.TIME_RELEVANCE, asByteBuffer(timeRelevanceScore));
        page.getMetadata().put(FieldNames.OUTLINK_SCORES, asByteBuffer(dvOutlinkScores));
        return parse;
    }

    private Map<String, Double> computeOutlinkScores(DocumentFragment doc, Outlink[] outlinks,
            URL baseUrl) {
        String text = TextExtractor.extractText(doc);
        Locale docLanguage = languageDetector.classify(text).getLocale();
        if (similarity == null) {
            return allScores(outlinks, 1.0 / outlinks.length);
        }
        double documentSimilarity = similarity.getSimilarity(text, docLanguage);
        Preconditions.checkArgument(!Double.isNaN(documentSimilarity), "NaN in documentSimilarity");

        Collection<String> links = getLinks(outlinks);
        Multimap<String, Element> urlRefs = findUrlRefs(doc, links, baseUrl);
        Map<String, Double> scores = Maps.newHashMapWithExpectedSize(urlRefs.keySet().size());
        for (Entry<String, Collection<Element>> entry : urlRefs.asMap().entrySet()) {
            String url = entry.getKey();
            StringBuilder anchorTexts = new StringBuilder();
            StringBuilder paragraphTexts = new StringBuilder();
            for (Element linkElement : entry.getValue()) {
                anchorTexts.append(TextExtractor.extractText(linkElement)).append("\n\n");
                paragraphTexts.append(WebPageUtils.findParagraphParent(linkElement)).append("\n\n");
            }

            double anchorSimilarity = similarity.getSimilarity(anchorTexts.toString(), docLanguage);
            double paragraphSimilarity = similarity.getSimilarity(paragraphTexts.toString(),
                docLanguage);
            Preconditions.checkArgument(!Double.isNaN(anchorSimilarity), "NaN in anchorSimilarity");
            Preconditions.checkArgument(!Double.isNaN(paragraphSimilarity),
                "NaN in paragraphSimilarity");
            double totalScore = (DOC_SCORE_WEIGHT * documentSimilarity + ANCHOR_SCORE_WEIGHT
                    * anchorSimilarity + PARAGRAPH_SCORE_WEIGHT * paragraphSimilarity)
                    / (DOC_SCORE_WEIGHT + ANCHOR_SCORE_WEIGHT + PARAGRAPH_SCORE_WEIGHT);
            Preconditions.checkArgument(!Double.isNaN(totalScore), "NaN in totalScore");
            scores.put(url, totalScore);
        }
        return scores;
    }

    private Map<String, Double> allScores(Outlink[] outlinks, double score) {
        Map<String, Double> scores = Maps.newHashMapWithExpectedSize(outlinks.length);
        for (Outlink outlink : outlinks) {
            scores.put(outlink.getToUrl(), score);
        }
        return scores;
    }

    private Collection<String> getLinks(Outlink[] outlinks) {
        return Arrays.stream(outlinks).map(o -> cleanURL(o.getToUrl())).collect(toSet());
    }

    URLCrawlEvent createEvent(String url, WebPage page, ContentAnalyser.Counts counts) {
        long crawlId = conf.getLong(Nutch.CRAWL_ID_KEY, -1);
        int docLength = page.getContent().limit();
        return new URLCrawlEvent(crawlId, url, docLength, new CountedSet<>(counts.getKeywords()),
            new CountedSet<>(counts.getEntities()), new DateTime(page.getFetchTime()));
    }

    /**
     * Find the link elements referring to the given URLs
     *
     * @param doc
     *            the DOM to search
     * @param urls
     *            the URLs to find
     * @param baseUrl
     *            the base URL of the document, used to dereference relative
     *            links
     * @return a Map that contains for each URL an ordered collection of
     *         referring elements
     */
    private Multimap<String, Element> findUrlRefs(DocumentFragment doc, Collection<String> urls, URL baseUrl) {
        Multimap<String, Element> found = ArrayListMultimap.create(urls.size(), 1);
        NodeList childNodes = doc.getChildNodes();
        for (int j = 0; j < childNodes.getLength(); j++) {
            Node node = childNodes.item(j);
            if (!(node instanceof Element)) {
                continue;
            }
            NodeList linkElements = ((Element) node).getElementsByTagName("a");
            for (int i = 0; i < linkElements.getLength(); i++) {
                Element link = (Element) linkElements.item(i);
                handleLink(link, baseUrl, urls, found);
            }
        }

        return found;
    }

    private void handleLink(Element link, URL baseUrl, Collection<String> urls, Multimap<String, Element> found) {
        if (link.hasAttribute("href")) {
            String href = link.getAttribute("href");
            String absoluteURL = toAbsoluteURL(baseUrl, href);
            if (urls.contains(absoluteURL)) {
                found.put(absoluteURL, link);
                if (logger.isTraceEnabled()) {
                    logger.trace("Found ref to URL '{}' with text '{}'", absoluteURL,
                        link.getTextContent());
                }
            }
        }
    }

    /**
     * Convert a possibly relative URL to an absolute URL
     *
     * @param baseURL
     * @param localURL
     * @return absolute URL
     */
    private String toAbsoluteURL(final URL baseURL, final String localURL) {
        String absoluteUrl = localURL;
        try {
            if (baseURL != null) {
                absoluteUrl = new URL(baseURL, localURL).toString();
            }
        } catch (MalformedURLException e) {
            // ignore, localURL should be absolute
        }
        return cleanURL(absoluteUrl);
    }

    /**
     * Remove some crud from an URL
     *
     * @param originalURL
     *            the url
     *
     * @return a new String with the cleaned URL
     */
    private String cleanURL(final String originalURL) {
        try {
            return urlNormalizer.normalize(originalURL, URLNormalizers.SCOPE_DEFAULT);
        } catch (MalformedURLException e) {
            logger.trace("Could not clean URL '{}', returning unchanged", originalURL, e);
            return originalURL;
        }
    }

}
