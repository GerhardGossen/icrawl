package de.l3s.icrawl.online;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.indexer.NutchDocument;
import org.apache.nutch.scoring.ScoreDatum;
import org.apache.nutch.scoring.ScoringFilter;
import org.apache.nutch.scoring.ScoringFilterException;
import org.apache.nutch.storage.WebPage;
import org.apache.nutch.storage.WebPage.Field;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multiset.Entry;

import de.l3s.icrawl.FieldNames;
import de.l3s.icrawl.domain.specification.CrawlSpecification;
import de.l3s.icrawl.domain.specification.CrawlSpecificationUtils;
import de.l3s.icrawl.domain.specification.WeightVector;
import de.l3s.icrawl.domain.specification.WeightingMethod;

import static de.l3s.icrawl.domain.support.ByteBufferUtils.*;

/**
 * Based on <tt>org.apache.nutch.scoring.opic.OPICScoringFilter</tt>.
 */
public class FocusScoringFilter implements ScoringFilter {
    private static final Logger logger = LoggerFactory.getLogger(FocusScoringFilter.class);
    private Configuration conf;
    // include BATCH_ID to force Gora to not clear its value when new inlinks are found
    private final Collection<Field> fields = EnumSet.of(Field.METADATA, Field.SCORE, Field.BATCH_ID);
    private CrawlSpecification crawlSpec;
    private WeightVector weights;

    private float scorePower;
    private float internalScoreFactor;
    private float externalScoreFactor;
    @SuppressWarnings("unused")
    private boolean countFiltered;
    private WeightingMethod weightingMethod;

    @Override
    public void setConf(Configuration conf) {
        this.conf = conf;
        this.crawlSpec = CrawlSpecificationUtils.get(conf);
        logger.debug("Using crawl specification: {}", crawlSpec);
        this.weights = CrawlSpecificationUtils.parseWeightVector(conf.get(FieldNames.WEIGHTS_KEY));
        logger.debug("Using weight vector: {}", this.weights);
        scorePower = conf.getFloat("indexer.score.power", 0.5f);
        internalScoreFactor = conf.getFloat("db.score.link.internal", 1.0f);
        externalScoreFactor = conf.getFloat("db.score.link.external", 1.0f);
        countFiltered = conf.getBoolean("db.score.count.filtered", false);
        weightingMethod = conf.getEnum(FieldNames.WEIGHTING_METHOD_KEY,
            WeightingMethod.DOCUMENT_VECTOR);
    }

    @Override
    public Configuration getConf() {
        return conf;
    }

    @Override
    public Collection<Field> getFields() {
        return fields;
    }


    @Override
    public void injectedScore(String url, WebPage row) throws ScoringFilterException {
        logger.debug("Setting score and cash to 1.0 for injected URL {}", url);
        row.setScore(1.0f);
        row.getMetadata().put(FieldNames.CASH, asByteBuffer(1.0f));
    }

    /**
     * Set to 0.0f (unknown value) - inlink contributions will bring it to a
     * correct level. Newly discovered pages have at least one inlink.
     */
    @Override
    public void initialScore(String url, WebPage row) throws ScoringFilterException {
        logger.trace("Setting score and cash to 0.0 as initial score for URL {}", url);
        row.setScore(0.0f);
        row.getMetadata().put(FieldNames.CASH, asByteBuffer(0.0f));
    }

    @Override
    public float generatorSortValue(String url, WebPage row, float initSort)
            throws ScoringFilterException {
        float ret = row.getScore() * initSort;
        if (logger.isDebugEnabled() && ret > 0.0) {
            logger.debug("Sort value for URL {}: {} * {} = {}", url, row.getScore(), initSort, ret);
        }
        return ret;
    }

    /** Increase the score by a sum of inlinked scores. */
    @Override
    public void updateScore(String url, WebPage row, List<ScoreDatum> inlinkedScoreData)
            throws ScoringFilterException {
        try {
        float adjust = 0.0f;
        for (ScoreDatum scoreDatum : inlinkedScoreData) {
            adjust += scoreDatum.getScore();
        }
        float oldScore = row.getScore();
        row.setScore(oldScore + adjust);
        ByteBuffer cashRaw = row.getMetadata().get(FieldNames.CASH);
        float cash = asFloat(cashRaw);
        row.getMetadata().put(FieldNames.CASH, asByteBuffer(cash + adjust));
        logger.trace("Updating URL {}, adding {} to score {} and cash {}", url, adjust, oldScore, cash);
        } catch (RuntimeException e) {
            logger.debug("Exception while updating score for URL {}", url, e);
            throw new ScoringFilterException(e);
        }
    }

    /** Get cash on hand, divide it by the number of outlinks and apply. */
    @Override
    public void distributeScoreToOutlinks(String fromUrl, WebPage row,
            Collection<ScoreDatum> scoreData, int allCount) throws ScoringFilterException {
        try {
            logger.debug("Using weighting method {}", weightingMethod);
            switch (weightingMethod) {
            case KEYWORDS:
                distributeWeightUsingKeywords(fromUrl, row, scoreData);
                break;
            case DOCUMENT_VECTOR:
                distributeWeightUsingScores(fromUrl, row, scoreData, true, false);
                break;
            case DV_FRESHNESS:
                distributeWeightUsingScores(fromUrl, row, scoreData, true, true);
                break;
            case FRESHNESS:
                distributeWeightUsingScores(fromUrl, row, scoreData, false, true);
                break;
            case GRAPH_ONLY:
                distributeWeightsUsingCash(fromUrl, row, scoreData);
                break;
            default:
                throw new IllegalArgumentException("Unknown weighting method: " + weightingMethod);
            }

            // reset cash to zero
            row.getMetadata().put(FieldNames.CASH, asByteBuffer(0.0f));
        } catch (RuntimeException e) {
            logger.debug("Exception while distributing scores for URL {}", fromUrl, e);
            throw new ScoringFilterException(e);
        }
    }

    private void distributeWeightsUsingCash(String fromUrl, WebPage row,
            Collection<ScoreDatum> scoreData) {
        ByteBuffer cashRaw = row.getMetadata().get(FieldNames.CASH);
        float cash = asFloat(cashRaw);
        if (cash == 0) {
            return;
        }

        String fromHost;
        try {
            fromHost = new URL(fromUrl).getHost();
        } catch (MalformedURLException e) {
            logger.debug("Cannot distribute score for record with invalid URL {}:", fromUrl, e);
            return;
        }
        Map<ScoreDatum, Boolean> scoreIsInternal = Maps.newHashMapWithExpectedSize(scoreData.size());
        double factorSum = 0.0;
        for (ScoreDatum scoreDatum : scoreData) {
            try {
                String toHost = new URL(scoreDatum.getUrl()).getHost();
                boolean internalLink = toHost.equalsIgnoreCase(fromHost);
                scoreIsInternal.put(scoreDatum, internalLink);
                factorSum += internalLink ? internalScoreFactor : externalScoreFactor;
            } catch (MalformedURLException e) {
                logger.debug("Failed with the following MalformedURLException: ", e);
                scoreIsInternal.put(scoreDatum, false);
            }
        }

        float scoreUnit = (float) (cash / factorSum);
        // internal and external score factor
        float internalScore = scoreUnit * internalScoreFactor;
        float externalScore = scoreUnit * externalScoreFactor;
        for (Map.Entry<ScoreDatum, Boolean> scoreDatum : scoreIsInternal.entrySet()) {
            if (scoreDatum.getValue()) {
                scoreDatum.getKey().setScore(internalScore);
            } else {
                scoreDatum.getKey().setScore(externalScore);
            }
        }
    }

    protected void distributeWeightUsingScores(String fromUrl, WebPage row,
            Collection<ScoreDatum> scoreData, boolean useScores, boolean useTimeRelevance) {
        boolean hasScores = row.getMetadata().containsKey(FieldNames.OUTLINK_SCORES);
        boolean hasTime = row.getMetadata().containsKey(FieldNames.TIME_RELEVANCE);
        if (!hasScores || !hasTime) {
            logger.info("Missing metadata for scores={} and/or timeRelevance={}, has keys: {}",
                hasScores, hasTime, row.getMetadata().keySet());
            return;
        }
        ByteBuffer scoresRaw = row.getMetadata().get(FieldNames.OUTLINK_SCORES);
        Map<String, Double> scores = asMap(scoresRaw, String.class, Double.class);
        double timeRelevanceScore = asDouble(row.getMetadata().get(FieldNames.TIME_RELEVANCE));
        logger.trace("Parsed scores for URL {}: {}, {}", fromUrl, scores, timeRelevanceScore);

        Set<String> unseenUrls = new HashSet<>(scores.keySet());

        for (ScoreDatum scoreDatum : scoreData) {
            String url = scoreDatum.getUrl();
            Double score = useScores ? scores.get(url) : 1.0;
            if (score == null) {
                continue;
            }
            if (useTimeRelevance) {
                score *= timeRelevanceScore;
            }
            unseenUrls.remove(url);

            scoreDatum.setScore(score.floatValue());
        }

        if (!unseenUrls.isEmpty()) {
            logger.info("Could not distribute scores for {} outlinks of {}", unseenUrls.size(), fromUrl);
            logger.trace("Expected outlinks {} from page '{}' were not found", unseenUrls, fromUrl);
        }
    }

    protected void distributeWeightUsingKeywords(String fromUrl, WebPage row,
            Collection<ScoreDatum> scoreData) {
        ByteBuffer keywordsRaw = row.getMetadata().get(FieldNames.KEYWORDS);
        Multiset<String> keywords = asMultiset(keywordsRaw, String.class);
            int docLength = asInt(row.getMetadata().get(FieldNames.DOC_LENGTH));
        Map<String, Double> features = Maps.newHashMapWithExpectedSize(keywords.elementSet().size());
        createKeywordFeatures(features, keywords, docLength);
        double weight = weights.weight(features);
        logger.trace("Got weight {} for page {}", weight, fromUrl);
        for (ScoreDatum scoreDatum : scoreData) {
            scoreDatum.setScore((float) weight);
        }
        row.getMetadata().put(FieldNames.WEIGHT, asByteBuffer(weight));
    }


    private void createKeywordFeatures(Map<String, Double> features, Multiset<String> keywords, int docLength) {
        double actualLength = docLength > 0 ? docLength : keywords.size();
        for (Entry<String> entry : keywords.entrySet()) {
            features.put(entry.getElement(), entry.getCount() / actualLength);
        }
    }

    /** Dampen the boost value by scorePower. */
    @Override
    public float indexerScore(String url, NutchDocument doc, WebPage row, float initScore) {
        return (float) Math.pow(row.getScore(), scorePower) * initScore;
    }

}
