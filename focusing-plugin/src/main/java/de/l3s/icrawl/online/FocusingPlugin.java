package de.l3s.icrawl.online;

import java.io.IOException;

import javax.jms.JMSException;

import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.plugin.Plugin;
import org.apache.nutch.plugin.PluginDescriptor;
import org.apache.nutch.plugin.PluginRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FocusingPlugin extends Plugin {
    private static final Logger logger = LoggerFactory.getLogger(FocusingPlugin.class);
    private EventSender eventSender;

    public FocusingPlugin(PluginDescriptor pDescriptor, Configuration conf) {
        super(pDescriptor, conf);
    }

    @Override
    public void startUp() throws PluginRuntimeException {
        try {
            this.eventSender = new EventSender(conf);
            logger.info("Initialised JMS connection");
        } catch (JMSException e) {
            throw new PluginRuntimeException(e);
        }
    }

    @Override
    public void shutDown() throws PluginRuntimeException {
        try {
            eventSender.close();
            logger.info("Closed JMS connection");
        } catch (IOException e) {
            throw new PluginRuntimeException(e);

        }
    }

    public EventSender getEventSender() {
        return eventSender;
    }
}
