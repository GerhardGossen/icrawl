package de.l3s.icrawl.online;

import java.io.Closeable;
import java.io.IOException;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.hadoop.conf.Configuration;
import org.fusesource.stomp.jms.StompJmsConnectionFactory;
import org.fusesource.stomp.jms.StompJmsDestination;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.google.common.base.Throwables;

import de.l3s.icrawl.domain.event.URLCrawlEvent;

public class EventSender implements Closeable {
    public static final String TOPIC_ID = "/topic/crawls";
    private Connection connection;
    private Session session;
    private MessageProducer producer;
    private final ObjectWriter writer = jsonMapper().writerWithType(URLCrawlEvent.class);
    private final int port;
    private final Object lock = new Object();

    public EventSender(Configuration conf) throws JMSException {
        port = conf.getInt("icrawl.messages.tcpPort", 61613);
    }

    private void init() throws JMSException {
        synchronized (lock) {
            if (producer != null) {
                return;
            }
            StompJmsConnectionFactory factory = new StompJmsConnectionFactory();
            factory.setBrokerURI("tcp://localhost:" + port);
            connection = factory.createConnection();
            connection.start();

            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = new StompJmsDestination(TOPIC_ID);
            producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
        }
    }


    public void send(URLCrawlEvent evt) {
        try {
            init();
            TextMessage msg = session.createTextMessage(writer.writeValueAsString(evt));
            producer.send(msg);
        } catch (JMSException | JsonProcessingException e) {
            throw Throwables.propagate(e);
        }
    }


    @Override
    public void close() throws IOException {
        try {
            if (session != null) {
                session.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (JMSException e) {
            throw new IOException("Could not close JMS connection", e);
        }
    }

    static ObjectMapper jsonMapper() {
        return new ObjectMapper().registerModules(new GuavaModule(), new JodaModule());
    }

}
