package de.l3s.icrawl.online;

import java.util.Collection;

import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.fetcher.FetcherJob;
import org.apache.nutch.protocol.Protocol;
import org.apache.nutch.protocol.ProtocolOutput;
import org.apache.nutch.storage.WebPage;
import org.apache.nutch.storage.WebPage.Field;

import com.google.common.collect.ImmutableSet;

import crawlercommons.robots.BaseRobotRules;

/**
 * Fake protocol implementation to preserve metadata field accross crawls.
 *
 * {@link FetcherJob} by default retrieve the metadata field from the data
 * store, therefore any values stored are lost when a new crawl result is
 * written. This extension works around this by registering a fake Protocol that
 * registers the field for retrieving from the data store.
 */
public class FieldRegistrationFilter implements Protocol {

    private static final Collection<Field> FIELDS = ImmutableSet.of(Field.METADATA);
    private Configuration conf;

    @Override
    public Collection<Field> getFields() {
        return FIELDS;
    }

    @Override
    public void setConf(Configuration conf) {
        this.conf = conf;
    }

    @Override
    public Configuration getConf() {
        return conf;
    }

    @Override
    public ProtocolOutput getProtocolOutput(String url, WebPage page) {
        throw new UnsupportedOperationException();
    }

    @Override
    public BaseRobotRules getRobotRules(String url, WebPage page) {
        throw new UnsupportedOperationException();
    }

}
