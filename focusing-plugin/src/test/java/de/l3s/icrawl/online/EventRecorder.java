package de.l3s.icrawl.online;

import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.fusesource.stomp.jms.StompJmsConnectionFactory;
import org.fusesource.stomp.jms.StompJmsDestination;

import com.fasterxml.jackson.databind.ObjectReader;

import de.l3s.icrawl.domain.event.URLCrawlEvent;

public class EventRecorder implements Closeable {
    private final Connection connection;
    private final Session session;
    private final ObjectReader reader = EventSender.jsonMapper().reader(URLCrawlEvent.class);

    public EventRecorder(int port) throws JMSException {
        StompJmsConnectionFactory factory = new StompJmsConnectionFactory();
        factory.setBrokerURI("tcp://localhost:" + port);
        connection = factory.createConnection();
        connection.start();

        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    }

    public void record(File outputFile) throws IOException, JMSException {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(outputFile))) {
            Destination destination = new StompJmsDestination(EventSender.TOPIC_ID);
            MessageConsumer consumer = session.createConsumer(destination);
            Message message;
            while ((message = consumer.receive()) != null) {
                if (message instanceof TextMessage) {
                    String text = ((TextMessage) message).getText();
                    URLCrawlEvent evt = reader.<URLCrawlEvent> readValue(text);
                    out.writeObject(evt);
                }
            }
        }
    }

    @Override
    public void close() throws IOException {
        try {
            session.close();
            connection.close();
        } catch (JMSException e) {
            throw new IOException(e);
        }
    }

    public static void main(String[] args) throws JMSException, IOException {
        int port = 61613;
        File outputFile;
        if (args.length == 0) {
            System.err.println("Usage: " + EventRecorder.class.getName() + " outputFile [port]");
            System.exit(1);
            return;
        } else {
            outputFile = new File(args[0]);
            if (args.length >= 2) {
                port = Integer.parseInt(args[1]);
            }
        }
        try (EventRecorder recorder = new EventRecorder(port)) {
            recorder.record(outputFile);
        }
    }

}
