package de.l3s.icrawl.online;

import org.junit.Test;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.SetMultimap;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class SerializationTest {
    private final static TypeReference<SetMultimap<String, String>> MAP_TYPE = new TypeReference<SetMultimap<String, String>>() {};
    private final static TypeReference<Multiset<String>> SET_TYPE = new TypeReference<Multiset<String>>() {};

    @Test
    public void testMultimap() throws Exception {
        SetMultimap<String, String> map = HashMultimap.create();
        map.put("foo", "bar");
        map.put("foo", "baz");
        ObjectMapper jsonMapper = new ObjectMapper().registerModule(new GuavaModule());
        String json = jsonMapper.writeValueAsString(map);
        SetMultimap<String, String> extractedMap = jsonMapper.readValue(json, MAP_TYPE);
        assertThat(extractedMap, equalTo(map));
    }

    @Test
    public void testMultiset() throws Exception {
        Multiset<String> set = HashMultiset.create();
        set.add("foo", 5);
        ObjectMapper jsonMapper = new ObjectMapper().registerModule(new GuavaModule());
        String json = jsonMapper.writeValueAsString(set);
        Multiset<String> extractedSet = jsonMapper.readValue(json, SET_TYPE);
        assertThat(extractedSet, equalTo(set));

    }
}
