package de.l3s.icrawl.online;

import java.io.File;
import java.util.Collection;
import java.util.Collections;

import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.scoring.ScoreDatum;
import org.apache.nutch.scoring.ScoringFilter;
import org.apache.nutch.scoring.ScoringFilterException;
import org.apache.nutch.storage.WebPage;
import org.junit.Test;

import de.l3s.icrawl.FieldNames;
import de.l3s.icrawl.domain.specification.CrawlSpecification;
import de.l3s.icrawl.domain.specification.CrawlSpecificationUtils;
import de.l3s.icrawl.domain.specification.WeightingMethod;
import de.l3s.icrawl.domain.support.ByteBufferUtils;
import de.l3s.icrawl.util.HadoopUtils;

public class FocusScoringFilterTest {

    @Test
    public void testGraphOnlyMethod() throws ScoringFilterException {
        Configuration conf = HadoopUtils.createConf(new File("."));
        conf.setEnum(FieldNames.WEIGHTING_METHOD_KEY, WeightingMethod.GRAPH_ONLY);
        CrawlSpecification spec = new CrawlSpecification();
        CrawlSpecificationUtils.set(conf, spec);
        ScoringFilter filter = new FocusScoringFilter();
        filter.setConf(conf);

        Collection<ScoreDatum> scoreData = Collections.singletonList(new ScoreDatum(1.0f,
            "http://foo.org/", "", 1));
        WebPage page = WebPage.newBuilder().build();
        page.getMetadata().put(FieldNames.CASH, ByteBufferUtils.asByteBuffer(1.0));
        filter.distributeScoreToOutlinks("", page, scoreData, scoreData.size());
    }

}
