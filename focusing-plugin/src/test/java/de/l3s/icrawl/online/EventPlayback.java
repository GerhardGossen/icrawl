package de.l3s.icrawl.online;

import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.fusesource.stomp.jms.StompJmsConnectionFactory;
import org.fusesource.stomp.jms.StompJmsDestination;

import com.fasterxml.jackson.databind.ObjectWriter;

import de.l3s.icrawl.domain.event.URLCrawlEvent;

public class EventPlayback implements Closeable {
    private final Connection connection;
    private final Session session;
    private final ObjectWriter writer = EventSender.jsonMapper().writerWithType(URLCrawlEvent.class);

    public EventPlayback(int port) throws JMSException {
        StompJmsConnectionFactory factory = new StompJmsConnectionFactory();
        factory.setBrokerURI("tcp://localhost:" + port);
        connection = factory.createConnection();
        connection.start();

        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    }

    public void playback(File inputFile) throws IOException, JMSException, ClassNotFoundException {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(inputFile))) {
            Destination destination = new StompJmsDestination(EventSender.TOPIC_ID);
            MessageProducer producer = session.createProducer(destination);
            Object event;
            System.out.println("Starting to read events");
            // TODO space out events according to their timestamps
            try {
                while ((event = in.readObject()) != null) {
                    System.out.println("Read event: " + event);
                    producer.send(session.createTextMessage(writer.writeValueAsString(event)));
                }
            } catch (EOFException e) {
                System.out.println("Finished reading events");
            }
        }
    }

    @Override
    public void close() throws IOException {
        try {
            session.close();
            connection.close();
        } catch (JMSException e) {
            throw new IOException(e);
        }
    }

    public static void main(String[] args) throws JMSException, IOException, ClassNotFoundException {
        int port = 61613;
        File inputFile;
        if (args.length == 0) {
            System.err.println("Usage: " + EventPlayback.class.getName() + " inputFile [port]");
            System.exit(1);
            return;
        } else {
            inputFile = new File(args[0]);
            if (args.length >= 2) {
                port = Integer.parseInt(args[1]);
            }
        }
        try (EventPlayback playback = new EventPlayback(port)) {
            playback.playback(inputFile);
        }
    }

}
