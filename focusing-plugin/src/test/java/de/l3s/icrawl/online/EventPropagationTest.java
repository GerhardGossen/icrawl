package de.l3s.icrawl.online;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMultiset;

import de.l3s.icrawl.domain.event.URLCrawlEvent;
import de.l3s.icrawl.domain.specification.NamedEntity;
import de.l3s.icrawl.domain.support.CountedSet;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class EventPropagationTest {
    private static final Logger logger = LoggerFactory.getLogger(EventPropagationTest.class);
    @Test
    public void testEventSerialisation() throws Exception {
        DateTime downloaded = new DateTime(2014, 6, 26, 12, 0, 0, DateTimeZone.UTC);
        CountedSet<String> keywords = new CountedSet<>(ImmutableMultiset.of("l3s"));
        CountedSet<NamedEntity> entities = new CountedSet<>();
        URLCrawlEvent event = new URLCrawlEvent(1, "http://www.l3s.de", 500, keywords, entities, downloaded);
        ObjectMapper mapper = EventSender.jsonMapper();
        String asString = mapper.writeValueAsString(event);
        logger.debug("JSON: {}", asString);
        URLCrawlEvent actual = mapper.readValue(asString, URLCrawlEvent.class);
        logger.debug("De-serialized: {}", actual);
        assertThat(actual, is(notNullValue()));
        assertThat(actual.getCrawlId(), is(1L));
        assertThat(actual.getUrl(), is("http://www.l3s.de"));
        assertThat(actual.getKeywords(), contains("l3s"));
        assertThat(actual.getEntities(), emptyIterable());
        assertThat(actual.getDownloaded(), is(downloaded));
    }
}
