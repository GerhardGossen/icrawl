package de.l3s.icrawl.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.google.common.io.Resources;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TextExtractorTest {

    @Test
    public void testExtractDocumentFragment() throws Exception {
        DocumentFragment document = loadDocumentAsFragment("text.html", "http://example.colm/");
        assertThat(TextExtractor.extractText(document), equalTo("Foo\n\nBar baz"));
    }

    @Test
    public void testIsBlockElement() {
        Element elem = elementWithTagName("P");
        assertTrue(TextExtractor.isBlockElement(elem));
    }

    @Test
    public void testIsNotBlockElement() throws Exception {
        assertFalse(TextExtractor.isBlockElement(elementWithTagName("a")));
    }

    @Test
    public void testIsNotBlockElementMadeUp() throws Exception {
        assertFalse(TextExtractor.isBlockElement(elementWithTagName("foo")));
    }

    Element elementWithTagName(String name) {
        Element elem = mock(Element.class);
        when(elem.getTagName()).thenReturn(name);
        return elem;
    }

    @Test
    public void testIsIgnoredElement() {
        assertTrue(TextExtractor.isIgnoredElement(elementWithTagName("STYLE")));
        assertTrue(TextExtractor.isIgnoredElement(elementWithTagName("script")));
    }

    @Test
    public void testExtractNode() throws Exception {
        Node document = loadDocumentAsFragment("text.html", "http://example.colm/").getFirstChild();
        assertThat(TextExtractor.extractText(document), equalTo("Foo\n\nBar baz"));
    }

    @Test
    public void testExtractDocument() throws Exception {
        Document document = loadDocument("text.html", "http://example.colm/");
        assertThat(TextExtractor.extractText(document), equalTo("Foo\n\nBar baz"));

    }

    private DocumentFragment loadDocumentAsFragment(String resourceName, String origUrl) throws SAXException,
            IOException {
        Document doc = loadDocument(resourceName, origUrl);
        DocumentFragment docFragment = doc.createDocumentFragment();
        NodeList body = doc.getElementsByTagName("BODY");
        for (int i = 0; i < body.getLength(); i++) {
            docFragment.appendChild(body.item(i));
        }
        return docFragment;
    }

    static Document loadDocument(String resourceName, String origUrl) throws SAXException,
            IOException {
        URL url = Resources.getResource(TextExtractorTest.class, resourceName);
        try (InputStream is = url.openStream()) {
            return WebPageUtils.parseHtml(is, origUrl);
        }
    }
}
