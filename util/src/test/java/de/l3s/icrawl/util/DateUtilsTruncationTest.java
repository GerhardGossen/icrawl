package de.l3s.icrawl.util;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.l3s.icrawl.util.DateUtils.DateTimeTruncation;
import de.l3s.icrawl.util.DateUtils.DurationTruncation;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DateUtilsTruncationTest {
    private static final Logger logger = LoggerFactory.getLogger(DateUtilsTruncationTest.class);
    @Test
    public void testDatetimeTruncationDay() {
        DateTime d1 = new DateTime(2014, 10, 21, 0, 0, 0);
        DateTime d2 = d1.plusHours(1);

        DateTime d1Trunc = DateUtils.truncateDate(d1, DateTimeTruncation.DAY);
        DateTime d2Trunc = DateUtils.truncateDate(d2, DateTimeTruncation.DAY);

        assertThat(d1Trunc, is(equalTo(d2Trunc)));
    }

    @Test
    public void testDatetimeTruncationHour() {
        DateTime d1 = new DateTime(2014, 10, 21, 0, 0, 0);
        DateTime d2 = d1.plusMinutes(1);

        DateTime d1Trunc = DateUtils.truncateDate(d1, DateTimeTruncation.HOUR);
        DateTime d2Trunc = DateUtils.truncateDate(d2, DateTimeTruncation.HOUR);

        logger.debug("Dates: {}/{} -> {}/{}", d1, d2, d1Trunc, d2Trunc);
        assertThat(d1Trunc, is(equalTo(d2Trunc)));
    }

    @Test
    public void testDurationTruncationHour() {
        Duration d1 = Duration.standardDays(5).plus(Duration.standardMinutes(5));
        Duration d2 = d1.plus(Duration.standardSeconds(1));

        Duration d1Trunc = DateUtils.truncateDuration(d1, DurationTruncation.HOUR);
        Duration d2Trunc = DateUtils.truncateDuration(d2, DurationTruncation.HOUR);

        logger.debug("Dates: {}/{} -> {}/{}", d1, d2, d1Trunc, d2Trunc);
        assertThat(d1Trunc, is(equalTo(d2Trunc)));
    }

    @Test
    public void testDurationTruncationDay() {
        Duration d1 = Duration.standardDays(5);
        Duration d2 = d1.plus(Duration.standardHours(1));

        Duration d1Trunc = DateUtils.truncateDuration(d1, DurationTruncation.DAY);
        Duration d2Trunc = DateUtils.truncateDuration(d2, DurationTruncation.DAY);

        logger.debug("Dates: {}/{} -> {}/{}", d1, d2, d1Trunc, d2Trunc);
        assertThat(d1Trunc, is(equalTo(d2Trunc)));
    }

    @Test
    public void testDurationTruncationRealData() throws Exception {
        Duration d1 = Duration.parse("PT212684.052S");
        Duration d2 = Duration.parse("PT212686.344S");

        Duration d1Trunc = DateUtils.truncateDuration(d1, DurationTruncation.HOUR);
        Duration d2Trunc = DateUtils.truncateDuration(d2, DurationTruncation.HOUR);

        logger.debug("Dates: {}/{} -> {}/{}", d1, d2, d1Trunc, d2Trunc);
        assertThat(d1Trunc, is(equalTo(d2Trunc)));

    }
}
