package de.l3s.icrawl.util;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;
import com.google.common.io.Resources;

import static de.l3s.icrawl.util.DateUtils.liberalParseDate;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class DateUtilsParseTest {
    private static final Logger logger = LoggerFactory.getLogger(DateUtilsParseTest.class);
    private Locale previousDefaultLocale;

    @Before
    public void setup() {
        previousDefaultLocale = Locale.getDefault();
        Locale.setDefault(Locale.GERMAN);
    }

    @After
    public void cleanup() {
        Locale.setDefault(previousDefaultLocale);
    }

    @Test
    public void testLiberalParseDate() throws IOException {
        URL formatsFile = Resources.getResource(getClass(), "dateformats.txt");
        for (String candidate : Resources.readLines(formatsFile, StandardCharsets.UTF_8)) {
            logger.debug("Parsing: '{}'", candidate);
            assertThat(candidate, liberalParseDate(candidate), is(notNullValue()));
        }
    }

    @Test
    public void testParseDateCorrectly() throws Exception {
        Map<String, DateTime> tests = ImmutableMap.<String, DateTime> builder()
            .put("2010-04-05", new LocalDate(2010, 4, 5).toDateTimeAtStartOfDay())
            .put("2013-02-07T14:26:41MEZ",
                new DateTime(2013, 2, 7, 14, 26, 41, DateTimeZone.forOffsetHours(1)))
            .put("2014-10-11 06:14:20 UTC", new DateTime(2014, 10, 11, 6, 14, 20, DateTimeZone.UTC))
            .put("19.10.2013", new LocalDate(2013, 10, 19).toDateTimeAtStartOfDay())
            .put("Fri, 05 Sep 2014 12:00:19 GMT",
                new DateTime(2014, 9, 5, 12, 0, 19, DateTimeZone.UTC))
            .put("Fri, 03 Oct 2014 12:36:19 +0200",
                new DateTime(2014, 10, 3, 12, 36, 19, DateTimeZone.forOffsetHours(+2)))
            .put("October 10, 2014 10:36 am", new DateTime(2014, 10, 10, 10, 36))
            .put("14/10/15", new LocalDate(2014, 10, 15).toDateTimeAtStartOfDay())
            .put("14/15/10", new LocalDate(2014, 10, 15).toDateTimeAtStartOfDay())
            .put("2013-09-30 03:06:52", new DateTime(2013, 9, 30, 3, 6, 52))
            .put("09/16/2014", new DateTime(2014, 9, 16, 0, 0))
            .put("10/10/2014 11:25:02 EDT",
                new DateTime(2014, 10, 10, 11, 25, 2, DateTimeZone.forID("America/New_York")))
            .put("Sat, Oct 4, 2014 10:00 BST",
                new DateTime(2014, 10, 4, 10, 0, DateTimeZone.forOffsetHours(1)))
            .put("2013-07-12@06:58:32 PDT",
                new DateTime(2013, 7, 12, 6, 58, 32, DateTimeZone.forID("America/Los_Angeles")))
            .put("2014-10-08 6:00:18 PM +0000",
                new DateTime(2014, 10, 8, 18, 0, 18, DateTimeZone.UTC))
            //            .put("31 октябрь 2014", new DateTime(2014, 10, 31, 0, 0))
            .put("20130607", new DateTime(2013, 6, 7, 0, 0))
            .put("Dec. 10, 2013, 11:55 a.m", new DateTime(2013, 12, 10, 11, 55))
            .build();

        for (Entry<String, DateTime> test : tests.entrySet()) {
            assertThat(test.getKey(), liberalParseDate(test.getKey()), is(equalTo(test.getValue())));
        }
    }
}
