package de.l3s.icrawl.util;

import org.joda.time.LocalDate;
import org.junit.Test;

import static de.l3s.icrawl.util.DateUtils.extractDate;
import static de.l3s.icrawl.util.DateUtils.extractDateFromUrl;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class DateUtilsTest {

    @Test
    public void testExtractDateFromUrl() {
        assertThat(
            extractDateFromUrl("http://blogs.wsj.com/chinarealtime/2014/01/09/a-micro-reading-of-chinas-local-government-audit/"),
            is(equalTo(new LocalDate(2014, 1, 9))));
        assertThat(
            extractDateFromUrl("https://ca.movies.yahoo.com/news/the-chinese-name-for-guardians-of-the-galaxy-is-amazing-122944127.html"),
            is(nullValue()));
        assertThat(
            extractDateFromUrl("http://www.zerohedge.com/news/2012-12-31/chinese-think-tank-conflict-japan-inevitable"),
            is(equalTo(new LocalDate(2012, 12, 31))));
        assertThat(
            extractDateFromUrl("http://blogs.law.harvard.edu/corpgov/2014/08/18/2014-proxy-season-review-2/"),
            is(equalTo(new LocalDate(2014, 8, 18))));
        assertThat(
            extractDateFromUrl("http://articles.latimes.com/2014/apr/11/nation/la-na-tt-stabbings-at-school-20140411"),
            is(equalTo(new LocalDate(2014, 4, 11))));
        assertThat(extractDateFromUrl("http://articles.latimes.com/1999/apr/28/news/ss-31711"),
            is(equalTo(new LocalDate(1999, 4, 28))));

    }

    @Test
    public void testExtracDateFromPath() {
        assertThat(
            extractDate("/html/2013-4/2013-4-22-17-3-18685.html"),
            is(equalTo(new LocalDate(2013, 4, 22))));
        assertThat(
            extractDate("/mainland/special/zhongguofangzu/content-3/detail_2013_02/04/21907778_0.shtml"),
            is(equalTo(new LocalDate(2013, 2, 4))));
    }
}
