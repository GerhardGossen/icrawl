package de.l3s.icrawl.util;

import org.apache.nutch.protocol.ProtocolStatusUtils;
import org.apache.nutch.storage.ProtocolStatus;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Document;

import static de.l3s.icrawl.util.TextExtractorTest.loadDocument;
import static de.l3s.icrawl.util.WebPageUtils.extractTextFromJavascript;
import static de.l3s.icrawl.util.WebPageUtils.mapHttpStatus;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class WebPageUtilsTest {

    @Test
    public void testMapHttpStatus() {
        assertThat(mapHttpStatus(ProtocolStatusUtils.STATUS_SUCCESS), is(200));
        assertThat(mapHttpStatus(ProtocolStatusUtils.STATUS_GONE), is(410));
        assertThat(mapHttpStatus(null), is(404));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUndefinedHttpStatus() {
        mapHttpStatus(ProtocolStatus.newBuilder().build());
    }

    @Test
    public void testExtractTextFromJavascript() throws Exception {
        Document dom = loadDocument("script.html", "http://www.example.org/");
        assertThat(extractTextFromJavascript(dom), is("Javascript can contain content too!"));
    }

    @Test
    public void testExtractTextFromInstagramJavascript() throws Exception {
        Document dom = loadDocument("instagram.com.html", "http://instagram.com/p/uyGBs8ODiR/");
        assertThat(extractTextFromJavascript(dom), containsString("#KICKEBOLAOUT"));
    }

    @Test
    public void testUnescapeTextFromInstagramJavascript() throws Exception {
        Document dom1 = loadDocument("instagram.com.1.html", "http://instagram.com/p/uqlRD9TF6C/");
        assertThat(extractTextFromJavascript(dom1), containsString("😷"));
    }

    @Test
    @Ignore
    public void testGetDocumentElement() throws Exception {
        Document dom = loadDocument("getDocumentElementError.html",
            "http://gloria.tv/media/UfquBkVAgsD");
        System.out.println(dom.getChildNodes().item(0));
        dom.getDocumentElement();
    }
}
