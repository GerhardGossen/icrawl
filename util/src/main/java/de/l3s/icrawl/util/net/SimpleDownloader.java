package de.l3s.icrawl.util.net;

import java.io.IOException;
import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.http.client.ResponseHandler;
import org.apache.http.client.protocol.HttpClientContext;

public class SimpleDownloader extends Downloader<Document> {
    private static class ContentExtractor extends ResponseAugmenter<Document> {
        protected ContentExtractor(Document candidate, HttpClientContext context) {
            super(candidate, context);
        }

        @Override
        protected Document process(String content) throws IOException {
            return new Document(getRealUrl().orElse(candidate.getUrl()), content);
        }
    }

    public Collection<Document> fetch(Collection<String> urls) {
        return fetchAll(urls.stream()
            .map(Document::new)
            .collect(Collectors.toMap(Document::getUrl, Function.identity())));
    }

    @Override
    protected ResponseHandler<Document> createResponseHandler(Document candidate,
            HttpClientContext context) {
        return new ContentExtractor(candidate, context);
    }


}
