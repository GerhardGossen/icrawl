package de.l3s.icrawl.util.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Optional;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.CharStreams;

import static java.nio.charset.StandardCharsets.UTF_8;

public abstract class ResponseAugmenter<T> implements ResponseHandler<T> {

    private static final Logger logger = LoggerFactory.getLogger(ResponseAugmenter.class);
    protected final T candidate;
    protected final HttpClientContext context;

    protected ResponseAugmenter(T candidate, HttpClientContext context) {
        this.candidate = candidate;
        this.context = context;
    }

    @Override
    public T handleResponse(HttpResponse response) throws IOException {
        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            return candidate;
        }

        ContentType contentType = ContentType.get(response.getEntity());
        if (isInvalidContentType(contentType)) {
            return candidate;
        }

        Charset charset = contentType.getCharset();
        if (charset == null) {
            charset = UTF_8;
        }
        String content;
        try (InputStream is = response.getEntity().getContent();
                InputStreamReader reader = new InputStreamReader(is, charset)) {
            content = CharStreams.toString(reader);
        }
        return process(content);
    }

    protected boolean isInvalidContentType(ContentType contentType) {
        return contentType == null || !contentType.getMimeType().contains("html")
                || !contentType.getMimeType().contains("text");

    }

    protected Optional<String> getRealUrl() {
        List<URI> redirects = context.getRedirectLocations();
        if (redirects != null && !redirects.isEmpty()) {
            logger.trace("Redirect chain: {}", redirects);
            return Optional.of(redirects.get(redirects.size() - 1).toASCIIString());
        } else {
            return Optional.empty();
        }
    }

    protected abstract T process(String content) throws IOException;

}
