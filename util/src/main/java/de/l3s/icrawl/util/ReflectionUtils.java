package de.l3s.icrawl.util;

import com.google.common.base.Throwables;

public final class ReflectionUtils {

    private ReflectionUtils() {}

    public static <T> T newInstance(Class<T> clazz) {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw Throwables.propagate(e);
        }
    }

}
