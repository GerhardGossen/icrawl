package de.l3s.icrawl.util.net;

import java.io.Closeable;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

public abstract class Downloader<T> implements Closeable {

    private static final Logger logger = LoggerFactory.getLogger(Downloader.class);
    protected final CloseableHttpClient httpClient;
    protected final ExecutorService executor;

    public Downloader() {
        PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager(
            100, TimeUnit.MICROSECONDS);
        connManager.setMaxTotal(100);
        this.httpClient = HttpClientBuilder.create()
            .setConnectionManager(connManager)
            .setUserAgent("iCrawl")
            .build();
        ThreadFactory tf = new ThreadFactoryBuilder().setNameFormat("downloader-%d").build();
        this.executor = new ThreadPoolExecutor(2, 20, 10, TimeUnit.SECONDS,
            new LinkedBlockingDeque<>(), tf);
    }

    private static <T> List<T> extractFinished(List<Future<T>> futures, List<T> fallbacks) {
        ImmutableList.Builder<T> results = ImmutableList.builder();

        for (int i = 0; i < futures.size() && i < fallbacks.size(); i++) {
            try {
                results.add(futures.get(i).get());
            } catch (CancellationException e) {
                logger.debug("Task timed out, using fallback", e);
                results.add(fallbacks.get(i));
            } catch (InterruptedException | ExecutionException e) {
                logger.info("Exception during processing, using fallback", e.getCause());
                results.add(fallbacks.get(i));
            }
        }
        return results.build();
    }

    protected List<T> fetchAll(Map<String, T> candidates) {
        List<Callable<T>> tasks = Lists.newArrayListWithExpectedSize(candidates.size());
        List<T> fallbacks = Lists.newArrayListWithExpectedSize(candidates.size());
        for (Entry<String, T> candidate : candidates.entrySet()) {
            tasks.add(() -> fetch(candidate.getKey(), candidate.getValue()));
            fallbacks.add(candidate.getValue());
        }
        List<Future<T>> futures = null;
        try {
            futures = executor.invokeAll(tasks, 60, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            logger.info("interrupted while analyzing candidates, returning partial results");
        }
        if (futures != null) {
            return extractFinished(futures, fallbacks);
        } else {
            return Collections.emptyList();
        }
    }

    private T fetch(final String url, final T candidate) throws IOException {
        HttpGet request = new HttpGet(url);
        HttpClientContext context = new HttpClientContext();
        return httpClient.execute(request, createResponseHandler(candidate, context), context);
    }

    protected abstract ResponseHandler<? extends T> createResponseHandler(final T candidate,
            HttpClientContext context);

    @Override
    public void close() throws IOException {
        httpClient.close();
    }

}
