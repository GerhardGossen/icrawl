package de.l3s.icrawl.util.net;


public class Document {
    private final String url;
    private final String content;

    public Document(String url) {
        this(url, null);
    }

    public Document(String url, String content) {
        this.url = url;
        this.content = content;
    }

    public String getUrl() {
        return url;
    }

    public String getContent() {
        return content;
    }
}