package de.l3s.icrawl.util;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.Mapper;

import com.google.common.base.Throwables;

public final class HadoopUtils {

    private HadoopUtils() {}

    public static Configuration createInternalCrawlDBConf(File icrawlDirectory) {
        Configuration conf = new Configuration(false);
        setupHadoop(conf);
        return conf;
    }

    public static void setupHadoop(Configuration conf) {
        conf.set("fs.file.impl", "org.apache.hadoop.fs.LocalFileSystem");

        conf.setQuietMode(false);
        conf.addResource("core-default.xml");
        conf.addResource("hdfs-default.xml");
        conf.addResource("mapred-default.xml");

    }

    public static Configuration setupNutch(Configuration conf, File icrawlDirectory) {
        File confDir = new File(icrawlDirectory, "conf");
        try {
            addResourceIfExists(conf, new File(confDir, "nutch-default.xml"));
            addResourceIfExists(conf, new File(confDir, "nutch-site.xml"));
        } catch (MalformedURLException e) {
            throw Throwables.propagate(e);
        }

        File pluginDir = new File(icrawlDirectory, "nutch-plugins");
        appendToConfValue(conf, "plugin.folders", pluginDir.getAbsolutePath(), ",");
        appendToConfValue(conf, "plugin.includes", "focusing", "|");
        appendToConfValue(conf, "plugin.excludes", "scoring-opic", "|");

        return conf;
    }

    static void addResourceIfExists(Configuration conf, File file)
            throws MalformedURLException {
        if (file.exists()) {
            conf.addResource(file.toURI().toURL());
        }
    }

    static void appendToConfValue(Configuration conf, String confName,
            String newValue, String separator) {
        String value = conf.get(confName);
        if (value == null) {
            value = newValue;
        } else {
            value += separator + newValue;
        }
        conf.set(confName, value);
    }

    public static void incrementCount(Mapper<?, ?, ?, ?>.Context context, Enum<?> counter) {
        if (context != null) {
            context.getCounter(counter).increment(1);
        }
    }

    public static String defaultDataDirectory(String appDirectory) {
        return defaultDataDirectory(new File(appDirectory));
    }

    public static String defaultDataDirectory(File appDirectory) {
        return new File(appDirectory.getParentFile(), "icrawl-data").getPath();
    }

    public static Configuration createConf(File icrawlDirectory) {
        Configuration conf = createInternalCrawlDBConf(icrawlDirectory);
        return setupNutch(conf, icrawlDirectory);
    }
}
