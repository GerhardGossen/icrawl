package de.l3s.icrawl.util;

import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.remoting.jaxws.JaxWsPortProxyFactoryBean;

public final class WebServiceUtils {
    public static final String ICRAWL_WS_NAMESPACE = "http://ws.services.icrawl.l3s.de/";
    public static final String CRAWLMANAGER_WS_NAMESPACE = "http://crawlmanager.icrawl.l3s.de/";

    private WebServiceUtils() {}

    public static <T> T createWSProxy(String namespaceUri, String wsBaseAddress,
            String serviceName, Class<T> interfaceClass) {
        JaxWsPortProxyFactoryBean factoryBean = new JaxWsPortProxyFactoryBean();
        factoryBean.setServiceInterface(interfaceClass);
        factoryBean.setNamespaceUri(namespaceUri);
        factoryBean.setServiceName(serviceName);
        factoryBean.setEndpointAddress(wsBaseAddress + serviceName);
        URL wsdlDocumentUrl;
        try {
            wsdlDocumentUrl = new URL(wsBaseAddress + serviceName + "?wsdl");
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("Could not construct WSDL URL", e);
        }
        factoryBean.setWsdlDocumentUrl(wsdlDocumentUrl);
        factoryBean.afterPropertiesSet();
        return interfaceClass.cast(factoryBean.getObject());
    }
}
