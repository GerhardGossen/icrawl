package de.l3s.icrawl.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Set;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.nutch.protocol.ProtocolStatusCodes;
import org.apache.nutch.storage.ProtocolStatus;
import org.cyberneko.html.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import static java.util.Locale.ENGLISH;

public final class WebPageUtils {
    private static final ImmutableMap<Integer, Integer> NUTCH_STATUS_TO_HTTP_STATUS_MAPPING = createStatusMapping();
    private static final Set<String> PARAGRAPH_ELEMENTS = ImmutableSet.of("p", "div", "li", "dd",
    "dt", "blockquote", "pre", "caption", "th", "td");
    private static final int MIN_PARAGRAPH_TOKENS = 50;
    /**
     * Minimal number of tokens a JS string needs to have to be recognized
     * as a string
     */
    private static final int MIN_JS_TOKENS = 3;

    private WebPageUtils() {}

    private static ImmutableMap<Integer, Integer> createStatusMapping() {
        ImmutableMap.Builder<Integer, Integer> builder = ImmutableMap.builder();
        builder.put(ProtocolStatusCodes.SUCCESS, 200);
        builder.put(ProtocolStatusCodes.FAILED, 500);
        builder.put(ProtocolStatusCodes.PROTO_NOT_FOUND, -1);
        builder.put(ProtocolStatusCodes.GONE, 410);
        builder.put(ProtocolStatusCodes.MOVED, 301);
        builder.put(ProtocolStatusCodes.TEMP_MOVED, 302);
        builder.put(ProtocolStatusCodes.NOTFOUND, 404);
        builder.put(ProtocolStatusCodes.RETRY, 500);
        builder.put(ProtocolStatusCodes.EXCEPTION, 500);
        builder.put(ProtocolStatusCodes.ACCESS_DENIED, 403);
        builder.put(ProtocolStatusCodes.ROBOTS_DENIED, 403);
        builder.put(ProtocolStatusCodes.REDIR_EXCEEDED, -1);
        builder.put(ProtocolStatusCodes.NOTFETCHING, 200);
        builder.put(ProtocolStatusCodes.NOTMODIFIED, 304);
        builder.put(ProtocolStatusCodes.WOULDBLOCK, -1);
        builder.put(ProtocolStatusCodes.BLOCKED, -1);
        return builder.build();
    }

    public static int mapHttpStatus(ProtocolStatus protocolStatus) {
        if (protocolStatus == null) {
            return 404;
        }
        Integer httpCode = NUTCH_STATUS_TO_HTTP_STATUS_MAPPING.get(protocolStatus.getCode());
        if (httpCode != null) {
            return httpCode.intValue();
        } else {
            throw new IllegalArgumentException("Unknown ProtocolStatus: " + protocolStatus);
        }
    }

    private static int tokenCount(Node n) {
        return TextExtractor.extractText(n).split("\\s+").length;
    }

    public static Element findParagraphParent(Element startElement, int minParagraphTokens) {
        Element prevElement = startElement;
        Element elem = startElement;
        while (elem != null && isParagraphElement(elem)
                && needsMoreTokens(elem, minParagraphTokens)
                && elem.getParentNode() instanceof Element) {
            prevElement = elem;
            elem = (Element) elem.getParentNode();
        }
        return prevElement;
    }

    private static boolean needsMoreTokens(Element elem, int minParagraphTokens) {
        return minParagraphTokens < 0 || tokenCount(elem) < minParagraphTokens;
    }

    private static boolean isParagraphElement(Element elem) {
        return PARAGRAPH_ELEMENTS.contains(elem.getNodeName().toLowerCase(ENGLISH));
    }

    public static Element findParagraphParent(Element startElement) {
        return findParagraphParent(startElement, MIN_PARAGRAPH_TOKENS);
    }

    public static Element findParagraphParent(Node node, int minParagraphTokens) {
        Node n = Preconditions.checkNotNull(node);
        while (n != null) {
            if (n instanceof Element) {
                return findParagraphParent((Element) n, minParagraphTokens);
            } else {
                n = n.getParentNode();
            }
        }
        return null;
    }

    public static String extractTextFromJavascript(Document dom) {
        StringBuilder sb = new StringBuilder();
        NodeList scriptElements = dom.getElementsByTagName("script");
        for (int i = 0; i < scriptElements.getLength(); i++) {
            String script = ((Element) scriptElements.item(i)).getTextContent();
            extractTextFromJavascript(script, sb);
        }
        return StringEscapeUtils.unescapeJavaScript(sb.toString().trim());
    }

    private static void extractTextFromJavascript(String script, StringBuilder sb) {
        int pos = -1;
        int startOfString = -1;
        while ((pos = script.indexOf("\"", pos + 1)) >= 0) {
            if (startOfString >= 0) {
                String s = script.substring(startOfString + 1, pos);
                if (s.split("\\s+").length > MIN_JS_TOKENS) {
                    sb.append("\n\n").append(s);
                }
                startOfString = -1;
            } else {
                startOfString = pos;
            }
        }
    }

    public static Document fetchAndParse(String documentUrl) throws HtmlParseException {
        InputSource source = new InputSource(documentUrl);
        source.setEncoding(StandardCharsets.UTF_8.name());
        return parse(source);
    }

    public static Document parseHtml(ByteBuffer content, String url) throws HtmlParseException {
        return parseHtml(new ByteBufferBackedInputStream(content), url);
    }

    public static Document parseHtml(InputStream is, String url) throws HtmlParseException {
        InputSource source = new InputSource(is);
        source.setEncoding(StandardCharsets.UTF_8.name());
        if (url != null) {
            source.setSystemId(url);
        }
        return parse(source);
    }

    public static Document parseHtml(String content, String url) throws HtmlParseException {
        InputSource source = new InputSource(new StringReader(content));
        source.setSystemId(url);
        return parse(source);
    }

    private static Document parse(InputSource source) throws HtmlParseException {
        try {
            DOMParser parser = new DOMParser();
            parser.parse(source);
            return parser.getDocument();
        } catch (SAXException | IOException e) {
            throw new HtmlParseException(source.getSystemId(), e);
        }
    }

}
