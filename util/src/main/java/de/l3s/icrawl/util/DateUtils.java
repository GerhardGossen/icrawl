package de.l3s.icrawl.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DateTimeUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;
import org.joda.time.LocalDate;
import org.joda.time.Partial;
import org.joda.time.ReadableInstant;
import org.joda.time.ReadablePartial;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

public class DateUtils {
    public enum DateTimeTruncation {
        DAY(new Partial()
            .with(DateTimeFieldType.hourOfDay(), 0)
            .with(DateTimeFieldType.minuteOfHour(), 0)
            .with(DateTimeFieldType.secondOfMinute(), 0)
            .with(DateTimeFieldType.millisOfSecond(), 0)),
        HOUR(new Partial()
            .with(DateTimeFieldType.minuteOfHour(), 0)
            .with(DateTimeFieldType.secondOfMinute(), 0)
            .with(DateTimeFieldType.millisOfSecond(), 0));

        private final ReadablePartial mask;

        private DateTimeTruncation(ReadablePartial mask) {
            this.mask = mask;
        }

        public ReadablePartial getMask() {
            return mask;
        }
    }

    public enum DurationTruncation {
        DAY(Duration.standardDays(1).getMillis()), HOUR(Duration.standardHours(1).getMillis());

        private final long millis;

        private DurationTruncation(long millis) {
            this.millis = millis;
        }

        public long getMillis() {
            return millis;
        }
    }

    public static final ImmutableMap<String, Integer> MONTH_MAPPINGS = ImmutableMap.<String, Integer> builder()
        .put("jan", 1)
        .put("feb", 2)
        .put("mar", 3)
        .put("apr", 4)
        .put("may", 5)
        .put("jun", 6)
        .put("jul", 7)
        .put("aug", 8)
        .put("sep", 9)
        .put("oct", 10)
        .put("nov", 11)
        .put("dec", 12)
        .put("january", 1)
        .put("february", 2)
        .put("march", 3)
        .put("april", 4)
        .put("june", 6)
        .put("july", 7)
        .put("august", 8)
        .put("september", 9)
        .put("october", 10)
        .put("november", 11)
        .put("december", 12)
        .build();
    public static final Map<String, DateTimeZone> TIME_ZONE_NAMES;
    static {
        TIME_ZONE_NAMES = new LinkedHashMap<>(DateTimeUtils.getDefaultTimeZoneNames());
        // British summer time
        TIME_ZONE_NAMES.put("BST", DateTimeZone.forOffsetHours(+1));
        // Central European Time
        TIME_ZONE_NAMES.put("CET", DateTimeZone.forOffsetHours(+1));
        // Mitteleuropäische Zeit (german name for CET)
        TIME_ZONE_NAMES.put("MEZ", DateTimeZone.forOffsetHours(+1));
    }
    public static final Set<String> META_DATE_KEYS = ImmutableSet.of("article:published_time",
        "datepublished", "og:updated_time", "last-modified", "article:modified_time", "date",
        "datemodified", "pubdate", "og:modified_time", "lastmod");
    public static final List<String> META_ATTRIBUTE_NAMES = ImmutableList.of("name", "property", "itemprop");
    private static final int FIRST_YEAR = 1990;
    // TODO vary based on document locale
    private static final List<DateTimeFormatter> FORMATS = ImmutableList.of(
        ISODateTimeFormat.dateTimeParser(),
        withTimeZoneShortName(ISODateTimeFormat.dateTimeParser()),
        withTimeZoneShortName(DateTimeFormat.forPattern("y-M-d' 'H:m:s ")),
        DateTimeFormat.shortDate().withLocale(Locale.getDefault()),
        withTimeZoneShortName(DateTimeFormat.forPattern("EEE, d MMM y H:m:s ")).withLocale(Locale.ENGLISH),
        DateTimeFormat.forPattern("E, d MMM y H:m:s Z").withLocale(Locale.ENGLISH).withOffsetParsed(),
        DateTimeFormat.forPattern("MMM dd, YYYY h:m a").withLocale(Locale.ENGLISH),
        DateTimeFormat.forPattern("YY/d/M"),
        DateTimeFormat.forPattern("YY/M/d"),
        DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss"),
        DateTimeFormat.forPattern("MM/dd/YYYY"),
        DateTimeFormat.forPattern("MMM. dd, YYYY, hh:mm a").withLocale(Locale.ENGLISH),
        withTimeZoneShortName(DateTimeFormat.forPattern("MM/dd/YYYY HH:mm:ss ")),
        withTimeZoneShortName(DateTimeFormat.forPattern("EEE, MMM d, YYYY H:m ")).withLocale(Locale.ENGLISH),
        DateTimeFormat.forPattern("d MMM y").withLocale(Locale.forLanguageTag("ru")),
        withTimeZoneShortName(DateTimeFormat.forPattern("y-M-d'@'H:m:s ")),
        DateTimeFormat.forPattern("YYYY-MM-dd hh:mm:ss a Z").withOffsetParsed(),
        DateTimeFormat.forPattern("YYYYMMdd"));

    private static final int LAST_YEAR = Calendar.getInstance().get(Calendar.YEAR);
    private static final Logger logger = LoggerFactory.getLogger(DateUtils.class);
    private static final Splitter URL_SEGMENT_SPLITTER = Splitter.onPattern("[-/_.]")
        .omitEmptyStrings();
    public static final ReadableInstant CUTOFF_DATE = new DateTime(2000, 1, 1, 0, 0);

    static class DateParse {
        int year = -1;
        int month = -1;
        int day = -1;
        boolean canOverrideDay = false;
        boolean canOverrideMonth = false;

        void tryParseFullDate(String segment) {
            if (year < 0) {
                int yearCandidate = Integer.parseInt(segment.substring(0, 4));
                if (FIRST_YEAR <= yearCandidate && yearCandidate <= yearCandidate) {
                    year = yearCandidate;
                    month = Integer.parseInt(segment.substring(4, 6));
                    day = Integer.parseInt(segment.substring(6));
                } else {
                    logger.debug(
                        "Ignoring possible date '{}' because it is out of the expected date range",
                        segment);
                }
            }
        }

        void tryParseYear(String segment) {
            int candidate = Integer.parseInt(segment);
            if (DateUtils.FIRST_YEAR <= candidate && candidate <= DateUtils.LAST_YEAR) {
                year = candidate;
            }
        }

        void tryParseMonthDay(String fullString, String segment) {
            int candidate = Integer.parseInt(segment);
            if (candidate > 12 && candidate <= 31 && day < 0) {
                day = candidate;
            } else if (candidate > 0) {
                tryAssignMonthOrDay(candidate);
            } else {
                logger.trace("Could not decide on '{}' in '{}'", candidate, fullString);
            }
        }

        private void tryAssignMonthOrDay(int candidate) {
            if (month >= 0 && canOverrideMonth) {
                month = candidate;
                day = candidate;
                canOverrideDay = true;
                canOverrideMonth = false;
            } else if (month >= 0) {
                if (day < 0 || canOverrideDay) {
                    day = candidate;
                    canOverrideDay = day == month;
                }
            } else {
                if (month < 0 || canOverrideMonth) {
                    month = candidate;
                    canOverrideMonth = year < 0;
                }
            }
        }

        public LocalDate toLocalDate() {
            return new LocalDate(year, month, day);
        }

        @Override
        public String toString() {
            return String.format("%4d-%2d-%2d", year, month, day);
        }

        void tryParseFullMonth(String segment) {
            if (month < 0) {
                month = DateUtils.MONTH_MAPPINGS.get(segment.toLowerCase(Locale.ENGLISH));
            }
        }

        boolean isFullySpecified() {
            return year > 0 && month > 0 && day > 0;
        }

        private void tryParseDigitsSegment(String path, String segment) {
            if (segment.length() == 8) {
                tryParseFullDate(segment);
            } else if (segment.length() == 4) {
                tryParseYear(segment);
            } else if (segment.length() == 2 || segment.length() == 1) {
                tryParseMonthDay(path, segment);
            }
        }

    }

    private DateUtils() {
        // prevent instantiation
    }

    public static LocalDate extractDate(String path) {
        final CharMatcher charMatcher = CharMatcher.inRange('0', '9');
        DateParse parse = new DateParse();
        for (String segment : URL_SEGMENT_SPLITTER.split(path)) {
            if (charMatcher.matchesAllOf(segment)) {
                parse.tryParseDigitsSegment(path, segment);
            } else if (MONTH_MAPPINGS.containsKey(segment.toLowerCase(Locale.ENGLISH))) {
                parse.tryParseFullMonth(segment);
            }
        }
        if (parse.isFullySpecified()) {
            try {
                return parse.toLocalDate();
            } catch (IllegalArgumentException e) {
                logger.trace("Not a valid date {} in '{}'", parse, path);
            }
        }
        return null;
    }

    public static LocalDate extractDateFromUrl(String url) {
        try {
            return extractDate(new URL(url).getPath());
        } catch (MalformedURLException e) {
            logger.debug("Malformed URL  '{}'", url);
            return null;
        }
    }

    /**
     * Try very hard to parse the parameter as a date.
     *
     * @return the parsed date or null if the format is unknown
     */
    public static DateTime liberalParseDate(String dateCandidate) {
        if (dateCandidate == null || dateCandidate.isEmpty()) {
            return null;
        }
        String normalized = normalizeHalfDay(dateCandidate);
        for (DateTimeFormatter formatter : FORMATS) {
            try {
                DateTime parsed = formatter.parseDateTime(normalized);
                logger.trace("'{}' parsed to {}", dateCandidate, parsed);
                if (parsed.isBefore(CUTOFF_DATE) || parsed.isAfterNow()) {
                    throw new IllegalArgumentException("Date out of range: " + parsed);
                }
                return parsed;
            } catch (IllegalArgumentException e) {
                logger.trace("Could not parse date: {}: {}", dateCandidate, e.getMessage());
            }
        }
        return null;

    }


    private static String normalizeHalfDay(String dateCandidate) {
        String normalized = dateCandidate.replaceAll("[aA]\\.?\\s*[mM]\\.?", "AM").replaceAll(
            "[pP]\\.?\\s*[mM]\\.?", "PM");
        if (!dateCandidate.equals(normalized)) {
            logger.debug("normalized '{}' to '{}'", dateCandidate, normalized);
        }
        return normalized;
    }

    public static DateTime truncateDate(DateTime dateTime, DateTimeTruncation truncation) {
        return truncateDate(dateTime, truncation.getMask());
    }

    public static DateTime truncateDate(DateTime dateTime, ReadablePartial dateMask) {
        return dateTime.withFields(dateMask);
    }

    public static Duration truncateDuration(Duration duration, DurationTruncation truncation) {
        return truncateDuration(duration, truncation.getMillis());
    }


    public static Duration truncateDuration(Duration duration, long durationTruncation) {
        return new Duration((duration.getMillis() / durationTruncation) * durationTruncation);
    }

    private static DateTimeFormatter withTimeZoneShortName(DateTimeFormatter formatter) {
        return new DateTimeFormatterBuilder().append(formatter)
            .appendTimeZoneShortName(TIME_ZONE_NAMES)
            .toFormatter();
    }

    public static boolean dateMetaKey(String key) {
        if (key == null) {
            return false;
        } else if (META_DATE_KEYS.contains(key.toLowerCase(Locale.ENGLISH))) {
            return true;
        }
        return false;
    }


    public static boolean isValidDate(DateTime dt) {
        return dt.isAfter(CUTOFF_DATE) && dt.isBeforeNow();
    }

    public static DateTime max(DateTime d1, DateTime d2) {
        return d1.isBefore(d2) ? d2 : d1;
    }
}
