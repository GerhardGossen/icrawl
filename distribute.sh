#!/bin/bash
set -o errexit
VERSION=`mvn -o help:evaluate -Dexpression=project.version | grep -v '\['`
echo Distributing v${VERSION}
mvn package -Pdistro -DskipTests -Dskip.jscompile
cd dist/target/icrawl-${VERSION}/icrawl-${VERSION}
rsync -avz bin conf lib nutch-* plugins okkam:icrawl/
rsync -avz bin conf lib nutch-* plugins asev:icrawl/
rsync -avz bin conf lib nutch-* plugins greymane:icrawl/
#rsync -avz --delete bin conf lib nutch-* godzilla:icrawl/
