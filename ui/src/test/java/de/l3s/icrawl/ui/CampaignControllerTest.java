package de.l3s.icrawl.ui;

import java.util.Objects;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.l3s.icrawl.crawlmanager.CrawlManager;
import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.service.CampaignService;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CampaignControllerTest {

    private final class FixedConverter implements Converter<String, Campaign> {
        private final Campaign out;
        private final String in;

        private FixedConverter(String in, Campaign out) {
            this.in = in;
            this.out = out;
        }

        @Override
        public Campaign convert(String source) {
            if (!Objects.equals(source, in)) {
                throw new IllegalArgumentException(source);
            }
            return out;
        }
    }

    private MockMvc mockMvc;

    @Before
    public void setup() {
        final Campaign campaign = new Campaign(0L, "foo", "foo", DateTime.now(), null, "foo");
        System.out.println(campaign.getId());
        CampaignService repository = Mockito.mock(CampaignService.class);
        when(repository.getCampaign(0)).thenReturn(campaign);
        Converter<String, Campaign> converter = new FixedConverter("0", campaign);
        FormattingConversionService conversionService = new DefaultFormattingConversionService();
        conversionService.addConverter(converter);
        CrawlManager crawlManager = Mockito.mock(CrawlManager.class);
        this.mockMvc = MockMvcBuilders.standaloneSetup(new CampaignController(repository),
            new CrawlController(crawlManager, repository, null, new ObjectMapper()))
            .setConversionService(conversionService)
            .build();
    }

    @Test
    public void testSaveCrawl() throws Exception {
        mockMvc.perform(
            post("/campaign/{id}/", 0).param("campaign", "0")
                .param("name", "foo")
                .param("specification.seeds", "http://www.l3s.de/\nhttp://www.google.de/")
                .param("specification.startTime", "2004-03-01T00:00:00")
                .param("specification.keywords", "")
                .param("specification.crawlSchedule.interval", "ONCE")
                .param("specification.downloadAssets", "")
                .param("specification.entities", ""))
            .andExpect(status().is3xxRedirection());
    }

}
