package de.l3s.icrawl.ui;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import de.l3s.icrawl.crawlmanager.CrawlManager;
import de.l3s.icrawl.domain.crawl.TaskExecutionRecord;
import de.l3s.icrawl.domain.status.RunningCrawlStatus;
import de.l3s.icrawl.repository.TaskExecutionRepository;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

public class StatusControllerTest {
    private MockMvc mockMvc;

    @Before
    public void setup() {
        StatusController controller = new StatusController();
        CrawlManager mockCrawlManager = Mockito.mock(CrawlManager.class);
        Mockito.when(mockCrawlManager.getRunningCrawls()).thenReturn(
            Collections.<RunningCrawlStatus> emptyList());
        controller.crawlManager = mockCrawlManager;
        TaskExecutionRepository executionRepository = Mockito.mock(TaskExecutionRepository.class);
        Mockito.when(executionRepository.findScheduledTasks()).thenReturn(
            Collections.<TaskExecutionRecord<?>> emptyList());
        controller.executionRepository = executionRepository;
        this.mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void testStatus() throws Exception {
        mockMvc.perform(get("/status/")).andExpect(model().attribute("section", "status"));
    }

}
