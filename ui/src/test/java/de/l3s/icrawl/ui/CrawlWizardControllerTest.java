package de.l3s.icrawl.ui;

import java.util.Collection;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.google.common.collect.ImmutableList;

import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.domain.specification.Crawl;
import de.l3s.icrawl.domain.specification.CrawlSpecification;
import de.l3s.icrawl.domain.specification.CrawlUrl;
import de.l3s.icrawl.service.CampaignService;
import de.l3s.icrawl.ui.wizard.MockSeedSearcher;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


public class CrawlWizardControllerTest {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper().registerModules(new JodaModule());
    private MockMvc mockMvc;

    @Before
    public void setup() {
        Campaign campaign = new Campaign("FooCampaign");
        campaign.setId(1L);
        CampaignService repository = Mockito.mock(CampaignService.class);
        when(repository.getCampaign(1L)).thenReturn(campaign);
        MockSeedSearcher searcher = new MockSeedSearcher();
        CrawlWizardController controller = new CrawlWizardController(repository, searcher);
        this.mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .alwaysDo(print())
                .build();
    }

    @Test
    public void testSearch() throws Exception {
        mockMvc.perform(get("/wizard/search").param("q", "water"))
            .andExpect(status().is2xxSuccessful())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$[0].title", containsString("water")))
            .andExpect(jsonPath("$[0].keywords", not(emptyArray())))
            .andExpect(jsonPath("$[0].entities", not(emptyArray())))
            .andExpect(jsonPath("$[0].description", containsString("water")))
            .andExpect(jsonPath("$[0].url", not(isEmptyString())));
    }

    @Test
    public void testLog() throws Exception {
        Collection<String> messages = ImmutableList.of(
            "{\"sessionId\":\"tWMCwG6zecBrhiurNGqOjYIEtDAyWcwU\",\"log\":[{\"date\":1410268618442,\"msg\":{\"type\":\"add\",\"targetType\":\"seed\",\"target\":\"http://google.com\",\"context\":\"manual\"}}]}",
            "{\"sessionId\":\"tWMCwG6zecBrhiurNGqOjYIEtDAyWcwU\",\"log\":[{\"date\":1410268626071,\"msg\":{\"type\":\"search\",\"target\":\"test\"}}]}",
            "{\"sessionId\":\"tWMCwG6zecBrhiurNGqOjYIEtDAyWcwU\",\"log\":[{\"date\":1410268634672,\"msg\":{\"type\":\"remove\",\"targetType\":\"keyword\",\"target\":\"qwe\",\"context\":\"manual\"}}]}",
            "{\"sessionId\":\"tWMCwG6zecBrhiurNGqOjYIEtDAyWcwU\",\"log\":[{\"date\":1410268586104,\"msg\":{\"type\":\"toggleSearchSource\",\"targetType\":\"twitter\",\"target\":false}},{\"date\":1410268586528,\"msg\":{\"type\":\"toggleSearchSource\",\"targetType\":\"web\",\"target\":false}}]}");
        for (String requestBody : messages) {
            mockMvc.perform(
                post("/wizard/log").content(requestBody).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        }
    }

    public void testCreate() throws Exception {
        CrawlSpecification spec = new CrawlSpecification();
        spec.setStartTime(DateTime.now());
        spec.addSeed(new CrawlUrl("http://www.l3s.de/"));
        Crawl crawl = new Crawl();
        crawl.setName("FooCrawl");
        crawl.setSpecification(spec);
        crawl.setCampaignId(1L);

        String crawlJson = OBJECT_MAPPER.writeValueAsString(crawl);

        mockMvc.perform(
            post("/wizard/create").contentType(MediaType.APPLICATION_JSON).content(crawlJson))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.url", is("http://localhost/campaign/1")));
    }
}
