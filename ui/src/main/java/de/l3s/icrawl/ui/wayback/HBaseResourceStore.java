package de.l3s.icrawl.ui.wayback;

import java.io.IOException;

import org.apache.nutch.storage.WebPage;
import org.apache.nutch.util.TableUtil;
import org.archive.wayback.ResourceStore;
import org.archive.wayback.core.CaptureSearchResult;
import org.archive.wayback.core.Resource;
import org.archive.wayback.exception.ResourceNotAvailableException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate;

public class HBaseResourceStore implements ResourceStore {
    private static final Logger logger = LoggerFactory.getLogger(HBaseResourceStore.class);
    private final CrawlDataRepositoryTemplate template;

    HBaseResourceStore(CrawlDataRepositoryTemplate template) {
        this.template = template;
    }

    @Override
    public Resource retrieveResource(CaptureSearchResult result)
            throws ResourceNotAvailableException {
        logger.debug("retrieveResource({})", result);
        String campaignId = result.getCustom(HBaseResourceIndex.CAMPAIGN_ID_KEY);
        try {
            String urlKey = TableUtil.reverseUrl(result.getOriginalUrl());
            WebPage page = template.getWebPage(Long.parseLong(campaignId), urlKey);
            return new HBaseResource(page);
        } catch (IOException e) {
            throw new ResourceNotAvailableException("Could not load resource from HBase",
                result.getOriginalUrl(), e);
        }
    }

    @Override
    public void shutdown() throws IOException {
        // TODO Auto-generated method stub

    }

}
