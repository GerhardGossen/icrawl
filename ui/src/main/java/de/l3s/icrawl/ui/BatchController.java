package de.l3s.icrawl.ui;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.inject.Inject;

import org.apache.nutch.storage.WebPage;
import org.joda.time.PeriodType;
import org.joda.time.format.PeriodFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import static java.util.stream.Collectors.toList;

import de.l3s.icrawl.crawlmanager.TaskExecutionService;
import de.l3s.icrawl.domain.crawl.BatchRecord;
import de.l3s.icrawl.domain.crawl.TaskExecutionRecord;
import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.domain.specification.Crawl;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate.ResultMapper;
import de.l3s.icrawl.service.CampaignService;
import static de.l3s.icrawl.ui.ControllerUtils.checkFound;

@Controller
public class BatchController {
    public class BatchPage {
        private final String key;
        private final float score;

        public BatchPage(String key, float score) {
            this.key = key;
            this.score = score;
        }

        public String getKey() {
            return key;
        }

        public float getScore() {
            return score;
        }
    }

    @Inject
    CrawlDataRepositoryTemplate template;

    @Inject
    TaskExecutionService repository;
    @Inject
    CampaignService metadataRepository;
    @Inject
    ObjectMapper jsonMapper;

    @ModelAttribute("tab")
    public String tab() {
        return "history";
    }

    @RequestMapping("/campaign/{campaign}/{crawl}/{execution}")
    public ModelAndView showExecution(@PathVariable Campaign campaign, @PathVariable Crawl crawl,
            @PathVariable TaskExecutionRecord<?> execution) throws JsonProcessingException {
        checkFound("campaign", campaign);
        checkFound("crawl", crawl);
        checkFound("taskExecution", execution);
        List<BatchRecord> batches = repository.getBatchesForExecution(execution.getId());
        return new ModelAndView("crawl/execution")
            .addObject("specification", jsonMapper.writeValueAsString(execution.getConfiguration()))
            .addObject("batches", batches)
            .addObject("histogramHeaders", IntStream.range(0, 100).mapToObj(i -> String.format("0.%02d", i)).collect(toList()))
            .addObject("relevanceScores", BatchRecord.relevanceScores(batches))
            .addObject("timeScores", BatchRecord.timeScores(batches))
            .addObject("scoreSums", BatchRecord.scoreSums(batches));
    }

    @RequestMapping("/campaign/{campaign}/{crawl}/{execution}/{batchId}/")
    public ModelAndView showBatch(@PathVariable Campaign campaign, @PathVariable Crawl crawl,
            @PathVariable final TaskExecutionRecord<?> execution, @PathVariable final int batchId,
            Locale userLang) throws IOException {
        checkFound("campaign", campaign);
        checkFound("crawl", crawl);
        checkFound("taskExecution", execution);

        String[] fields = new String[] { WebPage.Field.SCORE.getName(),
                WebPage.Field.BATCH_ID.getName() };
        List<BatchPage> batchData = template.query(campaign, fields, "", 1000,
            new ResultMapper<BatchPage>() {
                @Override
                public BatchPage map(String key, WebPage page) {
                    return new BatchPage(key, page.getScore().floatValue());
                }
            }, CrawlDataRepositoryTemplate.batchQuery(execution, batchId));
        BatchRecord batch = repository.getBatch(execution.getId(), batchId);
        PeriodType periodType = PeriodType.dayTime().withMillisRemoved();
        return new ModelAndView("crawl/batch").addObject("batch", batch)
            .addObject("batchData", batchData)
            .addObject("count", batchData.size())
            .addObject("steps", batch.getSteps())
            .addObject("periodFormat", PeriodFormat.wordBased(userLang))
            .addObject(periodType);
    }
}
