package de.l3s.icrawl.ui.wayback;

import org.archive.wayback.ResourceIndex;

import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate;

public class HBaseResourceIndexFactory {

    private final CrawlDataRepositoryTemplate template;

    public HBaseResourceIndexFactory(CrawlDataRepositoryTemplate template) {
        this.template = template;
    }

    public ResourceIndex get(long crawlId) {
        return new HBaseResourceIndex(template, crawlId);
    }
}
