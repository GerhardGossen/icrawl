package de.l3s.icrawl.ui;

import javax.inject.Inject;
import javax.jms.ConnectionFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

import static de.l3s.icrawl.process.JmsConfig.*;
import de.l3s.icrawl.crawlmanager.CrawlManager;
import de.l3s.icrawl.process.CrawlDbManager;
import de.l3s.icrawl.process.CrawlManagerManager;
import de.l3s.icrawl.process.JmsConfig;
import de.l3s.icrawl.process.MessageBusManager;
import de.l3s.icrawl.process.MetadataDbManager;

@Configuration
@Profile("ui")
@Import(JmsConfig.class)
public class ServicesConfig {
    private static final Logger logger = LoggerFactory.getLogger(ServicesConfig.class);

    @Inject
    JmsConfig jmsConfig;

    @Bean
    CrawlDbManager crawlDbManager() {
        return jmsConfig.createProxy(CRAWL_DB_MANAGER_QUEUE, CrawlDbManager.class, true);
    }

    @Bean
    MetadataDbManager metadataDbManager() {
        return jmsConfig.createProxy(METADATA_DB_MANAGER_QUEUE, MetadataDbManager.class, true);
    }

    @Bean
    MessageBusManager messageBusManager() {
        return jmsConfig.createProxy(MESSAGE_BUS_MANAGER_QUEUE, MessageBusManager.class, true);
    }

    @Bean
    CrawlManagerManager remoteCrawlManager() {
        return jmsConfig.createProxy(CRAWL_MANAGER_MANAGER_QUEUE, CrawlManagerManager.class, true);
    }

    @Bean
    CrawlManager crawlManager(ConnectionFactory connectionFactory) {
        logger.info("Using JMS invoker for crawl manager");
        return jmsConfig.createProxy(CRAWL_MANAGER_QUEUE, CrawlManager.class, false);
    }
}
