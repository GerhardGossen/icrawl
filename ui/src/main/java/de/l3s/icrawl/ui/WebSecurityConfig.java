package de.l3s.icrawl.ui;
import java.util.List;

import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.google.common.collect.Lists;

import de.l3s.icrawl.repository.EvaluatorRepository;
import de.l3s.icrawl.service.EvaluatorUserDetailsService;
@Configuration
@EnableWebMvcSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Inject
    EvaluatorRepository evaluatorRepository;
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .csrf().disable()
            .authorizeRequests()
                .antMatchers("/evaluation/signin").permitAll()
                .antMatchers("/evaluation/signin/check").permitAll()
                .antMatchers("/evaluation/signup").permitAll()
                .antMatchers("/evaluation/signup/create").permitAll()
                .antMatchers("/evaluation/**").authenticated()
                .antMatchers("/**").permitAll()
            .and()
            .formLogin()
                .loginPage("/evaluation/signin")
                    .usernameParameter("email")
                    .passwordParameter("password")
                .loginProcessingUrl("/evaluation/signin/check")
                .defaultSuccessUrl("/evaluation/evaluate", true)
                .failureUrl("/evaluation/signin?error=WRONG_USERNAME_PASS")
                .permitAll()
            .and()
            .logout()
                .logoutUrl("/evaluation/signout")
                .logoutSuccessUrl("/evaluation/signin");
    }
    @Override
    @Bean
    protected UserDetailsService userDetailsService() {
        return new EvaluatorUserDetailsService(evaluatorRepository);
    }
    @Bean
    AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(userDetailsService());
        provider.setPasswordEncoder(passwordEncoder());
        return provider;
    }
    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    @Bean
    ProviderManager providerManager() {
        List<AuthenticationProvider> providers = Lists.newArrayList(authenticationProvider());
        return new ProviderManager(providers);
    }
}