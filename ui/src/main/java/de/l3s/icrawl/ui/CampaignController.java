package de.l3s.icrawl.ui;

import java.util.Collection;

import javax.inject.Inject;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.domain.specification.Crawl;
import de.l3s.icrawl.service.CampaignService;

import static de.l3s.icrawl.ui.ControllerUtils.checkFound;

@Controller
@RequestMapping("/")
public class CampaignController {
    private static final Logger logger = LoggerFactory.getLogger(CampaignController.class);

    private final CampaignService repository;

    @Inject
    CampaignController(CampaignService repository) {
        this.repository = repository;
    }

    @ModelAttribute("section")
    String section() {
        return "campaigns";
    }

    @RequestMapping("/")
    public ModelAndView list() {
        logger.debug("Listing campaigns");
        return new ModelAndView("campaign/list", "campaigns",
            repository.getActiveCampaignsForUser("foo"));
    }

    @RequestMapping("/add")
    public ModelAndView add() {
        return new ModelAndView("campaign/edit").addObject("campaign", new Campaign())
            .addObject("action", "/");
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String create(@Valid Campaign campaign) {
        checkFound("campaign", campaign);
        campaign.setOwner("foo");
        campaign.setCreated(DateTime.now());
        Campaign savedCampaign = repository.addCampaign(campaign);
        return "redirect:/campaign/" + savedCampaign.getId() + "/add";
    }

    @RequestMapping(value = "campaign/{campaign}", method = RequestMethod.GET)
    public ModelAndView show(@PathVariable Campaign campaign) {
        checkFound("campaign", campaign);
        Collection<Crawl> crawls = campaign.getCrawls();
        return new ModelAndView("campaign/show").addObject("crawls", crawls);
    }

    static ModelAndView redirectToCampaign(long campaign) {
        return new ModelAndView("redirect:/campaign/" + campaign);
    }

}
