package de.l3s.icrawl.ui;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import de.l3s.icrawl.domain.evaluation.Evaluator;
import de.l3s.icrawl.domain.evaluation.EvaluatorScore;
import de.l3s.icrawl.domain.evaluation.Label;
import de.l3s.icrawl.domain.specification.Crawl;
import de.l3s.icrawl.service.CampaignService;
import de.l3s.icrawl.service.EvaluationService;

@Controller
@RequestMapping("evaluation")
public class EvaluationController {
    private static final Logger logger = LoggerFactory.getLogger(EvaluationController.class);
    private static final long CAMPAIGN_TO_EVALUATE = 1;

    @Inject
    EvaluationService evaluationService;
    @Inject
    CampaignService crawlService;

    @RequestMapping("/")
    public String start(Authentication principal) {
        logger.info("/, principal={}", principal);
        if (principal == null || !principal.isAuthenticated()) {
            return "redirect:evaluation/signin";
        } else {
            return "evaluation/description";
        }
    }

    @RequestMapping("signup")
    public ModelAndView signUp() {
        return new ModelAndView("evaluation/signup", "evaluator", new Evaluator());
    }

    @RequestMapping(value = "signup/create", method = RequestMethod.POST)
    public ModelAndView createAccount(@Valid @ModelAttribute("evaluator") Evaluator evaluator,
            BindingResult result) {
        logger.info("saving user");
        if (result.hasErrors()) {
            logger.debug("Form has errors: {}", result);
            return new ModelAndView("evaluation/signup").addObject("evaluator", evaluator);
        }
        try {
            evaluationService.createUser(evaluator);
        } catch (Exception e) {
            logger.debug("User already exists: {}", e);
            result.rejectValue("email", "Email is already used");
            return new ModelAndView("evaluation/signup").addObject("evaluator", evaluator);
        }
        return new ModelAndView("redirect:/evaluation/");
    }
    @RequestMapping(value = "signin", method = RequestMethod.GET)
    public ModelAndView signInForm(@RequestParam(value="errorCode", required=false, defaultValue="0") Integer errorCode) {
        logger.info("Showing signin form");
        String errorMsg = null;
        if (errorCode == 1) {
            errorMsg = "Wrong username or password";
        }
        return new ModelAndView("evaluation/signin").addObject("error", errorMsg);
    }
    @RequestMapping("evaluate")
    public ModelAndView showEvaluationForm() {
        Crawl crawl = getEvaluatedCrawl();
        return new ModelAndView("evaluation/evaluate").addObject("specification",
            crawl.getSpecification());
    }
    private Crawl getEvaluatedCrawl() {
        Collection<Crawl> crawls = crawlService.getCampaign(CAMPAIGN_TO_EVALUATE).getCrawls();
        if (crawls.isEmpty()) {
            throw new IllegalStateException("Cannot start evaluation without any crawls");
        }
        return crawls.iterator().next();
    }
    @RequestMapping(value = "evaluate", method = RequestMethod.POST)
    public String saveLabel(@Valid Label label) {
        label.setCampaign(crawlService.getCampaign(CAMPAIGN_TO_EVALUATE));
        evaluationService.addLabel(label);
        return "redirect:/evaluation/evaluate";
    }

    @RequestMapping(value = "evaluate/urls", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getEvaluationUrls(@AuthenticationPrincipal UserDetails userDetails) {
        Evaluator evaluator = evaluationService.getCurrentEvaluator(userDetails);

        return evaluationService.getNextBatchUrls(evaluator);
    }

    @RequestMapping(value = "evaluate/vote", method = RequestMethod.POST)
    @ResponseBody
    public List<EvaluatorScore> voteEvaluationUrl(
            @RequestParam(value = "url") String urlStr,
            @RequestParam(value = "comment", required = false, defaultValue = "") String comment,
            @RequestParam(value = "relevance") int relevance,
            @AuthenticationPrincipal UserDetails userDetails) {

        Evaluator evaluator = evaluationService.getCurrentEvaluator(userDetails);

        evaluationService.addVote(evaluator, urlStr, relevance, comment);

        return evaluationService.getEvaluatorScores();
    }
}