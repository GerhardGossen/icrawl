package de.l3s.icrawl.ui.format;

import de.l3s.icrawl.domain.specification.NamedEntity;
import de.l3s.icrawl.domain.specification.NamedEntity.Type;

public class EntityPropertyEditor extends LinePropertyEditor<NamedEntity> {
    @Override
    protected NamedEntity convertLine(String line) {
        return new NamedEntity(Type.PERSON, line); // TODO
    }

    @Override
    protected String printItem(NamedEntity value) {
        return value.toString();
    }
}