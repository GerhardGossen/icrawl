package de.l3s.icrawl.ui;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import de.l3s.icrawl.domain.api.OAuthAccessToken;
import de.l3s.icrawl.service.OAuthAccessTokenService;

@Controller
@RequestMapping("/settings/")
public class SettingsController {

    @Inject
    OAuthAccessTokenService tokensService;

    @RequestMapping
    public ModelAndView list() {
        return showAccessTokenForm(new OAuthAccessToken());
    }

    private ModelAndView showAccessTokenForm(OAuthAccessToken form) {
        return new ModelAndView("settings").addObject("accessTokens", tokensService.findAll())
            .addObject("accessTokenForm", form);
    }

    @RequestMapping(value = "addToken", method = RequestMethod.POST)
    public ModelAndView addToken(@Valid OAuthAccessToken accessToken, BindingResult errors) {
        if (errors.hasErrors()) {
            return showAccessTokenForm(accessToken);
        }
        tokensService.add(accessToken);
        return new ModelAndView("redirect:/settings/");
    }

    @RequestMapping(value = "enableToken/{token}", method = RequestMethod.POST)
    public String enableToken(@PathVariable OAuthAccessToken token) {
        tokensService.enable(token);
        return "redirect:/settings/";
    }

    @RequestMapping(value = "disableToken/{token}", method = RequestMethod.POST)
    public String disableToken(@PathVariable OAuthAccessToken token) {
        tokensService.disable(token);
        return "redirect:/settings/";
    }
}
