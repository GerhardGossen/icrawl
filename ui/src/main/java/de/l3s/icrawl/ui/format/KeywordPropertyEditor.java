package de.l3s.icrawl.ui.format;

public class KeywordPropertyEditor extends LinePropertyEditor<String> {
    @Override
    protected String convertLine(String line) {
        return line;
    }

    @Override
    protected String printItem(String value) {
        return value;
    }
}