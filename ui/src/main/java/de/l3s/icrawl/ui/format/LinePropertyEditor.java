package de.l3s.icrawl.ui.format;

import java.beans.PropertyEditorSupport;
import java.util.Collection;
import java.util.Set;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

abstract class LinePropertyEditor<T> extends PropertyEditorSupport {
    @Override
    public void setAsText(String text) {
        String[] lines = text.split("\\r?\\n");
        Set<T> items = Sets.newHashSetWithExpectedSize(lines.length);
        for (String line : lines) {
            if (!line.trim().isEmpty()) {
                items.add(convertLine(line));
            }
        }
        setValue(items);
    }

    @Override
    public String getAsText() {
        @SuppressWarnings("unchecked")
        Collection<T> values = (Collection<T>) getValue();
        if (values == null) {
            return "";
        }
        Collection<String> texts = Lists.newArrayListWithExpectedSize(values.size());
        for (T value : values) {
            texts.add(printItem(value));
        }
        return Joiner.on("\n").join(texts);
    }

    protected abstract T convertLine(String line);

    protected abstract String printItem(T value);
}