package de.l3s.icrawl.ui;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import org.apache.nutch.storage.WebPage;
import org.apache.nutch.util.TableUtil;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.domain.specification.Crawl;
import de.l3s.icrawl.repository.CrawlDataRepository;
import de.l3s.icrawl.repository.CrawlDataRepository.WebPageInfo;

import static de.l3s.icrawl.ui.ControllerUtils.checkFound;


@Controller
@RequestMapping("/storage")
public class StorageController {
    public enum PageSelection {
        ALL, FETCHED, UNFETCHED
    }

    static final int ITEMS_PER_PAGE = 100;
    private static final Logger logger = LoggerFactory.getLogger(StorageController.class);
    private static final DateTimeFormatter FORMATTER = DateTimeFormat.mediumDateTime();
    private final CrawlDataRepository repository;

    @Inject
    public StorageController(CrawlDataRepository repository)
            throws IOException {
        this.repository = repository;
    }

    @ModelAttribute("tab")
    String tab() {
        return "data";
    }

    @RequestMapping("{campaign}/{crawl}")
    public ModelAndView list(@PathVariable Campaign campaign, @PathVariable Crawl crawl,
            @RequestParam(defaultValue = "") String startKey,
            @RequestParam(defaultValue = "FETCHED") PageSelection selection) throws IOException {
        checkFound("campaign", campaign);
        checkFound("crawl", crawl);
        String key = startKey.startsWith("http") ? TableUtil.reverseUrl(startKey) : startKey;
        logger.info("startKey={}, selection={}", key, selection);
        List<WebPageInfo> urls;
        int maxItems = ITEMS_PER_PAGE + 1;
        switch (selection) {
        case ALL:
            urls = repository.listWebpagesForCampaign(campaign, maxItems, key);
            break;
        case FETCHED:
            urls = repository.listFetchedWebpagesForCampaign(campaign, maxItems, key);
            break;
        case UNFETCHED:
            urls = repository.listUnfetchedWebpagesForCampaign(campaign, maxItems, key);
            break;
        default:
            throw new IllegalArgumentException("Unhandeled crawl selection " + selection);
        }
        String nextStartKey = null;
        if (urls.size() > ITEMS_PER_PAGE) {
            nextStartKey = urls.get(ITEMS_PER_PAGE).getKey();
            urls = urls.subList(0, ITEMS_PER_PAGE);
        }
        return new ModelAndView("crawl/list_resources").addObject("urls", urls)
            .addObject("nextStartKey", nextStartKey)
            .addObject("selection", selection)
            .addObject("formatter", FORMATTER);
    }

    @RequestMapping(value = "{campaign}/{crawl}", params = "url")
    public ModelAndView showResource(@PathVariable Campaign campaign, @PathVariable Crawl crawl,
            @RequestParam String url) throws IOException {
        checkFound("campaign", campaign);
        checkFound("crawl", crawl);
        WebPage webPage = repository.getWebPage(campaign, url);
        checkFound("webpage", webPage);
        return new ModelAndView("crawl/resource")
            .addObject("webPage", webPage)
            .addObject("url", TableUtil.unreverseUrl(url));

    }
}
