package de.l3s.icrawl.ui;

import java.beans.PropertyEditorSupport;
import java.io.IOException;
import java.util.Set;

import javax.inject.Inject;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.base.Throwables;

import de.l3s.icrawl.crawlmanager.CrawlManager;
import de.l3s.icrawl.crawlmanager.TaskExecutionService;
import de.l3s.icrawl.domain.crawl.CrawlStatistics;
import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.domain.specification.Crawl;
import de.l3s.icrawl.domain.specification.CrawlSpecification;
import de.l3s.icrawl.domain.status.CrawlStatus;
import de.l3s.icrawl.service.CampaignService;
import de.l3s.icrawl.ui.format.CrawlUrlPropertyEditor;
import de.l3s.icrawl.ui.format.EntityPropertyEditor;
import de.l3s.icrawl.ui.format.KeywordPropertyEditor;

import static com.google.common.base.Preconditions.checkNotNull;
import static de.l3s.icrawl.ui.CampaignController.redirectToCampaign;
import static de.l3s.icrawl.ui.ControllerUtils.asUri;
import static de.l3s.icrawl.ui.ControllerUtils.checkFound;

@Controller
@RequestMapping(value = "/campaign/{campaign}", name = "CrawlC")
public class CrawlController {
    private static final CrawlController CONTROLLER = MvcUriComponentsBuilder.on(CrawlController.class);
    private static final Logger logger = LoggerFactory.getLogger(CrawlController.class);
    private final CrawlManager crawlManager;
    private final CampaignService repository;
    private final ObjectMapper jsonMapper;
    private final TaskExecutionService executionRepository;

    @Inject
    CrawlController(CrawlManager crawlManager, CampaignService repository,
            TaskExecutionService executionRepository, ObjectMapper jsonMapper) {
        this.crawlManager = checkNotNull(crawlManager);
        this.repository = checkNotNull(repository);
        this.executionRepository = executionRepository;
        this.jsonMapper = jsonMapper.disable(SerializationFeature.WRITE_NULL_MAP_VALUES)
            .setSerializationInclusion(Include.NON_EMPTY);
    }

    @ModelAttribute("section")
    String section() {
        return "campaigns";
    }

    @InitBinder
    void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Set.class, "specification.seeds",
            new CrawlUrlPropertyEditor());
        binder.registerCustomEditor(Set.class, "specification.entities",
            new EntityPropertyEditor());
        binder.registerCustomEditor(Set.class, "specification.keywords",
            new KeywordPropertyEditor());
        binder.registerCustomEditor(CrawlSpecification.class,
            new PropertyEditorSupport() {
                @Override
                public void setAsText(String text) throws IllegalArgumentException {
                    try {
                        setValue(jsonMapper.readValue(text, CrawlSpecification.class));
                    } catch (IOException e) {
                        throw new IllegalArgumentException(e);
                    }
                }
            });
    }

    private ModelAndView editForm(Campaign campaign, Crawl crawl, String actionUri) {
        return editForm(campaign, crawl, actionUri, "edit");
    }

    private ModelAndView editForm(Campaign campaign, Crawl crawl, String actionUri, String tab) {
        try {
            return new ModelAndView("crawl/edit").addObject("campaign", campaign)
                .addObject("campaignJson", jsonMapper.writeValueAsString(campaign))
                .addObject("crawl", crawl)
                .addObject("crawlJson", jsonMapper.writeValueAsString(crawl))
                .addObject("action", actionUri)
                .addObject("tab", tab);
        } catch (JsonProcessingException e) {
            throw Throwables.propagate(e);
        }
    }

    @RequestMapping("add")
    public ModelAndView add(@PathVariable Campaign campaign) {
        checkFound("campaign", campaign);
        Crawl crawl = new Crawl();
        crawl.setCampaign(campaign);
        String actionUri = asUri(CONTROLLER.create(campaign, null, null));
        return editForm(campaign, crawl, actionUri, "crawl");
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ModelAndView create(@PathVariable Campaign campaign, @Valid Crawl crawl,
            BindingResult result) {
        checkFound("campaign", campaign);

        logger.debug("About to save crawl: {}", crawl);
        if (result.hasErrors()) {
            logger.debug("Crawl has errors: {}", result);
            String actionUri = asUri(CONTROLLER.create(campaign, null, null));
            return editForm(campaign, crawl, actionUri, "crawl");
        }
        crawl.setCreated(DateTime.now());
        repository.addCrawl(crawl);
        return redirectToCampaign(campaign.getId());
    }

    @RequestMapping(value = "", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody String createFromJson(@PathVariable Campaign campaign,
            @Valid @RequestBody Crawl crawl, BindingResult result) {
        if (result.hasErrors()) {
            return "Error: " + result.getAllErrors();
        } else {
            crawl.setCreated(DateTime.now());
            crawl.setCampaign(campaign);
            Crawl savedCrawl = repository.addCrawl(crawl);
            return MvcUriComponentsBuilder.fromMappingName("CrawlC#show")
                .arg(0, campaign)
                .arg(1, savedCrawl)
                .build();
        }
    }

    @RequestMapping(value = "{crawl}", name = "show")
    public ModelAndView show(@PathVariable Campaign campaign, @PathVariable Crawl crawl)
            throws IOException {
        checkFound("campaign", campaign);
        checkFound("crawl", crawl);
        return new ModelAndView("crawl/show")
            .addObject("specification", jsonMapper.writeValueAsString(crawl))
            .addObject("executions", executionRepository.getExecutionsForCrawl(crawl))
            .addObject("tab", "crawl");
    }

    @RequestMapping(value = "{crawl}/editJSON", method = RequestMethod.GET)
    public ModelAndView editJson(@PathVariable Campaign campaign, @PathVariable Crawl crawl)
            throws IOException {
        checkFound("campaign", campaign);
        checkFound("crawl", crawl);
        return new ModelAndView("crawl/edit-json")
            .addObject("specification", jsonMapper.writeValueAsString(crawl.getSpecification()))
            .addObject("tab", "edit");
    }

    @RequestMapping(value = "{crawl}/editJSON", method = RequestMethod.POST)
    public ModelAndView storeJson(@PathVariable Campaign campaign, @PathVariable Crawl crawl,
            @RequestParam("specification") @Valid CrawlSpecification specification) {

        crawl.setSpecification(CrawlSpecification.upgrade(specification));
        repository.updateCrawl(crawl);
        return new ModelAndView("redirect:/campaign/" + campaign.getId() + "/" + crawl.getId());
    }

    @RequestMapping("{crawl}/edit")
    public ModelAndView edit(@PathVariable Campaign campaign, @PathVariable Crawl crawl) {
        checkFound("campaign", campaign);
        checkFound("crawl", crawl);
        String actionUri = String.format("/campaign/%d/%d/", campaign.getId(), crawl.getId());
        return editForm(campaign, crawl, actionUri);
    }


    @RequestMapping(value = "{originalCrawl}", method = RequestMethod.POST)
    public ModelAndView update(@PathVariable Campaign campaign, @PathVariable Crawl originalCrawl,
            @Valid Crawl crawl, BindingResult result) {
        checkFound("campaign", campaign);
        checkFound("crawl", originalCrawl);
        if (result.hasErrors()) {
            String actionUri = String.format("/campaign/%d/%d/", campaign.getId(), crawl.getId());
            return editForm(campaign, crawl, actionUri);
        }
        repository.updateCrawl(crawl);
        return redirectToCampaign(campaign.getId());
    }

    @RequestMapping(value = "{crawlId}", method = RequestMethod.DELETE)
    public ModelAndView delete(@PathVariable long campaign, @PathVariable long crawlId,
            RedirectAttributes redirectAttrs) {
        crawlManager.deleteCrawl(crawlId);
        redirectAttrs.addFlashAttribute("message", "Successfully deleted crawl #" + crawlId);
        return redirectToCampaign(campaign);
    }

    @RequestMapping("{crawl}/status")
    public ModelAndView status(@PathVariable Campaign campaign, @PathVariable Crawl crawl,
            @RequestParam(defaultValue = "false") boolean details) {
        checkFound("campaign", campaign);
        checkFound("crawl", crawl);
        CrawlStatistics statistics = crawlManager.getCrawlStatistics(campaign.getId(), details);
        logger.debug("details requested: {}", details);
        logger.info("crawl statistics: {}", statistics);
        return new ModelAndView("crawl/statistics").addObject("statistics", statistics).addObject(
            "tab", "statistics");
    }

    @RequestMapping(value = "{crawlId}/stop", method = RequestMethod.POST)
    public String stop(@PathVariable long campaign, @PathVariable long crawlId,
            RedirectAttributes flash) {
        CrawlStatus status = crawlManager.stopCrawl(crawlId);
        flash.addFlashAttribute("message", "Stopping crawl #" + crawlId + ": " + status);
        return "redirect:/status/";
    }

    @RequestMapping(value = "/{crawlId}/start", method = RequestMethod.POST)
    public String start(@PathVariable long campaign, @PathVariable long crawlId) {
        crawlManager.startCrawl(crawlId);
        return "redirect:/status/";
    }

    @RequestMapping(value = "/{crawlId}/export", method = RequestMethod.POST)
    public String export(@PathVariable long campaign, @PathVariable long crawlId) {
        // TODO get export path from user
        crawlManager.exportCampaign(campaign, "/tmp/export-" + crawlId);
        return "redirect:/status/";
    }
}
