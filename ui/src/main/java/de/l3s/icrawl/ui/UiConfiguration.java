package de.l3s.icrawl.ui;

import java.io.File;
import java.nio.charset.StandardCharsets;

import javax.annotation.PostConstruct;
import javax.servlet.Filter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import nz.net.ultraq.thymeleaf.LayoutDialect;

import org.apache.jasper.servlet.JspServlet;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.webapp.WebAppContext;
import org.eclipse.jetty.webapp.WebAppContext.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafAutoConfiguration;
import org.springframework.boot.autoconfigure.velocity.VelocityAutoConfiguration;
import org.springframework.boot.autoconfigure.web.HttpMapperProperties;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.repository.support.DomainClassConverter;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.messageresolver.SpringMessageResolver;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;
import com.fasterxml.jackson.datatype.joda.JodaModule;

import de.l3s.icrawl.DbConfig;
import de.l3s.icrawl.repository.RepositoriesBase;
import de.l3s.icrawl.service.CampaignService;
import de.l3s.icrawl.util.HadoopUtils;

import static org.thymeleaf.templatemode.StandardTemplateModeHandlers.HTML5;

@Configuration
@Import({ DbConfig.class, MvcConfig.class })
@EnableAutoConfiguration(exclude = {ThymeleafAutoConfiguration.class, VelocityAutoConfiguration.class })
@EnableCaching
@PropertySource("classpath:application.properties")
@ComponentScan(basePackageClasses = { UiConfiguration.class, CampaignService.class,
        RepositoriesBase.class })
@EnableSpringDataWebSupport
public class UiConfiguration extends AbstractAnnotationConfigDispatcherServletInitializer {
    @SuppressWarnings("hiding")
    private static final Logger logger = LoggerFactory.getLogger(UiConfiguration.class);

    @Bean
    ITemplateResolver templateResolver() {
        SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
        templateResolver.setCacheTTLMs(1L);
        templateResolver.setTemplateMode(HTML5.getTemplateModeName());
        templateResolver.setPrefix("classpath:/templates/");
        templateResolver.setSuffix(".html");
        return templateResolver;
    }

    @Bean
    SpringTemplateEngine templateEngine() {
        SpringTemplateEngine engine = new SpringTemplateEngine();
        engine.setTemplateResolver(templateResolver());
        engine.addDialect(new LayoutDialect());
        SpringMessageResolver messageResolver = new SpringMessageResolver();
        messageResolver.setMessageSource(messageSource());
        engine.setMessageResolver(messageResolver);
        return engine;
    }

    @Bean
    ViewResolver viewResolver() {
        ThymeleafViewResolver resolver = new ThymeleafViewResolver();
        resolver.setExcludedViewNames(new String[] { "graph", "*.jsp" });
        resolver.setTemplateEngine(templateEngine());
        resolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
        resolver.setOrder(1);
        return resolver;
    }

    @Bean
    MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:/messages/messages");
        messageSource.setFallbackToSystemLocale(false);
        return messageSource;
    }

    @Bean
    static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        logger.info("Initializing propertySourcesPlaceholderConfigurer bean");
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    org.apache.hadoop.conf.Configuration conf(@Value("${icrawl.directory}") File appDirectory) {
        return HadoopUtils.createConf(appDirectory);
    }

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return null;
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[] { UiConfiguration.class, ServicesConfig.class };
    }

    @Override
    protected String[] getServletMappings() {
        return new String[] { "/", "*,jsp" };
    }

    @Override
    protected Filter[] getServletFilters() {
        return new Filter[] { characterEncodingFilter(), hiddenHttpMethodFilter(), corsFilter() };
    }

    @Bean
    Filter corsFilter() {
        return new SimpleCORSFilter();
    }

    @Bean
    HiddenHttpMethodFilter hiddenHttpMethodFilter() {
        return new HiddenHttpMethodFilter();
    }

    @Bean
    CharacterEncodingFilter characterEncodingFilter() {
        CharacterEncodingFilter encodingFilter = new CharacterEncodingFilter();
        encodingFilter.setEncoding(StandardCharsets.UTF_8.name());
        return encodingFilter;
    }


    @Bean
    EmbeddedServletContainerCustomizer servletContainerCustomizer() {
        return new EmbeddedServletContainerCustomizer() {

            @Override
            public void customize(ConfigurableEmbeddedServletContainer container) {
                container.setRegisterJspServlet(false);
            }
        };
    }

    @Bean
    ServletRegistrationBean jspServlet() {
        JspServlet jspServlet = new JspServlet();
        return new ServletRegistrationBean(jspServlet, "*.jsp") {
            @Override
            public void onStartup(ServletContext servletContext) throws ServletException {
                super.onStartup(servletContext);
                if (servletContext instanceof WebAppContext.Context) {
                    WebAppContext.Context jettyContext = (Context) servletContext;
                    Resource baseResource = Resource.newClassPathResource("/jsp");
                    logger.info("baseResource={}", baseResource);
                    jettyContext.getContextHandler().setBaseResource(baseResource);
                } else {
                    throw new ServletException("Unknown context class " + servletContext);
                }
            }
        };
    }

    @Bean
    CacheManager cacheManager() {
        return new GuavaCacheManager();
    }

    @Bean
    HttpMessageConverters httpMessageConverters() {
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
        messageConverter.setObjectMapper(objectMapper());
        return new HttpMessageConverters(messageConverter);
    }

    @Bean
    ObjectMapper objectMapper() {
        return new ObjectMapper()
            .registerModules(new JodaModule(), new GuavaModule(), new Hibernate4Module())
            .enable(SerializationFeature.INDENT_OUTPUT)
            .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    @Autowired
    HttpMapperProperties jacksonProperties;

    @PostConstruct
    void configure() {
        jacksonProperties.setJsonPrettyPrint(true);
        jacksonProperties.setJsonSortKeys(true);
    }

    @Bean
    @Autowired
    public DomainClassConverter<FormattingConversionService> domainClassConverter(
            FormattingConversionService cs) {
        return new DomainClassConverter<FormattingConversionService>(cs);
    }
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(UiConfiguration.class);
        app.run(args);
    }
}
