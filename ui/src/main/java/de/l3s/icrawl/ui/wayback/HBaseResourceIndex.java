package de.l3s.icrawl.ui.wayback;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;

import org.apache.nutch.storage.WebPage;
import org.apache.nutch.util.TableUtil;
import org.archive.wayback.ResourceIndex;
import org.archive.wayback.core.CaptureSearchResult;
import org.archive.wayback.core.CaptureSearchResults;
import org.archive.wayback.core.SearchResults;
import org.archive.wayback.core.UrlSearchResults;
import org.archive.wayback.core.WaybackRequest;
import org.archive.wayback.exception.AccessControlException;
import org.archive.wayback.exception.BadQueryException;
import org.archive.wayback.exception.ResourceIndexNotAvailableException;
import org.archive.wayback.exception.ResourceNotInArchiveException;
import org.archive.wayback.webapp.PerfStats;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Objects;

import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate.ResultMapper;
import static de.l3s.icrawl.util.WebPageUtils.mapHttpStatus;

public class HBaseResourceIndex implements ResourceIndex {
    public static final String CAMPAIGN_ID_KEY = "CAMPAIGN_ID";
    private static final Logger logger = LoggerFactory.getLogger(HBaseResourceIndex.class);
    private final CrawlDataRepositoryTemplate template;
    private final long campaignId;

    HBaseResourceIndex(CrawlDataRepositoryTemplate template, long campaignId) {
        this.template = template;
        this.campaignId = campaignId;
    }

    private static final class CaptureResultMapper implements ResultMapper<CaptureSearchResult> {
        private final String urlKey;
        private final long campaignId;

        private CaptureResultMapper(long campaignId, String urlKey) {
            this.campaignId = campaignId;
            this.urlKey = urlKey;
        }

        @Override
        public CaptureSearchResult map(String key, WebPage page) {
            if (page.getStatus() >= 500 || !Objects.equal(key, urlKey)) {
                return null;
            }
            CaptureSearchResult result = new CaptureSearchResult();
            result.setUrlKey(key);
            result.setCaptureDate(new Date(page.getFetchTime()));
            result.setRedirectUrl("-");
            result.setOriginalHost(getOriginalHost(page));
            if (page.getBaseUrl() != null) {
                result.setOriginalUrl(page.getBaseUrl().toString());
            }
            result.setHttpCode(String.valueOf(mapHttpStatus(page.getProtocolStatus())));
            result.putCustom(CAMPAIGN_ID_KEY, String.valueOf(campaignId));
            if (page.getContentType() != null) {
                result.setMimeType(page.getContentType().toString());
            }
            logger.trace("Found result {}@{}", result.getUrlKey(), result.getCaptureDate());
            return result;
        }
    }

    enum PerfStat {
        INDEX_LOAD
    }

    @Override
    public SearchResults query(WaybackRequest request) throws ResourceIndexNotAvailableException,
            ResourceNotInArchiveException, BadQueryException, AccessControlException {
        logger.info("query({})", request.getRequestUrl());
        try {
            PerfStats.timeStart(PerfStat.INDEX_LOAD);
            return doQuery(request);
        } catch (IOException e) {
            logger.info("Exception in query {}:", request, e);
            throw new ResourceIndexNotAvailableException(e.getMessage());
        } finally {
            PerfStats.timeEnd(PerfStat.INDEX_LOAD);
        }
    }

    private SearchResults doQuery(WaybackRequest wbRequest) throws BadQueryException,
            ResourceNotInArchiveException, IOException {
        String urlKey = null;
        try {
            urlKey = TableUtil.reverseUrl(wbRequest.getRequestUrl());
        } catch (MalformedURLException ue) {
            throw new BadQueryException(ue.toString());
        }

        SearchResults searchResults;

        if (wbRequest.isReplayRequest() || wbRequest.isCaptureQueryRequest()) {
            searchResults = getCaptureResults(wbRequest, urlKey);
        } else if (wbRequest.isUrlQueryRequest()) {
            searchResults = getUrlSearchResults(wbRequest);
        } else {
            throw new BadQueryException("Unknown Query Type");
        }
        if (searchResults.getReturnedCount() == 0) {
            throw new ResourceNotInArchiveException(wbRequest.getRequestUrl() + " was not found");
        }
        return searchResults;
    }

    private UrlSearchResults getUrlSearchResults(WaybackRequest wbRequest) {
        throw new UnsupportedOperationException("getUrlSearchResults");
    }

    private CaptureSearchResults getCaptureResults(WaybackRequest wbRequest, final String urlKey)
            throws IOException {
        logger.info("getCaptureResults({})", urlKey);
        Date replayDate = wbRequest.getReplayDate();
        List<CaptureSearchResult> results = template.query(campaignId, new String[] { "status",
                "fetchTime", "protocolStatus", "contentType", "baseUrl" }, urlKey, 100,
            new CaptureResultMapper(campaignId, urlKey));
        CaptureSearchResult closest = null;
        long closestTimeDiff = Long.MAX_VALUE;

        CaptureSearchResults ret = new CaptureSearchResults();
        for (CaptureSearchResult csr : results) {
            if (csr == null) {
                continue;
            }
            ret.addSearchResult(csr);
            long timeDiff = timeDiff(replayDate, csr);
            if (closest == null || timeDiff < closestTimeDiff) {
                closest = csr;
                closestTimeDiff = timeDiff;
            }
        }
        logger.debug("got {} results for {}", ret.size(), urlKey);
        if (closest != null) {
            ret.setClosest(closest);
        }
        ret.setReturnedCount(ret.size());
        return ret;
    }

    private static String getOriginalHost(WebPage page) {
        CharSequence baseUrl = page.getBaseUrl();
        if (baseUrl == null) {
            return "";
        }
        try {
            return new URL(baseUrl.toString()).getHost();
        } catch (MalformedURLException e) {
            return baseUrl.toString();
        }
    }

    private long timeDiff(Date replayDate, CaptureSearchResult csr) {
        return Math.abs(csr.getCaptureDate().getTime() - replayDate.getTime());
    }

    @Override
    public void shutdown() throws IOException {}
}
