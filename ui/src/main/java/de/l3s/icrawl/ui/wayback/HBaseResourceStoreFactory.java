package de.l3s.icrawl.ui.wayback;

import org.archive.wayback.ResourceStore;

import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate;

public class HBaseResourceStoreFactory {

    private final CrawlDataRepositoryTemplate template;

    public HBaseResourceStoreFactory(CrawlDataRepositoryTemplate template) {
        this.template = template;
    }

    public ResourceStore get(long crawlId) {
        return new HBaseResourceStore(template);
    }
}
