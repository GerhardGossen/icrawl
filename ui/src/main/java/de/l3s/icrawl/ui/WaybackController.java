package de.l3s.icrawl.ui;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import de.l3s.icrawl.ui.wayback.CrawlResourceRequestMapper;

@Controller
@RequestMapping(WaybackController.PATH_MAPPING)
public class WaybackController {
    private static final Logger logger = LoggerFactory.getLogger(WaybackController.class);
    static final String PATH_MAPPING = "wayback";
    @Inject
    CrawlResourceRequestMapper requestMapper;

    @RequestMapping("{campaignId:\\d+}/**")
    public void wayback(HttpServletRequest request, HttpServletResponse response,
            @PathVariable("campaignId") long campaignId) throws NotFoundException,
            ServletException,
            IOException {
        String uri = extractUri(request);
        logger.debug("About to handle request for crawl {}, url {}", campaignId, uri);
        boolean handled = requestMapper.handleRequest(request, response, campaignId, uri);
        if (!handled) {
            throw new NotFoundException(uri);
        }
    }

    static String extractUri(HttpServletRequest request) {
        String requestUri = request.getRequestURI();
        int pos = requestUri.indexOf(PATH_MAPPING);
        if (pos < 0) {
            return null;
        }
        pos = requestUri.indexOf("/", pos + PATH_MAPPING.length() + 1);
        return pos >= 0 ? requestUri.substring(pos + 1) : null;
    }

    @RequestMapping("{crawlId:\\d+}/jsp/graph")
    public String waybackJsp() {
        return "graph.jsp";
    }
}
