package de.l3s.icrawl.ui;

import java.util.Arrays;

import javax.inject.Inject;

import org.archive.wayback.ReplayDispatcher;
import org.archive.wayback.ReplayRenderer;
import org.archive.wayback.RequestParser;
import org.archive.wayback.archivalurl.ArchivalUrlCSSReplayRenderer;
import org.archive.wayback.archivalurl.ArchivalUrlJSReplayRenderer;
import org.archive.wayback.archivalurl.ArchivalUrlRequestParser;
import org.archive.wayback.archivalurl.ArchivalUrlResultURIConverterFactory;
import org.archive.wayback.archivalurl.ArchivalUrlSAXRewriteReplayRenderer;
import org.archive.wayback.archivalurl.FastArchivalUrlReplayParseEventHandler;
import org.archive.wayback.replay.DefaultClosestResultSelector;
import org.archive.wayback.replay.HttpHeaderProcessor;
import org.archive.wayback.replay.IdentityHttpHeaderProcessor;
import org.archive.wayback.replay.JSPReplayRenderer;
import org.archive.wayback.replay.RedirectRewritingHttpHeaderProcessor;
import org.archive.wayback.replay.ReplayRendererSelector;
import org.archive.wayback.replay.SelectorReplayDispatcher;
import org.archive.wayback.replay.TransparentReplayRenderer;
import org.archive.wayback.replay.charset.RotatingCharsetDetector;
import org.archive.wayback.replay.html.ContextResultURIConverterFactory;
import org.archive.wayback.replay.selector.AlwaysMatchSelector;
import org.archive.wayback.replay.selector.IdentityRequestSelector;
import org.archive.wayback.replay.selector.MimeTypeSelector;
import org.archive.wayback.replay.selector.RedirectSelector;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.google.common.collect.ImmutableList;

import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate;
import de.l3s.icrawl.ui.wayback.CrawlResourceRequestMapper;
import de.l3s.icrawl.ui.wayback.HBaseResourceIndexFactory;
import de.l3s.icrawl.ui.wayback.HBaseResourceStoreFactory;

@Configuration
public class WaybackConfiguration {
    @Inject
    org.apache.hadoop.conf.Configuration conf;
    //@Value("${wayback.replayPrefix}")
    String replayPrefix = "/wayback/";

    @Bean
    CrawlResourceRequestMapper requestHandler() {
        return new CrawlResourceRequestMapper(resourceIndexFactory(), resourceStoreFactory(),
            requestParser(), replay(), uriConverterFactory(), replayPrefix);
    }

    @Bean
    RequestParser requestParser() {
        ArchivalUrlRequestParser requestParser = new ArchivalUrlRequestParser();
        requestParser.init();
        return requestParser;
    }

    @Bean
    HBaseResourceIndexFactory resourceIndexFactory() {
        return new HBaseResourceIndexFactory(template());
    }

    @Bean
    CrawlDataRepositoryTemplate template() {
        return new CrawlDataRepositoryTemplate(conf);
    }

    @Bean
    HBaseResourceStoreFactory resourceStoreFactory() {
        return new HBaseResourceStoreFactory(template());
    }

    // TODO QueryRendererFactory

    @Bean
    ReplayDispatcher replay() {
        SelectorReplayDispatcher replay = new SelectorReplayDispatcher();
        replay.setClosestSelector(new DefaultClosestResultSelector());

        replay.setSelectors(ImmutableList.of(redirectSelector(), identityRequestSelector(),
            htmlSelector(), cssSelector(), jsSelector(), alwayMatchSelector()));
        return replay;
    }

    IdentityRequestSelector identityRequestSelector() {
        // Explicit (via "id_" flag) IDENTITY/RAW REPLAY
        IdentityRequestSelector idRequestSelector = new IdentityRequestSelector();
        idRequestSelector.setRenderer(new TransparentReplayRenderer(
            new IdentityHttpHeaderProcessor()));
        return idRequestSelector;
    }

    AlwaysMatchSelector alwayMatchSelector() {
        // DEFAULT-TRANSPARENT REPLAY
        AlwaysMatchSelector amSelector = new AlwaysMatchSelector();
        amSelector.setRenderer(archivalTransparentReplayRenderer());
        return amSelector;
    }

    MimeTypeSelector jsSelector() {
        // JS REPLAY
        MimeTypeSelector jsSelector = new MimeTypeSelector();
        jsSelector.setMimeContains(Arrays.asList("text/javascript", "application/javascript",
            "application/x-javascript"));
        jsSelector.setRenderer(archivalJSReplayRenderer());
        return jsSelector;
    }

    MimeTypeSelector cssSelector() {
        // CSS REPLAY
        MimeTypeSelector cssSelector = new MimeTypeSelector();
        cssSelector.setMimeContains(Arrays.asList("text/css"));
        cssSelector.setRenderer(archivalCSSReplayRenderer());
        return cssSelector;
    }

    MimeTypeSelector htmlSelector() {
        // HTML REPLAY
        MimeTypeSelector mtSelector = new MimeTypeSelector();
        mtSelector.setMimeContains(Arrays.asList("text/html", "application/xhtml"));
        mtSelector.setRenderer(archivalSAXReplayRenderer());
        return mtSelector;
    }

    @Bean
    ReplayRenderer archivalTransparentReplayRenderer() {
        return new TransparentReplayRenderer(httpHeaderProcessor());
    }

    @Bean
    ReplayRenderer archivalSAXReplayRenderer() {
        ArchivalUrlSAXRewriteReplayRenderer renderer = new ArchivalUrlSAXRewriteReplayRenderer(
            httpHeaderProcessor());
        renderer.setCharsetDetector(new RotatingCharsetDetector());
        FastArchivalUrlReplayParseEventHandler delegator = new FastArchivalUrlReplayParseEventHandler();
        delegator.setJspInsertPath("/replay/DisclaimChooser.jsp");
        renderer.setDelegator(delegator);
        return renderer;
    }

    @Bean
    ReplayRenderer archivalJSReplayRenderer() {
        ArchivalUrlJSReplayRenderer renderer = new ArchivalUrlJSReplayRenderer(
            httpHeaderProcessor());
        renderer.setJspInserts(Arrays.asList("/WEB-INF/replay/ArchiveCSSComment.jsp"));
        return renderer;
    }

    @Bean
    ReplayRenderer archivalCSSReplayRenderer() {
        ArchivalUrlCSSReplayRenderer renderer = new ArchivalUrlCSSReplayRenderer(
            httpHeaderProcessor());
        renderer.setJspInserts(Arrays.asList("/WEB-INF/replay/ArchiveCSSComment.jsp"));
        return renderer;
    }

    @Bean
    HttpHeaderProcessor httpHeaderProcessor() {
        RedirectRewritingHttpHeaderProcessor processor = new RedirectRewritingHttpHeaderProcessor();
        processor.setPrefix("X-Archive-Orig-");
        return processor;
    }

    @Bean
    ReplayRendererSelector redirectSelector() {
        RedirectSelector redirectSelector = new RedirectSelector();
        JSPReplayRenderer renderer = new JSPReplayRenderer();
        renderer.setTargetJsp("/replay/UrlRedirectNotice.jsp");
        redirectSelector.setRenderer(renderer);
        return redirectSelector;
    }

    @Bean
    ContextResultURIConverterFactory uriConverterFactory() {
        return new ArchivalUrlResultURIConverterFactory();
    }

    @Bean
    public ViewResolver jspViewResolver() {
        InternalResourceViewResolver jspViewResolver = new InternalResourceViewResolver();
        jspViewResolver.setPrefix("/jsp/");
        jspViewResolver.setSuffix("");
        return jspViewResolver;
    }
}
