package de.l3s.icrawl.ui.format;

import de.l3s.icrawl.domain.specification.CrawlUrl;

public class CrawlUrlPropertyEditor extends LinePropertyEditor<CrawlUrl> {

    @Override
    protected CrawlUrl convertLine(String line) {
        return new CrawlUrl(line);
    }

    @Override
    protected String printItem(CrawlUrl value) {
        return value.getUrl();
    }
}