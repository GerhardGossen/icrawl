package de.l3s.icrawl.ui;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.AbstractJsonpResponseBodyAdvice;
import org.springframework.web.util.UriComponentsBuilder;

import de.l3s.icrawl.domain.evaluation.SessionLog;
import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.domain.specification.Crawl;
import de.l3s.icrawl.service.CampaignService;
import de.l3s.icrawl.ui.wizard.FormError;
import de.l3s.icrawl.ui.wizard.FormResponse;
import de.l3s.icrawl.ui.wizard.FormSuccess;
import de.l3s.icrawl.ui.wizard.SeedSearchResult;
import de.l3s.icrawl.ui.wizard.SeedSearcher;

import static com.google.common.base.Preconditions.checkArgument;

@RestController
@RequestMapping("/wizard")
public class CrawlWizardController {
    private static final Logger logger = LoggerFactory.getLogger(CrawlWizardController.class);
    private final CampaignService repository;
    private final SeedSearcher searcher;

    @ControllerAdvice
    private static class JsonpAdvice extends AbstractJsonpResponseBodyAdvice {

        public JsonpAdvice() {
            super("callback");
        }
    }

    @Inject
    public CrawlWizardController(CampaignService repository, SeedSearcher searcher) {
        this.repository = repository;
        this.searcher = searcher;
    }

    @RequestMapping("search")
    @Cacheable("search")
    public List<SeedSearchResult> search(@RequestParam("q") String query,
            @RequestParam(value = "source", defaultValue = "web") String source) {
        return searcher.search(source, query);
    }

    @RequestMapping(value = "create", method = RequestMethod.POST)
    public ResponseEntity<? extends FormResponse> create(@RequestBody @Valid Crawl crawl,
            BindingResult errors, UriComponentsBuilder pathBuilder) {
        if (errors.hasErrors()) {
            return new ResponseEntity<>(new FormError(errors), HttpStatus.BAD_REQUEST);
        } else {
            Campaign campaign = repository.getCampaign(crawl.getCampaignId());
            crawl.setCampaign(campaign);
            repository.addCrawl(crawl);
            String url = buildCampaignPath(pathBuilder, crawl);
            logger.debug("Redirecting to URL {}", url);
            return new ResponseEntity<>(new FormSuccess(url), HttpStatus.CREATED);
        }
    }

    private String buildCampaignPath(UriComponentsBuilder pathBuilder, Crawl crawl) {
        return pathBuilder.path("/campaign/{id}")
            .buildAndExpand(crawl.getCampaign().getId())
            .toUriString();
    }

    @RequestMapping(value = "edit/{campaign}/{crawlId}", method = RequestMethod.POST)
    public ResponseEntity<FormResponse> edit(@RequestBody @Valid Crawl crawl,
            BindingResult errors, UriComponentsBuilder pathBuilder,
            @PathVariable("campaign") Campaign campaign, @PathVariable("crawlId") long crawlId) {
        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().<FormResponse> body(new FormError(errors));
        } else {
            checkArgument(campaign.getId() == crawl.getCampaignId(),
                "Invalid campaign ID, expected %s, got %s", campaign.getId(), crawl.getCampaignId());
            crawl.setId(crawlId);
            crawl.setCampaign(campaign);
            repository.updateCrawl(crawl);
            String url = buildCampaignPath(pathBuilder, crawl);
            return ResponseEntity.<FormResponse> ok(new FormSuccess(url));
        }
    }

    @RequestMapping(value = "log", method = RequestMethod.POST)
    public ResponseEntity<String> log(@RequestBody @Valid SessionLog sessionLog) {
        return ResponseEntity.ok("Saved");
    }

    @ExceptionHandler
    public ResponseEntity<ErrorMessage> exceptionHandler(Exception ex) {
        logger.info("Got exception: ", ex);
        return new ResponseEntity<>(new ErrorMessage(ex.getMessage()), HttpStatus.BAD_REQUEST);
    }
}
