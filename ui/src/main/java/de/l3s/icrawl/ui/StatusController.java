package de.l3s.icrawl.ui;

import java.util.List;

import javax.inject.Inject;

import org.joda.time.DateTime;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.Lists;

import de.l3s.icrawl.crawlmanager.CrawlManager;
import de.l3s.icrawl.domain.status.AugmentedCrawlStatus;
import de.l3s.icrawl.domain.status.RunningCrawlStatus;
import de.l3s.icrawl.process.CrawlDbManager;
import de.l3s.icrawl.process.MessageBusManager;
import de.l3s.icrawl.process.MetadataDbManager;
import de.l3s.icrawl.repository.TaskExecutionRepository;
import de.l3s.icrawl.service.CampaignService;

@Controller
@RequestMapping("/status")
public class StatusController {

    @Inject
    CrawlDbManager crawlDbManager;

    @Inject
    MetadataDbManager metadataDbManager;

    @Inject
    CrawlManager crawlManager;

    @Inject
    MessageBusManager messageBusManager;

    @Inject
    CampaignService repository;

    @Inject
    TaskExecutionRepository executionRepository;

    @ModelAttribute("section")
    String section() {
        return "status";
    }

    // TODO split off crawl status display

    @RequestMapping("/")
    public ModelAndView status() {
        return new ModelAndView("status")
            .addObject("crawlDB", crawlDbManager)
            .addObject("metadataDB", metadataDbManager)
            .addObject("crawlManager", crawlManager)
            .addObject("messageBusManager", messageBusManager)
            .addObject("runningCrawls", getRunningCrawls())
            .addObject("scheduledTasks", executionRepository.findScheduledTasks());
    }

    private List<AugmentedCrawlStatus> getRunningCrawls() {
        List<RunningCrawlStatus> runningCrawls = crawlManager.getRunningCrawls();
        List<AugmentedCrawlStatus> ret = Lists.newArrayListWithExpectedSize(runningCrawls.size());
        for (RunningCrawlStatus crawlStatus : runningCrawls) {
            long campaign = crawlStatus.getCampaign().getId();
            long crawl = crawlStatus.getCrawl().getId();
            String name = repository.getCrawl(crawl).getName();
            DateTime startTime = crawlStatus.getStartTime();
            long executionId = crawlStatus.getExecutionId();
            int batchSeq = crawlStatus.getBatchSeq();
            String phase = crawlStatus.getPhase();
            double phaseProgress = crawlStatus.getPhaseProgress();
            ret.add(new AugmentedCrawlStatus(campaign, crawl, name, startTime, executionId,
                batchSeq, phase, phaseProgress));
        }
        return ret;
    }
}
