package de.l3s.icrawl.ui.wayback;

import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.annotation.PreDestroy;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.archive.wayback.ReplayDispatcher;
import org.archive.wayback.RequestParser;
import org.archive.wayback.ResultURIConverter;
import org.archive.wayback.query.Renderer;
import org.archive.wayback.replay.html.ContextResultURIConverterFactory;
import org.archive.wayback.webapp.AccessPoint;
import org.archive.wayback.webapp.WaybackCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;

public class CrawlResourceRequestMapper implements Closeable {
    private static final Logger logger = LoggerFactory.getLogger(CrawlResourceRequestMapper.class);
    private static final String REQUEST_CONTEXT_PREFIX = "webapp-request-context-path-prefix";

    class AccessPointCacheHandler extends CacheLoader<Long, AccessPoint> implements
            RemovalListener<Long, AccessPoint> {

        @Override
        public void onRemoval(RemovalNotification<Long, AccessPoint> notification) {
            logger.debug("Closing accesspoint for crawl {} because of {}", notification.getKey(),
                notification.getCause());
            WaybackCollection collection = notification.getValue().getCollection();
            try {
                collection.shutdown();
            } catch (IOException e) {
                throw Throwables.propagate(e);
            }
        }

        @Override
        public AccessPoint load(Long key) throws Exception {
            String context = String.valueOf(key);
            String pathPrefix = pathPrefix(context);
            ResultURIConverter uriConverter = uriConverterFactory.getContextConverter(pathPrefix);
            AccessPoint ap = new AccessPoint();
            WaybackCollection collection = new WaybackCollection();
            collection.setResourceIndex(resourceIndexFactory.get(key));
            collection.setResourceStore(resourceStoreFactory.get(key));
            ap.setCollection(collection);
            ap.setParser(parser);
            Renderer queryRenderer = new Renderer();
            queryRenderer.setQueryUriConverter(uriConverter);
            ap.setQuery(queryRenderer);
            ap.setReplay(replay);
            ap.setUriConverter(uriConverter);
            ap.setStaticPrefix("/");
            ap.setReplayPrefix("/");
            ap.setQueryPrefix(pathPrefix);
            logger.debug("using replayPrefix /");
            ap.setEnableMemento(false);
            return ap;
        }

    }

    private final HBaseResourceIndexFactory resourceIndexFactory;
    private final HBaseResourceStoreFactory resourceStoreFactory;
    private final RequestParser parser;
    private final ReplayDispatcher replay;
    private final ContextResultURIConverterFactory uriConverterFactory;
    private final String replayPrefix;

    private final LoadingCache<Long, AccessPoint> accessPointCache;

    public CrawlResourceRequestMapper(HBaseResourceIndexFactory resourceIndexFactory,
            HBaseResourceStoreFactory resourceStoreFactory, RequestParser requestParser,
            ReplayDispatcher replay, ContextResultURIConverterFactory uriConverterFactory,
            String replayPrefix) {
        this.resourceIndexFactory = resourceIndexFactory;
        this.resourceStoreFactory = resourceStoreFactory;
        this.parser = requestParser;
        this.replay = replay;
        this.uriConverterFactory = uriConverterFactory;
        this.replayPrefix = replayPrefix;
        AccessPointCacheHandler cacheHandler = new AccessPointCacheHandler();
        this.accessPointCache = CacheBuilder.newBuilder()
            .expireAfterAccess(60, TimeUnit.SECONDS)
            .removalListener(cacheHandler)
            .build(cacheHandler);
    }

    public boolean handleRequest(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
            long campaignId, String uri) throws IOException, ServletException {
        try {
            AccessPoint accessPoint = accessPointCache.get(campaignId);
            if (accessPoint != null) {
                accessPoint.setServletContext(httpRequest.getServletContext());
                httpRequest.setAttribute(REQUEST_CONTEXT_PREFIX, pathPrefix(campaignId));

                return accessPoint.handleRequest(httpRequest, httpResponse);
            } else {
                return false;
            }
        } catch (ExecutionException e) {
            throw new IOException("Could not create AccessPoint for campaign " + campaignId, e);
        }
    }

    String pathPrefix(long campaignId) {
        return pathPrefix(Long.toString(campaignId));
    }

    @Override
    @PreDestroy
    public void close() throws IOException {
        accessPointCache.invalidateAll();
    }

    String pathPrefix(String campaignId) {
        return replayPrefix + campaignId + "/";
    }

}
