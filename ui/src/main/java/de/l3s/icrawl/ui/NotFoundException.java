package de.l3s.icrawl.ui;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@ResponseStatus(NOT_FOUND)
public class NotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private final String resource;

    public NotFoundException(String resource) {
        super();
        this.resource = resource;
    }

    @Override
    public String getMessage() {
        return "Could not find resource for URL '" + resource + "'";
    }
}
