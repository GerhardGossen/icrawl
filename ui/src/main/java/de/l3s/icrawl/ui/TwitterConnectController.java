package de.l3s.icrawl.ui;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import de.l3s.icrawl.domain.api.OAuthAccessToken;
import de.l3s.icrawl.service.OAuthAccessTokenService;


@Controller
@RequestMapping("/twitter")
public class TwitterConnectController {

    @Inject
    OAuthAccessTokenService tokenService;

    @RequestMapping("{baseToken}")
    public String connect(HttpSession session, @PathVariable OAuthAccessToken baseToken) throws TwitterException {
        String callbackUri = ServletUriComponentsBuilder.fromCurrentRequest()
            .pathSegment("callback")
            .build().toUriString();
        Twitter twitter = getConnection(baseToken);
        twitter.setOAuthAccessToken(null);
        RequestToken requestToken = twitter.getOAuthRequestToken(callbackUri);
        session.setAttribute("requestToken", requestToken);
        return "redirect:" + requestToken.getAuthorizationURL();
    }

    private Twitter getConnection(OAuthAccessToken baseToken) {
        Configuration conf = new ConfigurationBuilder()
            .setOAuthConsumerKey(baseToken.getConsumerKey())
            .setOAuthConsumerSecret(baseToken.getConsumerSecret())
            .build();
        return new TwitterFactory(conf).getInstance();
    }

    @RequestMapping("{baseToken}/callback")
    public String callback(@RequestParam("oauth_verifier") String verifier,
            HttpSession session, @PathVariable OAuthAccessToken baseToken) throws TwitterException {
        Twitter twitter = getConnection(baseToken);
        RequestToken requestToken = (RequestToken) session.getAttribute("requestToken");
        AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);
        session.removeAttribute("requestToken");
        OAuthAccessToken oAuthAccessToken = new OAuthAccessToken();
        oAuthAccessToken.setService("twitter");
        oAuthAccessToken.setConsumerKey(baseToken.getConsumerKey());
        oAuthAccessToken.setConsumerSecret(baseToken.getConsumerSecret());
        oAuthAccessToken.setAccessToken(accessToken.getToken());
        oAuthAccessToken.setAccessTokenSecret(accessToken.getTokenSecret());
        oAuthAccessToken.setEnabled(true);
        tokenService.add(oAuthAccessToken);
        return "redirect:/settings/";
    }
}
