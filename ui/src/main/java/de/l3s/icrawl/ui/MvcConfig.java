package de.l3s.icrawl.ui;

import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.GenericConverter;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.datetime.joda.JodaTimeFormatterRegistrar;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

import com.google.common.collect.ImmutableSet;

import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.domain.specification.Crawl;

@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {

    public static class CrawlStringifier implements GenericConverter {

        private final static Set<ConvertiblePair> TYPES = ImmutableSet.of(new ConvertiblePair(
            Crawl.class, String.class), new ConvertiblePair(Campaign.class, String.class));

        @Override
        public Set<ConvertiblePair> getConvertibleTypes() {
            return TYPES;
        }

        @Override
        public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
            if (source instanceof Crawl) {
                return ((Crawl) source).getId().toString();
            } else if (source instanceof Campaign) {
                return ((Campaign) source).getId().toString();
            }
            return null;
        }

    }

    private static final Logger logger = LoggerFactory.getLogger(MvcConfig.class);
    @Autowired
    private RequestMappingHandlerAdapter requestMappingHandlerAdapter;

    @PostConstruct
    public void init() {
        requestMappingHandlerAdapter.setIgnoreDefaultModelOnRedirect(true);
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        logger.info("Adding view controllers");
        registry.addViewController("/about").setViewName("about");
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        logger.info("Adding formatters");
        JodaTimeFormatterRegistrar registrar = jodaTimeFormatter();
        registrar.registerFormatters(registry);
    }

    @Bean
    JodaTimeFormatterRegistrar jodaTimeFormatter() {
        JodaTimeFormatterRegistrar registrar = new JodaTimeFormatterRegistrar();
        registrar.setDateTimeStyle("MM");
        return registrar;
    }

    @Autowired
    FormattingConversionService conversionService;

    @PostConstruct
    void initialize() {
        // @Bean converter is not found, register manually
        conversionService.addConverter(crawlConverter());
    }

    GenericConverter crawlConverter() {
        return new CrawlStringifier();
    }

}