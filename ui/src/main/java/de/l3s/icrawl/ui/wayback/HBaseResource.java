package de.l3s.icrawl.ui.wayback;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.nutch.storage.WebPage;
import org.archive.util.binsearch.ByteBufferInputStream;
import org.archive.wayback.core.Resource;

import com.google.common.collect.Maps;

import de.l3s.icrawl.util.WebPageUtils;

public class HBaseResource extends Resource {

    private final WebPage page;

    public HBaseResource(WebPage page) {
        this.page = page;
        setInputStream(new ByteBufferInputStream(page.getContent()));
    }

    @Override
    public void close() throws IOException {
        page.clear();
    }

    @Override
    public int getStatusCode() {
        return WebPageUtils.mapHttpStatus(page.getProtocolStatus());
    }

    @Override
    public long getRecordLength() {
        return page.getContent().limit();
    }

    @Override
    public Map<String, String> getHttpHeaders() {
        Map<String, String> headers = Maps.newHashMapWithExpectedSize(page.getHeaders().size());
        for (Entry<CharSequence, CharSequence> entry : page.getHeaders().entrySet()) {
            headers.put(entry.getKey().toString(), entry.getValue().toString());
        }
        return headers;
    }

}
