package de.l3s.icrawl.ui;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

final class ControllerUtils {

    private ControllerUtils() {}

    static String asUri(ModelAndView methodCall) {
        return MvcUriComponentsBuilder.fromMethodCall(methodCall).build().toUriString();
    }

    public static <T> T checkFound(String type, T o) {
        if (o == null) {
            throw new NotFoundException(type);
        }
        return o;
    }

}
