package de.l3s.icrawl.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.domain.specification.Crawl;
import de.l3s.icrawl.domain.specification.CrawlSpecification;
import de.l3s.icrawl.domain.specification.CrawlUrl;
import de.l3s.icrawl.domain.specification.WeightingMethod;
import de.l3s.icrawl.service.CampaignService;
import de.l3s.icrawl.ui.format.KeywordPropertyEditor;

import static java.util.stream.Collectors.toSet;

@Controller
@RequestMapping("/evaluation_setup")
public class EvaluationSetupController {
    public enum Configuration {
        Unfocused, Topic, Time, TopicTime, TopicTimeTwitter
    }

    public static class SpecificationForm {
        private String name;
        private DateTime startTime;
        private Collection<String> seedUrls;
        private Collection<String> twitterQueries;
        private Collection<String> referenceUrls;
        private Collection<String> keywords;
        private Set<Configuration> configurations = EnumSet.allOf(Configuration.class);

        @NotEmpty
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }

        @NotNull
        @Future
        @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
        public DateTime getStartTime() {
            return startTime;
        }

        public void setStartTime(DateTime startTime) {
            this.startTime = startTime;
        }

        @NotEmpty
        public Collection<String> getSeedUrls() {
            return seedUrls;
        }

        public void setSeedUrls(Collection<String> seedUrls) {
            this.seedUrls = seedUrls;
        }

        @NotEmpty
        public Collection<String> getTwitterQueries() {
            return twitterQueries;
        }
        public void setTwitterQueries(Collection<String> twitterQueries) {
            this.twitterQueries = twitterQueries;
        }

        @NotEmpty
        public Collection<String> getReferenceUrls() {
            return referenceUrls;
        }
        public void setReferenceUrls(Collection<String> referenceUrls) {
            this.referenceUrls = referenceUrls;
        }

        @NotEmpty
        public Collection<String> getKeywords() {
            return keywords;
        }
        public void setKeywords(Collection<String> keywords) {
            this.keywords = keywords;
        }

        @NotEmpty
        public Set<Configuration> getConfigurations() {
            return configurations;
        }
        public void setConfigurations(Set<Configuration> configurations) {
            this.configurations = configurations;
        }
    }

    @Inject
    CampaignService repository;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Collection.class, "seedUrls", new KeywordPropertyEditor());
        binder.registerCustomEditor(Collection.class, "twitterQueries", new KeywordPropertyEditor());
        binder.registerCustomEditor(Collection.class, "referenceUrls", new KeywordPropertyEditor());
        binder.registerCustomEditor(Collection.class, "keywords", new KeywordPropertyEditor());
    }

    @RequestMapping
    public ModelAndView showForm() {
        SpecificationForm form = new SpecificationForm();
        return showForm(form, new BeanPropertyBindingResult(form, "specificationForm"));
    }

    private ModelAndView showForm(SpecificationForm form, BindingResult errors) {
        return new ModelAndView("evaluation_setup/form").addObject(form)
            .addObject("allConfigurations", EnumSet.allOf(Configuration.class));
    }

    @RequestMapping(method = RequestMethod.POST)
    @Transactional
    public ModelAndView save(@Valid SpecificationForm form, BindingResult errors) {
        if (errors.hasErrors()) {
            return showForm(form, errors);
        }
        List<Campaign> campaigns = new ArrayList<>();
        if (form.configurations.contains(Configuration.Unfocused)) {
            CrawlSpecification spec = new CrawlSpecification();
            spec.setSeeds(form.seedUrls.stream().map(CrawlUrl::new).collect(toSet()));
            spec.setStartTime(form.startTime);
            spec.setWeightingMethod(WeightingMethod.GRAPH_ONLY);
            campaigns.add(createCampaign(form, spec, "Unfocused"));
        }
        if (form.configurations.contains(Configuration.Topic)) {
            CrawlSpecification spec = new CrawlSpecification();
            spec.setSeeds(form.seedUrls.stream().map(CrawlUrl::new).collect(toSet()));
            spec.setReferenceDocuments(new HashSet<>(form.referenceUrls));
            spec.setKeywords(new HashSet<>(form.keywords));
            spec.setStartTime(form.startTime);
            spec.setWeightingMethod(WeightingMethod.DOCUMENT_VECTOR);
            campaigns.add(createCampaign(form, spec, "Topic"));
        }
        if (form.configurations.contains(Configuration.Time)) {
            CrawlSpecification spec = new CrawlSpecification();
            spec.setSeeds(form.seedUrls.stream().map(CrawlUrl::new).collect(toSet()));
            spec.setStartTime(form.startTime);
            spec.setWeightingMethod(WeightingMethod.FRESHNESS);
            campaigns.add(createCampaign(form, spec, "Time"));
        }
        if (form.configurations.contains(Configuration.TopicTime)) {
            CrawlSpecification spec = new CrawlSpecification();
            spec.setSeeds(form.seedUrls.stream().map(CrawlUrl::new).collect(toSet()));
            spec.setReferenceDocuments(new HashSet<>(form.referenceUrls));
            spec.setKeywords(new HashSet<>(form.keywords));
            spec.setStartTime(form.startTime);
            spec.setWeightingMethod(WeightingMethod.DV_FRESHNESS);
            campaigns.add(createCampaign(form, spec, "TopicTime"));
        }
        if (form.configurations.contains(Configuration.TopicTimeTwitter)) {
            CrawlSpecification spec = new CrawlSpecification();
            Set<CrawlUrl> seeds = new HashSet<>();
            seeds.addAll(form.seedUrls.stream().map(CrawlUrl::new).collect(toSet()));
            seeds.addAll(form.twitterQueries.stream()
                .map(q -> new CrawlUrl("twitter:" + q))
                .collect(toSet()));
            spec.setSeeds(seeds);
            spec.setReferenceDocuments(new HashSet<>(form.referenceUrls));
            spec.setKeywords(new HashSet<>(form.keywords));
            spec.setStartTime(form.startTime);
            spec.setWeightingMethod(WeightingMethod.DV_FRESHNESS);
            campaigns.add(createCampaign(form, spec, "TopicTimeTwitter"));
        }
        return new ModelAndView("evaluation_setup/success").addObject("campaigns", campaigns);
    }

    private Campaign createCampaign(SpecificationForm form, CrawlSpecification spec, String name) {
        Campaign c = new Campaign(form.name + " — " + name);
        c.setOwner("foo");
        Campaign campaign = repository.addCampaign(c);
        Crawl crawl = new Crawl();
        crawl.setSpecification(spec);
        crawl.setName(name);
        crawl.setCampaign(campaign);
        campaign.addCrawl(crawl);
        repository.addCrawl(crawl);
        return campaign;
    }
}
