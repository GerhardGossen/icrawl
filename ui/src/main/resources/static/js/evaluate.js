$(document).ready(function () {
  var
    sites = [],
    currentSite = -1,

    iframe = $('iframe'),
    liveVersionPill = $('.sidebar .version-switcher .live'),
    archivedVersionPill = $('.sidebar .version-switcher .archived'),
    ratingItems = $('.rating > .list-group-item'),
    submitBtn = $('.btn-success'),
    comment = $($('#comment')[0]),
    progressBar = $('.progress .progress-bar')
  ;

  $('#instructions').modal('show');
  $('#continueEvaluation .btn-success').click(function () {
    nextSite();
  });

  function switchToArchivedVersion() {
    iframe.attr('src', 'http://web.archive.org/web/2id_/' + sites[currentSite]);
    liveVersionPill.removeClass('active');
    archivedVersionPill.addClass('active');
  }

  function switchToLiveVersion() {
    iframe.attr('src', sites[currentSite]);
    liveVersionPill.addClass('active');
    archivedVersionPill.removeClass('active');
  }

  function setProgress(i) {
    var
      percent = i * 100 / sites.length,
      html = 'Progress: <b>' + i + '/' + sites.length + '</b>'
    ;

    progressBar.width(percent + '%');

    if (percent > 50) {
      progressBar.html(html);
      $(progressBar.parent().find('span')[0]).html('');
    } else {
      progressBar.html('');
      $(progressBar.parent().find('span')[0]).html(html);
    }
  }

  function nextSite() {
    function showNextSite() {
      currentSite++;
      ratingItems.removeClass('active');
      comment.val('');
      switchToArchivedVersion();
      setProgress(currentSite+1);
    }

    if (currentSite >= sites.length - 1) {
      $.get('/evaluation/evaluate/urls?page=0', function (data, result, response) {
        if (response.status !== 200) {
          throw new Error(result);
        }

        if (data.length == 0) {
          alert('It looks like you evaluated all of the sites!');
          return;
        }

        sites = sites.concat(data);
        showNextSite();
      });
    } else {
      showNextSite();
    }
  }
  
  function updateRatings(data) {
    var
      $tbody = $('#continueEvaluation table > tbody'), 
      i, $tr, $td;
    
    $tbody.html('');
    
    for (var i = 0; i < data.length; i++) {
      $tr = $('<tr>');
      
      $tr.append($('<td>').html(i+1));
      $tr.append($('<td>').html(data[i][2]));
      $tr.append($('<td>').html(data[i][1]));
      $tr.append($('<td>').html(data[i][3]));
      
      $tbody.append($tr);
    }
  }

  liveVersionPill.click(switchToLiveVersion);
  archivedVersionPill.click(switchToArchivedVersion);

  ratingItems.click(function (event) {
    $('.rating').removeClass('has-error');
    $('.rating').tooltip('hide');
    ratingItems.removeClass('active');
    $(event.target).addClass('active');
  });

  submitBtn.click(function () {
    var active = $('.rating > .list-group-item.active');

    if (active.length < 1) {
      $('.rating').addClass('has-error');
      $('.rating').tooltip('show');
      return;
    }

    if ($(active[0]).attr('data-value') === 'dontknow' && !comment.val()) {
      comment.parent().addClass('has-error');
      comment.tooltip('show');
      return;
    }

    $.post('/evaluation/evaluate/vote', {
      url: sites[currentSite],
      comment: comment.val(),
      relevance: active.attr('data-value')
    }).done(function (data) {
      updateRatings(data);
      
      if (currentSite == sites.length - 1) {
        $('#continueEvaluation').modal('show');
      } else {
        nextSite();
      }
    }).fail(function () {
      alert('Failed to send data to the server. Please make sure that you are connected to internet and try again.');
    });
  });

  comment.keypress(function () {
    comment.parent().removeClass('has-error');
    comment.tooltip('hide');
  });

  nextSite();
});
