Ext.define('iCrawlWizard.Application', {
  extend: 'Ext.app.Application',
  name: 'iCrawlWizard'
});

function makeId() {
  var
    characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
    id = '', i
  ;

  for (i = 0; i < 32; i++) {
    id += characters.charAt(Math.floor(Math.random() * characters.length));
  }

  return id;
}

iCrawlWizard.sessionId = makeId();
iCrawlWizard.logCache = [];
iCrawlWizard.log = function (message) {
  var date = new Date();

  iCrawlWizard.logCache.push({
    date: date,
    msg: message
  });

  Ext.log({
    msg: 'Logged user action at ' + Ext.Date.format(date, 'H:i:s') + ' - ' + JSON.stringify(message),
    level: 'info'
  });
};

setInterval(function () {
  if (iCrawlWizard.logCache.length === 0) {
    return;
  }

  var log = Ext.Array.clone(iCrawlWizard.logCache);
  iCrawlWizard.logCache = [];

  log = Ext.Array.map(log, function (item) {
    item.date = item.date.getTime();

    return item;
  });

  Ext.Ajax.request({
    url: ICRAWL_WIZARD_API_BASE_URL + '/log',
    jsonData: {
      sessionId: iCrawlWizard.sessionId,
      log: log
    },
    failure: function (response) {
      var result = JSON.parse(response.responseText);

      Ext.Msg.alert(result.error + ': ' + result.exception, result.message);
    }
  });
}, 1000);
