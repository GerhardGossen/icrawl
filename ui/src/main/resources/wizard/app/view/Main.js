Ext.define('iCrawlWizard.view.main.Main', {
  extend: 'Ext.panel.Panel',
  xtype: 'app-main',

  requires: [
    'iCrawlWizard.view.main.MainController'
  ],

  controller: 'main',

  title: 'iCrawl Wizard',

  layout: {
    type: 'vbox',
    align: 'stretch'
  },

  items: [
    {
      xtype: 'container',
      layout: {
        type: 'hbox',
        align: 'stretch'
      },
      flex: 1,
      items: [
        {
          xtype: 'search',
          flex: 4,
          sources: {
            web: 'Web',
            twitter: 'Twitter'
          }
        },
        {
          xtype: 'container',
          flex: 1,
          layout: {
            type: 'accordion',
            animate: true,
            multi: true
          },
          items: [
            {
              xtype: 'listbox',
              itemId: 'seeds',
              title: 'Seeds (URLs)',
              canAdd: true,
              validateFnFailedText: 'Please input valid URL (e.g. http://...)',
              validateFn: function (value) {
                return /^(http[s]?:\/\/|twitter:)/.test(value);
              },
              listData: typeof EXISTING_CRAWL !== 'undefined' ? EXISTING_CRAWL.specification.seeds.map(function (item) {
                return item.url;
              }) : []
            },
            {
              xtype: 'listbox',
              itemId: 'keywords',
              title: 'Keywords',
              canAdd: true,
              listData: typeof EXISTING_CRAWL !== 'undefined' ? EXISTING_CRAWL.specification.keywords : []
            },
            {
              xtype: 'listbox',
              itemId: 'entities',
              title: 'Entities',
              listData: typeof EXISTING_CRAWL !== 'undefined' ? EXISTING_CRAWL.specification.entities : [],
              listDataGetValue: function (item) {
                return item.labels[0].name
              }
            }
          ]
        }
      ]
    },
    {
      xtype: 'crawlinfoform',
      formData: typeof EXISTING_CRAWL !== 'undefined' ? EXISTING_CRAWL : {},
      listeners: {
        submit: 'onCrawlInfoFormSubmit'
      }
    }
  ]
});
