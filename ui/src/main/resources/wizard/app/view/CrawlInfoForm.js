Ext.define('iCrawlWizard.view.CrawlInfoForm', {
  extend: 'Ext.form.Panel',
  alias: 'wiget.crawlinfoform',
  xtype: 'crawlinfoform',
  cls: 'crawlinfoform',

  requires: [
    'Ext.layout.container.HBox',
    'Ext.container.Container',
    'Ext.form.field.Text',
    'Ext.form.field.Checkbox',
    'Ext.form.field.Date',
    'Ext.form.field.Time',
    'Ext.form.field.ComboBox',
    'Ext.button.Button'
  ],

  formData: null,

  listeners: {
    beforeaction: function (form, action) {
      if (action.type === 'submit') {
        if (!this.getForm().isValid()) {
          return false;
        }

        this.fireEvent('submit', this.getForm().getValues(false, false, true));

        return false;
      }
    },

    afterrender: function (form) {
      var
        data = {},
        fd = form.formData
      ;

      if (typeof fd !== 'object') {
        return;
      }

      form.query('[name=startDate]')[0].setMinValue(0);
      form.query('[name=endDate]')[0].setMinValue(0);

      if (typeof fd.specification === 'object') {
        fd = Ext.Object.merge(fd, fd.specification);

        if (typeof fd.crawlSchedule === 'object') {
          fd = Ext.Object.merge(fd, fd.crawlSchedule);
        }
      }

      if (fd.name) {
        data.name = fd.name;
      }

      if (typeof fd.startTime === 'number') {
        data.startDate = data.startTime = new Date(fd.startTime * 1000);
      }

      if (fd.interval) {
        data.repeat = fd.interval;
      }

      if (typeof fd.endTime === 'number' && fd.endTime > 0) {
        data.endDate = data.endTime = new Date(fd.endTime * 1000);
      }

      data.downloadResources = !!fd.downloadResources;

      form.getForm().setValues(data);

      setInterval(function () {
        if (!form.query('[name=now]')[0].getValue()) {
          return;
        }

        form.query('[name=startDate]')[0].setValue(new Date());
        form.query('[name=startTime]')[0].setValue(new Date());
      }, 1000);
    }
  },

  height: 40,
  border: true,

  layout: 'hbox',

  defaults: {
    margin: '8px 0 0 0'
  },

  items: [
    { xtype: 'container', width: 10 },
    {
      xtype: 'textfield',
      fieldLabel: 'Name',
      labelWidth: 45,
      flex: 1,
      allowBlank: false,
      name: 'name'
    },
    { xtype: 'container', width: 10 },
    {
      xtype: 'datefield',
      fieldLabel: 'Start time',
      format: 'd.m.Y',
      startDay: 1,
      labelWidth: 65,
      width: 210,
      minValue: new Date(),
      allowBlank: false,
      name: 'startDate'
    },
    { xtype: 'container', width: 5 },
    {
      xtype: 'timefield',
      format: 'H:i',
      width: 70,
      allowBlank: false,
      name: 'startTime'
    },
    { xtype: 'container', width: 10 },
    {
      xtype: 'checkbox',
      boxLabel: 'Now',
      name: 'now',
      checked: false,
      listeners: {
        change: function (box, checked) {
          var
            startDate = box.up().query('[name=startDate]')[0],
            startTime = box.up().query('[name=startTime]')[0]
          ;

          if (checked) {
            startDate.setValue(new Date());
            startTime.setValue(new Date());
            startDate.disable();
            startTime.disable();
          } else {
            startDate.enable();
            startTime.enable();
          }
        }
      }
    },
    { xtype: 'container', width: 10 },
    {
      xtype: 'combo',
      fieldLabel: 'Repetition',
      forceSelection: true,
      width: 250,
      value: 'ONCE',
      store: {
        xtype: 'store',
        fields: [ 'value' ],
        data : [
          { value: 'ONCE' },
          { value: 'DAILY' },
          { value: 'WEEKLY' },
          { value: 'MONTHLY' },
          { value: 'YEARLY' }
        ]
      },
      queryMode: 'local',
      displayField: 'value',
      valueField: 'value',
      labelWidth: 65,
      name: 'repeat'
    },
    { xtype: 'container', width: 10 },
    {
      xtype: 'datefield',
      fieldLabel: 'End time',
      format: 'd.m.Y',
      startDay: 1,
      width: 210,
      minValue: new Date(),
      labelWidth: 60,
      name: 'endDate'
    },
    { xtype: 'container', width: 5 },
    {
      xtype: 'timefield',
      format: 'H:i',
      width: 70,
      name: 'endTime'
    },
    { xtype: 'container', width: 10 },
    {
      xtype: 'checkbox',
      boxLabel: 'Download resources',
      name: 'downloadResources',
      checked: false
    },
    { xtype: 'container', width: 10 },
    {
      xtype: 'button',
      text: 'Start',
      width: 70,
      handler: function () {
        this.up('form').submit();
      }
    },
    { xtype: 'container', width: 10 }
  ]
});
