Ext.define('iCrawlWizard.component.listBox.store.ListItems', {
  extend: 'Ext.data.Store',
  model: 'iCrawlWizard.component.listBox.model.ListItem',

  proxy: {
    type: 'memory',
    reader: {
      type: 'json',
      rootProperty: 'items'
    }
  }
});
