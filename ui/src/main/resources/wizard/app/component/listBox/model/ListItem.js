Ext.define('iCrawlWizard.component.listBox.model.ListItem', {
  extend: 'Ext.data.Model',

  fields: [
    { name: 'data', type: 'auto' },
    { name: 'value', type: 'string' }
  ]
});
