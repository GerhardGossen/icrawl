Ext.define('iCrawlWizard.component.listBox.view.ListBox', {
  extend: 'Ext.grid.Panel',
  alias: 'wiget.listbox',
  xtype: 'listbox',
  cls: 'listbox',

  requires: [
    'Ext.grid.plugin.DragDrop',

    'iCrawlWizard.component.listBox.model.ListItem',
    'iCrawlWizard.component.listBox.store.ListItems'
  ],

  hideHeaders: true,
  rowLines: false,
  columns: [
    { dataIndex: 'value', flex: 1 },
    {
      width: 36,
      renderer: function (a, b, item, c, d, store) {
        var id = Ext.id();

        Ext.defer(function() {
          Ext.widget('button', {
            renderTo: id,
            cls: 'removeItemBtn',
            scale: 'small',
            handler: function() {
              Ext.Msg.confirm(
                'Remove item',
                'Are you sure want to remove "' + item.get('value') + '" from this list?',
                function (btn) {
                  if (btn === 'yes') {
                    store.remove(item);
                  }
                }
              );
            }
          });
        }, 50);

        return Ext.String.format('<div id="{0}"></div>', id);
      }
    }
  ],

  listData: [],
  listDataGetValue: null,
  canAdd: false,
  validateFn: undefined,
  validateFnFailedText: 'Validation function failed',

  addItem: function (data, value) {
    if (typeof this.validateFn === 'function') {
      if (!this.validateFn(data)) {
        throw new Error(this.validateFnFailedText);
      }
    }

    if (this.getStore().findExact('value', value) >= 0) {
      throw new Error('Item "' + value + '" is already exists in list');
    }

    this.getStore().add({ data: data, value: value });
  },

  addItemOrShowErrorOnFail: function (data, value, dontShowPromptOnFail) {
    var listbox = this;

    try {
      listbox.addItem.call(listbox, data, value);
    } catch (e) {
      Ext.Msg.alert('Error', e.message, function () {
        if (!dontShowPromptOnFail) {
          listbox.addItemMsg.call(listbox, data, value);
        }
      });
    }
  },

  addItemMsg: function (defaultValue) {
    var listbox = this.up('listbox') || this;

    defaultValue = typeof defaultValue === 'string' ? defaultValue : '';

    Ext.Msg.prompt('New item', 'Please enter new item value:', function (btn, text) {
      if (btn === 'ok') {
        listbox.addItemOrShowErrorOnFail.call(listbox, text, text);
      }
    }, this, false, defaultValue);
  },

  initComponent: function () {
    var
      me = this,
      data = Ext.isArray(this.listData) ? this.listData : []
    ;

    data = Ext.Array.map(data, function (item) {
      return {
        data: item,
        value: Ext.isFunction(me.listDataGetValue) ? me.listDataGetValue(item) : item
      }
    });

    this.store = Ext.create('iCrawlWizard.component.listBox.store.ListItems', {
      data: data
    });

    this.viewConfig = {
      plugins: {
        ptype: 'gridviewdragdrop',
        dragText: 'Drag and drop to set importance',
        dragGroup: this.id,
        dropGroup: this.id
      }
    };


    this.callParent(arguments);
  },

  constructor: function (config) {
    if (config.canAdd) {
      config.tbar = [{
        xtype: 'button',
        iconCls: 'addItemIcon',
        text: 'Add',
        handler: this.addItemMsg
      }];
    }

    this.callParent(arguments);
  }
});
