Ext.define('iCrawlWizard.component.search.view.Panel', {
  extend: 'Ext.container.Container',
  alias: 'widget.search',
  xtype: 'search',

  requires: [
    'iCrawlWizard.component.search.controller.SearchController',

    'iCrawlWizard.component.search.model.SearchResult',

    'iCrawlWizard.component.search.store.SearchResults',

    'iCrawlWizard.component.search.view.SearchField',
    'iCrawlWizard.component.search.view.SearchResults'
  ],

  controller: 'search',
  sources: {},

  layout: {
    type: 'vbox',
    align: 'stretch'
  },

  initComponent: function () {
    var
      sourceId,
      items = []
    ;

    for (sourceId in this.sources) {
      if (this.sources.hasOwnProperty(sourceId)) {
        items.push({
          xtype: 'searchresults',
          itemId: sourceId + 'SearchResults',
          title: this.sources[sourceId],
          border: true,
          flex: 1,
          scroll: 'vertical',
          store: Ext.create('iCrawlWizard.component.search.store.SearchResults', {
            searchSource: sourceId
          })
        });
      }
    }

    this.items = [
      {
        xtype: 'searchfield',
        height: 75,
        sources: this.sources
      },
      {
        xtype: 'container',
        flex: 1,
        layout: {
          type: 'hbox',
          align: 'stretch'
        },
        items: items
      }
    ];

    this.callParent(arguments);
  }
});
