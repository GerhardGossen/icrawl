Ext.define('iCrawlWizard.component.search.store.SearchResults', {
  extend: 'Ext.data.Store',
  model: 'iCrawlWizard.component.search.model.SearchResult',

  constructor: function (params) {
    this.callParent(arguments);

    this.setProxy({
      type: 'ajax',
      url: ICRAWL_WIZARD_API_BASE_URL + '/search?source=' + params.searchSource,
      timeout: 120 * 1000 // 2 minutes in milliseconds
    });
  }
});
