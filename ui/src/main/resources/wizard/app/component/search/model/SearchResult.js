Ext.define('iCrawlWizard.component.search.model.SearchResult', {
  extend: 'Ext.data.Model',

  fields: [
    { name: 'title', type: 'string' },
    { name: 'keywords', type: 'auto' },
    { name: 'entities', type: 'auto' },
    { name: 'description', type: 'string' },
    { name: 'url', type: 'string' }
  ]
});
