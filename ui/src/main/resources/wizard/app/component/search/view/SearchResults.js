Ext.define('iCrawlWizard.component.search.view.SearchResults', {
  extend: 'Ext.grid.Panel',
  alias: 'wiget.searchresults',
  xtype: 'searchresults',
  cls: 'searchresults',

  closable: true,
  closeAction: 'hide',
  hideHeaders: true,
  rowLines: false,
  margin: '0 10 10 10',

  listeners: {
    beforeselect: function () {
      return false;
    },
    cellclick: function (gridView, el, a, record) {
      this.fireEvent('urlClick', 'seeds', record.get('url'), record.get('url'), this.getStore().searchSource);
    },
    rowbodyclick: function (a, el) {
      var
        hoveredLink = Ext.query('a:hover', true, el)[0],
        list, data, value;

      if (!hoveredLink) {
        return;
      }

      list = hoveredLink.getAttribute('data-list');
      data = hoveredLink.getAttribute('data-data');
      value = hoveredLink.textContent || hoveredLink.innerText;

      if (!hoveredLink || !list) {
        return;
      }

      this.fireEvent('urlClick', list, data, value, this.getStore().searchSource);
    }
  },
  columns: [
    {
      dataIndex: 'title',
      tdCls: 'resultTitle',
      flex: 1
    }
  ],

  viewConfig: {
    enableTextSelection: true
  },

  features: [{
    ftype: 'rowbody',
    setupRowData: function(record, rowIndex, rowValues) {
      var keywords = record.get('keywords').map(function (keyword) {
        return "<a data-list='keywords' data-data='" + keyword + "'>" + keyword + "</a>";
      });

      var entities = record.get('entities').map(function (entity) {
        return "<a data-list='entities' data-data='" + JSON.stringify(entity) + "'>" + entity.labels[0].name + "</a>";
      });

      Ext.apply(rowValues, {
        rowBody: '<div class="resultBody">' +
          "<p class='url'><a href='" + record.get('url') + "' target='_blank'>" + record.get('url') + "</a></p>" +
          (keywords.length > 0 ? "<p class='keywords'><b>Keywords:</b> " + keywords.join(', ') + "</p>" : '' ) +
          (entities.length > 0 ? "<p class='entities'><b>Entities:</b> " + entities.join(', ') + "</p>" : '' ) +
          "<p class='description'>" + record.get('description') + "</p>" +
          "</div>",
        rowBodyColspan: this.view.headerCt.getColumnCount()
      });
    }
  }]
});
