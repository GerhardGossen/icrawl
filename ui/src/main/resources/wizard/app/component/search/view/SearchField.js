Ext.define('iCrawlWizard.component.search.view.SearchField', {
  extend: 'Ext.container.Container',
  alias: 'widget.searchfield',
  xtype: 'searchfield',
  cls: 'searchfield',

  requires: [
    'Ext.form.Label'
  ],

  margin: '20 40 20 40',
  layout: {
    type: 'vbox',
    align: 'stretch'
  },

  sources: {},

  setSourceEnabled: function (sourceName, enabled) {
    var source = this.query('checkboxfield[name=' + sourceName + ']')[0];

    source.suspendEvent('change');
    source.setValue(enabled);
    source.resumeEvent('change');
  },

  initComponent: function () {
    var
      me = this,
      sourceId, sources
    ;

    sources = [{
      xtype: 'label',
      text: 'Search sources:',
      margin: '3 40 0 0',
      style: {
        fontWeight: 'bold'
      }
    }];

    for (sourceId in this.sources) {
      if (this.sources.hasOwnProperty(sourceId)) {
        sources.push({
          xtype: 'checkboxfield',
          margin: '0 40 0 0',
          name: sourceId,
          boxLabel: this.sources[sourceId],
          checked: true,
          listeners: {
            change: function (field, newValue) {
              me.fireEvent('changeSearchSource', field.name, newValue);
            }
          }
        });
      }
    }

    this.items = [
      {
        xtype: 'container',
        height: 50,
        layout: {
          type: 'hbox',
          align: 'stretch'
        },
        items: [
          {
            xtype: 'textfield',
            height: 50,
            flex: 1,
            fieldStyle: {
              fontSize: '24px'
            },

            enableKeyEvents: true,
            listeners: {
              keyup: function (field, event) {
                if (event.keyCode !== 13) {
                  return
                }

                me.fireEvent('startSearch', field.value);
              }
            }
          },
          {
            xtype: 'button',
            text: 'Search',
            scale: 'large',
            listeners: {
              click: function (btn) {
                me.fireEvent('startSearch', btn.up().query('textfield')[0].value);
              }
            }
          }
        ]
      },
      {
        xtype: 'container',
        layout: 'hbox',
        items: sources
      }
    ];

    this.callParent(arguments);
  }
});