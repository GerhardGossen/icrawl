Ext.define('iCrawlWizard.component.search.controller.SearchController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.search',

  config: {
    control: {
      searchfield: {
        startSearch: function (query) {
          Ext.Array.map(this.findSearchResultsPanels(), function (searchResults) {
            searchResults.getStore().load({ params: { q: query } });
          }, this);
        },
        changeSearchSource: function (sourceName, enabled) {
          this.findSearchResultsPanel(sourceName).setVisible(enabled);
        }
      },

      searchresults: {
        hide: function (panel) {
          var sourceName = panel.itemId.replace(/SearchResults$/, '');

          this.view.fireEvent('toogleSearchSource', sourceName, false);
          this.getSearchField().setSourceEnabled(sourceName, false);
        },
        show: function (panel) {
          var sourceName = panel.itemId.replace(/SearchResults$/, '');

          this.view.fireEvent('toogleSearchSource', sourceName, true);
          this.getSearchField().setSourceEnabled(sourceName, true);
        },

        urlClick: function (list, data, value, context) {
          this.view.fireEvent('urlClick', list, data, value, context + ':' + this.lastSearchQuery);
        }
      }
    }
  },

  lastSearchQuery: '',

  startSearch: function (query) {
    this.view.fireEvent('startSearch', query);

    this.lastSearchQuery = query;
    Ext.Array.map(this.findSearchResultsPanels(), function (searchResults) {
      searchResults.getStore().load({ params: { q: query } });
    }, this);
  },

  getSearchField: function () {
    return this.view.query('searchfield')[0];
  },

  findSearchResultsPanels: function () {
    return this.view.query('searchresults');
  },

  findSearchResultsPanel: function (name) {
    return this.view.query('searchresults#' + name + 'SearchResults')[0];
  }
});
