Ext.define('iCrawlWizard.view.main.MainController', {
  extend: 'Ext.app.ViewController',

  requires: [
    'iCrawlWizard.component.search.view.Panel',
    'iCrawlWizard.component.listBox.view.ListBox',
    'iCrawlWizard.view.CrawlInfoForm'
  ],

  alias: 'controller.main',

  config: {
    control: {
      search: {
        urlClick: 'onSearchResultUrlClick',
        startSearch: function (query) {
          iCrawlWizard.log({
            type: 'search',
            target: query
          });
        },
        toogleSearchSource: function (sourceName, enabled) {
          iCrawlWizard.log({
            type: 'toggleSearchSource',
            targetType: sourceName,
            target: enabled
          });
        }
      },
      listbox: {
        log: function (data) {
          iCrawlWizard.log(data);
        }
      }
    }
  },

  onSearchResultUrlClick: function (list, data, value, context) {
    var listbox = this.getListbox(list);

    try {
      data = JSON.parse(data);
    } catch (e) {}

    listbox.addItemOrShowErrorOnFail.call(listbox, data, value, true, context);
  },

  onCrawlInfoFormSubmit: function (data) {
    var url = '/create', result, log;

    result = {
      name: data.name,
      campaignId: ICRAWL_WIZARD_CAMPAIGN_ID,
      specification: {
        startTime: this.getUnixTimeStamp(data.startDate, data.startTime),
        seeds: this.getListboxData('seeds'),
        entities: this.getListboxData('entities'),
        keywords: this.getListboxData('keywords'),
        downloadResources: data.downloadResources === 'on',
        crawlSchedule: {
          interval: data.repeat,
          endTime: this.getUnixTimeStamp(data.endDate, data.endTime)
        }
      }
    };

    if (typeof EXISTING_CRAWL === 'object' && EXISTING_CRAWL.id) {
      url = '/edit/' + ICRAWL_WIZARD_CAMPAIGN_ID + '/' + EXISTING_CRAWL.id;
    }

    Ext.Ajax.request({
      url: ICRAWL_WIZARD_API_BASE_URL + url,
      success: function (response) {
        var result = JSON.parse(response.responseText);

        window.location = result.url;
      },
      failure: function (response) {
        var result = JSON.parse(response.responseText);

        Ext.Msg.alert(result.error + ': ' + result.exception, result.message);
      },
      jsonData: result
    });
  },

  getListbox: function (listboxName) {
    return this.view.query('listbox#' + listboxName)[0];
  },

  getListboxData: function (listboxName) {
    var
      data = this.getListbox(listboxName).getStore().getData(),
      results = [], i
    ;

    for (i = 0; i < data.items.length; i++) {
      results.push(data.items[i].get('data'));
    }

    return results;
  },

  getUnixTimeStamp: function (date, time) {
    var dateTime = Ext.Date.parse(date + ' ' + (time || '00:00'), 'd.m.Y H:i');

    if (dateTime === undefined) {
      return 0;
    }

    return parseInt(dateTime.getTime() / 1000, 10);
  }
});
