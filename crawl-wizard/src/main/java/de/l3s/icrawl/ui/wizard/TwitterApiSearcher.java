package de.l3s.icrawl.ui.wizard;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import twitter4j.HashtagEntity;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.URLEntity;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;
import com.google.common.collect.Ordering;

import de.l3s.icrawl.domain.specification.NamedEntity;

@Service
public class TwitterApiSearcher implements ApiSearcher {
    private static final class TweetedUrlComparator implements
            Comparator<Map.Entry<String, Collection<Status>>> {
        @Override
        public int compare(Entry<String, Collection<Status>> e1,
                Entry<String, Collection<Status>> e2) {
            int cmp = Integer.compare(e2.getValue().size(), e1.getValue().size());
            if (cmp != 0) {
                return cmp;
            } else {
                return e1.getKey().compareTo(e2.getKey());
            }
        }
    }

    private static final Ordering<Entry<String, Collection<Status>>> TWEET_ORDERING = Ordering.from(new TweetedUrlComparator());
    private final Twitter twitter;

    @Inject
    public TwitterApiSearcher(Twitter twitter) {
        this.twitter = twitter;
    }

    @Override
    public List<SeedSearchResult> search(String queryTerms, int count, int offset) {
        Multimap<String, Status> tweetedUrls;
        try {
            tweetedUrls = retrieveTweetedUrls(queryTerms, count);
        } catch (TwitterException e) {
            throw new SearchException("Could not get tweets for query '" + queryTerms + "'", e);
        }

        Set<Entry<String, Collection<Status>>> tweetsByUrl = tweetedUrls.asMap().entrySet();
        List<Entry<String, Collection<Status>>> sortedTweets = TWEET_ORDERING.sortedCopy(tweetsByUrl);
        ImmutableList.Builder<SeedSearchResult> results = ImmutableList.builder();
        for (Entry<String, Collection<Status>> entry : sortedTweets) {
            String description = extractDescription(entry.getValue());
            Collection<String> keywords = extractKeywords(entry.getValue());
            Collection<NamedEntity> entities = Collections.emptySet();
            results.add(new SeedSearchResult(entry.getKey(), keywords, entities,
                description, entry.getKey()));
        }
        return results.build();
    }

    String extractDescription(Collection<Status> tweets) {
        // deduplicate texts, keep order
        Set<String> texts = new LinkedHashSet<>();
        for (Status tweet : tweets) {
            texts.add(tweet.getText());
        }
        StringBuilder description = new StringBuilder(texts.size() * (140 + 1));
        for (String text : texts) {
            description.append(text).append("\n");
        }
        return description.toString();
    }

    Collection<String> extractKeywords(Collection<Status> tweets) {
        Multiset<String> keywords = HashMultiset.create();
        for (Status tweet : tweets) {
            for (HashtagEntity hashtag : tweet.getHashtagEntities()) {
                keywords.add(hashtag.getText());
            }
        }
        Multiset<String> sortedKeywords = Multisets.copyHighestCountFirst(keywords);
        return Lists.newArrayList(Iterables.limit(sortedKeywords.elementSet(), 5));
    }

    Multimap<String, Status> retrieveTweetedUrls(String queryTerms, int count)
            throws TwitterException {
        Multimap<String, Status> tweetedUrls = HashMultimap.create(count, 16);
        Query query = new Query(queryTerms);
        query.setCount(count);
        QueryResult queryResult;
        do {
            queryResult = twitter.search(query);
            for (Status tweet : queryResult.getTweets()) {
                extractUrls(tweetedUrls, tweet);
            }
        } while (tweetedUrls.keySet().size() < count
                && (query = queryResult.nextQuery()) != null);
        return tweetedUrls;
    }

    private void extractUrls(Multimap<String, Status> tweetedUrls, Status tweet) {
        for (URLEntity tweetUrl : tweet.getURLEntities()) {
            String url = tweetUrl.getExpandedURL();
            if (url == null) {
                url = tweetUrl.getURL();
            }
            tweetedUrls.put(url, tweet);
        }
    }

    @Override
    public String getKey() {
        return "twitter";
    }
}