package de.l3s.icrawl.ui.wizard;

import java.util.List;

public interface SeedCandidateAnalyzer {

    List<SeedSearchResult> analyze(List<SeedSearchResult> candidates);

}
