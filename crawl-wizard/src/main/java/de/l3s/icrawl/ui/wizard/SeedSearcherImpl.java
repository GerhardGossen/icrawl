package de.l3s.icrawl.ui.wizard;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;

@Service
public class SeedSearcherImpl implements SeedSearcher {
    private static final Logger logger = LoggerFactory.getLogger(SeedSearcherImpl.class);
    private static final int RESULTS_PER_PAGE = 50;
    private final Map<String, ApiSearcher> searchers;
    private final SeedCandidateAnalyzer analyzer;

    @Inject
    public SeedSearcherImpl(List<? extends ApiSearcher> searchers, SeedCandidateAnalyzer analyzer) {
        this.analyzer = analyzer;
        ImmutableMap.Builder<String, ApiSearcher> searchersByKey = ImmutableMap.builder();
        for (ApiSearcher apiSearcher : searchers) {
            searchersByKey.put(apiSearcher.getKey(), apiSearcher);
        }
        this.searchers = searchersByKey.build();
    }

    @Override
    public List<SeedSearchResult> search(String source, String query) {
        Preconditions.checkNotNull(source, "Null source");
        Preconditions.checkNotNull(query, "Null query");
        ApiSearcher searcher = searchers.get(source);
        if (searcher != null) {
            List<SeedSearchResult> candidates = searcher.search(query, RESULTS_PER_PAGE, 0);
            if (candidates.isEmpty()) {
                logger.info("Could not find any results for query '{}' in source '{}'", query,
                    source);
            }
            return analyzer.analyze(candidates);
        } else {
            throw new IllegalArgumentException("Unknown source " + source);
        }
    }

}
