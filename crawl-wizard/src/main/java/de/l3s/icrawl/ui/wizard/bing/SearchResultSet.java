package de.l3s.icrawl.ui.wizard.bing;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("d")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchResultSet {

    private List<SearchResult> results;

    public List<SearchResult> getResults() {
        return results;
    }

    public void setResults(List<SearchResult> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return String.format("BingSearchResultSet [results=%s]", results);
    }

}
