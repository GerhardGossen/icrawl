package de.l3s.icrawl.ui.wizard.bing;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy.PascalCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonIgnoreProperties("__metadata")
@JsonNaming(PascalCaseStrategy.class)
public class SearchResult {
    private String id;
    private String title;
    private String description;
    private String displayUrl;
    private String url;

    @JsonProperty("ID")
    public String getID() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    @JsonProperty
    public String getDisplayUrl() {
        return displayUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setID(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDisplayUrl(String displayUrl) {
        this.displayUrl = displayUrl;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return String.format(
            "BingSearchResult [id=%s, title=%s, description=%s, displayUrl=%s, url=%s]", id, title,
            description, displayUrl, url);
    }


}
