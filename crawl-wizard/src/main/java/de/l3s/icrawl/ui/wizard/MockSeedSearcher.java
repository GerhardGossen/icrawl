package de.l3s.icrawl.ui.wizard;

import java.util.Collection;
import java.util.List;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

import de.l3s.icrawl.domain.specification.NamedEntity;
import de.l3s.icrawl.domain.specification.NamedEntity.Type;

public class MockSeedSearcher implements SeedSearcher {

    @Override
    public List<SeedSearchResult> search(String source, String query) {
        Preconditions.checkNotNull(query, "Null query term");
        String title = "Page about " + query;
        String[] queryTerms = query.split(" ");
        Collection<String> keywords = ImmutableList.<String> builder()
            .add("water", "h2o")
            .add(queryTerms)
            .build();
        Collection<NamedEntity> entities = ImmutableList.<NamedEntity> builder()
            .add(new NamedEntity(Type.PERSON, "Water Man"),
                new NamedEntity(Type.PERSON, "Dihydrogenoxyde Man"))
            .build();
        String description = query + " is a ...";
        String url = "http://en.wikipedia.org/wiki/" + query;
        SeedSearchResult result = new SeedSearchResult(title, keywords, entities, description, url);

        ImmutableList.Builder<SeedSearchResult> resultList = ImmutableList.builder();
        for (int i = 0; i < 25; i++) {
            resultList.add(result);
        }
        return resultList.build();
    }

}
