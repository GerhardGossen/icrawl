package de.l3s.icrawl.ui.wizard;

import java.util.List;

import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

public class FormError implements FormResponse {

    private final List<ObjectError> errors;

    public FormError(BindingResult errors) {
        this.errors = errors.getAllErrors();
    }

    public List<ObjectError> getErrors() {
        return errors;
    }

}
