package de.l3s.icrawl.ui.wizard;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;

import javax.annotation.PreDestroy;

import org.springframework.stereotype.Service;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

import de.l3s.icrawl.reddit.Link;
import de.l3s.icrawl.reddit.RedditSearchApi;
import de.l3s.icrawl.reddit.RedditSearchApi.Sort;

@Service
public class RedditApiSearcher implements ApiSearcher, Closeable {
    private final RedditSearchApi reddit;

    public RedditApiSearcher() {
        this.reddit = new RedditSearchApi();
    }

    @Override
    public List<SeedSearchResult> search(String queryTerms, int count, int offset) {
        List<Link> results = reddit.search(queryTerms, count, Sort.RELEVANCE);
        return Lists.transform(results, new Function<Link, SeedSearchResult>() {
            @Override
            public SeedSearchResult apply(Link link) {
                return new SeedSearchResult(link.getTitle(), link.getSelftext(), link.getUrl());
            }
        });
    }

    @Override
    public String getKey() {
        return "reddit";
    }

    @Override
    @PreDestroy
    public void close() throws IOException {
        reddit.close();
    }

}
