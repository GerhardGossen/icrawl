package de.l3s.icrawl.ui.wizard;

import java.io.IOException;

import org.openimaj.text.nlp.language.LanguageDetector;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.web.util.UriComponentsBuilder;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import de.l3s.icrawl.online.ContentAnalyser;
import de.l3s.icrawl.online.LabelerFactory;

@Configuration
@PropertySources({ @PropertySource("twitter4j.properties"), @PropertySource("bing.properties") })
public class WizardConfiguration {
    @Bean
    Twitter twitter() {
        return TwitterFactory.getSingleton();
    }

    @Bean
    UriComponentsBuilder uriComponentsBuilder() {
        return UriComponentsBuilder.fromPath("/");
    }

    @Bean
    ContentAnalyser contentAnalyser() throws IOException {
        return new ContentAnalyser(languageDetector(), labelerFactory());
    }

    @Bean
    LabelerFactory labelerFactory() {
        return LabelerFactory.defaultFactory();
    }

    @Bean
    LanguageDetector languageDetector() throws IOException {
        return new LanguageDetector();
    }
}
