package de.l3s.icrawl.ui.wizard;

import java.util.List;

/**
 * Find potential seed URLs using a keyword query.
 */
public interface SeedSearcher {

    /**
     * Search for a user-specified keyword query using the given source.
     * 
     * @param source
     *            where to search, e.g. "web", "twitter" (implementation
     *            specific)
     * @param query
     *            one or more space-separated keywords
     * @return ordered list of search results (most relevant first)
     */
    List<SeedSearchResult> search(String source, String query);

}
