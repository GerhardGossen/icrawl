package de.l3s.icrawl.ui.wizard;

public class FormSuccess implements FormResponse {

    private final String url;

    public FormSuccess(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
