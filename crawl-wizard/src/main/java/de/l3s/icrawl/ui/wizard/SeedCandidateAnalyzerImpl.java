package de.l3s.icrawl.ui.wizard;

import java.util.List;

import javax.inject.Inject;

import org.apache.http.client.ResponseHandler;
import org.apache.http.client.protocol.HttpClientContext;
import org.springframework.stereotype.Service;

import de.l3s.icrawl.online.ContentAnalyser;
import de.l3s.icrawl.util.net.Downloader;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

@Service
public class SeedCandidateAnalyzerImpl extends Downloader<SeedSearchResult> implements
        SeedCandidateAnalyzer {
    private final ContentAnalyser contentAnalyser;

    @Inject
    public SeedCandidateAnalyzerImpl(ContentAnalyser contentAnalyser) {
        super();
        this.contentAnalyser = contentAnalyser;
    }

    @Override
    protected ResponseHandler<SeedSearchResult> createResponseHandler(
            final SeedSearchResult candidate, HttpClientContext context) {
        return new ResponseAnalyzer(candidate, context, contentAnalyser);
    }

    @Override
    public List<SeedSearchResult> analyze(List<SeedSearchResult> candidates) {
        return fetchAll(candidates.stream().collect(toMap(SeedSearchResult::getUrl, identity())));
    }
}
