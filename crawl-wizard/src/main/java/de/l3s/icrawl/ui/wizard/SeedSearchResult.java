package de.l3s.icrawl.ui.wizard;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.google.common.collect.ImmutableList;

import de.l3s.icrawl.domain.specification.NamedEntity;

public class SeedSearchResult {

    private final String title;
    private final Collection<String> keywords;
    private final Collection<NamedEntity> entities;
    private final String description;
    private final String url;

    public SeedSearchResult(String title, String description, String url) {
        this(title, Collections.<String> emptySet(), Collections.<NamedEntity> emptySet(),
            description, url);
    }

    public SeedSearchResult(String title, Collection<String> keywords, Collection<NamedEntity> entities,
            String description, String url) {
        this.title = title;
        this.keywords = keywords;
        this.entities = entities;
        this.description = description;
        this.url = url;
    }

    /** Web page title */
    public String getTitle() {
        return title;
    }

    /** Keywords that were detected on this page */
    public Collection<String> getKeywords() {
        return keywords;
    }

    /** Entities that were detected on this page */
    public Collection<NamedEntity> getEntities() {
        return entities;
    }

    /** Web page description or part of page content */
    public String getDescription() {
        return description;
    }

    /** Web page URL */
    public String getUrl() {
        return url;
    }

    @Override
    public String toString() {
        return String.format(
            "SeedSearchResult [title=%s, keywords=%s, entities=%s, description=%s, url=%s]", title,
            keywords, entities, description, url);
    }

    public SeedSearchResult updateWith(String title, List<String> newKeywords,
            List<NamedEntity> newEntities, String url) {
        return new SeedSearchResult(title,
            extend(keywords, newKeywords),
            extend(entities, newEntities),
            description, url);
    }

    private static <T> Collection<T> extend(Collection<T> original, Collection<T> extension) {
        if (original.isEmpty()) {
            return extension;
        } else if (extension.isEmpty()) {
            return original;
        } else {
            ImmutableList.Builder<T> concat = ImmutableList.builder();
            return concat.addAll(original).addAll(extension).build();
        }
    }

}
