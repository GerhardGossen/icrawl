package de.l3s.icrawl.ui.wizard;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;

import org.apache.http.client.protocol.HttpClientContext;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import de.l3s.icrawl.domain.specification.NamedEntity;
import de.l3s.icrawl.online.ContentAnalyser;
import de.l3s.icrawl.online.ContentAnalyser.Counts;
import de.l3s.icrawl.util.HtmlParseException;
import de.l3s.icrawl.util.WebPageUtils;
import de.l3s.icrawl.util.net.ResponseAugmenter;

import static com.google.common.base.MoreObjects.firstNonNull;

class ResponseAnalyzer extends ResponseAugmenter<SeedSearchResult> {
    private final ContentAnalyser contentAnalyser;

    ResponseAnalyzer(SeedSearchResult candidate, HttpClientContext context, ContentAnalyser analyser) {
        super(candidate, context);
        this.contentAnalyser = analyser;
    }

    @Override
    public SeedSearchResult process(String content) throws IOException {
        if (Thread.currentThread().isInterrupted()) {
            return candidate;
        }

        DocumentFragment fragment = parseHtml(candidate.getUrl(), content);

        String title = firstNonNull(extractTitle(fragment), candidate.getTitle());
        ContentAnalyser.Counts counts = contentAnalyser.analyze(fragment, new HashSet<String>());
        List<String> keywords = counts.getDetectedKeywords();
        List<NamedEntity> entities = Counts.topK(counts.getEntities(), 10);
        String url = getRealUrl().orElse(candidate.getUrl());

        return candidate.updateWith(title, keywords, entities, url);
    }

    private String extractTitle(DocumentFragment fragment) {
        for (int i = 0, length = fragment.getChildNodes().getLength(); i < length; i++) {
            Node node = fragment.getChildNodes().item(i);
            if (node instanceof Element) {
                String title = extractTitle((Element) node);
                if (title != null) {
                    return title;
                }
            }
        }
        return null;
    }

    private String extractTitle(Element root) {
        NodeList titles = root.getElementsByTagName("title");
        for (int j = 0, titlesLength = titles.getLength(); j < titlesLength; j++) {
            String titleText = titles.item(j).getTextContent();
            if (titleText != null && !titleText.isEmpty()) {
                return titleText;
            }
        }
        return null;
    }

    private DocumentFragment parseHtml(String url, String content) throws IOException {
        try {
            Document doc = WebPageUtils.parseHtml(content, url);
            DocumentFragment fragment = doc.createDocumentFragment();
            NodeList nodes = doc.getElementsByTagName("html");
            for (int i = 0; i < nodes.getLength(); i++) {
                Node node = nodes.item(i);
                fragment.appendChild(node);
            }
            return fragment;
        } catch (DOMException | HtmlParseException e) {
            throw new IOException("Could not parse document at URL '" + url + "'", e);
        }
    }
}