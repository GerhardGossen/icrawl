package de.l3s.icrawl.ui.wizard;

import java.util.List;

public interface ApiSearcher {

    List<SeedSearchResult> search(String queryTerms, int count, int offset);

    String getKey();

}
