package de.l3s.icrawl.ui.wizard.bing;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.google.common.collect.ImmutableList;

import de.l3s.icrawl.ui.wizard.ApiSearcher;
import de.l3s.icrawl.ui.wizard.SearchException;
import de.l3s.icrawl.ui.wizard.SeedSearchResult;
import static com.google.common.net.MediaType.JSON_UTF_8;

@Service
public class BingApiSearcher implements ApiSearcher, Closeable {
    private static final Logger logger = LoggerFactory.getLogger(BingApiSearcher.class);
    private static final String WEB_SEARCH_HOST = "api.datamarket.azure.com";
    private static final String WEB_SEARCH_URL = "https://" + WEB_SEARCH_HOST
            + "/Bing/SearchWeb/v1/Web";
    private static final ObjectReader OBJECT_READER = new ObjectMapper().reader(
        SearchResultSet.class).with(DeserializationFeature.UNWRAP_ROOT_VALUE);
    private final HttpClient client;

    @Inject
    public BingApiSearcher(@Value("${bing.apiKey}") String apiKey) {
        logger.debug("Using Bing API key: {}", apiKey);
        BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(new AuthScope(WEB_SEARCH_HOST, 443),
            new UsernamePasswordCredentials(apiKey, apiKey));

        client = HttpClientBuilder.create()
            .setUserAgent("iCrawl")
            .setDefaultHeaders(
                Arrays.asList(new BasicHeader(HttpHeaders.ACCEPT, JSON_UTF_8.toString())))
            .setDefaultCredentialsProvider(credentialsProvider)
            .build();
    }

    @Override
    public List<SeedSearchResult> search(String queryTerms, int count, int offset) {
        URI uri = UriComponentsBuilder.fromHttpUrl(WEB_SEARCH_URL)
            .queryParam("$format", "json")
            .queryParam("$top", count)
            .queryParam("$skip", offset)
            .queryParam("Query", "'" + queryTerms + "'")
            .build().encode().toUri();
        logger.debug("Using URL {}", uri);

        HttpPost post = new HttpPost(uri);
        try {
            HttpResponse response = client.execute(post);
            checkResponse(response);

            InputStream content = response.getEntity().getContent();
            return parse(content);
        } catch (IOException e) {
            throw new SearchException("Could not search for '" + queryTerms + "'", e);
        }
    }

    private void checkResponse(HttpResponse response) {
        StatusLine status = response.getStatusLine();
        if (status.getStatusCode() != HttpStatus.SC_OK) {
            throw new SearchException("Server returned error " + status);
        }
        String contentType = response.getFirstHeader(HttpHeaders.CONTENT_TYPE).getValue();
        if (!JSON_UTF_8.toString().equals(contentType)) {
            logger.debug("Got contentType {}, expected {}", contentType, JSON_UTF_8.toString());
            throw new SearchException("Illegal response content type '" + contentType
                    + "', expected JSON");
        }
    }

    @Override
    public void close() throws IOException {
        if (client instanceof Closeable) {
            ((Closeable) client).close();
        }
    }

    static List<SeedSearchResult> parse(InputStream is) throws IOException {
        SearchResultSet resultSet = OBJECT_READER.readValue(is);
        ImmutableList.Builder<SeedSearchResult> ret = ImmutableList.builder();
        for (SearchResult result : resultSet.getResults()) {
            String title = result.getTitle();
            String description = result.getDescription();
            String url = result.getUrl();
            SeedSearchResult searchResult = new SeedSearchResult(title, description, url);
            ret.add(searchResult);
        }
        return ret.build();

    }

    @Override
    public String getKey() {
        return "web";
    }
}
