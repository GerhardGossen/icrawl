package de.l3s.icrawl.ui.wizard.bing;

import java.io.InputStream;
import java.net.URL;
import java.util.List;

import org.junit.Test;

import com.google.common.io.Resources;

import de.l3s.icrawl.ui.wizard.SeedSearchResult;
import de.l3s.icrawl.ui.wizard.bing.BingApiSearcher;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

public class BingApiSearcherTest {

    @Test
    public void test() throws Exception {
        URL resource = Resources.getResource(getClass(), "search_result.json");
        try (InputStream input = resource.openStream()) {
            List<SeedSearchResult> resultSet = BingApiSearcher.parse(input);
            assertThat(resultSet, hasSize(49));
            assertThat(resultSet.get(0).getUrl(), containsString("l3s.de/"));
        }
    }

}
