package de.l3s.icrawl.ui.wizard;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import static de.l3s.icrawl.ui.wizard.TwitterApiSearcherTest.createMockTwitter;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class SeedSearcherImplTest {

    private final class MockSeedCandidateAnalyzer implements SeedCandidateAnalyzer {
        @Override
        public List<SeedSearchResult> analyze(List<SeedSearchResult> candidates) {
            return candidates;
        }
    }

    private SeedSearcherImpl searcher;

    @Before
    public void setUp() throws Exception {
        TwitterApiSearcher twitterApiSearcher = new TwitterApiSearcher(createMockTwitter());
        this.searcher = new SeedSearcherImpl(singletonList(twitterApiSearcher),
            new MockSeedCandidateAnalyzer());
    }

    @Test
    public void testSearchTwitter() {
        List<SeedSearchResult> results = searcher.search("twitter", "foo");
        assertThat(results, hasSize(TwitterApiSearcherTest.RESULTS_PER_PAGE));
        assertThat(results, not(contains(nullValue())));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSearch_notimplemented() {
        searcher.search("foo", "foo");
    }


}
