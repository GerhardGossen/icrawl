package de.l3s.icrawl.ui.wizard;

import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import twitter4j.HashtagEntity;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.URLEntity;

import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class TwitterApiSearcherTest {


    private static class HashtagEntityImpl implements HashtagEntity {
        private static final long serialVersionUID = 1L;
        private final int id;

        public HashtagEntityImpl(int id) {
            this.id = id;
        }

        @Override
        public String getText() {
            return "hashtag_" + id;
        }

        @Override
        public int getStart() {
            return 30;
        }

        @Override
        public int getEnd() {
            return getStart() + getText().length();
        }

    }

    private static class URLEntityImpl implements URLEntity {
        private static final long serialVersionUID = 1L;
        private final int id;

        public URLEntityImpl(int id) {
            this.id = id;
        }

        @Override
        public String getText() {
            return "text_" + id;
        }

        @Override
        public String getURL() {
            return "url_" + id;
        }

        @Override
        public String getExpandedURL() {
            return "expanded_url_" + id;
        }

        @Override
        public String getDisplayURL() {
            return "display_url_" + id;
        }

        @Override
        public int getStart() {
            return 0;
        }

        @Override
        public int getEnd() {
            return getDisplayURL().length();
        }

    }

    static final int RESULTS_PER_PAGE = 25;
    private TwitterApiSearcher searcher;

    @Before
    public void setup() throws Exception {
        Twitter twitter = createMockTwitter();
        this.searcher = new TwitterApiSearcher(twitter);
    }

    static Twitter createMockTwitter() throws TwitterException {
        List<Status> tweets = createTweets(RESULTS_PER_PAGE);

        QueryResult queryResult = Mockito.mock(QueryResult.class);
        when(queryResult.getTweets()).thenReturn(tweets);

        Twitter twitter = Mockito.mock(Twitter.class);
        when(twitter.search(any(Query.class))).thenReturn(queryResult);
        return twitter;
    }

    private static List<Status> createTweets(int count) {
        List<Status> tweets = Lists.newArrayListWithExpectedSize(count);
        for (int i = 0; i < count; i++) {
            URLEntity url = new URLEntityImpl(i);
            HashtagEntity hashtag = new HashtagEntityImpl(i);
            Status tweet = Mockito.mock(Status.class);
            when(tweet.getText()).thenReturn("text_" + i);
            when(tweet.getURLEntities()).thenReturn(new URLEntity[] { url });
            when(tweet.getHashtagEntities()).thenReturn(new HashtagEntity[] { hashtag });

            tweets.add(tweet);
        }
        return tweets;
    }

    @Test
    public void testExtractKeywords() {
        Collection<Status> tweets = createTweets(5);
        Collection<String> keywords = searcher.extractKeywords(tweets);
        assertThat(keywords,
            hasItems("hashtag_0", "hashtag_1", "hashtag_2", "hashtag_3", "hashtag_4"));
    }

    @Test
    public void testExtractDescription() {
        Collection<Status> tweets = createTweets(5);
        String description = searcher.extractDescription(tweets);
        assertThat(description, is("text_0\ntext_1\ntext_2\ntext_3\ntext_4\n"));
    }

    @Test
    public void testRetrieveTweetedUrls() throws TwitterException {
        Multimap<String, Status> tweetedUrls = searcher.retrieveTweetedUrls("foo", RESULTS_PER_PAGE);
        assertThat(tweetedUrls.entries(), hasSize(RESULTS_PER_PAGE));
    }

}
