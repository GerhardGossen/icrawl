# Evaluation scripts #

This module contains the scripts used for the relevance and freshness evaluation. The following scripts are available:

## bin/urls ##

Extract data about crawled documents and prints them to a file. Use as:

    bin/urls campaignId referenceVectorFile [outputFile]

The output has the following field:

- score: Relevance score based on the reference vector
- fetchTimestamp: time the page was fetched, as milliseconds since the epoch
- modifiedTimestamp: time when the document was created or last modified, same format as `fetchTimestamp`
- language :: detected language of the content, as a two-letter ISO-639 code
- dateSource: source for the modifiedTimestamp, as a name from `de.l3s.icrawl.evaluation.WebPageDateExtractor.DateSource`
- batchId :: ID of the batch as which this document was crawled, typically `\d+-\d+`
- numTokens: number of textual tokens in the document content
- numHashtags: number of #hashtags in the document content

Implemented by `de.l3s.icrawl.evaluation.ExtractUrls`.

## bin/twitterfetcher ##

Follow the Twitter stream and download all posted URLs. Use as:

    bin/twitterfetcher seedFile [campaignId]

All downloaded documents are stored in Accumulo in Nutch's format in the table `crawl_${campaignId}_webpage`. If `campaignId` is not set, a default of 99 is used.

The seedFile has one query term per line. A query term can be either a user name (starting with `@`) or a keyword.

Implemented by `de.l3s.icrawl.evaluation.twitterfetcher.TwitterFetcher`.

## bin/batchrelevance ##

Compute the average relevance for a batch. Use as:

    bin/batchrelevance campaignId referenceVectorFile [outputFile]

`referenceVectorFile` needs to be a file created by `bin/vector`. If no output file is specified, the results are written to STDOUT.

The output has the format `batchId \t score`, where `batchId` is the ID used by Nutch and `score` is a floating point number in [0,1] giving the average similarity of documents fetched in that batch to the reference vector (larger is better).

Implemented by `de.l3s.icrawl.evaluation.BatchRelevance`.

## bin/vector ##

Create a reference vector based on the seed URLs of a crawl. Use as:

    bin/vector campaignId outputFile [maxTerms] [-df]

where `outputFile` is the file the vector is written to. `maxTerms` limits the number of terms to include in the vector to the top N (default is to use all). If `-df` is given, the vectors of the individual seed documents are merged taking into account the number of documents containing the term.

Implemented by `de.l3s.icrawl.evaluation.ReferenceVectorExtractor`.

## bin/fetchmarks ##

Fix the Fetch marks of the documents downloaded by the TwitterFetcher. Use as:

    bin/fetchmarks campaignId

After this script has been run, you can execute `bin/nutch parse` to run the Nutch ParseJob on the TwitterFetcher results.

Implemented by `de.l3s.icrawl.evaluation.UpdateFetchMark`.

## bin/freshness ##

Computes statistics about the freshness of a crawl. Use as:

    bin/freshness campaignId [outputFile]

Outputs the mean, min, max, stddev and the quartiles of the freshness either to `outputFile` or to STDOUT.

Implemented by `de.l3s.icrawl.evaluation.FreshnessComputation`.

## bin/metatags ##

Counts the prevalence of date meta-tags. Use as:

	bin/metatags campaignId

Prints the found meta-tags and their number they are found in the crawl.

Implemented by `de.l3s.icrawl.evaluation.MetaTagsCounter`.

## Database analysis helpers ##

All of the following have no helper scripts, you need to run them directly.
Make sure to check the source code if table names etc. are correct.

### Load URLs into database ###

Use `de.l3s.icrawl.evaluation.DbInserter` to load URL information into a
PostgreSQL database. Arguments are file names for files produced by `bin/urls`.

### Add batch information to TwitterFetcher crawls ###

As TwitterFetcher has no concept of batches, we need to generate artificial
batchIds for the graphs of freshness/relevance over time. This can be done with
`de.l3s.icrawl.evaluation.twitterfetcher.BatchIdProducer` which by default
creates batches that are 15 mins long.
