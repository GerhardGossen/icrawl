package de.l3s.icrawl.evaluation;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import org.apache.hadoop.util.Tool;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import de.l3s.icrawl.DbConfig;
import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.domain.specification.Crawl;
import de.l3s.icrawl.domain.specification.CrawlSpecification;
import de.l3s.icrawl.online.DocumentVectorSimilarity;
import de.l3s.icrawl.online.DocumentVectorUtils;
import de.l3s.icrawl.repository.CampaignRepository;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate;
import de.l3s.icrawl.util.HadoopUtils;

@Component
public class ReferenceVectorExtractor implements Tool {
    public class Arguments {

        @Option(aliases = "--icrawl.directory", name = "dir")
        public String icrawlDirectory;

        @Argument(index = 0, required = true, usage = "campaign ID")
        public long campaignId;
        @Argument(index = 1, required = true, usage = "vector output file")
        public File outputFile;
        @Argument(index = 2, required = false, usage = "max top terms to use for reference vectors")
        public int maxTerms = -1;

        @Option(name = "-df", usage = "use document frequency in seed set in addition to TF?")
        public boolean useDocumentFrequency = false;

    }

    private org.apache.hadoop.conf.Configuration conf;
    @Inject
    private CampaignRepository campaignRepository;
    @Inject
    private PlatformTransactionManager transactionManager;
    @Inject
    private CrawlDataRepositoryTemplate template;

    @Configuration
    @EnableAutoConfiguration
    @EnableTransactionManagement
    @Import(DbConfig.class)
    @PropertySource("application.properties")
    @ComponentScan
    public static class ReferenceVectorExtractorConfig {
        @Bean
        org.apache.hadoop.conf.Configuration conf(@Value("${icrawl.directory}") File appDirectory) {
            return HadoopUtils.createConf(appDirectory);
        }

        @Bean
        CrawlDataRepositoryTemplate template(org.apache.hadoop.conf.Configuration conf) {
            return new CrawlDataRepositoryTemplate(conf);
        }

    }

    public static void main(String[] args) throws Exception {
        SpringApplication app = new SpringApplication(ReferenceVectorExtractorConfig.class);
        ApplicationContext applicationContext = app.run(args);
        int returnCode = applicationContext.getBean("referenceVectorExtractor", Tool.class).run(
            args);
        System.exit(returnCode);
    }

    @Override
    public void setConf(org.apache.hadoop.conf.Configuration conf) {
        this.conf = conf;
    }

    @Override
    public org.apache.hadoop.conf.Configuration getConf() {
        return conf;
    }

    @Transactional
    public DocumentVectorSimilarity createSimilarityMeasureForCampaign(long campaignId,
            int maxTerms, boolean useDF) throws IOException {
        TransactionStatus transactionStatus = transactionManager.getTransaction(new DefaultTransactionDefinition(
            TransactionDefinition.PROPAGATION_REQUIRED));
        try {
            Campaign campaign = campaignRepository.findOne(campaignId);
            if (campaign == null) {
                throw new IllegalArgumentException("No campaign found for id " + campaignId);
            }
            Crawl crawl = campaign.getCrawls().iterator().next();
            CrawlSpecification spec = crawl.getSpecification();
            return DocumentVectorUtils.createCrawlReferenceVector(template, campaignId, spec,
                false, maxTerms, useDF);
        } finally {
            transactionManager.commit(transactionStatus);
        }
    }

    @Override
    public int run(String[] args) throws Exception {
        Arguments arguments = CLIUtils.parseArguments(args, new Arguments(), "usage: java "
                + ReferenceVectorExtractor.class.getName()
                + " campaignId outputFile [maxTerms]");
        DocumentVectorSimilarity similarity = createSimilarityMeasureForCampaign(
            arguments.campaignId, arguments.maxTerms, arguments.useDocumentFrequency);
        similarity.writeToFile(arguments.outputFile);
        System.out.println(similarity);
        return 0;
    }

}
