package de.l3s.icrawl.evaluation.twitterfetcher;

import java.util.Map;
import java.util.concurrent.BlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;

import de.l3s.icrawl.api.ApiFetcherContext;
import de.l3s.icrawl.domain.api.ApiFetcherDocument;
import de.l3s.icrawl.domain.status.ApiStatus.ErrorBehavior;
import de.l3s.icrawl.domain.status.ApiStatus.ErrorType;

final class UrlEnqueuer implements ApiFetcherContext {
    private static final Logger logger = LoggerFactory.getLogger(UrlEnqueuer.class);
    private final BlockingQueue<String> urlQueue;
    private final Meter urlsCounter;
    private final Meter tweetsCounter;

    UrlEnqueuer(BlockingQueue<String> urlQueue, MetricRegistry metrics) {
        this.urlQueue = urlQueue;
        this.urlsCounter = metrics.meter("stream.urls");
        this.tweetsCounter = metrics.meter("stream.tweets");
    }

    @Override
    public void reportError(ErrorType type, ErrorBehavior behavior, String message,
            Exception e) {
        logger.debug("Got error: {} {} {}: ", type, behavior, message, e);
    }

    @Override
    public void writeDocument(ApiFetcherDocument doc) {
        tweetsCounter.mark();
    }

    @Override
    public void writeOutlink(String uri, Map<String, String> metadata) {
        logger.debug("Enqueuing URL {}", uri);
        urlQueue.add(uri);
        urlsCounter.mark();
    }

    @Override
    public void writeRedirect(String fromUri, String toUri, Map<String, String> metadata) {
        writeOutlink(toUri, metadata);
    }
}