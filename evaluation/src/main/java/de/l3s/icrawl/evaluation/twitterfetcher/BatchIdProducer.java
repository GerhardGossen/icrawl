package de.l3s.icrawl.evaluation.twitterfetcher;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbcp.BasicDataSource;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ConnectionCallback;
import org.springframework.jdbc.core.JdbcTemplate;

import com.google.common.collect.LinkedHashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multiset.Entry;

import de.l3s.icrawl.evaluation.DBUtils;

public class BatchIdProducer {
    private static final class BatchIdCreator implements ConnectionCallback<Multiset<String>> {
        private final Duration batchLength;

        private BatchIdCreator(Duration batchLength) {
            this.batchLength = batchLength;
        }

        @Override
        public Multiset<String> doInConnection(Connection con) throws SQLException,
                DataAccessException {
            PreparedStatement stmt = con.prepareStatement(
                "SELECT * FROM urls WHERE crawl = ? ORDER BY fetchtime",
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
            stmt.setString(1, "twitter_ebola");
            stmt.execute();
            ResultSet rs = stmt.getResultSet();
            Multiset<String> batchIdCounts = LinkedHashMultiset.create();
            DateTime batchBoundary = null;
            int currentBatch = 1;
            while (rs.next()) {
                DateTime currentDate = new DateTime(rs.getTimestamp("fetchtime"));
                if (batchBoundary == null) {
                    batchBoundary = currentDate.plus(batchLength);
                } else if (currentDate.isAfter(batchBoundary)) {
                    batchBoundary = batchBoundary.plus(batchLength);
                    currentBatch++;
                }
                String batchId = "1-" + currentBatch;
                batchIdCounts.add(batchId);
                rs.updateString("batchid", batchId);
                rs.updateRow();
            }
            return batchIdCounts;
        }
    }

    private BatchIdProducer() {
        // prevent instantiation
    }

    public static void main(String[] args) throws SQLException {
        BasicDataSource ds = DBUtils.createDataSource();
        try {
            JdbcTemplate template = new JdbcTemplate(ds);
            final Duration batchLength = Duration.standardMinutes(15);
            Multiset<String> countsByBatch = template.execute(new BatchIdCreator(batchLength));

            for (Entry<String> entry : countsByBatch.entrySet()) {
                System.out.format("%s\t%d%n", entry.getElement(), entry.getCount());
            }
        } finally {
            ds.close();
        }
    }

}
