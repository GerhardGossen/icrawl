package de.l3s.icrawl.evaluation;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;

import org.apache.commons.dbcp.BasicDataSource;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import com.google.common.io.Files;
import com.google.common.io.LineProcessor;

public class DbInserter {
    private static final class FileRowsInserter implements LineProcessor<Long> {
        private static final int FIELD_COUNT = 9;
        private final JdbcTemplate template;
        private final String crawlName;
        private long insertedRows = 0;

        private FileRowsInserter(JdbcTemplate template, String crawlName) {
            this.template = template;
            this.crawlName = crawlName;
        }

        @Override
        public boolean processLine(String line) throws IOException {
            String[] cells = line.split("\t", FIELD_COUNT);
            if (cells.length < FIELD_COUNT) {
                logger.warn("Malformed input row: '{}'", line);
                return true;
            }
            String url = cells[0];
            double score = Double.parseDouble(cells[1]);
            DateTime fetchTimestamp = new DateTime(Long.parseLong(cells[2]));
            long modifiedTimeStamp = Long.parseLong(cells[3]);
            DateTime modifiedTime = modifiedTimeStamp > 0 ? new DateTime(
                modifiedTimeStamp) : null;
            String language = cells[4];
            String dateSource = cells[5];
            String batchId = cells[6];
            int numTokens = Integer.parseInt(cells[7]);
            int numHashtags = Integer.parseInt(cells[8]);
            template.update(
                "INSERT INTO urls (url, score, fetchtime, modifiedtime, language, datesource, batchid, crawl, tokens, hashtags) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                url, score, fetchTimestamp.toDate(), modifiedTime != null ? modifiedTime.toDate()
                        : null, language, dateSource, batchId, crawlName, numTokens, numHashtags);
            insertedRows++;
            return true;
        }

        @Override
        public Long getResult() {
            return insertedRows;
        }
    }

    private static final Logger logger = LoggerFactory.getLogger(DbInserter.class);

    private DbInserter() {
        // prohibit instantiation
    }

    public static void main(String[] args) throws SQLException, IOException {
        BasicDataSource dataSource = DBUtils.createDataSource();
        final JdbcTemplate template = new JdbcTemplate(dataSource);

        try {
            for (final String filename : args) {
                String crawlName = Files.getNameWithoutExtension(filename)
                    .replaceFirst("urls_", "");
                long insertedRows = Files.readLines(new File(filename), StandardCharsets.UTF_8,
                    new FileRowsInserter(template, crawlName));
                logger.debug("Inserted {} rows from file '{}'", insertedRows, crawlName);
            }
        } finally {
            dataSource.close();
        }
        logger.info("Done!");
    }
}
