package de.l3s.icrawl.evaluation.twitterfetcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.hadoop.conf.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.CsvReporter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.ScheduledReporter;
import com.google.common.collect.Iterables;
import com.google.common.io.Files;

import de.l3s.icrawl.api.ApiFetcherContext;
import de.l3s.icrawl.api.OAuthCredentialsProvider;
import de.l3s.icrawl.apifetcher.InMemoryCredentialsProvider;
import de.l3s.icrawl.apifetcher.OAuthCredentialUtils;
import de.l3s.icrawl.apifetcher.TwitterApiFetcher;
import de.l3s.icrawl.domain.api.ApiRequest;
import de.l3s.icrawl.nutch.Injector;
import de.l3s.icrawl.nutch.InjectorSetupException;
import de.l3s.icrawl.util.HadoopUtils;

/**
 * Download all URLs that are tweeted by certain users or that match certain
 * keywords.
 */
public class TwitterFetcher implements AutoCloseable {
    private static final Logger logger = LoggerFactory.getLogger(TwitterFetcher.class);
    private final TwitterApiFetcher apiFetcher;
    private final ApiFetcherContext context;

    /**
     * Create an empty fetcher. Use {@link #follow(ApiRequest)} to start
     * following topics.
     *
     * @param urlQueue
     *            the target for extracted URLs
     * @param conf
     *            the iCrawl configuration
     * @param metrics
     * @throws IOException
     */
    public TwitterFetcher(BlockingQueue<String> urlQueue, Configuration conf, MetricRegistry metrics) throws IOException {
        OAuthCredentialsProvider provider = new InMemoryCredentialsProvider(OAuthCredentialUtils.fromProperties("twitterFetcher.properties"));
        this.apiFetcher = new TwitterApiFetcher(provider);
        apiFetcher.setConfiguration(conf);
        this.context = new UrlEnqueuer(urlQueue, metrics);
    }

    public static void main(String[] args) throws IOException, InterruptedException,
            InjectorSetupException {
        if (args.length < 1) {
            System.err.println("Usage: java " + TwitterFetcher.class.getName()
                    + " seeds.txt [campaignId (default 99)]");
            System.err.println("Format of seeds.txt: One keyword or username per line, usernames must be prefixed with @");
        }
        File icrawlDirectory = new File(".");
        Properties props = new Properties();
        try (InputStream is = new FileInputStream(new File(icrawlDirectory, "icrawl.properties"))) {
            props.load(is);
        }
        Configuration conf = HadoopUtils.createConf(icrawlDirectory);
        MetricRegistry metrics = new MetricRegistry();
        final BlockingQueue<String> urlQueue = new LinkedBlockingQueue<>();

        final File queueDumpFile = new File(".queue");
        if (queueDumpFile.exists()) {
            List<String> oldUrls = Files.readLines(queueDumpFile, StandardCharsets.UTF_8);
            logger.info("Continuing with {} URLs from last run", oldUrls.size());
            urlQueue.addAll(oldUrls);
            queueDumpFile.delete();
        }

        final TwitterFetcher twitterFetcher = new TwitterFetcher(urlQueue, conf, metrics);
        twitterFetcher.follow(parseSeeds(new File(args[0])));
        long campaignId = 99;
        if (args.length >= 2) {
            campaignId = Long.parseLong(args[1]);
        }

        Injector injector = new Injector(conf, "crawl_" + campaignId);
        final UrlFetcher urlFetcher = new UrlFetcher(urlQueue, injector, metrics);
        urlFetcher.fetchContinuously();

        final ScheduledReporter reporter = CsvReporter.forRegistry(metrics)
            .convertRatesTo(TimeUnit.SECONDS)
            .convertDurationsTo(TimeUnit.MILLISECONDS)
            .formatFor(Locale.ENGLISH)
            .build(new File("metrics"));
        reporter.start(1, TimeUnit.MINUTES);

        // Run in background threads until process gets killed, then clean up
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    twitterFetcher.close();
                } catch (IOException e) {
                    logger.info("Failed to stop Twitter fetcher: ", e);
                }
                List<String> failedUrls = urlFetcher.stop();
                try {
                    dumpQueueToFile(queueDumpFile, urlQueue, failedUrls);
                } catch (IOException e) {
                    logger.info("Could not write queue to file {}, discarding {} items ",
                        queueDumpFile, urlQueue.size(), e);
                }
                reporter.stop();
                reporter.report();
            }
        });
    }

    /**
     * Parse a file with Twitter seeds.
     *
     * The expected format of the file is one seed per line. If the seed starts
     * with '@', it is assumed to be a Twitter screen name, otherwise it is used
     * as a query keyword. Lines starting with '#' are ignored.
     *
     * @return a combined ApiRequest for the file
     * @throws IOException
     *             when the file cannot be read
     */
    protected static ApiRequest parseSeeds(File seedsFile) throws IOException {
        Set<String> keywords = new HashSet<>();
        Set<String> usernames = new HashSet<>();
        List<String> lines = Files.readLines(seedsFile, StandardCharsets.UTF_8);
        for (String seed : lines) {
            String entry = seed.trim();
            if (entry.startsWith("#")) {
                // comment
                continue;
            } else if (entry.startsWith("@") && entry.length() > 1) {
                String name = entry.substring(1).trim();
                if (!name.isEmpty()) {
                    usernames.add(name);
                }
            } else {
                if (!entry.isEmpty()) {
                    keywords.add(entry);
                }
            }
        }

        return new ApiRequest("twitter", "twitter", null, keywords, usernames, null, null, null);
    }

    private static void dumpQueueToFile(final File queueDumpFile,
            final BlockingQueue<String> urlQueue, List<String> failedUrls) throws IOException {
        Iterable<String> allUrls = Iterables.concat(urlQueue, failedUrls);
        if (!queueDumpFile.exists()) {
            Files.asCharSink(queueDumpFile, StandardCharsets.UTF_8).writeLines(allUrls);
            logger.info("Wrote {} URLs to file {}, will resume on next run", urlQueue.size()
                    + failedUrls.size(), queueDumpFile);
        } else {
            logger.info("Queue dump file already exists, discarding existing queue (size: {})",
                urlQueue.size() + failedUrls.size());
        }
    }

    @Override
    public void close() throws IOException {
        apiFetcher.close();
    }

    /**
     * Start tracking the given topics.
     */
    public void follow(ApiRequest apiRequest) {
        apiFetcher.addRequest(apiRequest, context);
    }
}
