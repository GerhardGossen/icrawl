package de.l3s.icrawl.evaluation;

import org.apache.commons.dbcp.BasicDataSource;
import org.postgresql.Driver;

public class DBUtils {
    private DBUtils() {
        // prevent instantiation
    }

    public static BasicDataSource createDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(Driver.class.getName());
        dataSource.setUrl("jdbc:postgresql://127.0.0.1:5433/icrawl");
        dataSource.setUsername("icrawl");
        dataSource.setPassword("icrawl");
        return dataSource;
    }

}
