package de.l3s.icrawl.evaluation;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import org.apache.commons.math.stat.descriptive.SummaryStatistics;
import org.apache.gora.mapreduce.GoraMapper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.util.Tool;
import org.apache.nutch.crawl.CrawlStatus;
import org.apache.nutch.storage.WebPage;
import org.apache.nutch.storage.WebPage.Field;
import org.kohsuke.args4j.Argument;
import org.openimaj.text.nlp.language.LanguageDetector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.google.common.collect.ImmutableSet;

import de.l3s.icrawl.FieldNames;
import de.l3s.icrawl.online.DocumentVectorSimilarity;
import de.l3s.icrawl.online.DocumentVectorUtils;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate.ResultLinesMapper;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate.StatusFilterQuerySetup;
import de.l3s.icrawl.util.HadoopUtils;

import static de.l3s.icrawl.domain.support.ByteBufferUtils.asString;

@Component
public class BatchRelevance implements Tool {
    private static final String USAGE = "Usage: java " + BatchRelevance.class.getName()
            + " campaignId referenceVectorFile [outputFile]";

    @org.springframework.context.annotation.Configuration
    @EnableAutoConfiguration
    @PropertySource("application.properties")
    public static class BatchRelevanceConfiguration {
        @Bean
        Configuration conf(@Value("${icrawl.directory}") File appDirectory) {
            return HadoopUtils.createConf(appDirectory);
        }

        @Bean
        CrawlDataRepositoryTemplate template(Configuration conf) {
            return new CrawlDataRepositoryTemplate(conf);
        }

        @Bean
        BatchRelevance batchRelevance() {
            return new BatchRelevance();
        }
    }

    enum Counters {
        PROCESSED, NO_CONTENT
    }

    private static final Logger logger = LoggerFactory.getLogger(BatchRelevance.class);

    public static class BatchRelevanceMapper extends
            GoraMapper<String, WebPage, Text, DoubleWritable> {
        private static final String UNKNOWN_BATCH_ID = "9999-9999";
        private final Text outKey = new Text();
        private final DoubleWritable outVal = new DoubleWritable();
        private DocumentVectorSimilarity similarity;
        private LanguageDetector languageDetector;

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            this.similarity = DocumentVectorUtils.readFromConfiguration(context.getConfiguration());
            this.languageDetector = new LanguageDetector();
        }

        @Override
        protected void map(String key, WebPage page, Context context) throws IOException,
                InterruptedException {
            String language = asString(page.getMetadata().get(FieldNames.LANGUAGE));
            CharSequence text = page.getText();
            if (text != null && text.length() > 0) {
                String content = text.toString();
                Locale locale;
                if (language.isEmpty()) {
                    locale = languageDetector.classify(content).getLocale();
                } else {
                    locale = Locale.forLanguageTag(language);
                }
                double docSimilarity = similarity.getSimilarity(content, locale);
                outKey.set(getBatchId(page));
                outVal.set(docSimilarity);
                context.write(outKey, outVal);
                context.getCounter(Counters.PROCESSED).increment(1);
            } else {
                logger.debug("No content, type={}", page.getContentType());
                context.getCounter(Counters.NO_CONTENT).increment(1);
            }
        }

        protected String getBatchId(WebPage page) {
            if (page.getBatchId() != null) {
                return page.getBatchId().toString();
            } else {
                return UNKNOWN_BATCH_ID;
            }
        }
    }

    public static class BatchRelevanceReducer extends
            Reducer<Text, DoubleWritable, Text, DoubleWritable> {
        private final DoubleWritable outVal = new DoubleWritable();
        @Override
        protected void reduce(Text batchId, Iterable<DoubleWritable> similarities, Context context)
                throws IOException, InterruptedException {
            SummaryStatistics statistics = new SummaryStatistics();
            for (DoubleWritable similarity : similarities) {
                if (!Double.isInfinite(similarity.get()) && !Double.isNaN(similarity.get())) {
                    statistics.addValue(similarity.get());
                }
            }
            logger.info("statistics for batch {}: {}", batchId, statistics);
            outVal.set(statistics.getMean());
            context.write(batchId, outVal);
        }
    }

    private static class Arguments {
        @Argument(index = 0, usage = "campaign ID", required = true)
        long campaignId;
        @Argument(index = 1, usage = "reference vector", required = true)
        File referenceVectorFile;
        @Argument(index = 2, usage = "output file", required = false)
        File outputFile;

    }

    @Inject
    private CrawlDataRepositoryTemplate template;
    @Inject
    private Configuration conf;

    @Override
    public int run(String[] args) throws Exception {
        Arguments arguments = CLIUtils.parseArguments(args, new Arguments(), USAGE);
        if (!arguments.referenceVectorFile.exists()) {
            throw new IllegalArgumentException(USAGE);
        }
        DocumentVectorSimilarity reference = DocumentVectorSimilarity.readFromFile(arguments.referenceVectorFile);
        List<BatchScore> scores = computeScores(arguments.campaignId, reference);
        if (arguments.outputFile == null) {
            writeScores(scores, System.out);
        } else {
            try (PrintStream out = new PrintStream(arguments.outputFile)) {
                writeScores(scores, out);
            }
        }
        return 0;
    }

    protected void writeScores(List<BatchScore> scores, PrintStream out) {
        for (BatchScore score : scores) {
            out.format(Locale.ENGLISH, "%s\t%f%n", score.getBatchId(), score.getScore());
        }
    }

    public List<BatchScore> computeScores(long campaignId, DocumentVectorSimilarity similarity)
            throws IOException {
        DocumentVectorUtils.storeToConfiguration(conf, similarity);
        return template.jobBuilder(campaignId, "Batch relevance for campaign " + campaignId)
            .setMapper(BatchRelevanceMapper.class, Text.class, DoubleWritable.class)
            .setReducer(BatchRelevanceReducer.class, Text.class, DoubleWritable.class)
            .setFields(
                ImmutableSet.of(Field.BATCH_ID, Field.STATUS, Field.TEXT, Field.CONTENT_TYPE))
            .setupQuery(new StatusFilterQuerySetup(CrawlStatus.STATUS_FETCHED))
            .executeAndGetResult(new ResultLinesMapper<Text, DoubleWritable, List<BatchScore>>() {

                private final List<BatchScore> result = new ArrayList<>();
                @Override
                public void processLine(Text key, DoubleWritable value) {
                    result.add(new BatchScore(key.toString(), value.get()));
                }

                @Override
                public List<BatchScore> getResult() {
                    Collections.sort(result);
                    return result;
                }
            });
    }

    public static void main(String[] args) throws Exception {
        SpringApplication app = new SpringApplication(BatchRelevanceConfiguration.class);
        ApplicationContext applicationContext = app.run(args);
        int returnCode = applicationContext.getBean("batchRelevance", Tool.class).run(args);
        System.exit(returnCode);
    }

    @Override
    public void setConf(Configuration conf) {
        this.conf = conf;
    }

    @Override
    public Configuration getConf() {
        return conf;
    }
}
