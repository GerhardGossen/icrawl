package de.l3s.icrawl.evaluation;

import java.util.Arrays;
import java.util.Collection;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

public final class CLIUtils {

    private CLIUtils() {
        // prevent instantiation
    }

    public static <T> T parseArguments(String[] cliArgs, T arguments, String usage) {
        Collection<String> applicationArgs = Collections2.filter(Arrays.asList(cliArgs),
            new Predicate<String>() {
                @Override
                public boolean apply(String input) {
                    return !input.startsWith("-");
                }
            });
        CmdLineParser parser = new CmdLineParser(arguments);
        try {
            parser.parseArgument(applicationArgs);
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            System.err.println(usage);
            parser.printUsage(System.err);
            System.exit(1);
        }
        return arguments;
    }

}
