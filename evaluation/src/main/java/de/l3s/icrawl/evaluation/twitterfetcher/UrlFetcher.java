package de.l3s.icrawl.evaluation.twitterfetcher;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.CachedGauge;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.codahale.metrics.Timer.Context;
import com.codahale.metrics.httpclient.InstrumentedHttpClientConnectionManager;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.io.ByteStreams;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

import de.l3s.icrawl.nutch.Injector;
import de.l3s.icrawl.nutch.InjectorInjectionException;

/**
 * Continuously download URLs from a queue and store the results in a Nutch
 * crawl DB.
 *
 * Implements a very simple strategy, e.g. it ignores robots.txt and politeness
 * constraints.
 */
class UrlFetcher {
    private static final class UrlFetcherRunnable implements Callable<List<String>> {
        private final BlockingQueue<String> urlQueue;
        private final Injector injector;
        private final CloseableHttpClient httpClient;
        private final ExecutorService threadPool;
        private final Histogram documentSize;
        private final Meter documentsDownloaded;
        private final Timer documentDownloadTimer;
        private Meter documentSkipped;
        private Meter documentFailed;

        public UrlFetcherRunnable(BlockingQueue<String> urlQueue, Injector injector,
                MetricRegistry metrics) {
            this.urlQueue = urlQueue;
            this.injector = injector;
            this.threadPool = Executors.newFixedThreadPool(10,
                new ThreadFactoryBuilder().setNameFormat("fetcher-%d").build());
            metrics.register("fetcher.threadPool.runningTasks",
                new CachedGauge<Integer>(10, TimeUnit.SECONDS) {
                    @Override
                    protected Integer loadValue() {
                        return ((ThreadPoolExecutor) threadPool).getActiveCount();
                    }
                });
            metrics.register("fetcher.threadPool.queuedTasks",
                new CachedGauge<Integer>(10, TimeUnit.SECONDS) {
                    @Override
                    protected Integer loadValue() {
                        return ((ThreadPoolExecutor) threadPool).getQueue().size();
                    }
                });

            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory> create()
                .register("http", PlainConnectionSocketFactory.getSocketFactory())
                .register("https", SSLConnectionSocketFactory.getSocketFactory())
                .build();
            PoolingHttpClientConnectionManager connManager = new InstrumentedHttpClientConnectionManager(
                metrics, socketFactoryRegistry,
                100, TimeUnit.MICROSECONDS);
            connManager.setMaxTotal(100);
            this.httpClient = HttpClientBuilder.create()
                .setConnectionManager(connManager)
                .setUserAgent("iCrawl")
                .disableCookieManagement()
                .setDefaultRequestConfig(
                    RequestConfig.custom()
                        .setSocketTimeout(3000)
                        .setConnectTimeout(3000)
                        .setConnectionRequestTimeout(5000)
                        .build())
                .build();
            this.documentSize = metrics.histogram("fetcher.documents.size");
            this.documentsDownloaded = metrics.meter("fetcher.documents.downloaded");
            this.documentDownloadTimer = metrics.timer("fetcher.documents.downloadTime");
            this.documentSkipped = metrics.meter("fetcher.documents.skipped");
            this.documentFailed = metrics.meter("fetcher.documents.failed");
        }

        @Override
        /** Continue fetching URLs from the queue until the thread is interrupted (e.g. by {@link Future#cancel(boolean)}) */
        public List<String> call() {
            logger.info("Starting UrlFetcher");
            while (!Thread.currentThread().isInterrupted()) {
                String url = null;
                try {
                    // use poll with timeout so that 'stopped' is checked even with an empty queue
                    url = urlQueue.poll(10, TimeUnit.MILLISECONDS);
                    if (url != null) {
                        logger.debug("About to download '{}'", url);
                        threadPool.execute(new UrlFetchTask(this, url));
                    }
                } catch (InterruptedException e) {
                    logger.info("UrlFetcher was interrupted, remaining URLs: {}", urlQueue.size());
                    break;
                } catch (Exception e) {
                    logger.warn("Failed to download URL {}: ", url, e);
                }
            }
            return shutdownAndExtractFailedUrls();
        }

        private List<String> shutdownAndExtractFailedUrls() {
            List<String> queuedUrls = new ArrayList<>();
            threadPool.shutdown();
            boolean terminated;
            try {
                terminated = threadPool.awaitTermination(30, TimeUnit.SECONDS);
            } catch (InterruptedException e1) {
                terminated = false;
            }
            if (!terminated) {
                List<Runnable> failedTasks = threadPool.shutdownNow();
                for (Runnable task : failedTasks) {
                    if (task instanceof UrlFetchTask) {
                        queuedUrls.add(((UrlFetchTask) task).url);
                    }
                }
            }
            try {
                injector.close();
            } catch (IOException e) {
                logger.info("Exception while shutting down injector: ", e);
            }
            try {
                httpClient.close();
            } catch (IOException e) {
                logger.info("Exception while shutting down HTTP client", e);
            }
            return queuedUrls;
        }
    }

    /** Retrieve the URL and store it in a Nutch crawl DB */
    private static class UrlFetchTask implements Runnable {
        private final String url;
        private final UrlFetcherRunnable fetcher;

        UrlFetchTask(UrlFetcherRunnable fetcher, String url) {
            this.url = url;
            this.fetcher = fetcher;
        }

        @Override
        public void run() {
            if (fetcher.injector.hasUrl(url)) {
                logger.debug("Already fetched URL '{}', skipping", url);
                fetcher.documentSkipped.mark();
                return;
            }
            Stopwatch time = Stopwatch.createStarted();
            Context metricsTime = fetcher.documentDownloadTimer.time();
            HttpGet request = new HttpGet(url);
            final HttpClientContext context = new HttpClientContext();
            try (CloseableHttpResponse response = fetcher.httpClient.execute(request, context)) {
                byte[] content = ByteStreams.toByteArray(response.getEntity().getContent());
                fetcher.documentSize.update(content.length);
                fetcher.documentsDownloaded.mark();
                String contentType = response.getEntity().getContentType().getValue();
                Header[] responseHeaders = response.getAllHeaders();
                Map<String, String> headers = Maps.newHashMapWithExpectedSize(responseHeaders.length);
                for (Header header : responseHeaders) {
                    headers.put(header.getName(), header.getValue());
                }
                Map<String, String> metadata;
                String finalUrl;
                if (context != null && context.getRedirectLocations() != null
                        && !context.getRedirectLocations().isEmpty()) {
                    List<URI> redirectLocations = context.getRedirectLocations();
                    metadata = ImmutableMap.of("redirections", redirectLocations.toString(),
                        "originalUrl", url);
                    finalUrl = getFinalUrl(redirectLocations).toASCIIString();
                    logger.debug("Found redirection from '{}' to '{}'", url, finalUrl);
                } else {
                    metadata = Collections.emptyMap();
                    finalUrl = url;
                }
                // TODO check server response code
                // TODO check if page has large iframe
                fetcher.injector.writeDocument(finalUrl, content, contentType, headers, metadata, "twitter");
                time.stop();
                metricsTime.stop();
                logger.debug("Downloaded and stored URL '{}' in {}", url, time);
            } catch (InjectorInjectionException e) {
                logger.info("Could not store response for URL '{}': {}", url, e);
                fetcher.documentFailed.mark();
            } catch (IOException e) {
                logger.info("Could not download URL '{}': {}", url, e);
                fetcher.documentFailed.mark();
            } catch (Exception e) {
                logger.warn("Unhandled exception: ", e);
                fetcher.documentFailed.mark();
            }
        }

        private URI getFinalUrl(List<URI> redirectLocations) {
            URI uri = redirectLocations.get(redirectLocations.size() - 1);
            if (uri != null) {
                URIBuilder uriBuilder = new URIBuilder(uri);
                List<NameValuePair> remainingParams = new ArrayList<>();
                for (NameValuePair queryParam : uriBuilder.getQueryParams()) {
                    if (queryParam.getName().startsWith("utm_")) {
                        continue;
                    }
                    remainingParams.add(queryParam);
                }
                uriBuilder.setParameters(remainingParams);
                try {
                    uri = uriBuilder.build();
                } catch (URISyntaxException e) {
                    logger.info("Unexpected exception while cleaning URI '{}'", uri, e);
                }
            }
            return uri;
        }
    }

    private static final Logger logger = LoggerFactory.getLogger(UrlFetcher.class);
    private final UrlFetcherRunnable runnable;
    private Future<List<String>> future;

    /**
     * Create a new instance.
     *
     * @param urlQueue
     *            incoming links
     * @param injector
     *            connection to a Nutch crawl DB
     * @param metrics
     */
    public UrlFetcher(BlockingQueue<String> urlQueue, Injector injector, MetricRegistry metrics) {
        this.runnable = new UrlFetcherRunnable(urlQueue, injector, metrics);
    }

    /**
     * Stop the crawl at the next possible moment.
     *
     * @return the unfetched URLs
     */
    public List<String> stop() {
        future.cancel(true);
        try {
            return future.get();
        } catch (InterruptedException e) {
            logger.info("Interrupted while waiting for fetcher to stop", e);
        } catch (ExecutionException e) {
            logger.warn("Fetcher failed with exception: ", e);
        }
        return Collections.emptyList();
    }

    public void fetchContinuously() {
        future = Executors.newSingleThreadExecutor().submit(runnable);
    }
}
