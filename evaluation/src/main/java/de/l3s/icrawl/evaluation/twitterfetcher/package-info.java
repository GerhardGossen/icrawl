/**
 * Evaluation baseline that only downloads tweeted URLs.
 */
package de.l3s.icrawl.evaluation.twitterfetcher;