package de.l3s.icrawl.evaluation;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.Files;
import com.google.common.io.LineProcessor;

import de.l3s.icrawl.util.DateUtils;

public class UrlDates {
    private static final class UrlWithDateRatioCounter implements LineProcessor<Double> {
        long withDate = 0;
        long totalLines = 0;
        long falsePositives = 0;

        @Override
        public boolean processLine(String line) throws IOException {
            String[] parts = line.split("\t", 2);
            if (DateUtils.extractDateFromUrl(parts[0]) != null) {
                withDate++;
            }
            totalLines++;
            return true;
        }

        @Override
        public Double getResult() {
            UrlDates.logger.info("Got with date: {}, total: {}, falsePositives: {}", withDate,
                totalLines, falsePositives);
            return withDate / (double) totalLines;
        }
    }

    private static final Logger logger = LoggerFactory.getLogger(UrlDates.class);

    private UrlDates() {
        // prevent instatiation
    }

    public static void main(String[] args) throws IOException {
        LineProcessor<Double> callback = new UrlWithDateRatioCounter();
        for (String arg : args) {
            File file = new File(arg);
            Double urlsWithDate = Files.readLines(file, StandardCharsets.UTF_8,
                callback);
            System.out.format(Locale.ENGLISH, "Processed file %s, URLs with date: %f%n", file,
                urlsWithDate);
        }
        System.out.println("Total: " + callback.getResult());
    }

}
