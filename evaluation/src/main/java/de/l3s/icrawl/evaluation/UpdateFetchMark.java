package de.l3s.icrawl.evaluation;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.avro.util.Utf8;
import org.apache.gora.accumulo.store.AccumuloStore;
import org.apache.gora.mapreduce.GoraMapper;
import org.apache.gora.mapreduce.GoraOutputFormat;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.Job;
import org.apache.nutch.storage.Mark;
import org.apache.nutch.storage.WebPage;
import org.apache.nutch.storage.WebPage.Field;

import com.google.common.collect.ImmutableSet;

import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate.JobSetup;
import de.l3s.icrawl.util.HadoopUtils;

public class UpdateFetchMark {
    private UpdateFetchMark() {
        // prohibit instantiation
    }

    public static class UpdateFetchMarkMapper extends GoraMapper<String, WebPage, String, WebPage> {
        @Override
        protected void map(String key, WebPage page, Context context) throws IOException,
                InterruptedException {

            Map<CharSequence, CharSequence> markers = page.getMarkers();
            if (markers != null) {
                CharSequence batchId = markers.get(new Utf8("FETCH_MARK"));
                if (batchId != null) {
                    Mark.FETCH_MARK.putMark(page, new Utf8(batchId.toString()));
                    context.write(key, page);
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        long campaignId = Long.parseLong(args[0]);
        File icrawlDirectory = new File(".");
        Configuration conf = HadoopUtils.createConf(icrawlDirectory);
        try (CrawlDataRepositoryTemplate template = new CrawlDataRepositoryTemplate(conf)) {

            template.jobBuilder(campaignId, "fetchmark")
                .setMapper(UpdateFetchMarkMapper.class, String.class, WebPage.class)
                .setOutputFormat(GoraOutputFormat.class)
                .setFields(ImmutableSet.of(Field.MARKERS))
                .setupJob(new JobSetup() {
                    @Override
                    public void setup(Job job) {
                        GoraOutputFormat.setOutput(job, AccumuloStore.class, String.class,
                            WebPage.class, true);
                    }
                })
                .execute();
        }
    }
}
