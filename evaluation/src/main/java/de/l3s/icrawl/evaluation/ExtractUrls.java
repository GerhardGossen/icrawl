package de.l3s.icrawl.evaluation;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.gora.mapreduce.GoraMapper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.nutch.crawl.CrawlStatus;
import org.apache.nutch.protocol.ProtocolStatusUtils;
import org.apache.nutch.storage.WebPage;
import org.apache.nutch.storage.WebPage.Field;
import org.apache.nutch.util.TableUtil;
import org.kohsuke.args4j.Argument;
import org.openimaj.text.nlp.language.LanguageDetector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.google.common.collect.ImmutableSet;

import de.l3s.icrawl.FieldNames;
import de.l3s.icrawl.apifetcher.TwitterApiFetcher;
import de.l3s.icrawl.online.DocumentVectorSimilarity;
import de.l3s.icrawl.online.DocumentVectorUtils;
import de.l3s.icrawl.online.WebPageDateExtractor.DateSource;
import de.l3s.icrawl.online.WebPageDateExtractor.WebPageDate;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate.ResultLinesMapper;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate.StatusFilterQuerySetup;
import de.l3s.icrawl.util.DateUtils;
import de.l3s.icrawl.util.HadoopUtils;
import de.l3s.icrawl.util.HtmlParseException;
import de.l3s.icrawl.util.TextExtractor;
import de.l3s.icrawl.util.WebPageUtils;

import static de.l3s.icrawl.domain.support.ByteBufferUtils.asLong;
import static de.l3s.icrawl.domain.support.ByteBufferUtils.asString;
import static de.l3s.icrawl.online.WebPageDateExtractor.getModifiedDate;
import static de.l3s.icrawl.util.HadoopUtils.incrementCount;
import static java.lang.Math.min;

public class ExtractUrls {

    public static class ExtractUrlsMapper extends GoraMapper<String, WebPage, Text, PageMetadata> {
        static final Pattern IFRAME_URL_EXCLUSIONS = Pattern.compile("embed|/ads?|player|youtube|feed|csp|widget|likebox|recaptcha|map");
        private static final int MINIMUM_IFRAME_ABSOLUTE_SIZE = 100 /* pixels */;
        private static final int MINIMUM_IFRAME_RELATIVE_SIZE = 20 /* percent */;
        private static final Pattern SOFT_ERROR_PATTERN = Pattern.compile(
            "(resource not found)|(an error has occured)", Pattern.CASE_INSENSITIVE);

        private final PageMetadata metadata = new PageMetadata();
        private final Text outKey = new Text();
        private LanguageDetector languageDetector;
        private DocumentVectorSimilarity similarity;

        static String findIframe(Document dom, String url) {
            URL baseUrl = null;
            if (url != null) {
                try {
                    baseUrl = new URL(url);
                } catch (MalformedURLException e) {
                    logger.trace("Not a valid URL for hostname extraction: '{}'", url);
                }
            }
            NodeList nodes = dom.getElementsByTagName("iframe");
            for (int i = 0; i < nodes.getLength(); i++) {
                Element elem = (Element) nodes.item(i);
                if (!isContentIframe(elem)) {
                    continue;
                }
                String iframeSource = elem.getAttribute("src");
                if (!iframeSource.isEmpty() && isExternalUrl(baseUrl, iframeSource)) {
                    try {
                        URL iframeUrl = new URL(baseUrl, iframeSource);
                        logger.debug("Found IFrame: '{}' @ '{}': {}x{}", iframeUrl, url,
                            elem.getAttribute("width"), elem.getAttribute("height"));
                        return iframeUrl.toString();
                    } catch (MalformedURLException e) {
                        logger.trace("Not a valid URL: '{}' @ '{}': ", iframeSource, url, e);
                    }
                }
            }
            return null;
        }
        private static int countOccurrences(String text, String regex) {
            int count = 0;
            Matcher matcher = Pattern.compile(regex).matcher(text);
            while (matcher.find()) {
                count++;
            }
            return count;
        }
        private static boolean isAcceptableIframeSize(String sizeAttribute) {
            String size = sizeAttribute.trim();
            if (size.isEmpty()) {
                return true;
            }
            try {
                if (size.endsWith("%")) {
                    return Integer.parseInt(size.substring(0, size.length() - 1)) >= MINIMUM_IFRAME_RELATIVE_SIZE;
                }
                if (size.endsWith("px")) {
                    size = size.substring(0, size.length() - 2);
                }
                return Integer.parseInt(size) >= MINIMUM_IFRAME_ABSOLUTE_SIZE;
            } catch (NumberFormatException e) {
                logger.debug("Not a valid IFrame size: '{}'", size);
                return false;
            }
        }
        /**
         * Check if the given <iframe /> is large enough to be visible as a
         * content element.
         */
        private static boolean isContentIframe(Element elem) {
            String width = elem.getAttribute("width");
            String height = elem.getAttribute("height");
            if (width.isEmpty() && height.isEmpty()) {
                return false;
            }
            if (!isAcceptableIframeSize(width) || !isAcceptableIframeSize(height)) {
                return false;
            }
            return !IFRAME_URL_EXCLUSIONS.matcher(elem.getAttribute("src")).find();
        }

        private static boolean isExternalUrl(URL baseUrl, String maybeRelativeUrl) {
            if (baseUrl == null) {
                return true;
            }
            if (!maybeRelativeUrl.substring(0, min(8, maybeRelativeUrl.length())).contains(":")) {
                return true;
            }
            URL absoluteUrl;
            try {
                absoluteUrl = new URL(maybeRelativeUrl);
            } catch (MalformedURLException e) {
                logger.trace("Not a valid absolute URL: '{}': ", maybeRelativeUrl, e);
                return false;
            }
            return !Objects.equals(baseUrl.getHost(), absoluteUrl.getHost());
        }

        @Override
        protected void map(String key, WebPage page,
                Mapper<String, WebPage, Text, PageMetadata>.Context context) throws IOException,
                InterruptedException {
            if (page.getStatus() != CrawlStatus.STATUS_FETCHED) {
                incrementCount(context, Counters.NOT_FETCHED);
                return;
            }
            if (isTwitterJsonRecord(page)) {
                incrementCount(context, Counters.TWITTER_JSON);
                return;
            }

            String url = TableUtil.unreverseUrl(key);
            outKey.set(url);
            metadata.reset();
            if (page.getContent() != null) {
                boolean successFull = extractMetadata(page, url, context);
                if (!successFull) {
                    return;
                }
            } else {
                context.getCounter(Counters.NO_CONTENT).increment(1);
                logger.debug("No content for '{}': {}", url,
                    ProtocolStatusUtils.toString(page.getProtocolStatus()));
                return;
            }
            metadata.setFetchTimestamp(getFetchTime(page));
            metadata.setBatchId(page.getBatchId() != null ? page.getBatchId().toString() : null);
            context.write(outKey, metadata);
        }

        private boolean isTwitterJsonRecord(WebPage page) {
            return page.getContentType() != null
                    && TwitterApiFetcher.TWITTER_MIME_TYPE.equals(page.getContentType().toString());
        }

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            similarity = DocumentVectorUtils.readFromConfiguration(context.getConfiguration());
            languageDetector = new LanguageDetector();
        }

        private boolean extractMetadata(WebPage page, String url,
                Mapper<String, WebPage, Text, PageMetadata>.Context context) {
            Document dom;
            try {
                dom = WebPageUtils.parseHtml(page.getContent(), url);
            } catch (HtmlParseException | DOMException | ArrayIndexOutOfBoundsException e) {
                logger.debug("Could not parse document '{}':", url, e);
                return false;
            }
            try {
                metadata.setModified(getModifiedDate(url, dom, page.getModifiedTime(), context));
                incrementCount(context, metadata.getDateSource());
                String iframeUrl = findIframe(dom, url);
                logger.trace("Found IFrame '{}'", iframeUrl);
                String text = extractText(dom, context);
                if (text == null || text.isEmpty()) {
                    return false;
                }
                if (SOFT_ERROR_PATTERN.matcher(text).find()) {
                    // content contains a 404/500 error message
                    incrementCount(context, Counters.SOFT_ERROR);
                    return false;
                }

                Locale locale = extractLanguage(page, text);
                metadata.setNumTokens(countOccurrences(text, "\\s+"));
                metadata.setNumHashtags(countOccurrences(text, "\\b#\\w"));
                metadata.setScore(similarity.getSimilarity(text, locale));
                incrementCount(context, Counters.PROCESSED);
            } catch (IllegalArgumentException e) {
                logger.debug("Could not analyze document '{}': ", url, e);
                return false;
            }
            return true;
        }

        private Locale extractLanguage(WebPage page, String text) {
            Locale locale;
            String language = asString(page.getMetadata().get(FieldNames.LANGUAGE));
            if (language.isEmpty()) {
                locale = languageDetector.classify(text).getLocale();
                language = locale.getLanguage();
            } else {
                locale = Locale.forLanguageTag(language);
            }
            metadata.setLanguage(language);
            return locale;
        }

        private String extractText(Document dom, Context context) {
            String text = TextExtractor.extractText(dom);
            if (text.trim().isEmpty()) {
                text = extractTextFromMetaTags(dom).trim();
                if (text.isEmpty()) {
                    text = WebPageUtils.extractTextFromJavascript(dom).trim();
                } else {
                    incrementCount(context, Counters.TEXT_FROM_META);
                }
                if (text.isEmpty()) {
                    incrementCount(context, Counters.EMPTY_DOCUMENT);
                } else {
                    incrementCount(context, Counters.TEXT_FROM_JAVASCRIPT);
                }
            }
            return text;
        }

        private String extractTextFromMetaTags(Document dom) {
            StringBuilder sb = new StringBuilder();
            NodeList metaElements = dom.getElementsByTagName("meta");
            for (int i = 0; i < metaElements.getLength(); i++) {
                handleMetaElement((Element) metaElements.item(i), sb);
            }
            return sb.toString().trim();
        }

        private long getFetchTime(WebPage page) {
            ByteBuffer bb = page.getMetadata().get(FieldNames.FETCH_TIME_KEY);
            return bb == null ? -1L : asLong(bb);
        }

        private void handleMetaElement(Element metaElem, StringBuilder sb) {
            for (String name : DateUtils.META_ATTRIBUTE_NAMES) {
                String attribute = metaElem.getAttribute(name);
                if (attribute != null) {
                    attribute = attribute.toLowerCase(Locale.ENGLISH);
                    if (attribute.contains("title") || attribute.contains("description")) {
                        sb.append("\n\n").append(metaElem.getAttribute("content"));
                    }
                }
            }
        }
    }

    public static class PageMetadata implements Writable {
        private String batchId;
        private DateSource dateSource;
        private long fetchTimestamp = -1;
        private String language;
        private long modifiedTimestamp = -1;
        private int numHashtags;
        private int numTokens;
        private double score = -1;

        public String getBatchId() {
            return batchId;
        }

        public DateSource getDateSource() {
            return dateSource;
        }

        public long getFetchTimestamp() {
            return fetchTimestamp;
        }

        public String getLanguage() {
            return language;
        }

        public long getModifiedTimestamp() {
            return modifiedTimestamp;
        }

        public int getNumHashtags() {
            return numHashtags;
        }

        public int getNumTokens() {
            return numTokens;
        }

        public double getScore() {
            return score;
        }

        @Override
        public void readFields(DataInput in) throws IOException {
            score = in.readDouble();
            fetchTimestamp = in.readLong();
            modifiedTimestamp = in.readLong();
            language = in.readUTF();
            String dateSourceIn = in.readUTF();
            dateSource = dateSourceIn.isEmpty() ? null : DateSource.valueOf(dateSourceIn);
            batchId = in.readUTF();
            numTokens = in.readInt();
            numHashtags = in.readInt();
        }

        public void reset() {
            score = -1;
            fetchTimestamp = -1;
            modifiedTimestamp = -1;
            language = null;
            dateSource = DateSource.NOT_FOUND;
            batchId = null;
            numTokens = -1;
            numHashtags = -1;
        }
        public void setBatchId(String batchId) {
            this.batchId = batchId;
        }

        public void setDateSource(DateSource dateSource) {
            this.dateSource = dateSource;
        }

        public void setFetchTimestamp(long fetchTimestamp) {
            this.fetchTimestamp = fetchTimestamp;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public void setModified(WebPageDate modified) {
            if (modified != null) {
                setModifiedTimestamp(modified.date.getMillis());
                setDateSource(modified.dateSource);
            } else {
                setModifiedTimestamp(-1);
                setDateSource(DateSource.NOT_FOUND);
            }
        }

        public void setModifiedTimestamp(long modifiedTimestamp) {
            this.modifiedTimestamp = modifiedTimestamp;
        }

        public void setNumHashtags(int numHashtags) {
            this.numHashtags = numHashtags;
        }

        public void setNumTokens(int numTokens) {
            this.numTokens = numTokens;
        }

        public void setScore(double score) {
            this.score = score;
        }

        @Override
        public String toString() {
            return String.format(Locale.ENGLISH, "%f\t%d\t%d\t%s\t%s\t%s\t%d\t%d", score,
                fetchTimestamp, modifiedTimestamp, language, dateSource, batchId, numTokens,
                numHashtags);
        }

        @Override
        public void write(DataOutput out) throws IOException {
            out.writeDouble(score);
            out.writeLong(fetchTimestamp);
            out.writeLong(modifiedTimestamp);
            out.writeUTF(language);
            out.writeUTF(dateSource != null ? dateSource.name() : "");
            out.writeUTF(batchId != null ? batchId : "");
            out.writeInt(numTokens);
            out.writeInt(numHashtags);
        }

    }

    private static class Arguments {
        @Argument(index = 0, usage = "campaign ID", required = true)
        public long campaignId;
        @Argument(index = 2, usage = "output file", required = false)
        public File outputFile;
        @Argument(index = 1, usage = "reference vector", required = true)
        public File referenceVector;
    }

    private enum Counters {
        EMPTY_DOCUMENT, NO_CONTENT, NOT_FETCHED, PROCESSED, SOFT_ERROR, TEXT_FROM_JAVASCRIPT, TEXT_FROM_META, TWITTER_JSON
    }

    private static final Logger logger = LoggerFactory.getLogger(ExtractUrls.class);

    private ExtractUrls() {
        // prohibit instantiation
    }

    public static void main(String[] args) throws IOException {
        Arguments arguments = CLIUtils.parseArguments(args, new Arguments(), "Usage: java "
                + ExtractUrls.class.getName() + " campaignId referenceVector");
        File icrawlDirectory = new File(".");
        Configuration conf = HadoopUtils.createConf(icrawlDirectory);

        DocumentVectorUtils.storeToConfiguration(conf,
            DocumentVectorSimilarity.readFromFile(arguments.referenceVector));

        try (CrawlDataRepositoryTemplate template = new CrawlDataRepositoryTemplate(conf);
                final PrintStream out = arguments.outputFile != null ? new PrintStream(
                    arguments.outputFile) : System.out) {

            Set<Field> fields = ImmutableSet.of(Field.FETCH_TIME, Field.STATUS, Field.METADATA,
                Field.CONTENT, Field.BATCH_ID, Field.PROTOCOL_STATUS, Field.CONTENT_TYPE,
                Field.PREV_FETCH_TIME);
            template.jobBuilder(arguments.campaignId, "extract URLs")
                .setMapper(ExtractUrlsMapper.class, Text.class, PageMetadata.class)
                .setFields(fields)
                .setupQuery(new StatusFilterQuerySetup(CrawlStatus.STATUS_FETCHED))
                .skipReduce(Text.class, PageMetadata.class)
                .executeAndGetResult(new ResultLinesMapper<Text, PageMetadata, Void>() {

                    @Override
                    public Void getResult() {
                        return null;
                    }

                    @Override
                    public void processLine(Text key, PageMetadata value) {
                        out.print(key.toString());
                        out.print('\t');
                        out.println(value);
                    }
                });
        }

    }
}
