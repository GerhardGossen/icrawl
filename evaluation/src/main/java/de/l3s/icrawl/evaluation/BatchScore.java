package de.l3s.icrawl.evaluation;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

import com.google.common.base.Objects;

public class BatchScore implements Writable, Comparable<BatchScore> {
    private String batchId;
    private double score;

    protected BatchScore() {}

    public BatchScore(String batchId, double score) {
        this.batchId = batchId;
        this.score = score;
    }

    public String getBatchId() {
        return batchId;
    }

    public double getScore() {
        return score;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeUTF(batchId);
        out.writeDouble(score);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        this.batchId = in.readUTF();
        this.score = in.readDouble();
    }

    @Override
    public String toString() {
        return String.format("%s\t%s", batchId, score);
    }

    @Override
    public int compareTo(BatchScore o) {
        String[] parts = batchId.split("-", 2);
        String[] oParts = o.batchId.split("-", 2);
        if (parts.length != oParts.length) {
            throw new IllegalArgumentException(String.format("Uncomparable batch IDs: '%s', '%s'",
                batchId, o.batchId));
        }
        for (int i = 0; i < parts.length; i++) {
            int cmp = Integer.compare(Integer.parseInt(parts[i]), Integer.parseInt(oParts[i]));
            if (cmp != 0) {
                return cmp;
            }
        }
        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof BatchScore)) {
            return false;
        }
        BatchScore o = (BatchScore) obj;
        return Objects.equal(batchId, o.batchId) && Objects.equal(score, o.score);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(batchId, score);
    }
}
