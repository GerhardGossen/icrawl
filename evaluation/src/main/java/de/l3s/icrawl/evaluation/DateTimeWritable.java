package de.l3s.icrawl.evaluation;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.chrono.ISOChronology;

import com.google.common.base.Preconditions;
import com.google.common.collect.ComparisonChain;

public class DateTimeWritable implements WritableComparable<DateTimeWritable> {
    private enum Type {
        DATE_TIME('T'), DURATION('D');
        private final char identifier;

        Type(char identifier) {
            this.identifier = identifier;
        }

        static Type fromIdentifier(char identifier) {
            for (Type t : Type.values()) {
                if (t.identifier == identifier) {
                    return t;
                }
            }
            throw new IllegalArgumentException("Unknown identifier: " + identifier);
        }
    }


    private String key;
    private Type type;
    private Comparable<?> payload;

    public DateTimeWritable() {}

    public DateTimeWritable(String key, DateTime dt) {
        this.key = key;
        this.type = Type.DATE_TIME;
        this.payload = dt;
    }

    public DateTimeWritable(String key, Duration duration) {
        this.key = key;
        this.type = Type.DURATION;
        this.payload = duration;
    }

    public String getKey() {
        return key;
    }

    public DateTime getDateTime() {
        Preconditions.checkState(type == Type.DATE_TIME, "Expected type == DATE_TIME, got '%s'", type);
        return (DateTime) payload;
    }

    public Duration getDuration() {
        Preconditions.checkState(type == Type.DURATION, "Expected type == DURATION, got '%s'", type);
        return (Duration) payload;
    }

    public Comparable<?> getPayload() {
        return payload;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeUTF(key);
        out.writeChar(type.identifier);
        switch (type) {
        case DATE_TIME:
            out.writeLong(((DateTime) payload).getMillis());
            break;
        case DURATION:
            out.writeLong(((Duration) payload).getMillis());
            break;
        default:
            throw new IllegalStateException("Unhandled type " + type);
        }
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        this.key = in.readUTF();
        this.type = Type.fromIdentifier(in.readChar());
        switch (type) {
        case DATE_TIME:
            this.payload = new DateTime(in.readLong(), ISOChronology.getInstance());
            break;
        case DURATION:
            this.payload = Duration.millis(in.readLong());
            break;
        default:
            throw new IllegalStateException("Unhandled type " + type);
        }

    }

    @Override
    public int compareTo(DateTimeWritable o) {
        return ComparisonChain.start()
            .compare(key, o.key)
            .compare(type, o.type)
            .compare(payload, o.payload)
            .result();
    }

    public void setDateTime(DateTime dateTime) {
        this.type = Type.DATE_TIME;
        this.payload = dateTime;
    }

    public void setDuration(Duration duration) {
        this.type = Type.DURATION;
        this.payload = duration;
    }

}
