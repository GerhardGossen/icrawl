package de.l3s.icrawl.evaluation;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.math.stat.descriptive.DescriptiveStatistics;
import org.apache.gora.mapreduce.GoraMapper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.util.Tool;
import org.apache.nutch.crawl.CrawlStatus;
import org.apache.nutch.storage.WebPage;
import org.apache.nutch.storage.WebPage.Field;
import org.apache.nutch.util.TableUtil;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.ReadableInstant;
import org.joda.time.ReadablePartial;
import org.kohsuke.args4j.Argument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.google.common.base.Joiner;
import com.google.common.base.Joiner.MapJoiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSortedMap;

import de.l3s.icrawl.FieldNames;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate.ResultLinesMapper;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate.StatusFilterQuerySetup;
import de.l3s.icrawl.util.DateUtils;
import de.l3s.icrawl.util.HadoopUtils;

import static de.l3s.icrawl.domain.support.ByteBufferUtils.asLong;

@Component
public class FreshnessComputation implements Tool {
    private static final class TimesHistogramMapper implements
            ResultLinesMapper<DateTimeWritable, LongWritable, TimesHistogram> {
        private final Map<DateTime, Long> modifiedTimes = new HashMap<>();
        private final Map<Duration, Long> freshnessDurations = new HashMap<>();

        @Override
        public void processLine(DateTimeWritable key, LongWritable count) {
            if (MODIFIED_KEY.equals(key.getKey())) {
                modifiedTimes.put(key.getDateTime(), count.get());
            } else if (FRESHNESS_KEY.equals(key.getKey())) {
                freshnessDurations.put(key.getDuration(), count.get());
            } else {
                logger.info("Unknown key '{}' in {} => {}", key.getKey(), key, count.get());
            }
        }

        @Override
        public TimesHistogram getResult() {
            return new TimesHistogram(modifiedTimes, freshnessDurations);
        }
    }

    public static class TimesHistogram {

        private final ImmutableSortedMap<DateTime, Long> modifiedTimes;
        private final ImmutableSortedMap<Duration, Long> freshnessDurations;

        public TimesHistogram(Map<DateTime, Long> modifiedTimes,
                Map<Duration, Long> freshnessDurations) {
            this.modifiedTimes = ImmutableSortedMap.copyOf(modifiedTimes);
            this.freshnessDurations = ImmutableSortedMap.copyOf(freshnessDurations);
        }

        public Map<DateTime, Long> getModifiedTimes() {
            return modifiedTimes;
        }

        public Map<Duration, Long> getFreshnessDurations() {
            return freshnessDurations;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("modifiedTimes:\n");
            MapJoiner joiner = Joiner.on("\n").withKeyValueSeparator(": ");
            joiner.appendTo(sb, modifiedTimes);
            sb.append("\n\nfreshnessDurations:\n");
            joiner.appendTo(sb, freshnessDurations);
            sb.append("\n");
            return sb.toString();
        }
    }

    private static final String FRESHNESS_KEY = "freshness";
    private static final String MODIFIED_KEY = "modified";

    @org.springframework.context.annotation.Configuration
    @EnableAutoConfiguration
    @PropertySource("application.properties")
    public static class FreshnessConfiguration {
        @Bean
        Configuration conf(@Value("${icrawl.directory}") File appDirectory) {
            return HadoopUtils.createConf(appDirectory);
        }

        @Bean
        CrawlDataRepositoryTemplate template(Configuration conf) {
            return new CrawlDataRepositoryTemplate(conf);
        }

        @Bean
        FreshnessComputation freshnessComputation() {
            return new FreshnessComputation();
        }
    }

    enum Counters {
        PROCESSED, NO_CONTENT, NO_FETCH_TIME, BOGUS_MODIFICATION_TIME, ERROR, CRAWL_BEFORE_MODIFY
    }

    private static final Logger logger = LoggerFactory.getLogger(FreshnessComputation.class);

    public static class FreshnessMapper extends
            GoraMapper<String, WebPage, DateTimeWritable, LongWritable> {
        private static final ReadableInstant CUTOFF_DATE = new DateTime(2000, 1, 1, 0, 0, 0);
        private final DateTimeWritable modifiedKey = new DateTimeWritable(MODIFIED_KEY,
            DateTime.now());
        private final DateTimeWritable freshnessKey = new DateTimeWritable(FRESHNESS_KEY,
            Duration.ZERO);
        private final LongWritable outVal = new LongWritable(1);

        private ReadablePartial dateMask;
        private long durationTruncation;

        @Override
        protected void setup(Context context)
                throws IOException, InterruptedException {
            this.dateMask = context.getConfiguration()
                .getEnum("modified.truncation", DateUtils.DateTimeTruncation.HOUR)
                .getMask();
            this.durationTruncation = context.getConfiguration()
                .getEnum("freshness.truncation", DateUtils.DurationTruncation.HOUR)
                .getMillis();
            Preconditions.checkArgument(durationTruncation > 0,
                "Illegal durationTruncation value %s", durationTruncation);
        }

        @Override
        protected void map(String key, WebPage page, Context context) throws IOException,
                InterruptedException {
            try {
                ByteBuffer fetchTimeBB = page.getMetadata().get(FieldNames.FETCH_TIME_KEY);
                if (fetchTimeBB == null) {
                    HadoopUtils.incrementCount(context, Counters.NO_FETCH_TIME);
                    return;
                }

                String url = TableUtil.unreverseUrl(key);
                long modifiedDateMillis = asLong(page.getMetadata().get(FieldNames.MODIFIED_DATE));
                DateTime modifiedDate = new DateTime(modifiedDateMillis);
                if (modifiedDateMillis != 0L) {
                    HadoopUtils.incrementCount(context, Counters.PROCESSED);
                    if (modifiedDate.isBefore(CUTOFF_DATE) || modifiedDate.isAfterNow()) {
                        logger.debug("Bogus modification date: {} for URL {}", modifiedDate, url);
                        HadoopUtils.incrementCount(context, Counters.BOGUS_MODIFICATION_TIME);
                        return;
                    }
                } else {
                    HadoopUtils.incrementCount(context, Counters.NO_CONTENT);
                    return;
                }
                DateTime modifiedDateTime = DateUtils.truncateDate(modifiedDate, dateMask);
                modifiedKey.setDateTime(modifiedDateTime);
                context.write(modifiedKey, outVal);
                DateTime fetchDateTime = new DateTime(page.getFetchTime());
                Duration freshness;
                if (fetchDateTime.isAfter(modifiedDateTime)) {
                    freshness = new Duration(modifiedDateTime, fetchDateTime);
                } else {
                    HadoopUtils.incrementCount(context, Counters.CRAWL_BEFORE_MODIFY);
                    freshness = new Duration(fetchDateTime, modifiedDateTime);
                }
                freshness = DateUtils.truncateDuration(freshness, durationTruncation);
                freshnessKey.setDuration(freshness);
                context.write(freshnessKey, outVal);
            } catch (Exception e) {
                logger.info("Exception while mapping: ", e);
                HadoopUtils.incrementCount(context, Counters.ERROR);
            }
        }
    }

    private static class Arguments {

        @Argument(index = 0, usage = "campaign ID", required = true)
        public long campaignId;

        @Argument(index = 1, usage = "output file", required = false)
        public File outputFile;

    }

    public static class FreshnessReducer extends
            Reducer<DateTimeWritable, LongWritable, DateTimeWritable, LongWritable> {
        private final LongWritable outVal = new LongWritable();

        @Override
        protected void reduce(DateTimeWritable key, Iterable<LongWritable> counts, Context context)
                throws IOException, InterruptedException {
            long sum = 0;
            for (LongWritable count : counts) {
                sum += count.get();
            }

            outVal.set(sum);
            context.write(key, outVal);
        }
    }

    @Inject
    private CrawlDataRepositoryTemplate template;
    @Inject
    private Configuration conf;

    @Override
    public int run(String[] args) throws IOException {
        Arguments arguments = CLIUtils.parseArguments(args, new Arguments(), "Usage: java "
                + getClass().getName() + " crawlId");

        TimesHistogram freshness = computeFreshness(arguments.campaignId);
        Map<Duration, Long> durations = freshness.getFreshnessDurations();
        DescriptiveStatistics statistics = new DescriptiveStatistics();
        try (PrintStream dataWriter = new PrintStream(new File("data.tsv"))) {
            for (Map.Entry<Duration, Long> entry : durations.entrySet()) {
                long standardHours = entry.getKey().getStandardHours();
                for (long i = 0; i < entry.getValue(); i++) {
                    statistics.addValue(standardHours);
                }
                dataWriter.format(Locale.ENGLISH, "%d\t%d%n", standardHours, entry.getValue().longValue());
            }
        }
        try (PrintStream out = arguments.outputFile != null ? new PrintStream(arguments.outputFile) : System.out) {
            out.format(Locale.ENGLISH, "mean\t%f%n", statistics.getMean());
            out.format(Locale.ENGLISH, "min\t%f%n", statistics.getMin());
            out.format(Locale.ENGLISH, "max\t%f%n", statistics.getMax());
            out.format(Locale.ENGLISH, "stdev\t%f%n", statistics.getStandardDeviation());
            out.format(Locale.ENGLISH, "25\t%f%n", statistics.getPercentile(25));
            out.format(Locale.ENGLISH, "50\t%f%n", statistics.getPercentile(50));
            out.format(Locale.ENGLISH, "75\t%f%n", statistics.getPercentile(75));
            out.format(Locale.ENGLISH, "100\t%f%n", statistics.getPercentile(100));
        }
        return 0;
    }

    public TimesHistogram computeFreshness(long campaignId) throws IOException {
        return template.jobBuilder(campaignId, "Freshness for campaign " + campaignId)
            .setMapper(FreshnessMapper.class, DateTimeWritable.class, LongWritable.class)
            .setReducer(FreshnessReducer.class, DateTimeWritable.class, LongWritable.class)
            .setFields(ImmutableSet.of(Field.METADATA, Field.STATUS))
            .setupQuery(new StatusFilterQuerySetup(CrawlStatus.STATUS_FETCHED))
            .executeAndGetResult(new TimesHistogramMapper());
    }

    public static void main(String[] args) throws Exception {
        SpringApplication app = new SpringApplication(FreshnessConfiguration.class);
        ApplicationContext applicationContext = app.run(args);
        int returnCode = applicationContext.getBean("freshnessComputation", Tool.class).run(args);
        System.exit(returnCode);
    }

    @Override
    public void setConf(Configuration conf) {
        this.conf = conf;
    }

    @Override
    public Configuration getConf() {
        return conf;
    }
}
