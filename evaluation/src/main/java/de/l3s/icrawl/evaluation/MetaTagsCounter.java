package de.l3s.icrawl.evaluation;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.apache.gora.mapreduce.GoraMapper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.reduce.LongSumReducer;
import org.apache.nutch.crawl.CrawlStatus;
import org.apache.nutch.storage.WebPage;
import org.apache.nutch.storage.WebPage.Field;
import org.apache.nutch.util.TableUtil;
import org.cyberneko.html.parsers.SAXParser;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multiset.Entry;
import com.google.common.collect.Multisets;
import com.google.common.collect.Sets;

import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate.ResultLinesMapper;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate.StatusFilterQuerySetup;
import de.l3s.icrawl.util.ByteBufferBackedInputStream;
import de.l3s.icrawl.util.DateUtils;
import de.l3s.icrawl.util.HadoopUtils;

public class MetaTagsCounter {
    public enum Dates {
        URL, META, NO_DATE, DATES_DIFFER

    }

    private static final Logger logger = LoggerFactory.getLogger(MetaTagsCounter.class);

    public enum Counters {
        NO_CONTENT, PROCESSED, PROCESSING_FAILED
    }

    public static class MetaTagsMapper extends GoraMapper<String, WebPage, Text, LongWritable> {
        private final class MetaTagCollector extends DefaultHandler {
            private final Set<String> attributes = new HashSet<>();
            private DateTime date;

            @Override
            public void startElement(String uri, String localName, String qName, Attributes atts)
                    throws SAXException {
                if ("meta".equalsIgnoreCase(localName)) {
                    String attribute = null;
                    for (String name : DateUtils.META_ATTRIBUTE_NAMES) {
                        String value = atts.getValue("", name);
                        if (value != null) {
                            attribute = name;
                        }
                    }

                    if (attribute != null) {
                        String key = atts.getValue("", attribute);
                        if (DateUtils.dateMetaKey(key)) {
                            String value = atts.getValue("", "content");
                            if (this.date == null) {
                                DateTime parsedDate = DateUtils.liberalParseDate(value);
                                this.date = parsedDate;
                            }
                        }
                    } else if (logger.isDebugEnabled()) {
                        logAttributeNames(atts);
                    }
                }
            }

            private void logAttributeNames(Attributes atts) {
                Set<String> names = Sets.newHashSetWithExpectedSize(atts.getLength());
                for (int i = 0; i < atts.getLength(); i++) {
                    names.add(atts.getQName(i));
                }
                if (names.size() != 1 || !names.contains("charset")) {
                    logger.debug("No known attributes, qnames: {}", names);
                }
            }

            public Set<String> getAttributes() {
                return Collections.unmodifiableSet(attributes);
            }

            public DateTime getDate() {
                return date;
            }
        }

        private final Text outKey = new Text();
        private static final LongWritable ONE = new LongWritable(1);
        private SAXParser parser;

        @Override
        protected void setup(Mapper<String, WebPage, Text, LongWritable>.Context context)
                throws IOException, InterruptedException {
            parser = new SAXParser();
        }

        @Override
        protected void map(String key, WebPage value, final Context context) throws IOException,
                InterruptedException {
            if (value.getContent() == null) {
                context.getCounter(Counters.NO_CONTENT).increment(1);
                return;
            }
            try {
                MetaTagCollector metaTagCollector = collectMetaTags(value, context);
                LocalDate urlDate = DateUtils.extractDateFromUrl(TableUtil.unreverseUrl(key));
                DateTime metaDate = metaTagCollector.getDate();
                if (urlDate != null) {
                    context.getCounter(Dates.URL).increment(1);
                }
                if (metaDate != null) {
                    context.getCounter(Dates.META).increment(1);
                }
                if (urlDate == null && metaDate == null) {
                    context.getCounter(Dates.NO_DATE).increment(1);
                }
                if (datesDiffer(urlDate, metaDate)) {
                    logger.debug("Got different dates: URL={}, meta={}", urlDate, metaDate);
                    context.getCounter(Dates.DATES_DIFFER).increment(1);
                }
            } catch (Exception e) {
                logger.info("Exception while parsing document for URL '{}'", key, e);
                context.getCounter(Counters.PROCESSING_FAILED).increment(1);
                return;
            }
            context.getCounter(Counters.PROCESSED).increment(1);
        }

        private boolean datesDiffer(LocalDate urlDate, DateTime metaDate) {
            return urlDate != null && metaDate != null && !metaDate.toLocalDate().equals(urlDate);
        }

        private MetaTagCollector collectMetaTags(WebPage value, final Context context)
                throws SAXException, IOException, InterruptedException {
            InputSource source = new InputSource(new ByteBufferBackedInputStream(
                value.getContent()));
            MetaTagCollector metaTagCollector = new MetaTagCollector();
            parser.setContentHandler(metaTagCollector);
            parser.parse(source);
            for (String attribute : metaTagCollector.getAttributes()) {
                outKey.set(attribute);
                context.write(outKey, ONE);
            }
            return metaTagCollector;
        }
    }

    public static class MetaTagsReducer extends LongSumReducer<Text> {}

    public static void main(String[] args) throws IOException {
        if (args.length < 1) {
            System.err.println("Usage: java " + MetaTagsCounter.class.getName() + " campaignId");
            System.exit(1);
        }
        long campaignId = Long.parseLong(args[0]);
        File icrawlDirectory = new File(".");
        Configuration conf = HadoopUtils.createConf(icrawlDirectory);

        try (CrawlDataRepositoryTemplate template = new CrawlDataRepositoryTemplate(conf)) {
            Multiset<String> tags = template.jobBuilder(campaignId, "Metatags counter")
                .setMapper(MetaTagsMapper.class, Text.class, LongWritable.class)
                .setFields(ImmutableSet.of(Field.CONTENT, Field.STATUS))
                .setReducer(MetaTagsReducer.class, Text.class, LongWritable.class)
                .setupQuery(new StatusFilterQuerySetup(CrawlStatus.STATUS_FETCHED))
                .executeAndGetResult(new ResultLinesMapper<Text, LongWritable, Multiset<String>>() {
                    private final Multiset<String> result = HashMultiset.create();
                    @Override
                    public void processLine(Text key, LongWritable value) {
                        result.add(key.toString(), (int) value.get());
                    }

                    @Override
                    public Multiset<String> getResult() {
                        return Multisets.copyHighestCountFirst(result);
                    }
                });
            for (Entry<String> tag : Iterables.limit(tags.entrySet(), 100)) {
                System.out.format("%s\t%d%n", tag.getElement(), tag.getCount());
            }
        }
    }

}
