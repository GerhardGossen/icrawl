package de.l3s.icrawl.evaluation;

import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.nutch.util.URLUtil;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowCallbackHandler;

import com.google.common.base.Joiner;
import com.google.common.base.Throwables;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;
import com.google.common.collect.Table;

public class ScoresDistribution {

    private static final class ScoreAndFreshnessGrouper implements RowCallbackHandler {
        private final Multimap<String, Long> domainsWithFreshness;
        private final Multimap<String, Double> domainsWithScore;

        private ScoreAndFreshnessGrouper(Multimap<String, Double> domainsWithScore,
                Multimap<String, Long> domainsWithFreshness) {
            this.domainsWithFreshness = domainsWithFreshness;
            this.domainsWithScore = domainsWithScore;
        }

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            String domainName;
            try {
                domainName = URLUtil.getDomainName(rs.getString("url"));
            } catch (MalformedURLException e) {
                throw Throwables.propagate(e);
            }
            double score = rs.getDouble("score");
            domainsWithScore.put(domainName, score);
            if (rs.getDate("modifiedtime") != null) {
                long fetchDate = rs.getDate("fetchtime").getTime();
                long modDate = rs.getDate("modifiedtime").getTime();

                long freshness = fetchDate - modDate;
                domainsWithFreshness.put(domainName, freshness);
            }
        }
    }

    private static final String TABLE = "urls_ukraine2";
    private static final int NUM_DOMAINS = 10;
    private static final String COMMON_SQL_CRITERIA = " AND modifiedtime IS NOT NUL AND language = 'en' AND tokens >= ?";

    private static final int MIN_TOKENS_IN_DOCUMENT = 5;
    private static final double INCREMENT = 0.1;
    private static final String HISTOGRAM_QUERY = "SELECT COUNT(url) FROM " + TABLE
            + " WHERE score >= ? AND SCORE < ? AND tokens >= "
            + MIN_TOKENS_IN_DOCUMENT;
    private static final String LANGUAGE_QUERY_SELECT = "SELECT language, COUNT(language) AS count FROM "
            + TABLE + " WHERE tokens >= "
            + MIN_TOKENS_IN_DOCUMENT;
    private static final String LANGUAGE_QUERY_GROUP = " GROUP by language ORDER BY count DESC";
    private static final String LANGUAGE_QUERY = LANGUAGE_QUERY_SELECT + LANGUAGE_QUERY_GROUP;
    private static int NUM_LANGUAGES = 5;

    private ScoresDistribution() {
        // prevent instatiation
    }

    public static void main(String[] args) throws SQLException, IOException {
        BasicDataSource ds = DBUtils.createDataSource();
        PrintStream out = System.out;
        try {
            JdbcTemplate template = new JdbcTemplate(ds);
            List<String> crawls = template.queryForList("SELECT DISTINCT crawl FROM " + TABLE,
                String.class);
            for (String crawl : crawls) {
                out.println("\n" + crawl);
                final Multimap<String, Double> domainsWithScore = ArrayListMultimap.create();
                final Multimap<String, Long> domainsWithFreshness = ArrayListMultimap.create();
                template.query("SELECT url, score, fetchtime, modifiedtime FROM " + TABLE
                        + " WHERE crawl = ?"
                            + COMMON_SQL_CRITERIA,
                    new ScoreAndFreshnessGrouper(domainsWithScore, domainsWithFreshness),
                    crawl /*, MIN_TOKENS_IN_DOCUMENT*/);
                Map<String, Double> avgScores = computeAverageDoubleValues(domainsWithScore);
                Map<String, Long> avgFreshness = computeAverageLongValues(domainsWithFreshness);
                Multiset<String> sortedDomains = getDomainsByFrequency(template, crawl);
                for (Multiset.Entry<String> string : Iterables.limit(sortedDomains.entrySet(), NUM_DOMAINS)) {
                    Long avgDomainFreshness = avgFreshness.get(string.getElement());
                    double freshness = avgDomainFreshness != null ? avgDomainFreshness
                            / (1000.0 * 60 * 60)
                            : -1.0;
                    out.format(Locale.ENGLISH, "%20s\t& %.2f\t& %,9.2f\t& %3d%n",
                        string.getElement(), avgScores.get(string.getElement()), freshness,
                        string.getCount());
                }
            }

            for (String crawl : crawls) {
                printScoreDistribution(template, crawl, "en");
            }
            printScoreDistribution(template, null, "en");

            printLanguageDistribution(template, crawls, System.out);

        } finally {
            ds.close();
        }

    }

    private static Multiset<String> getDomainsByFrequency(JdbcTemplate template,
            String crawl) throws MalformedURLException {
        Multiset<String> domains = HashMultiset.create();
        List<String> crawlUrls = template.queryForList("SELECT url FROM " + TABLE
                + " WHERE crawl = ?"
                + COMMON_SQL_CRITERIA, String.class, crawl/*, MIN_TOKENS_IN_DOCUMENT*/);
        for (String url : crawlUrls) {
            String domainName = URLUtil.getDomainName(url);
            domains.add(domainName);
        }
        return Multisets.copyHighestCountFirst(domains);
    }

    private static <T> Map<T, Double> computeAverageDoubleValues(Multimap<T, Double> values) {
        Map<T, Double> averages = new HashMap<>();
        for (Entry<T, Collection<Double>> entry : Multimaps.asMap(values).entrySet()) {
            double sum = 0.0;
            for (Double d : entry.getValue()) {
                sum += d;
            }
            double avg = sum / entry.getValue().size();
            averages.put(entry.getKey(), avg);
        }
        return averages;
    }

    private static <T> Map<T, Long> computeAverageLongValues(Multimap<T, Long> values) {
        Map<T, Long> averages = new HashMap<>();
        for (Entry<T, Collection<Long>> entry : Multimaps.asMap(values).entrySet()) {
            long sum = 0L;
            for (Long d : entry.getValue()) {
                sum += d;
            }
            long avg = sum / entry.getValue().size();
            averages.put(entry.getKey(), avg);
        }
        return averages;
    }

    private static void printLanguageDistribution(JdbcTemplate template, List<String> crawls,
            PrintStream out) throws IOException {
        Table<String, String, Long> languages = HashBasedTable.create();
        for (String crawl : crawls) {
            languages.column(crawl).putAll(getLanguageDistribution(template, crawl));
        }
        languages.column("all").putAll(getLanguageDistribution(template, null));
        Set<String> topLanguages = findMostFrequentLanguages(languages);

        crawls.add("all");
        Joiner.on("\t").appendTo(out, Iterables.concat(Collections.singleton("language"), crawls));
        out.println();

        Multiset<String> otherLanguages = HashMultiset.create();
        for (Entry<String, Map<String, Long>> languageMapping : languages.rowMap().entrySet()) {
            if (topLanguages.contains(languageMapping.getKey())) {
                out.print(languageMapping.getKey());
                for (String crawl : crawls) {
                    out.print('\t');
                    Long count = languageMapping.getValue().get(crawl);
                    out.print(count != null ? count : 0);
                }
                out.println();
            } else {
                for (String crawl : crawls) {
                    Long count = languageMapping.getValue().get(crawl);
                    otherLanguages.add(crawl, count != null ? count.intValue() : 0);
                }
            }
        }
        out.print("Other");
        for (String crawl : crawls) {
            out.print('\t');
            out.print(otherLanguages.count(crawl));
        }

    }

    private static Set<String> findMostFrequentLanguages(Table<String, String, Long> languages) {
        Multiset<String> totalLanguageCounts = HashMultiset.create();
        for (Entry<String, Map<String, Long>> languageCount : languages.rowMap().entrySet()) {
            for (Long crawlLanguageCount : languageCount.getValue().values()) {
                totalLanguageCounts.add(languageCount.getKey(), crawlLanguageCount.intValue());
            }
        }
        return FluentIterable.from(
            Multisets.copyHighestCountFirst(totalLanguageCounts).elementSet())
            .limit(NUM_LANGUAGES)
            .toSet();
    }

    private static Map<String, Long> getLanguageDistribution(JdbcTemplate template,
            final String crawl) {
        ResultSetExtractor<Map<String, Long>> handler = new ResultSetExtractor<Map<String, Long>>() {
            @Override
            public Map<String, Long> extractData(ResultSet rs) throws SQLException {
                final Map<String, Long> values = new HashMap<>();
                while (rs.next()) {
                    values.put(rs.getString("language"), rs.getLong("count"));
                }
                return values;
            }
        };
        if (crawl != null) {
            return template.query(LANGUAGE_QUERY_SELECT + " AND crawl = ? "
                    + LANGUAGE_QUERY_GROUP, handler, crawl);
        } else {
            return template.query(LANGUAGE_QUERY, handler);
        }
    }

    static void printScoreDistribution(JdbcTemplate template, String crawl, String language) {
        for (double boundary = 0.0; boundary < 0.99; boundary += INCREMENT) {
            double upperBoundary = boundary + INCREMENT;
            String query = HISTOGRAM_QUERY;
            List<Object> arguments = Lists.newArrayList((Object) boundary, upperBoundary);
            if (crawl != null) {
                query += " AND crawl = ?";
                arguments.add(crawl);
            }
            if (language != null) {
                query += " AND language = ?";
                arguments.add(language);
            }
            Long count = template.queryForObject(query, arguments.toArray(), Long.class);
            System.out.format(Locale.ENGLISH, "%s\t%.1f-%.1f\t%d%n", crawl != null ? crawl : "all",
                boundary, upperBoundary, count);
        }
    }
}
