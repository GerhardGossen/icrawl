CREATE TABLE urls_nepal (
  url character varying NOT NULL,
  score double precision,
  fetchtime timestamp with time zone,
  modifiedtime timestamp with time zone,
  language character varying(3),
  datesource character varying,
  batchid character varying,
  crawl character varying,
  tokens integer,
  hashtags integer,
  id integer NOT NULL DEFAULT nextval('urls_id_seq'::regclass),
  PRIMARY KEY (id)
)