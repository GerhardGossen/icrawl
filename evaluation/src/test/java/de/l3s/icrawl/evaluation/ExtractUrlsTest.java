package de.l3s.icrawl.evaluation;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;

import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.google.common.io.Resources;

import de.l3s.icrawl.evaluation.ExtractUrls.ExtractUrlsMapper;
import de.l3s.icrawl.util.WebPageUtils;

import static de.l3s.icrawl.evaluation.ExtractUrls.ExtractUrlsMapper.findIframe;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

public class ExtractUrlsTest {

    @Test
    public void testFalsePositiveURLForAd() throws Exception {
        String iframeUrl = "http://www.aidsportal.org/web/guest/resource?id=036434ad-d682-43ca-95f8-0e7d167695e2";
        Matcher matcher = ExtractUrlsMapper.IFRAME_URL_EXCLUSIONS.matcher(iframeUrl);
        assertFalse(matcher.find());
    }

    @Test
    public void test() throws SAXException, IOException {
        String url = "http://linkis.com/PRAdk";
        try (InputStream is = Resources.getResource("linkis.com.html").openStream()) {
            Document document = WebPageUtils.parseHtml(is, url);
            assertThat(findIframe(document, url), not(isEmptyOrNullString()));
        }
    }

}
