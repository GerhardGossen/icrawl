package de.l3s.icrawl.api.rss;

import org.junit.Before;
import org.junit.Test;

import de.l3s.icrawl.api.ApiFetcherContext;
import de.l3s.icrawl.domain.api.ApiRequest;

import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

public class FetcherTest {
    private Fetcher fetchi;
    private static String url1 = "http://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml";
    private static String url2 = "http://rss.nytimes.com/services/xml/rss/nyt/US.xml";

    @Before
    public void prepare() {
        fetchi = new Fetcher();
    }

    @Test
    public void testaddURI() {
        fetchi.addUri(url1, null);
        assertFalse("No tasks in task list", fetchi.tasks.isEmpty());
    }

    @Test
    public void testRun() {
        ApiRequest request = new ApiRequest("rss", url1);
        fetchi.addRequest(request, null);
        assertThat(fetchi.tasks, hasKey(request));
    }

    @Test
    public void testHowManyTasks() {
        fetchi.addUri("rss:" + url1, null);
        fetchi.addUri("rss:" + url2, null);
        assertThat(fetchi.tasks.entrySet(), hasSize(2));
    }

    @Test
    public void testHowManyTasksWith2Crawls() {
        fetchi.addUri("rss:" + url1, null);
        fetchi.addUri("rss:" + url2, null);
        assertThat(fetchi.tasks.entrySet(), hasSize(2));
    }

    @Test
    public void testKillACrawl() {
        ApiFetcherContext ctx1 = new MockContext(1);
        ApiFetcherContext ctx2 = new MockContext(2);
        fetchi.addUri("rss:" + url1, ctx1);
        fetchi.addUri("rss:" + url2, ctx1);
        fetchi.addUri("rss:" + url2, ctx2);
        fetchi.kill();
        assertThat(fetchi.tasks.entrySet(), hasSize(1));
    }

    @Test
    public void testStopACrawl() {
        ApiFetcherContext context = new MockContext(1);
        fetchi.addUri("rss:" + url1, context);
        fetchi.addUri("rss:" + url2, context);
        fetchi.addUri("rss:" + url2, new MockContext(2));
        fetchi.stop(context);
        assertThat(fetchi.tasks.entrySet(), hasSize(1));
    }
}
