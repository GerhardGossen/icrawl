package de.l3s.icrawl.api.rss;

import java.util.Map;

import de.l3s.icrawl.api.ApiFetcherContext;
import de.l3s.icrawl.domain.api.ApiFetcherDocument;
import de.l3s.icrawl.domain.status.ApiStatus.ErrorBehavior;
import de.l3s.icrawl.domain.status.ApiStatus.ErrorType;

/** ApiFetcherContext implementation for tests. Handles only the equalTo method. */
public class MockContext implements ApiFetcherContext {

    private final long crawlId;

    public MockContext(long crawlId) {
        this.crawlId = crawlId;
    }

    @Override
    public void writeDocument(ApiFetcherDocument doc) {}

    @Override
    public void writeOutlink(String uri, Map<String, String> metadata) {}

    @Override
    public void writeRedirect(String fromUri, String toUrlí, Map<String, String> metadata) {}

    @Override
    public void reportError(ErrorType type, ErrorBehavior behavior, String message, Exception e) {}

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof MockContext) {
            MockContext other = (MockContext) obj;
            return crawlId == other.crawlId;
        } else {
            return false;
        }
    }
}
