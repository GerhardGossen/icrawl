package de.l3s.icrawl.api.rss;

import org.junit.Before;
import org.junit.Test;

import de.l3s.icrawl.domain.api.ApiRequest;

import static org.junit.Assert.assertEquals;

public class RSSTaskTest {
    private static Fetcher fetchi;
    private static String url = "http://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml";

    @Before
    public void prepare() {
        fetchi = new Fetcher();
    }

    @Test
    public void testGetURI() {
        ApiRequest request = new ApiRequest("rss:", url);
    	fetchi.addRequest(request, new MockContext(2));
    	RSSTask task = new RSSTask(request, fetchi);
    	assertEquals(task.getURI(), url);
    }
}
