package de.l3s.icrawl.api.rss;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.math.IntMath;
import com.rometools.fetcher.FeedFetcher;
import com.rometools.fetcher.FetcherEvent;
import com.rometools.fetcher.FetcherException;
import com.rometools.fetcher.FetcherListener;
import com.rometools.fetcher.impl.FeedFetcherCache;
import com.rometools.fetcher.impl.HashMapFeedInfoCache;
import com.rometools.fetcher.impl.HttpURLFeedFetcher;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.FeedException;

import de.l3s.icrawl.domain.api.ApiFetcherDocument;
import de.l3s.icrawl.domain.api.ApiRequest;


public class RSSTask implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(Fetcher.class);

    private static final long FETCH_INTERVAL = 1;
    private static final int MAX_FAILURES = 5;
    /** the URL from which the RSS feed is retrieved */
    private final URL url;
    private final Fetcher fetcher;
    private final long sleepTime;
    /** ROME's FeedFetcher for fetching RSS feeds */
    private final FeedFetcher feedFetcher;
    /** Listener on the FeedFetcher to inform about status of fetch and if HTTP Conditional GET is available */
    private final FetcherEventListenerImpl listener;
    /** list of URLs in the recently fetched RSS feed  */
    private final List<String> links;
    private final ApiRequest request;
    /** hash of the RSS feed to check if RSS feed has been updated */
    private int entriesHash = -1;
    private int failures = 0;


    /**
     * constructor
     * @param request the RSS feed's URL
     * @param fet the Fetcher itself
     */
    public RSSTask(ApiRequest request, Fetcher fet) {
        try {
            this.url = new URL(request.getHost());
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("'" + request.getHost() + "' is not a valid URL", e);
        }
        this.request = request;
        this.fetcher = fet;
        this.sleepTime = RSSTask.FETCH_INTERVAL;
        FeedFetcherCache feedInfoCache = HashMapFeedInfoCache.getInstance();
        this.listener = new FetcherEventListenerImpl();
        this.feedFetcher = new HttpURLFeedFetcher(feedInfoCache);
        this.feedFetcher.addFetcherEventListener(listener);
        this.links = new ArrayList<>();
    }

    /**
     * method to reschedule this RSSTask if still crawls (contexts) available awaiting new feeds
     */
    void reschedule() {
        //careful: do not destroy cache of fetcher
        long normalTimeInSeconds = TimeUnit.MINUTES.convert(sleepTime, TimeUnit.SECONDS);
        long nextFetchTime = nextFetchTime(normalTimeInSeconds, failures, 30L);
        fetcher.schedule(this, nextFetchTime, TimeUnit.SECONDS);
    }

    /**
     * Get time for next fetch, using exponential backoff in case of failure.
     *
     * @param normalTime
     *            minimum time
     * @param failures
     *            number of failed fetches
     * @param backOffTime
     *            additional interval in case of failures
     * @return value >= normalTime, exponentially increasing in the number of
     *         failures
     */
    static long nextFetchTime(long normalTime, int failures, long backOffTime) {
        if (failures <= 0) {
            return normalTime;
        } else {
            return normalTime + IntMath.pow(2, failures) * backOffTime;
        }
    }

    /**
     * create a new list of the links that have been fetched
     * @param liste
     */
    void handleFetchedList(List<SyndEntry> liste) {
    	List<String> newLinks = new ArrayList<>();
    	List<String> oldList = this.links;
    	this.links.clear();
    	for (SyndEntry entry : liste) {
            String url = entry.getLink();
            this.links.add(url);
            if (!oldList.contains(url)) {
            	newLinks.add(url);
                logger.debug("Found new Feed-Entry: " + entry.getTitle() + ", " + entry.getUri());
                handleNewEntry(entry);
            }
        }
    }

    /**
     * method to inform every crawl (context) about recently fetched feed entry
     * @param entry
     */
    void handleNewEntry(SyndEntry entry) {
    	String uri = entry.getUri();
    	long time = entry.getPublishedDate().getTime();
    	String content = entry.getTitle() + " " + entry.getDescription().getValue();
        Map<String, String> headers = new Hashtable<>();
    	ApiFetcherDocument doc = ApiFetcherDocument.builder().setUrl(
        		uri).setFetchTime(time).setContent(content).setContentType("application/rss+xml").
        		setHeaders(headers).build();
        this.fetcher.writeDocument(request, doc);
    }

    @Override
    public void run() {
        try {
            SyndFeed feed = feedFetcher.retrieveFeed(url);
            //			System.out.println("Started");
            List<SyndEntry> liste = feed.getEntries();
            //if HTTP CONDITIONAL GET supported do not update
            if (listener.feedPolled() & !listener.feedUnchanged()) {
                //if hash of entries has not changed do not update
                int hash = liste.hashCode();
                if (this.entriesHash != hash) {
                    this.entriesHash = hash;

                    handleFetchedList(liste);
                }
            }
        } catch (IllegalArgumentException | IOException | FeedException | FetcherException e) {
            failures++;
            logger.info("Failed to fetch/parse feed (attempt {}/{}): {}", failures, MAX_FAILURES,
                url, e);
            }
        if (failures < MAX_FAILURES) {
            reschedule();
        } else {
            logger.warn("Could not fetch/parse feed {} times, giving up: {}", failures, url);
        }

    }

    /**
     * a listener for ROME's FeedFetcher to inform about status of fetching and retrieval,
     * i.e. if the feed has not been changed since last fetch according to HTTP Conditional Get
     */
    static class FetcherEventListenerImpl implements FetcherListener {
        private boolean polled = false;
        private boolean unchanged = false;

        public boolean feedUnchanged() {
            boolean tmp = this.unchanged;
            this.unchanged = false;
            return tmp;
        }

        public boolean feedPolled() {
            boolean tmp = this.polled;
            this.polled = false;
            return tmp;
        }

        @Override
        public void fetcherEvent(final FetcherEvent event) {
            final String eventType = event.getEventType();
            if (FetcherEvent.EVENT_TYPE_FEED_POLLED.equals(eventType)) {
                this.polled = true;
                //                System.err.println("\tEVENT: Feed Polled. URL = " + event.getUrlString());
            } else if (FetcherEvent.EVENT_TYPE_FEED_RETRIEVED.equals(eventType)) {
                //                System.err.println("\tEVENT: Feed Retrieved. URL = " + event.getUrlString());
            } else if (FetcherEvent.EVENT_TYPE_FEED_UNCHANGED.equals(eventType)) {
                this.unchanged = true;
                //                System.err.println("\tEVENT: Feed Unchanged. URL = " + event.getUrlString());
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RSSTask other = (RSSTask) obj;
        if (other.getURI() != this.getURI()) {
            return false;
        }
        return true;
    }

	public String getURI() {
		return this.url.toString();
	}
}
