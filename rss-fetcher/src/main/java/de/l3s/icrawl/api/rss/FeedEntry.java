package de.l3s.icrawl.api.rss;

import org.joda.time.DateTime;


public class FeedEntry {
	private String feedUri;
	private String entryUri;
	private String title;
	private String description;
	private DateTime date;
	
	/**
	 * @return the feed's URI
	 */
	public String getFeedUri() {
		return this.feedUri;
	}	
	/**
	 * @return the feed's entry's URI
	 */
	public String getEntryUri() {
		return this.entryUri;
	}
	/**
	 * @return the title of the entry, null if none
	 */
	public String getTitle() {
		return this.title;
	}
	/**
	 * @return the description of the entry, null if none
	 */
	public String getDescription() {
		return this.description;
	}
	/**
	 * @return the date the entry was published or if null the date the feed was published or null
	 */
	public DateTime getPublishedDate() {
		return this.date;
	}

	/**
	 * @param uri the feed's URI
	 */
	public void setFeedUri(String uri) {
		this.feedUri = uri;
	}
	/**
	 * @param uri the feed's entry's URI
	 */
	public void setEntryUri(String uri) {
		this.entryUri = uri;
	}
	/**
	 * @param title the title of the entry
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @param desc the description of the entry
	 */
	public void setDescription(String desc) {
		this.description = desc;
	}
	/**
	 * @param date the date the entry was published or if null the date the feed was published or null
	 */
	public void setPublishedDate(DateTime date) {
		this.date = date;
	}

}
