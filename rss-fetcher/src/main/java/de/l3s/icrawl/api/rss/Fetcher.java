package de.l3s.icrawl.api.rss;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import de.l3s.icrawl.api.AbstractApiFetcher;
import de.l3s.icrawl.domain.api.ApiFetcherDocument;
import de.l3s.icrawl.domain.api.ApiRequest;

// TODO check format of API request passing
public class Fetcher extends AbstractApiFetcher {
    private static final Logger logger = LoggerFactory.getLogger(Fetcher.class);

    private final static int NTHREADS = 5;
    private final ScheduledExecutorService executor;
    /**
     * map of URLs to RSSTasks
     */
    final Map<ApiRequest, RSSTask> tasks = new HashMap<>();
    /**
     * References to running RSSTask's.
     *
     * Used to prevent parallel execution and to allow canceling.
     */
    final Map<String, ScheduledFuture<?>> scheduledTasks = new HashMap<>();

    /**
     * write a @param document to all crawls waiting for the fetched results
     * from @param request
     */
    @Override
    public void writeDocument(ApiRequest request, ApiFetcherDocument document) {
    	super.writeDocument(request, document);
    }

    public Fetcher() {
        ThreadFactory tf = new ThreadFactoryBuilder().setNameFormat("rss-fetcher-%d").build();
        this.executor = Executors.newScheduledThreadPool(Fetcher.NTHREADS, tf);
    }

    /**
     * schedule an RSSTask
     * @param task the RSSTask
     * @param delay to next run
     * @param unit TimeUnit
     */
    void schedule(RSSTask task, long delay, TimeUnit unit) {
    	synchronized (tasks) {
    		//if task has been removed from tasks-list return
    		if (!tasks.containsKey(task.getURI())) {
    			return;
    		}
            ScheduledFuture<?> future = scheduledTasks.get(task.getURI());
            if (future == null || future.isDone()) {
                future = executor.schedule(task, delay, unit);
                scheduledTasks.put(task.getURI(), future);
            } else {
                logger.debug("Already scheduled task for feed '{}', not scheduling", task.getURI());
            }

        }
    }

	@Override
	public String getUriScheme() {
		return "atom:rss:";
	}

	/**
	 * checks if tasks contains already an RSSTask for specific @param uri. If not creates a new RSSTask.
	 */
	@Override
    protected void startFollowing(ApiRequest apiRequest) {
		synchronized (this.tasks) {
            RSSTask task;
            if (tasks.containsKey(apiRequest)) {
                task = tasks.get(apiRequest);
            } else {
                task = new RSSTask(apiRequest, this);
                schedule(task, 0, TimeUnit.SECONDS);
                this.tasks.put(apiRequest, task);
                logger.debug("neue URI::crawlId: {}, taskURI: {}", apiRequest);
            }
        }
	}

	/**
	 * removes RSSTask for the specific @param uri from list tasks and cancels the future execution
	 */
	@Override
    protected void stopFollowing(ApiRequest apiRequest) {
		synchronized (tasks) {
            tasks.remove(apiRequest);
            ScheduledFuture<?> future = scheduledTasks.remove(apiRequest);
            if (future != null) {
                future.cancel(true);
            }
        }
	}
}
