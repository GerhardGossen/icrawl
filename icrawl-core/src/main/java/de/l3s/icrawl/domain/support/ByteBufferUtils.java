package de.l3s.icrawl.domain.support;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.nutch.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.google.common.base.Preconditions;
import com.google.common.base.Throwables;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableMultiset;
import com.google.common.collect.Multiset;

public final class ByteBufferUtils {
    private static final Logger logger = LoggerFactory.getLogger(ByteBufferUtils.class);
    private static final Pattern ENTRY_PATTERN = Pattern.compile("^(.*) (\\d+)$");
    private static final ObjectMapper JSON_MAPPER = new ObjectMapper()
        .registerModules(new JodaModule(), new GuavaModule())
        .configure(JsonGenerator.Feature.QUOTE_NON_NUMERIC_NUMBERS, false)
        .configure(JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS, true);

    private ByteBufferUtils() {}


    public static <T> Multiset<T> asMultiset(ByteBuffer bb, Class<T> entryType) {
        if (bb == null) {
            return HashMultiset.create();
        }
        String s = Bytes.toString(bb);
        if (s == null) {
            return HashMultiset.create();
        }
        if (s.startsWith("{")) {
            try {
                TypeReference<CountedSet<T>> typeRef = new TypeReference<CountedSet<T>>() {};
                CountedSet<T> countedSet = JSON_MAPPER.readValue(s, typeRef);
                return countedSet.asMultiset();
            } catch (IOException e) {
                throw Throwables.propagate(e);
            }
        } else {
            Preconditions.checkArgument(entryType.isAssignableFrom(String.class),
                "Old asMultiset API only available for String entries, got %s", entryType.getName());
            @SuppressWarnings("unchecked")
            Multiset<T> castMultiset = (Multiset<T>) oldAsMultiset(s);
            return castMultiset;
        }
    }

    private static Multiset<String> oldAsMultiset(String s) {
        ImmutableMultiset.Builder<String> ret = ImmutableMultiset.builder();
        for (String entry : s.split(",")) {
            Matcher m = ENTRY_PATTERN.matcher(entry);
            if (m.matches()) {
                try {
                    ret.setCount(m.group(1), Integer.parseInt(m.group(2)));
                } catch (NumberFormatException e) {
                    logger.info("Invalid Multiset entry: '{}'", entry);
                }
            } else {
                logger.info("Invalid Multiset entry: '{}'", entry);
            }
        }
        return ret.build();
    }

    public static <T> List<T> asList(ByteBuffer bb, Class<T> entryType) {
        if (bb == null) {
            return Collections.emptyList();
        }
        TypeReference<List<T>> typeRef = new TypeReference<List<T>>() {};
        try {
            return JSON_MAPPER.readValue(bb.array(), typeRef);
        } catch (IOException e) {
            logger.debug("Could not read List<{}> from ByteBuffer: {}", entryType.getName(), e);
            return new ArrayList<>();
        }
    }

    public static ByteBuffer stringify(Multiset<?> multiset) {
        try {
            CountedSet<?> countedSet = new CountedSet<>(multiset);
            return ByteBuffer.wrap(JSON_MAPPER.writeValueAsBytes(countedSet));
        } catch (JsonProcessingException e) {
            throw Throwables.propagate(e);
        }
    }

    public static ByteBuffer stringify(List<String> list) {
        try {
            return ByteBuffer.wrap(JSON_MAPPER.writeValueAsBytes(list));
        } catch (JsonProcessingException e) {
            throw Throwables.propagate(e);
        }
    }

    public static ByteBuffer asByteBuffer(long l) {
        return ByteBuffer.wrap(Bytes.toBytes(l));
    }

    public static ByteBuffer asByteBuffer(String s) {
        return ByteBuffer.wrap(s.getBytes(StandardCharsets.UTF_8));
    }

    public static long asLong(ByteBuffer bb) {
        return bb != null ? bb.getLong() : 0L;
    }

    public static int asInt(ByteBuffer bb) {
        return bb != null && bb.limit() >= 4 ? bb.getInt() : 0;
    }

    public static String asString(ByteBuffer bb) {
        return bb != null ? new String(bb.array(), StandardCharsets.UTF_8) : "";
    }


    public static ByteBuffer asByteBuffer(float f) {
        return ByteBuffer.wrap(Bytes.toBytes(f));
    }


    public static ByteBuffer asByteBuffer(double d) {
        return ByteBuffer.wrap(Bytes.toBytes(d));
    }

    public static ByteBuffer asByteBuffer(Map<String, Double> map) {
        try {
            return ByteBuffer.wrap(JSON_MAPPER.writeValueAsBytes(map));
        } catch (JsonProcessingException e) {
            throw Throwables.propagate(e);
        }
    }

    public static <K, V> Map<K, V> asMap(ByteBuffer bb, Class<K> keyClass, Class<V> valueClass) {
        if (bb == null) {
            return new HashMap<>();
        } else {
            TypeReference<Map<K, V>> typeRef = new TypeReference<Map<K, V>>() {};
            try {
                return JSON_MAPPER.readValue(bb.array(), typeRef);
            } catch (IOException e) {
                throw Throwables.propagate(e);
            }
        }
    }

    public static float asFloat(ByteBuffer bb) {
        return bb == null ? 0.0f : Bytes.toFloat(bb.array(), bb.arrayOffset() + bb.position());
    }


    public static double asDouble(ByteBuffer bb) {
        return bb == null ? 0.0 : Bytes.toDouble(bb.array(), bb.arrayOffset() + bb.position());
    }

}
