package de.l3s.icrawl.domain.crawl;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.primitives.Longs;

public class CrawlStatistic implements Serializable {

    private static final long serialVersionUID = 1L;
    private String name;
    private Map<String, Long> values;
    private String type;
    static final double SUMMARY_THRESHOLD_RATIO = 0.05;
    static final String SUMMARY_OTHER_LABEL = "Other";

    public CrawlStatistic(String name, Map<String, Long> values, String type) {
        this.name = name;
        this.values = asSortedMap(values);
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public Map<String, Long> getValues() {
        return values;
    }

    public String getType() {
        return type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValues(Map<String, Long> values) {
        this.values = asSortedMap(values);
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).addValue(name).addValue(values).addValue(type).toString();
    }

    CrawlStatistic summarize() {
        long total = 0;
        for (Long value : values.values()) {
            total += value;
        }
        long threshold = (long) (total * CrawlStatistic.SUMMARY_THRESHOLD_RATIO);
        long otherCount = 0;
        int countOtherLabels = 0;
        String lastOtherLabel = null;
        ImmutableMap.Builder<String, Long> builder = ImmutableMap.builder();
        for (Entry<String, Long> entry : values.entrySet()) {
            if (entry.getValue() >= threshold) {
                builder.put(entry);
            } else {
                otherCount += entry.getValue();
                countOtherLabels++;
                lastOtherLabel = entry.getKey();
            }
        }
        if (otherCount > 0) {
            builder.put(countOtherLabels > 1 ? CrawlStatistic.SUMMARY_OTHER_LABEL : lastOtherLabel, otherCount);
        }
        return new CrawlStatistic(name, builder.build(), type);
    }

    private static <T> Map<T, Long> asSortedMap(Map<T, Long> map) {
        List<Entry<T, Long>> list = Lists.newArrayList(map.entrySet());
        Collections.sort(list, new Comparator<Entry<T, Long>>() {
            @Override
            public int compare(Entry<T, Long> o1, Entry<T, Long> o2) {
                return Longs.compare(o2.getValue(), o1.getValue());
            }
        });
        ImmutableMap.Builder<T, Long> builder = ImmutableMap.builder();
        for (Entry<T, Long> sortedEntry : list) {
            builder.put(sortedEntry);
        }
        return builder.build();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CrawlStatistic)) {
            return false;
        }
        CrawlStatistic other = (CrawlStatistic) obj;
        return Objects.equal(this.name, other.name) && Objects.equal(this.values, other.values);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(name, values);
    }
}
