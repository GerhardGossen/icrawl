package de.l3s.icrawl.domain.support;

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Properties;

import javax.inject.Inject;

import org.hibernate.PropertyValueException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.metamodel.relational.Size;
import org.hibernate.metamodel.relational.Size.LobMultiplier;
import org.hibernate.type.SerializationException;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.usertype.Sized;
import org.hibernate.usertype.UserType;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.google.common.base.Objects;

public class JsonCollapsedUserType implements UserType, ParameterizedType, Sized {
    private static final Size[] SIZES = new Size[] { new Size(0, 0, 16 * 1024 * 1024,
        LobMultiplier.NONE) };
    public static final String PROP_CLASS = "class";
    private Class<?> clazz;
    @Inject
    private final ObjectMapper mapper = new ObjectMapper().registerModules(new JodaModule(),
        new GuavaModule());

    @Override
    public Object assemble(Serializable cached, Object owner) {
        return deepCopy(cached);
    }

    @Override
    public Object deepCopy(Object value) {
        return value == null ? null : fromJson(toJson(value), value.getClass());
    }

    @Override
    public Serializable disassemble(Object value) {
        return (Serializable) deepCopy(value);
    }

    @Override
    public boolean equals(Object x, Object y) {
        return Objects.equal(x, y);
    }

    @Override
    public int hashCode(Object x) {
        return Objects.hashCode(x);
    }

    @Override
    public boolean isMutable() {
        return true;
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SessionImplementor session, Object owner)
            throws SQLException {
        String property = names[0];
        Clob clob = rs.getClob(property);
        if (clob == null || rs.wasNull()) {
            return null;
        }
        return fromJson(clob.getCharacterStream(), clazz);
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index,
            SessionImplementor session) throws SQLException {
        if (value != null) {
            st.setClob(index, toJson(value));
        } else {
            st.setNull(index, Types.CLOB);
        }
    }

    @Override
    public Object replace(Object original, Object target, Object owner) {
        return deepCopy(original);
    }

    @Override
    public Class<?> returnedClass() {
        return clazz;
    }

    @Override
    public void setParameterValues(Properties parameters) {
        String clazzName = parameters.getProperty(PROP_CLASS);
        try {
            this.clazz = Class.forName(clazzName);
        } catch (ClassNotFoundException e) {
            throw new PropertyValueException("Not a valid class name: " + clazzName,
                getClass().getName(), PROP_CLASS);
        }
    }

    @Override
    public int[] sqlTypes() {
        return new int[] { Types.CLOB };
    }

    private Object fromJson(Reader reader, Class<?> type) {
        try {
            return mapper.readValue(reader, type);
        } catch (IOException e) {
            throw new SerializationException("Could not parse JSON", e);
        }
    }

    private Reader toJson(Object value) {
        try {
            String json = mapper.writeValueAsString(value);
            return new StringReader(json);
        } catch (JsonProcessingException e) {
            throw new SerializationException("Could not serialize object", e);
        }
    }

    @Override
    public Size[] dictatedSizes() {
        return SIZES;
    }

    @Override
    public Size[] defaultSizes() {
        return SIZES;
    }

}
