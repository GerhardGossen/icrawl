package de.l3s.icrawl.domain.event;

import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.l3s.icrawl.domain.specification.NamedEntity;
import de.l3s.icrawl.domain.support.CountedSet;

/** Signals that an URL was downloaded and added to the crawl DB. */
public class URLCrawlEvent implements Serializable {
    private static final long serialVersionUID = 1L;
    /** Identifier for the crawl this event belongs to. */
    private final long crawlId;
    private final String url;
    /** Document size in bytes. */
    private final long size;
    @NotNull
    private final CountedSet<String> keywords;
    @NotNull
    private final CountedSet<NamedEntity> entities;
    /** Time when the download was completed. */
    private final DateTime downloaded;

    @JsonCreator
    public URLCrawlEvent(@JsonProperty("crawlId") long crawlId, @JsonProperty("url") String url,
            @JsonProperty("size") long size, @JsonProperty("keywords") CountedSet<String> keywords,
            @JsonProperty("entities") CountedSet<NamedEntity> entities,
            @JsonProperty("downloaded") DateTime downloaded) {
        this.crawlId = crawlId;
        this.url = url;
        this.size = size;
        this.keywords = keywords;
        this.entities = entities;
        this.downloaded = downloaded;
    }

    public long getCrawlId() {
        return crawlId;
    }

    public String getUrl() {
        return url;
    }

    public long getSize() {
        return size;
    }

    public CountedSet<String> getKeywords() {
        return keywords;
    }

    public CountedSet<NamedEntity> getEntities() {
        return entities;
    }

    public DateTime getDownloaded() {
        return downloaded;
    }

    @Override
    public int hashCode() {
        return Objects.hash(crawlId, url, size, keywords, entities, downloaded);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof URLCrawlEvent)) {
            return false;
        }
        URLCrawlEvent other = (URLCrawlEvent) obj;
        return crawlId == other.crawlId && Objects.equals(url, other.url) && size == other.size
                && Objects.equals(downloaded, other.downloaded)
                && Objects.equals(keywords, other.keywords)
        /* && Objects.equals(entities, other.entities) */;
    }

    @Override
    public String toString() {
        return String.format(
            "URLCrawlEvent [crawlId=%s, url=%s, size=%s, keywords=%s, downloaded=%s]", crawlId,
            url, size, keywords, downloaded);
    }


}
