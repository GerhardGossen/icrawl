package de.l3s.icrawl.domain.specification;

import java.io.IOException;
import java.io.Reader;

import org.apache.hadoop.conf.Configuration;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.google.common.base.Throwables;

import de.l3s.icrawl.FieldNames;

public final class CrawlSpecificationUtils {
    public static final String EXTRACT_ENTITIES_KEY = "icrawl.online.extractEntities";
    private static final ObjectMapper JSON_FACTORY = new ObjectMapper().registerModule(new JodaModule());

    private CrawlSpecificationUtils() {}

    public static CrawlSpecification parse(Reader crawlSpecStream) throws IOException {
        return JSON_FACTORY.readValue(crawlSpecStream, CrawlSpecification.class);
    }

    public static String toString(CrawlSpecification specification) {
        try {
            return JSON_FACTORY.writeValueAsString(specification);
        } catch (JsonProcessingException e) {
            throw Throwables.propagate(e);
        }
    }

    public static CrawlSpecification parse(String crawlSpecification) {
        try {
            return JSON_FACTORY.readValue(crawlSpecification, CrawlSpecification.class);
        } catch (JsonParseException | JsonMappingException e) {
            throw new IllegalArgumentException("Not a valid JSON string: " + crawlSpecification, e);
        } catch (IOException e) {
            throw Throwables.propagate(e);
        }
    }

    public static CrawlSpecification get(Configuration conf) {
        return parse(conf.get(FieldNames.CRAWL_SPECIFICATION_KEY));
    }

    public static WeightVector parseWeightVector(String s) {
        if (s == null || s.isEmpty()) {
            return new WeightVector();
        } else {
            try {
                return JSON_FACTORY.readValue(s, WeightVector.class);
            } catch (IOException e) {
                throw Throwables.propagate(e);
            }
        }
    }

    public static String toString(WeightVector wv) {
        try {
            return JSON_FACTORY.writeValueAsString(wv);
        } catch (JsonProcessingException e) {
            throw Throwables.propagate(e);
        }
    }

    public static void set(Configuration conf, CrawlSpecification spec) {
        conf.set(FieldNames.CRAWL_SPECIFICATION_KEY, toString(spec));
    }
}
