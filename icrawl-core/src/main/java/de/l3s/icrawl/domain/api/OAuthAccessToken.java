package de.l3s.icrawl.domain.api;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotBlank;

import com.google.common.base.MoreObjects;

@Entity
public class OAuthAccessToken implements OAuthCredentials, Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @NotBlank
    private String service = "twitter";

    @NotBlank
    private String consumerKey;
    @NotBlank
    private String consumerSecret;
    @NotBlank
    private String accessToken;
    @NotBlank
    private String accessTokenSecret;

    private boolean used = false;

    private boolean enabled = true;

    public Long getId() {
        return id;
    }

    public String getService() {
        return service;
    }

    @Override
    public String getConsumerKey() {
        return consumerKey;
    }

    @Override
    public String getConsumerSecret() {
        return consumerSecret;
    }

    @Override
    public String getAccessToken() {
        return accessToken;
    }

    @Override
    public String getAccessTokenSecret() {
        return accessTokenSecret;
    }

    public boolean isUsed() {
        return used;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }

    public void setService(String service) {
        this.service = service;
    }

    public void setConsumerSecret(String consumerSecret) {
        this.consumerSecret = consumerSecret;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public void setAccessTokenSecret(String accessTokenSecret) {
        this.accessTokenSecret = accessTokenSecret;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .add("service", service)
            .add("consumerKey", consumerKey)
            .add("consumerSecret", consumerSecret)
            .add("accessToken", accessToken)
            .add("accessTokenSecret", accessTokenSecret)
            .add("used", used)
            .add("enabled", enabled)
            .toString();
    }

}
