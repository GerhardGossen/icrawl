package de.l3s.icrawl.domain.crawl;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonTypeName;

import de.l3s.icrawl.domain.crawl.WarcExporterRecord.ExportConfig;
import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.domain.specification.TaskConfiguration;

@Entity
@DiscriminatorValue("export")
public class WarcExporterRecord extends TaskExecutionRecord<ExportConfig> {

    @JsonTypeName("export")
    public static class ExportConfig extends TaskConfiguration {
        private String path;
        private String user;

        public ExportConfig() {}

        public ExportConfig(String path, String user) {
            this.path = path;
            this.user = user;
        }

        public String getPath() {
            return path;
        }

        public String getUser() {
            return user;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public void setUser(String user) {
            this.user = user;
        }
    }

    WarcExporterRecord() {}

    public WarcExporterRecord(Long id, Campaign campaign, DateTime startTime, DateTime endtime,
            String user, String path) {
        super(id, campaign, null, startTime, endtime, new ExportConfig(path, user));
    }
}
