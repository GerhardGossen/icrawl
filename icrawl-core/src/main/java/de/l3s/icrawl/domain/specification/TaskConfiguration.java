package de.l3s.icrawl.domain.specification;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

import de.l3s.icrawl.domain.crawl.WarcExporterRecord;

@JsonTypeInfo(include = As.PROPERTY, property = "type", use = Id.NAME, defaultImpl = CrawlSpecification.class)
@JsonSubTypes({ @JsonSubTypes.Type(name = "crawl", value = CrawlSpecification.class),
        @JsonSubTypes.Type(name = "apifetcher", value = ApiFetcherConfig.class),
        @JsonSubTypes.Type(name = "export", value = WarcExporterRecord.ExportConfig.class) })
public class TaskConfiguration {
}
