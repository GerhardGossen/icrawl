package de.l3s.icrawl.domain.status;

import com.google.common.base.MoreObjects;

/**
 * Status of an export job.
 *
 * This object describes the status of the job at the time it was created and is
 * not updated automatically. Instead, it needs to be refreshed from the CrawlManager.
 */
public class ExportStatus implements TaskStatus {
    private double progress;
    private long campaignId;
    private String status;
    private String reason;

    protected ExportStatus() {}

    ExportStatus(long campaignId, String status, String reason, double progress) {
        this.campaignId = campaignId;
        this.status = status;
        this.reason = reason;
        this.progress = progress;
    }

    public static ExportStatus starting(long campaignId, String user) {
        return new ExportStatus(campaignId, STARTING, "Start requested by user '" + user + "'", 0.0);
    }

    public static ExportStatus running(long campaignId, double progress) {
        return new ExportStatus(campaignId, RUNNING, "Running", progress);
    }

    public static ExportStatus queued(long campaignId, String reason) {
        return new ExportStatus(campaignId, QUEUED, reason, 0);
    }

    public static ExportStatus finished(long campaignId) {
        return new ExportStatus(campaignId, FINISHED, "Finished exporting", 1);
    }

    public static ExportStatus stopping(long campaignId, String user, double progress) {
        return new ExportStatus(campaignId, STOPPING, "Stop requested by user '" + user + "'",
            progress);
    }

    public static ExportStatus aborted(long campaignId, String user, double progress) {
        return new ExportStatus(campaignId, ABORTED, "Export aborted because of request by user '"
                + user + "'", progress);
    }

    public static ExportStatus failed(long campaignId, String error, double progress) {
        return new ExportStatus(campaignId, FAILED, "Export failed with error message: " + error,
            progress);
    }

    /**
     * Returns the current status of the job.
     *
     * See the String constants in {@link ExportStatus} for expected values.
     *
     * @return the status of the export job
     */
    @Override
    public String getStatus() {
        return status;
    }

    /**
     * Get the human-readable reason for the last state change.
     *
     * @return An English sentence describing the last state change
     */
    @Override
    public String getReason() {
        return reason;
    }

    @Override
    public double getProgress() {
        return progress;
    }

    /**
     * Get the campaign being exported.
     *
     * @return the campaign descriptor
     */
    public long getCampaign() {
        return campaignId;
    }

    void setProgress(double progress) {
        this.progress = progress;
    }

    void setCampaign(long campaignId) {
        this.campaignId = campaignId;
    }

    void setStatus(String status) {
        this.status = status;
    }

    void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .addValue(status)
            .addValue(campaignId)
            .add("progress", progress)
            .add("reason", reason)
            .toString();
    }
}
