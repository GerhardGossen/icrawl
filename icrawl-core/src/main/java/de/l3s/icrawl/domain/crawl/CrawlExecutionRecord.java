package de.l3s.icrawl.domain.crawl;

import java.util.Objects;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.joda.time.DateTime;

import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.domain.specification.Crawl;
import de.l3s.icrawl.domain.specification.CrawlSpecification;

@Entity
@DiscriminatorValue("crawl")
public class CrawlExecutionRecord extends TaskExecutionRecord<CrawlSpecification> {

    public CrawlExecutionRecord() {}

    public CrawlExecutionRecord(Crawl crawl) {
        this(null, crawl.getCampaign(), crawl, crawl.getSpecification().getStartTime(),
            null, crawl.getSpecification());
    }

    public CrawlExecutionRecord(Long id, Campaign campaign, Crawl crawl, DateTime startTime,
            DateTime endtime, CrawlSpecification specification) {
        super(id, campaign, crawl, startTime, endtime, specification);
    }

    public String crawlKey() {
        return Crawl.crawlKey(getCampaign());
    }

    @Override
    public String toString() {
        return String.format(
            "CrawlExecution [id=%s, campaign=%s, crawlId=%s, startTime=%s, endtime=%s, specification=%s]",
            id, campaign, crawl, startTime, endtime, configuration);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CrawlExecutionRecord)) {
            return false;
        } else {
            CrawlExecutionRecord other = (CrawlExecutionRecord) obj;
            return this.id == other.id && this.campaign == other.campaign
                    && this.crawl == other.crawl && Objects.equals(endtime, other.endtime)
                    && Objects.equals(startTime, other.startTime)
                    && Objects.equals(configuration, other.configuration);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
