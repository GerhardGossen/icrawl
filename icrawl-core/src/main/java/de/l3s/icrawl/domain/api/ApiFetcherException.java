package de.l3s.icrawl.domain.api;

/**
 * Could not retrieve data through a Web platform API.
 */
public class ApiFetcherException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ApiFetcherException(String message) {
        super(message);
    }

    public ApiFetcherException(Throwable cause) {
        super(cause);
    }

    public ApiFetcherException(String message, Throwable cause) {
        super(message, cause);
    }

}
