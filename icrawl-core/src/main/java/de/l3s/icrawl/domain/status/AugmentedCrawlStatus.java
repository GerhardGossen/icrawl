package de.l3s.icrawl.domain.status;

import org.joda.time.DateTime;

public class AugmentedCrawlStatus {

    private final long campaign;
    private final long crawl;
    private final String name;
    private final DateTime startTime;
    private final long execution;
    private final int batchSeq;
    private final String phase;
    private final double phaseProgress;

    public AugmentedCrawlStatus(long campaign, long crawl, String name, DateTime startTime,
            long execution, int batchSeq, String phase, double phaseProgress) {
        this.campaign = campaign;
        this.crawl = crawl;
        this.name = name;
        this.startTime = startTime;
        this.execution = execution;
        this.batchSeq = batchSeq;
        this.phase = phase;
        this.phaseProgress = phaseProgress;
    }

    public long getCampaign() {
        return campaign;
    }

    public long getCrawl() {
        return crawl;
    }

    public String getName() {
        return name;
    }

    public DateTime getStartTime() {
        return startTime;
    }

    public long getExecution() {
        return execution;
    }

    public int getBatchSeq() {
        return batchSeq;
    }

    public String getPhase() {
        return phase;
    }

    public int getPhaseProgress() {
        return (int) (100 * phaseProgress);
    }

    @Override
    public String toString() {
        return String.format(
            "AugmentedCrawlStatus [campaign=%s, crawl=%s, name=%s, startTime=%s, batch=%s, phase=%s, phaseProgress=%s]",
            campaign, crawl, name, startTime, batchSeq, phase, phaseProgress);
    }

}
