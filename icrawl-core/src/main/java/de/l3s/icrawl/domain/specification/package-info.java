/**
 * Crawl specification entities.
 */
@TypeDefs({
    @TypeDef(defaultForType = DateTime.class, typeClass = PersistentDateTime.class),
    @TypeDef(name = "jsonMap", typeClass = JsonCollapsedUserType.class, parameters = @Parameter(name = JsonCollapsedUserType.PROP_CLASS, value = "java.util.Map")),
    @TypeDef(defaultForType = CrawlSpecification.class, typeClass = JsonCollapsedUserType.class, parameters = @Parameter(name = JsonCollapsedUserType.PROP_CLASS, value = "de.l3s.icrawl.domain.specification.CrawlSpecification"))
})
package de.l3s.icrawl.domain.specification;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.jadira.usertype.dateandtime.joda.PersistentDateTime;
import org.joda.time.DateTime;

import de.l3s.icrawl.domain.support.JsonCollapsedUserType;

