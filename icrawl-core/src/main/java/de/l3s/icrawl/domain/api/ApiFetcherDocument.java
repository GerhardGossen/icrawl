package de.l3s.icrawl.domain.api;

import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.apache.commons.httpclient.HttpStatus;

import com.google.common.base.Preconditions;

/**
 * A document retrieved from a (social media) API.
 *
 * See {@link Builder} for instructions on how to create instances.
 */
public class ApiFetcherDocument {

    private final String uri;
    private final int statusCode;
    private final String statusMessage;
    private final Map<String, String> headers;
    private final String contentType;
    private final byte[] content;
    private final long fetchTime;
    private final Long modifiedTime;
    private final String sourceUrl;

    ApiFetcherDocument(String uri, int statusCode, String statusMessage,
            Map<String, String> headers,
            String contentType, byte[] content, long fetchTime, Long modifiedTime,
            String sourceUrl) {
        this.uri = Preconditions.checkNotNull(uri);
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.headers = Preconditions.checkNotNull(headers);
        this.contentType = Preconditions.checkNotNull(contentType);
        this.content = Preconditions.checkNotNull(content);
        this.fetchTime = fetchTime;
        this.modifiedTime = modifiedTime;
        this.sourceUrl = sourceUrl;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getUri() {
        return uri;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public String getContentType() {
        return contentType;
    }

    public byte[] getContent() {
        return content;
    }

    public long getFetchTime() {
        return fetchTime;
    }

    public Long getModifiedTime() {
        return modifiedTime;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    /**
     * Construct a ApiFetcherDocument step by step.
     *
     * Example:
     *
     * <pre>
     * ApiFetcherDocument doc = ApiFetcherDocument.builder()
     *                            .setUrl(url)
     *                            .setStatusCode(200)
     *                            ...
     *                            .build();
     * </pre>
     *
     * When this document is retrieved as part of a collection (e.g. a search
     * result), than the values for the collection document can be used except
     * where noted otherwise.
     */
    public static class Builder {
        private String url;
        private int statusCode = -1;
        private String statusMessage;
        private Map<String, String> headers;
        private String contentType;
        private byte[] content;
        private long fetchTime = -1;
        private Long modifiedTime;
        private String sourceUrl;

        /**
         * Add the URL of the document.
         *
         * @param url
         *            a URL or URI of the document. If this document is
         *            retrieved as part of e.g. a search result, than the
         *            specified URL may be different than the URL used in the
         *            request.
         * @return the builder for chaining
         */
        public Builder setUrl(String url) {
            this.url = url;
            return this;
        }

        /**
         * Set the status code of the request.
         *
         * @param statusCode
         *            the status code of the API request. If possible use or map
         *            to the HTTP status code
         * @return the builder for chaining
         */
        public Builder setStatusCode(int statusCode) {
            this.statusCode = statusCode;
            return this;
        }

        /**
         * Set the status message of the request.
         *
         * @param statusMessage
         *            the status message of the API request. When this value is
         *            not set, the default HTTP message for the status code is
         *            used
         * @return the builder for chaining
         */
        public Builder setStatusMessage(String statusMessage) {
            this.statusMessage = statusMessage;
            return this;
        }

        /**
         * Set the HTTP headers
         *
         * @param headers
         *            the full HTTP headers returned for the request
         * @return the builder for chaining
         */
        public Builder setHeaders(Map<String, String> headers) {
            this.headers = headers;
            return this;
        }

        /**
         * Set the MIME type of the content.
         *
         * @param contentType
         *            the content MIME type. If the content is of an
         *            API-specific type, than a application-specific type should
         *            be used (e.g. application/twitter-tweet+json for a JSON
         *            encoded tweet)
         * @return the builder for chaining
         */
        public Builder setContentType(String contentType) {
            this.contentType = contentType;
            return this;
        }

        /**
         * Set the body of the request as a String.
         *
         * If the body is a binary format, use {@link #setContent(byte[])}
         * instead.
         *
         * @param content
         *            the payload of the document
         * @return the builder for chaining
         */
        public Builder setContent(String content) {
            if (content != null) {
                this.content = content.getBytes(StandardCharsets.UTF_8);
            }
            return this;
        }

        /**
         * Set the body of the request as a byte array.
         *
         * If the body is acually in a textual format, use
         * {@link #setContent(String)} instead or make sure that the body is
         * encoded in UTF-8.
         *
         * @param content
         *            the payload of the document
         * @return the builder for chaining
         */
        public Builder setContent(byte[] content) {
            this.content = content;
            return this;
        }

        /**
         * Set the timestamp of fetching the document.
         *
         * @param fetchTime
         *            the time when the fetch was started or finished as a Unix
         *            timestamp (in milliseconds)
         * @return the builder for chaining
         */
        public Builder setFetchTime(long fetchTime) {
            this.fetchTime = fetchTime;
            return this;
        }

        /**
         * Set the last-modified time of the document (optional).
         *
         * This parameter should be set to the value provided by the API either
         * through HTTP headers or as part of the data. Leave unset if the
         * last-modified time is not provided.
         *
         * API fetchers MAY also discard last-modified times that are likely to
         * be wrong (e.g. in the future).
         *
         * @param modifiedTime
         *            the last modified time of this document
         * @return the builder for chaining
         */
        public Builder setModifiedTime(long modifiedTime) {
            this.modifiedTime = modifiedTime;
            return this;
        }

        public Builder setSourceUrl(String sourceUrl) {
            this.sourceUrl = sourceUrl;
            return this;
        }

        /**
         * Check the set values and create a new ApiFetcherDocument object.
         *
         * @return an {@link ApiFetcherDocument} instance with the provided
         *         values
         * @throws IllegalStateException
         *             if one or more required fields are not set
         */
        public ApiFetcherDocument build() {
            Preconditions.checkState(url != null, "url is not set");
            Preconditions.checkState(statusCode >= 0, "statusCode is not set");
            if (statusMessage == null) {
                statusMessage = HttpStatus.getStatusText(statusCode);
            }
            Preconditions.checkState(headers != null, "headers is not set");
            Preconditions.checkState(contentType != null, "content type is not set");
            Preconditions.checkState(content != null, "content is not set");
            Preconditions.checkState(fetchTime >= 0, "fetchTime is not set");
            return new ApiFetcherDocument(url, statusCode, statusMessage, headers, contentType,
                content,
                fetchTime, modifiedTime, sourceUrl);
        }
    }
}
