/**
 * Domain classes related to API fetching.
 */
package de.l3s.icrawl.domain.api;