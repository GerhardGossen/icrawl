package de.l3s.icrawl.domain.support;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multiset;

public class CountedSet<T> implements Iterable<T> {
    static class Entry<T> {
        @JsonUnwrapped
        private T payload;
        private int count;

        Entry() {}

        public Entry(T payload, Integer count) {
            this.payload = payload;
            this.count = count;
        }

        public T getPayload() {
            return payload;
        }

        public int getCount() {
            return count;
        }

        public void setPayload(T payload) {
            this.payload = payload;
        }

        public void setCount(int count) {
            this.count = count;
        }
    }

    private final Map<T, Integer> values;

    public CountedSet() {
        values = new HashMap<>();
    }

    public CountedSet(Multiset<T> multiset) {
        this.values = Maps.newHashMapWithExpectedSize(multiset.elementSet().size());
        for (Multiset.Entry<T> entry : multiset.entrySet()) {
            values.put(entry.getElement(), entry.getCount());
        }
    }

    public int add(T key) {
        return add(key, 1);
    }

    public int add(T key, int count) {
        Integer oldValue = values.get(key);
        if (oldValue != null) {
            values.put(key, oldValue + count);
            return oldValue.intValue();
        } else {
            values.put(key, count);
            return 0;
        }
    }

    public int get(T key) {
        Integer value = values.get(key);
        return value != null ? value.intValue() : 0;
    }

    public boolean contains(T key) {
        return values.containsKey(key);
    }

    public Multiset<T> asMultiset() {
        Multiset<T> multiset = HashMultiset.create(values.size());
        for (Map.Entry<T, Integer> entry : values.entrySet()) {
            multiset.add(entry.getKey(), entry.getValue());
        }
        return multiset;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof CountedSet)) {
            return false;
        }
        CountedSet<?> other = (CountedSet<?>) obj;
        return values.equals(other.values);
    }

    @JsonGetter
    List<Entry<T>> getEntries() {
        List<Entry<T>> vals = Lists.newArrayListWithExpectedSize(values.size());
        for (Map.Entry<T, Integer> entry : values.entrySet()) {
            vals.add(new Entry<>(entry.getKey(), entry.getValue()));
        }
        return vals;
    }

    @JsonSetter
    void setEntries(List<Entry<T>> vals) {
        for (Entry<T> entry : vals) {
            values.put(entry.getPayload(), entry.getCount());
        }
    }

    @Override
    public int hashCode() {
        return values.hashCode();
    }

    @Override
    public String toString() {
        return values.toString();
    }

    @Override
    public Iterator<T> iterator() {
        return values.keySet().iterator();
    }
}
