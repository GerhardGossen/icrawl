package de.l3s.icrawl.domain.crawl;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.hadoop.io.Writable;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multiset.Entry;

public class ScoreHistogram implements Writable {
    private final int[] values01;
    private final int[] values12;
    private final Multiset<Integer> valuesOther;

    public ScoreHistogram() {
        this(new int[100], new int[10], HashMultiset.create());
    }

    public ScoreHistogram copy() {
        LoggerFactory.getLogger(getClass()).info("Creating clone of Histogram");
        return new ScoreHistogram(
            Arrays.copyOf(values01, values01.length),
            Arrays.copyOf(values12, values12.length),
            HashMultiset.create(valuesOther));
    }

    @JsonCreator
    ScoreHistogram(@JsonProperty("values01") int[] values01,
            @JsonProperty("values12") int[] values12,
            @JsonProperty("valuesOther") Multiset<Integer> valuesOther) {
        this.values01 = values01;
        this.values12 = values12;
        this.valuesOther = valuesOther;
    }

    public void add(double v) {
        if (0 <= v && v < 1) {
            values01[(int) (v * 100)]++;
        } else if (1 <= v && v < 2) {
            values12[(int) ((v - 1) * 10)]++;
        } else {
            valuesOther.add((int) v);
        }
    }

    public void addAll(Collection<Double> values) {
        values.forEach(this::add);
    }

    public void add(ScoreHistogram other) {
        add(values01, other.values01);
        add(values12, other.values12);
        valuesOther.addAll(other.valuesOther);
    }

    private void add(int[] target, int[] source) {
        for (int i = 0; i < target.length && i < source.length; i++) {
            target[i] += source[i];
        }
    }

    public int[] getValues01() {
        return values01;
    }

    public int[] getValues12() {
        return values12;
    }

    public Multiset<Integer> getValuesOther() {
        return valuesOther;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        for (int i : values01) {
            out.writeInt(i);
        }
        for (int i : values12) {
            out.writeInt(i);
        }
        out.writeInt(valuesOther.elementSet().size());
        for (Entry<Integer> entry : valuesOther.entrySet()) {
            out.writeInt(entry.getElement());
            out.writeInt(entry.getCount());
        }
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        for (int i = 0; i < values01.length; i++) {
            values01[i] = in.readInt();
        }
        for (int i = 0; i < values12.length; i++) {
            values12[i] = in.readInt();
        }
        valuesOther.clear();
        int otherSize = in.readInt();
        for (int i = 0; i < otherSize; i++) {
            int element = in.readInt();
            int count = in.readInt();
            valuesOther.add(element, count);
        }
    }

    public void clear() {
        Arrays.setAll(values01, idx -> 0);
        Arrays.setAll(values12, idx -> 0);
        valuesOther.clear();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < values01.length; i++) {
            sb.append("0.").append(i < 10 ? "0" : "").append(i).append(": ").append(values01[i]).append("\n");
        }
        for(int i = 0; i < values12.length; i++) {
            sb.append("1.").append(i).append("0: ").append(values12[i]).append("\n");
        }
        for (Entry<Integer> entry: valuesOther.entrySet()) {
            sb.append(entry.getElement()).append(".00: ").append(entry.getCount()).append("\n");
        }
        return sb.toString();
    }
}