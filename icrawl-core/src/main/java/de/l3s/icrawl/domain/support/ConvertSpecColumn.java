package de.l3s.icrawl.domain.support;

import java.io.StringReader;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.internal.util.SerializationHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.fasterxml.jackson.datatype.joda.JodaModule;

public class ConvertSpecColumn {
    private static final Logger logger = LoggerFactory.getLogger(ConvertSpecColumn.class);

    public static void main(String[] args) throws SQLException {
        if (args.length < 1) {
            System.err.println("Usage: " + ConvertSpecColumn.class.getName() + " db [table column]");
            System.exit(1);
        }
        String db = args[0];
        String table;
        String column;
        if (args.length >= 3) {
            table = args[1];
            column = args[2];
        } else {
            table = "crawl";
            column = "specification";
        }
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName("org.hsqldb.jdbcDriver");
        ds.setUsername("sa");
        ds.setPassword("");
        ds.setUrl("jdbc:hsqldb:file:" + db + ";shutdown=true");
        final ObjectMapper jsonMapper = new ObjectMapper().registerModules(new GuavaModule(),
            new JodaModule());

        JdbcTemplate template = new JdbcTemplate(ds);
        template.execute("ALTER TABLE " + table + " ADD COLUMN " + column + "_json LONGVARCHAR");
        template.query(
            con -> con.prepareStatement("SELECT id, " + column + ", " + column + "_json FROM "
                    + table,
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE),
            (ResultSet rs) -> {
                try {
                    Object value = SerializationHelper.deserialize(rs.getBytes(column));
                    String jsonString = jsonMapper.writeValueAsString(value);
                    rs.updateClob(column + "_json", new StringReader(jsonString));
                    rs.updateRow();
                } catch (JsonProcessingException e) {
                    logger.info("Exception: ", e);
                }
            });
        template.execute("ALTER TABLE " + table + " ALTER COLUMN " + column + "_json SET NOT NULL");
        template.execute("ALTER TABLE " + table + " ALTER COLUMN " + column + " RENAME TO "
                + column + "_binary");
        template.execute("ALTER TABLE " + table + " ALTER COLUMN " + column + "_json RENAME TO "
                + column);
        ds.close();
    }

}
