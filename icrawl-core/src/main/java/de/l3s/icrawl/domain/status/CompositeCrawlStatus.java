package de.l3s.icrawl.domain.status;

import java.util.Collection;

// TODO implement useful behaviour
public class CompositeCrawlStatus extends CrawlStatus {

    private static final long serialVersionUID = 1L;
    private final Collection<TaskStatus> jobStatuses;

    public CompositeCrawlStatus(Collection<TaskStatus> jobStatuses) {
        this.jobStatuses = jobStatuses;
    }

}
