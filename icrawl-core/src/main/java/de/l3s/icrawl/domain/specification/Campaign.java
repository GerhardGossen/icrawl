package de.l3s.icrawl.domain.specification;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedDate;

/**
 * A collection of crawls that make up one final archive.
 */
@Entity
public class Campaign implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private Long id;
    /**
     * User-defined short name of the campaign.
     */
    @NotEmpty
    private String name;
    /**
     * User-defined long description of the campaign.
     *
     * TODO Allow some form of markup?
     */
    private String description;
    /**
     * Creation date of the campaign.
     */
    @CreatedDate
    @Nonnull
    private DateTime created;
    /**
     * Date when this campaign was moved to the archive.
     *
     * A null value means that this campaign is still active.
     */
    @Nullable
    private DateTime archived;
    /**
     * Name of the user who created the campaign.
     */
    private String owner;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "campaign")
    private final List<Crawl> crawls = new ArrayList<>();

    public Campaign() {}

    public Campaign(String name) {
        this.name = name;
        this.created = DateTime.now();
    }

    public Campaign(Long id, String name, String description, DateTime created, DateTime archived,
            String owner) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.created = created;
        this.archived = archived;
        this.owner = owner;
    }

    public boolean isActive() {
        return archived == null;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public DateTime getCreated() {
        return created;
    }

    public DateTime getArchived() {
        return archived;
    }

    public List<Crawl> getCrawls() {
        return crawls;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public void addCrawl(Crawl crawl) {
        this.crawls.add(crawl);
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOwner() {
        return owner;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setArchived(DateTime archived) {
        this.archived = archived;
    }

    @Override
    public String toString() {
        return String.format("Campaign[%d, %s]", id, name);
    }
}
