package de.l3s.icrawl.domain.evaluation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class EvaluationUrl {
    @NotNull
    @Id
    private String url;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "url")
    private final Collection<EvaluationVote> votes = new ArrayList<>();

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Collection<EvaluationVote> getVotes() {
        return votes;
    }

    public void addVote(EvaluationVote vote) {
        votes.add(vote);
    }

    @Override
    public int hashCode() {
        return Objects.hash(url);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof EvaluationUrl)) {
            return false;
        }
        EvaluationUrl other = (EvaluationUrl) obj;

        return Objects.equals(this.url, other.url);
    }
}
