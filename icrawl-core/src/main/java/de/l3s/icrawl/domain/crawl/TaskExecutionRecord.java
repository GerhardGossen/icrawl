package de.l3s.icrawl.domain.crawl;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import com.google.common.base.Preconditions;

import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.domain.specification.Crawl;
import de.l3s.icrawl.domain.specification.TaskConfiguration;
import de.l3s.icrawl.domain.support.JsonCollapsedUserType;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type")
public abstract class TaskExecutionRecord<T extends TaskConfiguration> {
    public enum ExecutionStatus {
        SCHEDULED, RUNNING, FINISHED, ABORTED
    }
    @Id
    @GeneratedValue
    protected Long id;
    protected DateTime startTime;
    protected DateTime endtime;
    @ManyToOne(optional = false)
    protected Campaign campaign;
    @Type(type = "de.l3s.icrawl.domain.support.JsonCollapsedUserType", parameters = @Parameter(name = JsonCollapsedUserType.PROP_CLASS, value = "de.l3s.icrawl.domain.specification.TaskConfiguration"))
    @Column(length = 16 * 1024 * 1024)
    protected T configuration;
    @ManyToOne(optional = false)
    protected Crawl crawl;
    @Enumerated(EnumType.STRING)
    protected ExecutionStatus executionStatus = ExecutionStatus.SCHEDULED;

    public TaskExecutionRecord() {}

    public TaskExecutionRecord(Long id, Campaign campaign, Crawl crawl, DateTime startTime,
            DateTime endtime, T config) {
        this.id = id;
        this.campaign = campaign;
        this.crawl = crawl;
        this.startTime = Preconditions.checkNotNull(startTime);
        this.endtime = endtime;
        this.configuration = Preconditions.checkNotNull(config);
    }

    public Long getId() {
        return id;
    }

    public DateTime getStartTime() {
        return startTime;
    }

    public DateTime getEndTime() {
        return endtime;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setStartTime(DateTime startTime) {
        this.startTime = Preconditions.checkNotNull(startTime);
    }

    public void setEndTime(DateTime endtime) {
        this.endtime = endtime;
    }

    public void setFinished() {
        this.setEndTime(DateTime.now());
        this.setExecutionStatus(ExecutionStatus.FINISHED);
    }

    public Campaign getCampaign() {
        return campaign;
    }

    public void setCampaign(Campaign campaign) {
        this.campaign = campaign;
    }

    public ExecutionStatus getExecutionStatus() {
        return executionStatus;
    }

    public void setExecutionStatus(ExecutionStatus executionStatus) {
        this.executionStatus = executionStatus;
    }

    public T getConfiguration() {
        return configuration;
    }

    public void setConfiguration(T configuration) {
        this.configuration = Preconditions.checkNotNull(configuration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, campaign);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TaskExecutionRecord<?> other = (TaskExecutionRecord<?>) obj;
        return id == other.id && campaign == other.campaign;
    }

    public Crawl getCrawl() {
        return crawl;
    }

    public void setCrawl(Crawl crawl) {
        this.crawl = Preconditions.checkNotNull(crawl);
    }

}
