package de.l3s.icrawl.domain.evaluation;

import javax.annotation.Nullable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import de.l3s.icrawl.domain.specification.Campaign;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "url", "evaluator_id", "campaign_id" }))
public class Label {
    @Id
    @GeneratedValue
    Long id;

    private String url;
    @Nullable
    private Integer rating;
    @Nullable
    private String comment;

    @ManyToOne(optional = false)
    private Evaluator evaluator;

    @ManyToOne(optional = false)
    private Campaign campaign;

    Label() {
        // default constructor for ORM
    }

    public Label(Evaluator evaluator, String url, Integer rating, String comment, Campaign campaign) {
        this.evaluator = evaluator;
        this.url = url;
        this.rating = rating;
        this.comment = comment;
        this.campaign = campaign;
    }

    public Long getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public Integer getRating() {
        return rating;
    }

    public String getComment() {
        return comment;
    }

    public Evaluator getEvaluator() {
        return evaluator;
    }

    public Campaign getCampaign() {
        return campaign;
    }

    void setId(Long id) {
        this.id = id;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setEvaluator(Evaluator evaluator) {
        this.evaluator = evaluator;
    }

    public void setCampaign(Campaign campaign) {
        this.campaign = campaign;
    }

}
