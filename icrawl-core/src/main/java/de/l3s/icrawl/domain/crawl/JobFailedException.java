package de.l3s.icrawl.domain.crawl;

public class JobFailedException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public JobFailedException(String jobName) {
        super("M/R job '" + jobName + "' failed");
    }

}
