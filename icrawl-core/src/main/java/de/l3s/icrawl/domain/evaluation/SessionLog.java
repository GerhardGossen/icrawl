package de.l3s.icrawl.domain.evaluation;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class SessionLog implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private String sessionId;

    @OneToMany(mappedBy = "session")
    List<LogEntry> log;

    public String getSessionId() {
        return sessionId;
    }

    public List<LogEntry> getLog() {
        return log;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setLog(List<LogEntry> log) {
        this.log = log;
    }

    public void addToLog(Collection<LogEntry> log) {
        this.log.addAll(log);
    }

}
