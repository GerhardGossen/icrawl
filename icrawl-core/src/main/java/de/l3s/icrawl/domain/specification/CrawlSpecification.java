package de.l3s.icrawl.domain.specification;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.google.common.base.MoreObjects;
import com.google.common.collect.Sets;

import de.l3s.icrawl.domain.specification.CrawlSchedule.Interval;

@JsonTypeName("crawl")
public class CrawlSpecification extends TaskConfiguration implements Serializable {
    private static final long serialVersionUID = 1L;

    private static final CrawlSchedule DEFAULT_CRAWL_SCHEDULE = new CrawlSchedule(Interval.ONCE,
        null);

    @NotNull
    private DateTime startTime;
    @NotEmpty
    private Set<CrawlUrl> seeds = new HashSet<>();
    @NotNull
    private Set<String> referenceDocuments = new HashSet<>();
    /**
     * Relevant items that should be contained in crawled web pages.
     */
    @NotNull
    private Set<NamedEntity> entities = new HashSet<>();
    /**
     * Relevant keywords that should occur in crawled web pages.
     *
     * The keywords will be matched in a case-insensitive manner.
     */
    @NotNull
    private Set<String> keywords = new HashSet<>();
    /**
     * Determines if web page resources (images, style sheets, etc) are
     * downloaded.
     */
    private boolean downloadResources;

    private WeightingMethod weightingMethod = WeightingMethod.DOCUMENT_VECTOR;

    @Valid
    private CrawlSchedule crawlSchedule;

    public CrawlSpecification() {}

    public DateTime getStartTime() {
        return startTime;
    }

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    public void setStartTime(DateTime startTime) {
        this.startTime = startTime;
    }

    public Set<CrawlUrl> getSeeds() {
        return seeds;
    }

    public void setSeeds(Set<CrawlUrl> seeds) {
        this.seeds = seeds;
    }

    public Set<String> getReferenceDocuments() {
        return referenceDocuments;
    }

    public void setReferenceDocuments(Set<String> referenceDocuments) {
        this.referenceDocuments = referenceDocuments;
    }

    public Set<NamedEntity> getEntities() {
        return entities;
    }

    public void setEntities(Set<NamedEntity> entities) {
        this.entities = entities;
    }

    public Set<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(Set<String> keywords) {
        this.keywords = keywords != null ? normalizeKeywords(keywords) : new HashSet<>();
    }

    private static Set<String> normalizeKeywords(Set<String> keywords) {
        Set<String> ret = Sets.newHashSetWithExpectedSize(keywords.size());
        for (String keyword : keywords) {
            ret.add(keyword.toLowerCase().trim());
        }
        return ret;
    }

    public boolean isDownloadResources() {
        return downloadResources;
    }

    public void setDownloadResources(boolean downloadResources) {
        this.downloadResources = downloadResources;
    }

    public void addSeed(CrawlUrl crawlUrl) {
        seeds.add(crawlUrl);
    }

    public void addReferenceDocument(String url) {
        referenceDocuments.add(url);
    }

    public void addKeyword(String keyword) {
        this.keywords.add(keyword);
    }

    public void addEntity(NamedEntity entity) {
        this.entities.add(entity);
    }

    public CrawlSchedule getCrawlSchedule() {
        return crawlSchedule != null ? crawlSchedule : DEFAULT_CRAWL_SCHEDULE;
    }

    public void setCrawlSchedule(CrawlSchedule crawlSchedule) {
        this.crawlSchedule = crawlSchedule;
    }

    public WeightingMethod getWeightingMethod() {
        return weightingMethod;
    }

    public void setWeightingMethod(WeightingMethod weightingMethod) {
        this.weightingMethod = weightingMethod;
    }

    @Override
    public int hashCode() {
        return Objects.hash(crawlSchedule, downloadResources, entities, keywords, seeds,
            referenceDocuments, startTime, weightingMethod);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CrawlSpecification)) {
            return false;
        }
        CrawlSpecification other = (CrawlSpecification) obj;
        return downloadResources == other.downloadResources
                && Objects.equals(startTime, other.startTime)
                && Objects.equals(seeds, other.seeds)
                && Objects.equals(referenceDocuments, other.referenceDocuments)
                && Objects.equals(entities, other.entities)
                && Objects.equals(keywords, other.keywords)
                && Objects.equals(crawlSchedule, other.crawlSchedule)
                && Objects.equals(weightingMethod, other.weightingMethod);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(CrawlSpecification.class)
            .add("startTime", startTime)
            .add("seeds", seeds)
            .add("referenceDocuments", referenceDocuments)
            .add("entities", entities)
            .add("keywords", keywords)
            .add("downloadResources", downloadResources)
            .add("crawlSchedule", crawlSchedule)
            .add("weightingMethod", weightingMethod)
            .toString();
    }

    /** Set values for added fields to default values */
    public static CrawlSpecification upgrade(CrawlSpecification spec) {
        if (spec.getReferenceDocuments() == null) {
            spec.setReferenceDocuments(new HashSet<>());
        }
        if (spec.getWeightingMethod() == null) {
            spec.setWeightingMethod(WeightingMethod.DOCUMENT_VECTOR);
        }
        return spec;
    }
}