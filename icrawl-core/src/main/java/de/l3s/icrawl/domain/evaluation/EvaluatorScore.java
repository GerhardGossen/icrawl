package de.l3s.icrawl.domain.evaluation;

public class EvaluatorScore {
    private final Evaluator user;
    private final long score;

    public EvaluatorScore(Evaluator user, long score) {
        this.user = user;
        this.score = score;
    }

    public Evaluator getUser() {
        return user;
    }

    public long getScore() {
        return score;
    }

}
