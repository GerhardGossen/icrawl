package de.l3s.icrawl.domain.specification;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

/**
 * Specify a single crawl.
 *
 * If both <code>entities</code> and <code>keywords</code> are empty, an
 * unfocused crawl is conducted.
 */
@Entity
public class Crawl implements Serializable {
    private static final long serialVersionUID = 1L;
    @CreatedDate
    private DateTime created;
    @NotNull
    private String name;
    @NotNull
    @Valid
    @Column(length = 16 * 1024 * 1024)
    private CrawlSpecification specification = new CrawlSpecification();
    @ManyToOne(optional = false)
    @JoinColumn(name = "campaign_id")
    private Campaign campaign;
    @Column(name = "campaign_id", updatable = false, insertable = false)
    Long campaignId;
    @Id
    @GeneratedValue
    private Long id;
    @LastModifiedDate
    private DateTime modified;
    private boolean scheduled = false;

    public Crawl() {
        created = DateTime.now();
    }

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((specification == null) ? 0 : specification.hashCode());
        result = prime * result + (id != null ? id.intValue() : 0);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Crawl other = (Crawl) obj;
        if (!Objects.equals(id, other.id)) {
            return false;
        }
        return name.equals(other.name);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Campaign getCampaign() {
        return campaign;
    }

    public void setCampaign(Campaign campaign) {
        this.campaign = campaign;
    }

    public CrawlSpecification getSpecification() {
        return specification;
    }

    public void setSpecification(CrawlSpecification specification) {
        this.specification = specification;
    }

    public DateTime getModified() {
        return modified;
    }

    public void setModified(DateTime modified) {
        this.modified = modified;
    }

    public boolean isScheduled() {
        return scheduled;
    }

    public void setScheduled(boolean scheduled) {
        this.scheduled = scheduled;
    }

    public Long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

    @Override
    public String toString() {
        return String.format("Crawl(%d, %s)", id, name);
    }

    public static String crawlKey(Crawl crawl) {
        return crawlKey(crawl.getCampaign());
    }

    public static String crawlKey(Campaign campaign) {
        return crawlKey(campaign.getId());
    }

    public static String crawlKey(long campaignId) {
        return "crawl_" + campaignId;
    }


}
