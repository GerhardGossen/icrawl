package de.l3s.icrawl.domain.specification;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Maps;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public class CrawlUrl implements Serializable {

    /** Distinguishes between URLs for the different fetchers. */
    public enum Type {
        WEB, API, LOD
    }

    private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(CrawlUrl.class);
    private final String url;
    private Map<String, Object> metadata;

    public CrawlUrl(String url) {
        this(url, new HashMap<>());
    }

    @JsonCreator
    public CrawlUrl(@Nonnull @JsonProperty("url") String url,
            @Nonnull @JsonProperty("metadata") Map<String, Object> metadata) {
        this.url = checkNotNull(url);
        this.metadata = metadata != null ? metadata : new HashMap<String, Object>();
    }

    public CrawlUrl(String url, Object... metadata) {
        this.url = checkNotNull(url);
        if (metadata.length == 1 && metadata[0] instanceof Map) {
            @SuppressWarnings("unchecked")
            Map<String, Object> mdMap = (Map<String, Object>) metadata[0];
            this.metadata = mdMap;
        } else {
            checkArgument(metadata.length % 2 == 0,
                "Number of metadata arguments must be even, got %s", metadata.length);
            Map<String, Object> mdMap = Maps.newHashMapWithExpectedSize(metadata.length / 2);
            for (int i = 0; i < metadata.length; i += 2) {
                checkArgument(metadata[i] instanceof String,
                    "Expected String at position %s, got %s", i, metadata[i].getClass());
                mdMap.put((String) metadata[i], metadata[i + 1]);
            }
            this.metadata = mdMap;
        }
    }

    public String getUrl() {
        return url;
    }

    @JsonInclude(Include.NON_EMPTY)
    public Map<String, Object> getMetadata() {
        return Collections.unmodifiableMap(metadata);
    }

    public void addMetadata(String key, Object value) {
        metadata.put(key, value);
    }

    public Object getMetadata(String key) {
        return metadata.get(key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(url, metadata);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CrawlUrl)) {
            return false;
        }
        CrawlUrl other = (CrawlUrl) obj;
        if (!Objects.equals(url, other.url)) {
            return false;
        }
        return Objects.equals(metadata, other.metadata);
    }

    @JsonIgnore
    public Type getType() {
        // TODO find better method
        if (url.startsWith("http") || url.startsWith("ftp")) {
            return Type.WEB;
        } else if (url.startsWith("twitter")) {
            return Type.API;
        } else {
            logger.info("Unknown URL scheme in URL, treating as WEB: {}", url);
            return Type.WEB;
        }
    }

    @Override
    public String toString() {
        final int maxLen = 5;
        return String.format("CrawlUrl [url=%s, metadata=%s]", url,
            metadata != null ? toString(metadata.entrySet(), maxLen) : null);
    }

    private String toString(Collection<?> collection, int maxLen) {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        int i = 0;
        for (Iterator<?> iterator = collection.iterator(); iterator.hasNext() && i < maxLen; i++) {
            if (i > 0) {
                builder.append(", ");
            }
            builder.append(iterator.next());
        }
        builder.append("]");
        return builder.toString();
    }

}
