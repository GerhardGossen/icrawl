package de.l3s.icrawl.domain.status;

import com.google.common.collect.Multiset;

public final class ApiStatus {

    public static enum ErrorType {
        /** Could not connect to the server (e.g. TCP errors). */
        NETWORK,
        /** The server is misbehaving (e.g. HTTP 500 error). */
        SERVER,
        /** The request by the client was invalid (e.g. authentication error). */
        CLIENT,
        /** The returned documents can not be parsed. */
        DOCUMENT
    }

    public static enum ErrorBehavior {
        /** Will not attempt to fetch the given URL anymore. */
        STOP,
        /** Will start one or more attempts to fetch at a later time. */
        RETRY,
        /** One or more retries failed, giving up. */
        STOP_AFTER_RETRY
    }

    private final Multiset<ErrorType> errors;
    private final long urls;
    private final long documents;
    private final long redirects;

    public ApiStatus(Multiset<ErrorType> errors, long urls, long documents, long redirects) {
        this.errors = errors;
        this.urls = urls;
        this.documents = documents;
        this.redirects = redirects;
    }

    public Multiset<ErrorType> getErrors() {
        return errors;
    }

    public long getUrls() {
        return urls;
    }

    public long getDocuments() {
        return documents;
    }

    public long getRedirects() {
        return redirects;
    }

    @Override
    public String toString() {
        return String.format("Status [errors=%s, urls=%s, documents=%s, redirects=%s]", errors,
            urls, documents, redirects);
    }

}