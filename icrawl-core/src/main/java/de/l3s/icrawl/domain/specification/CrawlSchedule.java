package de.l3s.icrawl.domain.specification;

import java.io.Serializable;
import java.util.Objects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Months;
import org.joda.time.ReadablePeriod;
import org.joda.time.Weeks;
import org.joda.time.Years;
import org.springframework.format.annotation.DateTimeFormat;

import de.l3s.icrawl.util.DateUtils;
import static com.google.common.base.Preconditions.checkNotNull;

/** Execute a crawl in a fixed interval, possibly until a given end time. */
public class CrawlSchedule implements Serializable {
    private static final long serialVersionUID = 1L;
    private Interval interval;
    private DateTime endTime;

    public enum Interval {
        ONCE(null), DAILY(Days.ONE), WEEKLY(Weeks.ONE), MONTHLY(Months.ONE), YEARLY(Years.ONE);
        private ReadablePeriod period;

        private Interval(ReadablePeriod period) {
            this.period = period;
        }

        public ReadablePeriod getPeriod() {
            return period;
        }
    }

    protected CrawlSchedule() {}

    public CrawlSchedule(@Nonnull String interval, @Nullable DateTime endTime) {
        this(Interval.valueOf(interval), endTime);
    }

    public CrawlSchedule(@Nonnull Interval interval, @Nullable DateTime endTime) {
        this.interval = checkNotNull(interval, "interval must be non-null");
        this.endTime = endTime;
    }

    /**
     * Return the date when the crawl should be executed next.
     *
     * @param startTime
     *            start date of the crawl
     * @param lastCrawlStart
     *            the start of the last scheduled crawl or null, if the crawl
     *            has not been executed yet.
     * @return a date in the future or null, if the crawl is finished.
     */
    public DateTime nextExecution(DateTime startTime, DateTime lastCrawlStart) {
        if (lastCrawlStart == null) {
            return startTime;
        } else if (interval == Interval.ONCE) {
            return null;
        } else {
            DateTime nextStart = lastCrawlStart.plus(interval.getPeriod());
            return isValidStart(nextStart) ? DateUtils.max(nextStart, DateTime.now()) : null;
        }
    }

    private boolean isValidStart(DateTime nextDate) {
        return endTime == null || nextDate.isBefore(endTime);
    }

    public Interval getInterval() {
        return interval;
    }

    public DateTime getEndTime() {
        return endTime;
    }

    public void setInterval(Interval interval) {
        this.interval = interval;
    }

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    public void setEndTime(DateTime endTime) {
        this.endTime = endTime;
    }

    @Override
    public int hashCode() {
        return Objects.hash(interval, endTime);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof CrawlSchedule)) {
            return false;
        }
        CrawlSchedule other = (CrawlSchedule) obj;
        return Objects.equals(this.interval, other.interval)
                && Objects.equals(this.endTime, other.endTime);
    }

    @Override
    public String toString() {
        return String.format("CrawlSchedule [interval=%s, endTime=%s]", interval, endTime);
    }

}
