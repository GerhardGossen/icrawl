package de.l3s.icrawl.domain.evaluation;

import java.io.Serializable;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
public class LogEntry implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    SessionLog session;

    @NotNull
    private DateTime date;

    @NotNull
    @Type(type = "jsonMap")
    private Map<String, Object> msg;

    public Long getId() {
        return id;
    }

    public DateTime getDate() {
        return date;
    }

    public Map<String, Object> getMsg() {
        return msg;
    }

    public SessionLog getSession() {
        return session;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public void setMsg(Map<String, Object> msg) {
        this.msg = msg;
    }

    public void setSession(SessionLog session) {
        this.session = session;
    }
}
