package de.l3s.icrawl.domain.api;

public interface OAuthCredentials {

    String getConsumerKey();

    String getConsumerSecret();

    String getAccessToken();

    String getAccessTokenSecret();

}
