package de.l3s.icrawl.domain.crawl;

import java.util.Collection;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.joda.time.DateTime;

import com.google.common.base.Preconditions;

import de.l3s.icrawl.domain.api.ApiRequest;
import de.l3s.icrawl.domain.specification.ApiFetcherConfig;
import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.domain.specification.Crawl;

@Entity
@DiscriminatorValue("api")
public class ApiFetcherExecutionRecord extends TaskExecutionRecord<ApiFetcherConfig> {
    ApiFetcherExecutionRecord() {}

    public ApiFetcherExecutionRecord(Long id, Campaign campaign, Crawl crawl, DateTime startTime,
            DateTime endTime, String api, Collection<ApiRequest> requests) {
        super(id, campaign, crawl, startTime, endTime, new ApiFetcherConfig(api, requests));
    }

    public static ApiFetcherExecutionRecord forCrawl(CrawlExecutionRecord crawlExecution,
            String api, Collection<ApiRequest> requests) {
        Preconditions.checkArgument(!requests.isEmpty());
        return new ApiFetcherExecutionRecord(null, crawlExecution.getCampaign(),
            crawlExecution.getCrawl(), crawlExecution.getStartTime(), crawlExecution.getEndTime(),
            api, requests);
    }
}
