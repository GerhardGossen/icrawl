package de.l3s.icrawl.domain.crawl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import com.google.common.collect.ImmutableMap;

import de.l3s.icrawl.FieldNames;
import de.l3s.icrawl.domain.support.Collapse;

import static de.l3s.icrawl.FieldNames.HISTOGRAMS_KEY;
import static java.util.Collections.singletonMap;
import static java.util.stream.Collectors.toList;

@Entity
public class BatchRecord {
    @Embeddable
    public static class BatchRecordKey implements Serializable {
        private static final long serialVersionUID = 1L;
        private long executionId;
        private int seqNumber;

        BatchRecordKey() {
            // for persistence
        }

        public BatchRecordKey(long executionId, int seqNumber) {
            this.executionId = executionId;
            this.seqNumber = seqNumber;
        }

        public long getExecutionId() {
            return executionId;
        }

        public int getSeqNumber() {
            return seqNumber;
        }

        public void setExecution(long executionId) {
            this.executionId = executionId;
        }

        public void setSeqNumber(int seqNumber) {
            this.seqNumber = seqNumber;
        }

        @Override
        public String toString() {
            return String.format("%d-%d", executionId, seqNumber);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            } else if (!(obj instanceof BatchRecordKey)) {
                return false;
            } else {
                BatchRecordKey other = (BatchRecordKey) obj;
                return executionId == other.executionId && seqNumber == other.seqNumber;
            }
        }

        @Override
        public int hashCode() {
            return Objects.hash(executionId, seqNumber);
        }
    }

    @EmbeddedId
    private BatchRecordKey id;
    private DateTime startTime;
    private DateTime endTime;
    @ManyToOne
    @JoinColumn(name = "executionId", referencedColumnName = "id", insertable = false, updatable = false)
    private CrawlExecutionRecord execution;
    @Type(type = "jsonMap")
    @Column(length = 16 * 1024 * 1024)
    private Map<String, Object> metadata = new HashMap<>();
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "batch")
    private List<StepExecutionRecord> steps;

    protected BatchRecord() {}

    public BatchRecord(BatchRecordKey id) {
        this.id = id;
    }

    public static BatchRecord startBatch(TaskExecutionRecord<?> task, BatchRecord previousBatch) {
        int seqNumber = previousBatch == null ? -1 : previousBatch.getSeqNumber() + 1;
        BatchRecord newBatch = new BatchRecord(new BatchRecordKey(task.getId(), seqNumber));
        newBatch.setStartTime(DateTime.now());
        return newBatch;
    }

    public int getSeqNumber() {
        return id.getSeqNumber();
    }

    public DateTime getStartTime() {
        return startTime;
    }

    public DateTime getEndTime() {
        return endTime;
    }

    @Collapse
    public Map<String, Object> getMetadata() {
        return metadata;
    }

    public void setStartTime(DateTime startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(DateTime endTime) {
        this.endTime = endTime;
    }

    public void addMetadata(String key, Object value) {
        metadata.put(key, value);
    }

    public void addMetadata(Map<String, Object> additionalMetadata) {
        this.metadata.putAll(additionalMetadata);
    }

    public void setMetadata(Map<String, Object> metadata) {
        this.metadata = metadata;
    }

    public String getCrawlKey() {
        return id.toString();
    }

    public List<StepExecutionRecord> getSteps() {
        return steps;
    }

    public CrawlExecutionRecord getTaskExecutionRecord() {
        return execution;
    }

    public static Map<String, Double> scoreSums(final List<BatchRecord> batches) {
        final StepExecutionRecord fallback = new StepExecutionRecord();
        fallback.addMetadata("scoreSum", "0.0");
        Function<? super BatchRecord, ? extends Double> scoreSumExtractor = batch -> {
            Object v = findStep("scoreCount", fallback).apply(batch)
                .getMetadata()
                .get("scoreSum");
            return v instanceof String ? Double.parseDouble((String) v) : 0.0;
        };
        return mapBatches(batches, scoreSumExtractor);
    }

    private static <T> LinkedHashMap<String, T> mapBatches(final List<BatchRecord> batches,
            Function<? super BatchRecord, ? extends T> valueFunc) {
        return batches.stream()
            .collect(
                Collectors.toMap(BatchRecord::getCrawlKey,
                valueFunc,
                (a, b) -> a,
                () -> new LinkedHashMap<>()));
    }

    public static Function<BatchRecord, StepExecutionRecord> findStep(final String stepName, final StepExecutionRecord fallback) {
        return batch -> batch.getSteps()
            .stream()
            .filter(ser -> ser.getName().equals(stepName))
            .findFirst()
            .orElse(fallback);
    }

    public static Map<String, List<Integer>> relevanceScores(List<BatchRecord> batches) {
        return batchHistograms(batches, FieldNames.SCORE_HISTOGRAM_KEY);
    }

    private static Map<String, List<Integer>> batchHistograms(List<BatchRecord> batches,
            String histogramKey) {
        List<Integer> fallbackValues = IntStream.range(0, 100).mapToObj(i -> Integer.valueOf(0)).collect(toList());
        final StepExecutionRecord fallback = new StepExecutionRecord();
        fallback.addMetadata(FieldNames.HISTOGRAMS_KEY,
            singletonMap(histogramKey, ImmutableMap.of("values01", fallbackValues)));

        @SuppressWarnings("unchecked")
        Function<BatchRecord, List<Integer>> f = findStep("scoreHistogram", fallback)
            .andThen(ser -> (Map<String, Map<String, List<Integer>>>) ser.getMetadata().get(HISTOGRAMS_KEY))
            .andThen(m -> m.get(histogramKey))
            .andThen(m -> m.get("values01"));
        return mapBatches(batches, f);
    }

    public static Map<String, List<Integer>> timeScores(List<BatchRecord> batches) {
        return batchHistograms(batches, FieldNames.TIME_HISTOGRAM_KEY);
    }
}
