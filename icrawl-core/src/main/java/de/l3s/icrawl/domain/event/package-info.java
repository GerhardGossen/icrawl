/**
 * Events occuring during a crawl.
 */
package de.l3s.icrawl.domain.event;