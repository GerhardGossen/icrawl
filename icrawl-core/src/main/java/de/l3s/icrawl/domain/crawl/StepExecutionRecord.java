package de.l3s.icrawl.domain.crawl;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;

import de.l3s.icrawl.domain.support.Collapse;

@Entity
public class StepExecutionRecord {
    @Id
    @GeneratedValue
    private long id;
    private String name;
    private DateTime startTime;
    private DateTime endTime;
    @Type(type = "jsonMap")
    @Column(length = 16 * 1024 * 1024)
    private Map<String, Object> metadata = new HashMap<>();
    @ManyToOne(optional = true)
    private BatchRecord batch;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public DateTime getStartTime() {
        return startTime;
    }

    public DateTime getEndTime() {
        return endTime;
    }

    @Collapse
    public Map<String, Object> getMetadata() {
        return metadata;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStartTime(DateTime startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(DateTime endTime) {
        this.endTime = endTime;
    }

    public void addMetadata(String key, Object value) {
        this.metadata.put(key, value);
    }

    public void addMetadata(Map<String, Object> additionalMetadata) {
        this.metadata.putAll(additionalMetadata);
    }

    public void setMetadata(Map<String, Object> metadata) {
        this.metadata = metadata;
    }

    public static StepExecutionRecord startStep(BatchRecord batch, String name) {
        StepExecutionRecord step = new StepExecutionRecord();
        step.setBatch(batch);
        step.setName(name);
        step.setStartTime(DateTime.now());
        return step;
    }

    public void setBatch(BatchRecord batch) {
        this.batch = batch;
    }

    /**
     * Start a step that belongs to the crawl itself (e.g. inject).
     */
    public static StepExecutionRecord startStep(TaskExecutionRecord<?> task, String name) {
        StepExecutionRecord step = new StepExecutionRecord();
        step.setBatch(null);
        step.setName(name);
        step.setStartTime(DateTime.now());
        return step;
    }

    public Duration getDuration() {
        if (startTime == null || endTime == null) {
            return null;
        } else {
            return new Interval(startTime, endTime).toDuration();
        }
    }

}
