package de.l3s.icrawl.domain.evaluation;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity @IdClass(EvaluationVoteId.class)
public class EvaluationVote implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotNull
    @Id
    @ManyToOne(optional = false)
    private EvaluationUrl url;

    @NotNull
    @Id
    @ManyToOne(optional = false)
    private Evaluator evaluator;

    @NotNull
    private Integer relevance;

    @NotNull
    private String comment;

    public EvaluationUrl getUrl() {
        return url;
    }

    public Evaluator getEvaluator() {
        return evaluator;
    }

    public Integer getRelevance() {
        return relevance;
    }

    public String getComment() {
        return comment;
    }

    public void setUrl(EvaluationUrl url) {
        this.url = url;
    }

    public void setEvaluator(Evaluator evaluator) {
        this.evaluator = evaluator;
    }

    public void setRelevance(Integer relevance) {
        this.relevance = relevance;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public int hashCode() {
        return Objects.hash(url);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof EvaluationVote)) {
            return false;
        }

        EvaluationVote other = (EvaluationVote) obj;

        return Objects.equals(url, other.url) && Objects.equals(evaluator, other.evaluator);
    }
}
