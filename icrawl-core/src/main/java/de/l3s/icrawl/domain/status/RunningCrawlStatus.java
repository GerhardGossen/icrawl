package de.l3s.icrawl.domain.status;

import org.joda.time.DateTime;

import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.domain.specification.Crawl;

public class RunningCrawlStatus extends CrawlStatus {
    private static final long serialVersionUID = 3L;
    private long executionId;
    private int batchSeq;
    private Long totalBatches;
    private String phase;
    private double phaseProgress;
    private DateTime startTime;


    // Default constructor for JAX-WS
    RunningCrawlStatus() {}

    public RunningCrawlStatus(Campaign campaignId, Crawl crawlId, long executionId, int batchSeq,
            String phase,
            double phaseProgress, DateTime startTime, Long totalBatches, String reason) {
        super(campaignId, crawlId, TaskStatus.RUNNING, reason, phaseProgress);
        // TODO better calculation of total progress
        this.executionId = executionId;
        this.batchSeq = batchSeq;
        this.phase = phase;
        this.phaseProgress = phaseProgress;
        this.startTime = startTime;
        this.totalBatches = totalBatches;
    }


    public long getExecutionId() {
        return executionId;
    }

    /**
     * Return the sequence number of the currently processed batch.
     *
     * @return -1 if the crawl has not been started yet, a monotonically
     *         increasing number otherwise
     */
    public int getBatchSeq() {
        return batchSeq;
    }

    public String getPhase() {
        return phase;
    }

    public double getPhaseProgress() {
        return phaseProgress;
    }

    public DateTime getStartTime() {
        return startTime;
    }


    /**
     * Retrieve the total number of planned batches.
     *
     * @return null, if no fixed number of batches is scheduled
     */
    public Long getTotalBatches() {
        return totalBatches;
    }

    public void setExecutionId(long executionId) {
        this.executionId = executionId;
    }

    public void setBatchSeq(int batchSeq) {
        this.batchSeq = batchSeq;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public void setPhaseProgress(double phaseProgress) {
        this.phaseProgress = phaseProgress;
    }

    public void setStartTime(DateTime startTime) {
        this.startTime = startTime;
    }

    public void setTotalBatches(Long totalBatches) {
        this.totalBatches = totalBatches;
    }

    @Override
    public String toString() {
        return String.format(
            "RunningCrawlStatus [batch=%s-%s, totalBatches=%s, phase=%s, phaseProgress=%s, startTime=%s, getCrawlId()=%s, getReason()=%s, getStatus()=%s, getProgress()=%s, getCampaignId()=%s]",
            executionId, batchSeq, totalBatches, phase, phaseProgress, startTime, getCrawl(),
            getReason(), getStatus(), getProgress(), getCampaign());
    }

}
