package de.l3s.icrawl.domain.status;

public interface TaskStatus {
    /** The job has been submitted to the crawl manager. */
    static final String STARTING = "starting";
    /** The job is running currently. */
    static final String RUNNING = "running";
    /** The job will be executed at a later point in time. */
    static final String QUEUED = "queued";
    /** The job has completed successfully. */
    static final String FINISHED = "finished";
    /** A stop of the job has been requested and is currently being processed. */
    static final String STOPPING = "stopping";
    /** The job has been stopped because of a user request. */
    static final String ABORTED = "aborted";
    /** The job was stopped because of a problem during processing. */
    static final String FAILED = "failed";
    static final String STOPPED = "stopped";
    static final String UNKNOWN = "unknown";

    String getReason();

    String getStatus();

    /**
     * Get the current progress of the job.
     *
     * @return the progress of the job in percent (0.0-1.0)
     */
    double getProgress();
}
