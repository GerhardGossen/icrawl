package de.l3s.icrawl.domain.crawl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import static com.google.common.collect.Collections2.transform;

public class CrawlStatistics implements Serializable, Iterable<CrawlStatistic> {
    private static final long serialVersionUID = 2L;
    private Map<String, CrawlStatistic> statistics;

    public CrawlStatistics(CrawlStatistic... statistics) {
        this(Arrays.asList(statistics));
    }

    public CrawlStatistics(Collection<CrawlStatistic> statistics) {
        this.statistics = build(statistics);
    }

    private Map<String, CrawlStatistic> build(Collection<CrawlStatistic> statistics) {
        ImmutableMap.Builder<String, CrawlStatistic> builder = ImmutableMap.builder();
        for (CrawlStatistic statistic : statistics) {
            builder.put(statistic.getName(), statistic);
        }
        return builder.build();
    }

    public CrawlStatistic get(String key) {
        return statistics.get(key);
    }

    public Collection<String> getNames() {
        return statistics.keySet();
    }

    public List<CrawlStatistic> getStatistics() {
        if (statistics != null) {
            return Lists.newArrayList(statistics.values());
        } else {
            return new ArrayList<CrawlStatistic>();
        }
    }

    public void setStatistics(List<CrawlStatistic> statistics) {
        this.statistics = build(statistics);
    }

    @Override
    public String toString() {
        return String.format("CrawlStatistics [%s]", statistics != null ? statistics.values() : null);
    }

    @Override
    public int hashCode() {
        return Objects.hash(statistics.values());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CrawlStatistics)) {
            return false;
        }
        CrawlStatistics other = (CrawlStatistics) obj;
        return Objects.equals(statistics, other.statistics);
    }

    /** Create statistics without tiny values. */
    public CrawlStatistics summarize() {
        return new CrawlStatistics(transform(statistics.values(),
            new Function<CrawlStatistic, CrawlStatistic>() {
                @Override
                public CrawlStatistic apply(CrawlStatistic input) {
                    return input.summarize();
                }
            }));
    }

    @Override
    public Iterator<CrawlStatistic> iterator() {
        return statistics != null ? statistics.values().iterator()
                : Collections.<CrawlStatistic> emptyIterator();
    }

}
