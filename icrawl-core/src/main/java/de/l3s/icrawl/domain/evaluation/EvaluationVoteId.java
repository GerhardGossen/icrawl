package de.l3s.icrawl.domain.evaluation;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Embeddable;

@Embeddable
class EvaluationVoteId implements Serializable {
    private static final long serialVersionUID = 1L;
    private String url;
    private Long evaluator;

    public EvaluationVoteId() {}

    public EvaluationVoteId(Long evaluator, String url) {
        this.url = url;
        this.evaluator = evaluator;
    }

    public String getUrl() {
        return url;
    }

    public Long getEvaluator() {
        return evaluator;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setEvaluator(Long evaluator) {
        this.evaluator = evaluator;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof EvaluationVoteId)) {
            return false;
        }

        EvaluationVoteId other = (EvaluationVoteId) obj;
        return Objects.equals(this.evaluator, other.evaluator)
                && Objects.equals(this.url, other.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(evaluator, url);
    }

}