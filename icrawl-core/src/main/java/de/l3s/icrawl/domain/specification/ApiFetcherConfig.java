package de.l3s.icrawl.domain.specification;

import java.util.Collection;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.google.common.collect.Sets;

import de.l3s.icrawl.api.ApiUrlParser;
import de.l3s.icrawl.domain.api.ApiRequest;

@JsonTypeName("apifetcher")
public class ApiFetcherConfig extends TaskConfiguration {
    private String api;
    private Collection<ApiRequest> requests;

    public ApiFetcherConfig() {}

    public ApiFetcherConfig(String api, Collection<ApiRequest> requests) {
        this.api = api;
        this.requests = requests;
    }

    public String getApi() {
        return api;
    }

    public Collection<ApiRequest> getRequests() {
        return requests;
    }

    public void setApi(String api) {
        this.api = api;

    }

    @Deprecated
    public void setRequestUri(String requestUri) {
        this.requests = Sets.newHashSet(new ApiUrlParser().parse(requestUri));
    }

    public void setRequests(Collection<ApiRequest> requests) {
        this.requests = requests;
    }

    @Override
    public String toString() {
        return String.format("%s=%s", api, requests);
    }

    @Override
    public int hashCode() {
        return Objects.hash(api, requests);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ApiFetcherConfig)) {
            return false;
        }
        ApiFetcherConfig o = (ApiFetcherConfig) obj;
        return Objects.equals(api, o.api) && Objects.equals(requests, o.requests);
    }

}