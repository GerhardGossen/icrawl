package de.l3s.icrawl.domain.status;

import de.l3s.icrawl.domain.specification.ApiFetcherConfig;


public class ApiFetcherStatus implements TaskStatus {

    private final String crawlId;
    private final String status;
    private final String reason;
    private final double progress;
    private final ApiStatus apiStatus;
    private final ApiFetcherConfig request;

    ApiFetcherStatus(String crawlId, ApiFetcherConfig apiFetcherConfig, String status,
            String reason, double progress, ApiStatus apiStatus) {
        this.crawlId = crawlId;
        this.request = apiFetcherConfig;
        this.status = status;
        this.reason = reason;
        this.progress = progress;
        this.apiStatus = apiStatus;
    }

    public static TaskStatus starting(String crawlId, ApiFetcherConfig apiFetcherConfig) {
        return new ApiFetcherStatus(crawlId, apiFetcherConfig, "starting", "", 0.0, null);
    }

    public static ApiFetcherStatus running(String crawlId, ApiFetcherConfig request,
            ApiStatus apiStatus) {
        return new ApiFetcherStatus(crawlId, request, "running", "", -1.0, apiStatus);
    }

    public static ApiFetcherStatus stopping(String crawlId, ApiFetcherConfig request,
            ApiStatus apiStatus) {
        return new ApiFetcherStatus(crawlId, request, "stopping", "", 0.99, apiStatus);
    }

    public static ApiFetcherStatus stopped(String crawlId, ApiFetcherConfig request,
            ApiStatus apiStatus) {
        return new ApiFetcherStatus(crawlId, request, "stopped", "", 1.0, apiStatus);
    }

    public static ApiFetcherStatus failed(String crawlId, ApiFetcherConfig request,
            ApiStatus apiStatus) {
        return new ApiFetcherStatus(crawlId, request, "failed", "", 1.0, apiStatus);
    }

    public static ApiFetcherStatus finished(String crawlId, ApiFetcherConfig request,
            ApiStatus apiStatus) {
        return new ApiFetcherStatus(crawlId, request, "finished", "", 1.0, apiStatus);
    }

    @Override
    public String getReason() {
        return reason;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public double getProgress() {
        return progress;
    }

    public String getCrawlId() {
        return crawlId;
    }

    public ApiFetcherConfig getRequest() {
        return request;
    }

    public ApiStatus getApiStatus() {
        return apiStatus;
    }

    @Override
    public String toString() {
        return String.format(
            "ApiFetcherStatus [crawlId=%s, api=%s, url=%s, status=%s, reason=%s, progress=%s, apiStatus=%s]",
            crawlId, request, status, reason, progress, apiStatus);
    }

}
