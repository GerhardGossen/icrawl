package de.l3s.icrawl.domain.api;

import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;

import de.l3s.icrawl.api.ApiUrlParser;

import static java.util.Collections.emptyMap;
import static java.util.Collections.emptySet;

public class ApiRequest {
    private static final Logger logger = LoggerFactory.getLogger(ApiRequest.class);
    private final String api;
    private final String host;
    private final Set<String> resources;
    private final Set<String> queries;
    private final Set<String> users;
    private final Set<String> relations;
    private final Set<String> locations;
    private final Map<String, String> params;

    @JsonCreator
    public ApiRequest(@JsonProperty("api") String api, @JsonProperty("host") String host,
            @JsonProperty("resources") Set<String> resources,
            @JsonProperty("queries") Set<String> queries, @JsonProperty("users") Set<String> users,
            @JsonProperty("relations") Set<String> relations,
            @JsonProperty("locations") Set<String> locations,
            @JsonProperty("params") Map<String, String> params) {
        this.api = api;
        this.host = host;
        this.resources = resources;
        this.queries = queries;
        this.users = users;
        this.relations = relations;
        this.locations = locations;
        this.params = params;
    }

    public ApiRequest(String api, String host) {
        this(api, host, emptySet(), emptySet(), emptySet(), emptySet(), emptySet(), emptyMap());
    }

    public ApiRequest(String api, String host, String... queries) {
        this(api, host, emptySet(), Sets.newHashSet(queries), emptySet(), emptySet(), emptySet(),
            emptyMap());
    }


    public String getApi() {
        return api;
    }

    public String getHost() {
        return host;
    }

    public Set<String> getResources() {
        return resources;
    }

    public Set<String> getQueries() {
        return queries;
    }

    public Set<String> getUsers() {
        return users;
    }

    public Set<String> getRelations() {
        return relations;
    }

    public Set<String> getLocations() {
        return locations;
    }

    public Map<String, String> getParams() {
        return params;
    }

    @Override
    public String toString() {
        return String.format(
            "ApiRequest [api=%s, host=%s, resources=%s, queries=%s, users=%s, relations=%s, locations=%s, params=%s]",
            api, host, resources, queries, users, relations, locations, params);
    }

    @Override
    public int hashCode() {
        return Objects.hash(api, host, resources, queries, users, relations, locations, params);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(this instanceof ApiRequest)) {
            return false;
        }
        ApiRequest that = (ApiRequest) obj;
        return Objects.equals(api, that.api) && Objects.equals(host, that.host)
                && Objects.equals(resources, that.resources)
                && Objects.equals(queries, that.queries) && Objects.equals(users, that.users)
                && Objects.equals(relations, that.relations)
                && Objects.equals(locations, that.locations)
                && Objects.equals(params, that.params);
    }

    public static ApiRequest fromUrl(String url) {
        Preconditions.checkArgument(url.contains(":"), "Not a valid API URL %s", url);

        String requestUrl;
        if (url.contains(":q/") || url.contains("/q/")) {
            requestUrl = url;
        } else {
            String api = url.substring(0, url.indexOf(":"));
            String query = url.substring(url.indexOf(":") + 1);
            requestUrl = String.format("%s:q/post/query=%s", api, query);
            logger.debug("Transformed URL '{}' to API request URL '{}'", url, requestUrl);
        }

        return new ApiUrlParser().parse(requestUrl);
    }
}