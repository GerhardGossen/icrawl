package de.l3s.icrawl.domain.specification;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonProperty;

import static com.google.common.base.Preconditions.checkNotNull;

public class WeightVector implements Serializable {
    private static final Logger logger = LoggerFactory.getLogger(WeightVector.class);
    private static final long serialVersionUID = 1L;
    @JsonProperty
    private final Map<String, Double> weights = new HashMap<>();

    public double get(String key) {
        return weights.get(weights.get(key));
    }

    public double put(String key, double value) {
        Double ret = weights.put(checkNotNull(key), value);
        return ret != null ? ret.doubleValue() : 0.0;
    }

    public void putAll(Map<String, Double> map) {
        for (Entry<String, Double> entry : map.entrySet()) {
            checkNotNull(entry.getValue(), "Null value for key %s", entry.getKey());
            put(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public String toString() {
        return "WeigthVector" + weights.toString();
    }

    public double weight(Map<String, Double> features) {
        double ret = 0.0;
        for (Entry<String, Double> weight : weights.entrySet()) {
            Double featureValue = features.get(weight.getKey());
            if (featureValue != null) {
                ret += weight.getValue() * featureValue;
            }
        }
        return ret;
    }

    public static WeightVector uniform(Collection<String> keys) {
        double weight = 1.0 / keys.size();
        WeightVector wv = new WeightVector();
        for (String key : keys) {
            logger.trace("Adding {} -> {}", key, weight);
            wv.put(key, weight);
        }
        return wv;
    }

}
