package de.l3s.icrawl.domain.status;

import java.io.Serializable;

import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.domain.specification.Crawl;

public class CrawlStatus implements TaskStatus, Serializable {
    private static final long serialVersionUID = 3L;
    private Campaign campaign;
    private Crawl crawl;
    private double progress;
    private String status;
    private String reason;

    // Default constructor for JAX-WS
    CrawlStatus() {}

    CrawlStatus(Campaign campaign, Crawl crawl, String status, String reason, double progress) {
        this.campaign = campaign;
        this.crawl = crawl;
        this.progress = progress;
        this.status = status;
        this.reason = reason;
    }

    public static CrawlStatus starting(Campaign campaign, Crawl crawl) {
        return new CrawlStatus(campaign, crawl, STARTING, "Start requested", 0.0);
    }

    public static CrawlStatus queued(Campaign campaign, Crawl crawl, String reason) {
        return new CrawlStatus(campaign, crawl, QUEUED, reason, 0);
    }

    public static CrawlStatus finished(Campaign campaign, Crawl crawl) {
        return new CrawlStatus(campaign, crawl, FINISHED, "Finished Crawling", 1);
    }

    public static CrawlStatus stopping(Campaign campaign, Crawl crawl, double progress) {
        return new CrawlStatus(campaign, crawl, STOPPING, "Stop requested", progress);
    }

    public static CrawlStatus stopped(Campaign campaign, Crawl crawl) {
        return new CrawlStatus(campaign, crawl, STOPPED, "Stopped", 1.0);
    }

    public static CrawlStatus aborted(Campaign campaign, Crawl crawl, double progress) {
        return new CrawlStatus(campaign, crawl, ABORTED,
            "Crawl aborted because of user request", progress);
    }

    public static CrawlStatus failed(Campaign campaign, Crawl crawl, String error, double progress) {
        return new CrawlStatus(campaign, crawl, FAILED,
            "Crawl failed with error message: "
                + error, progress);
    }

    public static CrawlStatus unknown(Campaign campaign, Crawl crawl, String message) {
        return new CrawlStatus(campaign, crawl, UNKNOWN, message, 0.0);
    }

    public Crawl getCrawl() {
        return crawl;
    }

    @Override
    public String getReason() {
        return reason;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public double getProgress() {
        return progress;
    }

    public Campaign getCampaign() {
        return campaign;
    }

    public void setProgress(double progress) {
        this.progress = progress;
    }

    public void setCrawl(Crawl crawl) {
        this.crawl = crawl;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setCampaign(Campaign campaign) {
        this.campaign = campaign;
    }

    @Override
    public String toString() {
        return String.format("%d: %s (%s, %f)", crawl.getId(), status, reason, progress);
    }

}
