package de.l3s.icrawl.domain.specification;

/** Method to calculate the relevance of a page */
public enum WeightingMethod {
    KEYWORDS(true), DOCUMENT_VECTOR(true), GRAPH_ONLY(false), DV_FRESHNESS(true), FRESHNESS(false);

    private final boolean requiresKeywords;

    private WeightingMethod(boolean requiresKeywords) {
        this.requiresKeywords = requiresKeywords;
    }

    public boolean requiresKeywords() {
        return requiresKeywords;
    }
}
