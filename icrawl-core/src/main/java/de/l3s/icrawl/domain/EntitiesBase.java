package de.l3s.icrawl.domain;

import org.springframework.boot.orm.jpa.EntityScan;

/** Marker class to be used with {@link EntityScan}. */
public class EntitiesBase {

    private EntitiesBase() {}

}
