package de.l3s.icrawl.repository;

import org.springframework.data.repository.CrudRepository;

import de.l3s.icrawl.domain.evaluation.Evaluator;
import de.l3s.icrawl.domain.evaluation.Label;

public interface LabelRepository extends CrudRepository<Label, Long> {
    Label findByEvaluatorAndUrl(Evaluator evaluator, String url);
}
