package de.l3s.icrawl.repository;

import org.springframework.data.repository.CrudRepository;

import de.l3s.icrawl.domain.crawl.StepExecutionRecord;

public interface StepExecutionRecordRepository extends CrudRepository<StepExecutionRecord, Long> {

}
