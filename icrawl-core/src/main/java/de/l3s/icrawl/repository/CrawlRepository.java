package de.l3s.icrawl.repository;

import java.util.Collection;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import de.l3s.icrawl.domain.specification.Crawl;

public interface CrawlRepository extends CrudRepository<Crawl, Long> {

    List<Crawl> findByModifiedAfter(DateTime since);

    @Query("SELECT c FROM Crawl c where scheduled = false")
    Collection<Crawl> findUnscheduledCrawls();

}
