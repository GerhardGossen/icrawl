package de.l3s.icrawl.repository;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.annotation.PreDestroy;

import org.apache.avro.util.Utf8;
import org.apache.gora.accumulo.store.AccumuloStore;
import org.apache.gora.filter.Filter;
import org.apache.gora.filter.FilterList;
import org.apache.gora.filter.FilterOp;
import org.apache.gora.filter.MapFieldValueFilter;
import org.apache.gora.filter.SingleFieldValueFilter;
import org.apache.gora.mapreduce.GoraMapper;
import org.apache.gora.query.Query;
import org.apache.gora.query.Result;
import org.apache.gora.store.DataStore;
import org.apache.gora.store.DataStoreFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.SequenceFile.Reader;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.Task;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.OutputFormat;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.nutch.storage.WebPage;
import org.apache.nutch.storage.WebPage.Field;
import org.apache.nutch.util.NutchJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.google.common.base.Throwables;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import com.google.common.collect.Lists;

import de.l3s.icrawl.FieldNames;
import de.l3s.icrawl.domain.crawl.JobFailedException;
import de.l3s.icrawl.domain.crawl.TaskExecutionRecord;
import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.domain.specification.Crawl;
import de.l3s.icrawl.util.ReflectionUtils;

public class CrawlDataRepositoryTemplate implements Closeable {
    private static final Logger logger = LoggerFactory.getLogger(CrawlDataRepositoryTemplate.class);
    public interface ResultMapper<T> {
        public T map(String key, WebPage page);
    }

    public interface ResultLinesMapper<KEY, VAL, RESULT> {
        void processLine(KEY key, VAL value);

        RESULT getResult();
    }

    public interface JobSetup {
        public void setup(Job job);
    }

    public interface QuerySetup {
        public void setup(Query<String, WebPage> query);
    }


    private final class DataStoreCacheHandler
            extends CacheLoader<Long, DataStore<String, WebPage>>
            implements RemovalListener<Long, DataStore<String, WebPage>> {
        @Override
        public void onRemoval(RemovalNotification<Long, DataStore<String, WebPage>> notification) {
            notification.getValue().close();
        }

        @Override
        public DataStore<String, WebPage> load(Long key) throws Exception {
            return createStoreForId(key);
        }

    }

    public class JobBuilder<MAP_KEY, MAP_VAL, REDUCE_KEY extends Writable, REDUCE_VAL extends Writable> {
        private String[] fields;
        private Class<MAP_KEY> mapperOutKeyClass;
        private Class<MAP_VAL> mapperOutValClass;
        @SuppressWarnings("rawtypes")
        private Class<? extends OutputFormat> outputFormatClass;
        private Class<REDUCE_KEY> reducerOutKeyClass;
        private Class<REDUCE_VAL> reducerOutValClass;
        private Class<? extends GoraMapper<String, WebPage, MAP_KEY, MAP_VAL>> mapperClass;
        private Class<? extends Reducer<MAP_KEY, MAP_VAL, REDUCE_KEY, REDUCE_VAL>> reducerClass;
        private Class<? extends Reducer<MAP_KEY, MAP_VAL, REDUCE_KEY, REDUCE_VAL>> combinerClass;
        private int numReducers;
        private long campaign;
        private String jobName;
        private final List<JobSetup> jobSetup = new ArrayList<>();
        private QuerySetup querySetup;
        private Configuration jobConf = null;

        private JobBuilder() {}

        public <KEY, VAL> JobBuilder<KEY, VAL, REDUCE_KEY, REDUCE_VAL> setMapper(
                Class<? extends GoraMapper<String, WebPage, KEY, VAL>> mapperClass,
                Class<KEY> mapperOutKeyClass, Class<VAL> mapperOutValClass) {
            checkCanConstruct(mapperClass);
            checkCanConstruct(mapperOutKeyClass);
            checkCanConstruct(mapperOutValClass);
            @SuppressWarnings("unchecked")
            JobBuilder<KEY, VAL, REDUCE_KEY, REDUCE_VAL> newThis = (JobBuilder<KEY, VAL, REDUCE_KEY, REDUCE_VAL>) this;
            newThis.mapperClass = mapperClass;
            newThis.mapperOutKeyClass = mapperOutKeyClass;
            newThis.mapperOutValClass = mapperOutValClass;
            return newThis;
        }

        public JobBuilder<MAP_KEY, MAP_VAL, REDUCE_KEY, REDUCE_VAL> setFields(String[] fields) {
            this.fields = fields;
            return this;
        }

        public JobBuilder<MAP_KEY, MAP_VAL, REDUCE_KEY, REDUCE_VAL> setFields(Collection<Field> fields) {
            this.fields = new String[fields.size()];
            int i = 0;
            for (Field field : fields) {
                this.fields[i] = field.getName();
                i++;
            }
            return this;
        }

        public <KEY extends Writable, VAL extends Writable> JobBuilder<MAP_KEY, MAP_VAL, KEY, VAL> setReducer(
                Class<? extends Reducer<MAP_KEY, MAP_VAL, KEY, VAL>> reducerClass,
                Class<KEY> reducerOutKeyClass, Class<VAL> reducerOutValClass) {
            checkCanConstruct(reducerClass);
            checkCanConstruct(reducerOutKeyClass);
            checkCanConstruct(reducerOutValClass);
            @SuppressWarnings("unchecked")
            JobBuilder<MAP_KEY, MAP_VAL, KEY, VAL> newThis = (JobBuilder<MAP_KEY, MAP_VAL, KEY, VAL>) this;
            newThis.reducerClass = reducerClass;
            newThis.reducerOutKeyClass = reducerOutKeyClass;
            newThis.reducerOutValClass = reducerOutValClass;
            return newThis;
        }

        public JobBuilder<MAP_KEY, MAP_VAL, REDUCE_KEY, REDUCE_VAL> setCombiner(
                Class<? extends Reducer<MAP_KEY, MAP_VAL, REDUCE_KEY, REDUCE_VAL>> combinerClass) {
            this.combinerClass = combinerClass;
            return this;
        }

        public JobBuilder<MAP_KEY, MAP_VAL, REDUCE_KEY, REDUCE_VAL> setNumReducers(int numReducers) {
            this.numReducers = numReducers;
            return this;
        }

        public JobBuilder<MAP_KEY, MAP_VAL, REDUCE_KEY, REDUCE_VAL> setOutputFormat(
                @SuppressWarnings("rawtypes") Class<? extends OutputFormat> outputFormatClass) {
            this.outputFormatClass = outputFormatClass;
            return this;
        }

        public JobBuilder<MAP_KEY, MAP_VAL, REDUCE_KEY, REDUCE_VAL> setupJob(JobSetup jobSetup) {
            this.jobSetup.add(jobSetup);
            return this;
        }

        public JobBuilder<MAP_KEY, MAP_VAL, REDUCE_KEY, REDUCE_VAL> setupQuery(QuerySetup querySetup) {
            this.querySetup = querySetup;
            return this;
        }

        public JobBuilder<MAP_KEY, MAP_VAL, REDUCE_KEY, REDUCE_VAL> setConf(Configuration conf) {
            this.jobConf = conf;
            return this;
        }

        public Job execute() throws IOException {
            Job job = new NutchJob(jobConf != null ? jobConf : conf, jobName);
            DataStore<String, WebPage> store = getStore(campaign);
            setupMapper(job, store);
            setupReducer(job);

            for (JobSetup setup : jobSetup) {
                setup.setup(job);
            }
            job.getConfiguration().set(FieldNames.SCHEMA_NAME_PROPERTY, Crawl.crawlKey(campaign) + "_webpage");
            logger.info("Running job on schema {}",
                job.getConfiguration().get(FieldNames.SCHEMA_NAME_PROPERTY));

            try {
                boolean success = job.waitForCompletion(true);
                Counter counter = job.getCounters().findCounter(Task.Counter.MAP_INPUT_RECORDS);
                if (counter == null || counter.getValue() == 0) {
                    logger.info("No input records for job {}", jobName);
                }
                logger.info("M/R job success={}", success);
            } catch (InterruptedException e) {
                return job;
            } catch (Exception e) {
                logger.info("Exception while creating M/R job", e);
                throw Throwables.propagate(e);
            } finally {
                store.close();
                cachedStores.invalidate(campaign);
            }
            return job;
        }

        private void setupReducer(Job job) {
            if (reducerClass != null) {
                Preconditions.checkArgument(reducerOutKeyClass != null,
                    "reducerOutKeyClass is null");
                Preconditions.checkArgument(reducerOutValClass != null,
                    "reducerOutValClass is null");
                job.setOutputKeyClass(reducerOutKeyClass);
                job.setOutputValueClass(reducerOutValClass);

                job.setReducerClass(reducerClass);
            } else {
                job.setNumReduceTasks(0);
                if (reducerOutKeyClass != null) {
                    job.setOutputKeyClass(reducerOutKeyClass);
                }
                if (reducerOutValClass != null) {
                    job.setOutputValueClass(reducerOutValClass);
                }
            }

            if (combinerClass != null) {
                job.setCombinerClass(combinerClass);
            }
            if (numReducers > 0) {
                job.setNumReduceTasks(numReducers);
            }
        }

        private void setupMapper(Job job, DataStore<String, WebPage> store) throws IOException {
            Query<String, WebPage> q = store.newQuery();
            q.setFields(fields);
            if (querySetup != null) {
                querySetup.setup(q);
                logger.debug("query.filter: {}", q.getFilter());
            }
            checkFilterFieldIncluded(q.getFilter(), fields);
            GoraMapper.initMapperJob(job, q, store, mapperOutKeyClass, mapperOutKeyClass,
                mapperClass, true);

            job.setOutputFormatClass(outputFormatClass);
            job.setMapOutputKeyClass(mapperOutKeyClass);
            job.setMapOutputValueClass(mapperOutValClass);
        }

        public <RESULT> RESULT executeAndGetResult(
                ResultLinesMapper<REDUCE_KEY, REDUCE_VAL, RESULT> outputMapper) throws IOException {
            final Path tmpFile = new Path("/tmp/icrawl/" + campaign + "/"
                    + new Random().nextInt(Integer.MAX_VALUE));

            setOutputFormat(SequenceFileOutputFormat.class);
            setupJob(new JobSetup() {
                @Override
                public void setup(Job job) {
                    FileOutputFormat.setOutputPath(job, tmpFile);
                }
            });
            Job job = execute();

            if (!job.isSuccessful()) {
                throw new JobFailedException(jobName);
            }
            FileSystem fs = getFilesSystem();
            REDUCE_KEY key = ReflectionUtils.newInstance(reducerOutKeyClass);
            REDUCE_VAL val = ReflectionUtils.newInstance(reducerOutValClass);
            logger.info("Reading from file using types {} -> {}", key.getClass(), val.getClass());
            for (FileStatus file : fs.listStatus(tmpFile)) {
                if (!file.getPath().getName().startsWith("part-")) {
                    continue;
                }
                logger.debug("reading job output from path {}", file.getPath());
                try (Reader reader = new SequenceFile.Reader(fs, file.getPath(), conf)) {
                    while (reader.next(key, val)) {
                        outputMapper.processLine(key, val);
                    }
                }
            }
            fs.delete(tmpFile, true);
            return outputMapper.getResult();
        }

        @SuppressWarnings("unchecked")
        public <KEY extends Writable, VAL extends Writable> JobBuilder<MAP_KEY, MAP_VAL, KEY, VAL> skipReduce(
                Class<KEY> mapperOutKeyClass, Class<VAL> mapperOutValClass) {
            reducerClass = null;
            numReducers = 0;
            this.reducerOutKeyClass = (Class<REDUCE_KEY>) mapperOutKeyClass;
            this.reducerOutValClass = (Class<REDUCE_VAL>) mapperOutValClass;
            JobBuilder<MAP_KEY, MAP_VAL, KEY, VAL> newThis = (JobBuilder<MAP_KEY, MAP_VAL, KEY, VAL>) this;
            return newThis;
        }
    }

    static final class BatchQuery implements QuerySetup {

        private final String batchKey;

        public BatchQuery(String batchKey) {
            this.batchKey = batchKey;
        }

        @Override
        public void setup(Query<String, WebPage> query) {
            SingleFieldValueFilter<String, WebPage> genBatchFilter = new SingleFieldValueFilter<>();
            genBatchFilter.setFieldName("batchId");
            genBatchFilter.setFilterOp(FilterOp.EQUALS);
            genBatchFilter.getOperands().add(new Utf8(batchKey));
            genBatchFilter.setFilterIfMissing(true);
            query.setFilter(genBatchFilter);
            logger.debug("filter={}", query.getFilter());
        }
    }

    public static final class StatusFilterQuerySetup implements QuerySetup {
        private final byte status;

        public StatusFilterQuerySetup(byte status) {
            this.status = status;
        }

        @Override
        public void setup(Query<String, WebPage> query) {
            SingleFieldValueFilter<String, WebPage> filter = new SingleFieldValueFilter<>();
            filter.setFieldName("status");
            filter.setFilterOp(FilterOp.EQUALS);
            filter.getOperands().add((int) status);
            query.setFilter(filter);
        }
    }

    private final Configuration conf;
    private final LoadingCache<Long, DataStore<String, WebPage>> cachedStores;
    private final Properties props;

    public CrawlDataRepositoryTemplate(Configuration conf) {
        this.conf = conf;
        this.props = DataStoreFactory.createProps();
        DataStoreCacheHandler cacheHandler = new DataStoreCacheHandler();
        this.cachedStores = CacheBuilder.newBuilder()
        .expireAfterAccess(60, TimeUnit.SECONDS)
        .removalListener(cacheHandler)
        .build(cacheHandler);

    }

    private DataStore<String, WebPage> createStoreForId(long campaignId) throws IOException {
        AccumuloStore<String, WebPage> webStore = new AccumuloStore<>();
        conf.set("preferred.schema.name", Crawl.crawlKey(campaignId) + "_webpage");

        webStore.setConf(conf);
        props.setProperty("gora.datastore.schema.name", "webpage");
        webStore.initialize(String.class, WebPage.class, props);
        return webStore;
    }

    public DataStore<String, WebPage> getStore(long campaignId) throws IOException {
        try {
            logger.debug("Getting store for campaign {}", campaignId);
            return cachedStores.get(campaignId);
        } catch (ExecutionException e) {
            throw new IOException("Could not connect to CrawlDB", e);
        }
    }

    @Override
    @PreDestroy
    public void close() throws IOException {
        cachedStores.invalidateAll();
    }

    public <T> List<T> query(Campaign campaign, String[] fields, String startKey,
            int maxItems, ResultMapper<T> resultMapper) throws IOException {
        return query(campaign.getId(), fields, startKey, maxItems, resultMapper);
    }

    public <T> List<T> query(long campaignId, String[] fields, String startKey, int maxItems,
            ResultMapper<T> resultMapper) throws IOException {
        return query(campaignId, fields, startKey, maxItems, resultMapper, null);
    }

    public <T> List<T> query(Campaign campaign, String[] fields, String startKey, int maxItems,
            ResultMapper<T> resultMapper, QuerySetup query) throws IOException {
        return query(campaign.getId(), fields, startKey, maxItems, resultMapper, query);
    }

    public <T> List<T> query(long campaignId, String[] fields, String startKey, int maxItems,
            ResultMapper<T> resultMapper, QuerySetup query) throws IOException {
        List<T> result = Lists.newArrayListWithExpectedSize(maxItems);
        DataStore<String, WebPage> webStore = getStore(campaignId);
        Query<String, WebPage> q = webStore.newQuery();
        q.setLimit(maxItems);
        q.setFields(fields);
        q.setStartKey(startKey);
        if (query != null) {
            query.setup(q);
            logger.debug("query.filter: {}", q.getFilter());
        }
        checkFilterFieldIncluded(q.getFilter(), fields);
        Result<String, WebPage> qResult = webStore.execute(q);
        try {
            if (qResult == null) {
                return Collections.emptyList();
            }
            while (qResult.next()) {
                result.add(resultMapper.map(qResult.getKey(), qResult.get()));
            }
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
            throw Throwables.propagate(e);
        } finally {
            if (qResult != null) {
                qResult.close();
            }
        }
        return result;
    }

    /**
     * Checks that the fields required for the filters are included in the
     * specified fields.
     *
     * @param filter
     *            a filter specification
     * @param fields
     *            the specified query fields
     * @throws IllegalArgumentException
     *             if a field required by <code>filter</code> is not included in
     *             <code>fields</code>
     */
    private void checkFilterFieldIncluded(Filter<?, ?> filter, String[] fields) {
        if (filter == null) {
            return;
        }
        String filterField = null;
        if (filter instanceof SingleFieldValueFilter<?, ?>) {
            filterField = ((SingleFieldValueFilter<?, ?>) filter).getFieldName();
        } else if (filter instanceof MapFieldValueFilter<?, ?>) {
            filterField = ((MapFieldValueFilter<?, ?>) filter).getFieldName();
        } else if (filter instanceof FilterList<?, ?>) {
            for (Filter<?, ?> listedFilter : ((FilterList<?, ?>) filter).getFilters()) {
                checkFilterFieldIncluded(listedFilter, fields);
            }
        }

        if (filterField != null && !Arrays.asList(fields).contains(filterField)) {
            throw new IllegalArgumentException("Filtering on field " + filterField
                    + ", but not in requested fields: " + Arrays.toString(fields));
        }
    }

    public WebPage getWebPage(Campaign campaign, String url) throws IOException {
        return getWebPage(campaign.getId(), url);
    }

    public WebPage getWebPage(long campaignId, String url) throws IOException {
        DataStore<String, WebPage> webStore = getStore(campaignId);
        return webStore.get(url);
    }

    public JobBuilder<Writable, Writable, Writable, Writable> jobBuilder(Campaign campaign,
            String jobName) {
        return jobBuilder(campaign.getId(), jobName);
    }

    public JobBuilder<Writable, Writable, Writable, Writable> jobBuilder(long campaignId,
            String jobName) {
        JobBuilder<Writable, Writable, Writable, Writable> builder = new JobBuilder<>();
        builder.campaign = campaignId;
        builder.jobName = jobName;
        return builder;
    }

    public FileSystem getFilesSystem() throws IOException {
        return FileSystem.get(conf);
    }

    public static QuerySetup batchQuery(TaskExecutionRecord<?> execution, int batchId) {
        String batchKey = execution.getId() + "-" + batchId;
        logger.debug("Getting data for key {}", batchKey);
        return batchQuery(batchKey);
    }
    
    public static QuerySetup batchQuery(String batchKey) {
        return new BatchQuery(batchKey);
    }
    
    

    private static void checkCanConstruct(Class<?> clz) {
        try {
            clz.getConstructor();
        } catch (NoSuchMethodException e) {
            throw new IllegalArgumentException("Cannot construct instance of class "
                    + clz.getName() + " using default constructor, maybe non-static inner class?",
                e);
        } catch (SecurityException e) {
            throw new IllegalArgumentException("Hidden default constructor for class "
                    + clz.getName(), e);
        }
    }

}

