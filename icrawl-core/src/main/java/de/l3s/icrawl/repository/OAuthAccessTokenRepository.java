package de.l3s.icrawl.repository;

import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import de.l3s.icrawl.domain.api.OAuthAccessToken;

public interface OAuthAccessTokenRepository extends CrudRepository<OAuthAccessToken, Long> {
    @Query("SELECT t FROM OAuthAccessToken t WHERE t.used = false AND t.enabled = true AND service = ?1")
    Optional<OAuthAccessToken> findFirstUnused(String service);

    Stream<OAuthAccessToken> findByService(String service);

}
