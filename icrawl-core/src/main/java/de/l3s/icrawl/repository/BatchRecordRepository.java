package de.l3s.icrawl.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import de.l3s.icrawl.domain.crawl.BatchRecord;
import de.l3s.icrawl.domain.crawl.TaskExecutionRecord;
import de.l3s.icrawl.domain.crawl.BatchRecord.BatchRecordKey;

public interface BatchRecordRepository extends CrudRepository<BatchRecord, BatchRecordKey> {

    BatchRecord findFirstByExecutionOrderByStartTimeDesc(TaskExecutionRecord<?> taskExecution);

    List<BatchRecord> findByExecutionId(long executionId);
}
