package de.l3s.icrawl.repository;

import org.springframework.data.repository.CrudRepository;

import de.l3s.icrawl.domain.evaluation.EvaluationVote;

public interface EvaluationVoteRepository extends CrudRepository<EvaluationVote, Long> {}
