package de.l3s.icrawl.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.CrudRepository;

import de.l3s.icrawl.domain.evaluation.EvaluationUrl;

public interface EvaluationUrlRepository extends CrudRepository<EvaluationUrl, Long> {
    EvaluationUrl findByUrl(String url);

    Slice<EvaluationUrl> findAll(Pageable pageable);

    @Query("from EvaluationUrl where url in (SELECT url FROM EvaluationUrl url LEFT JOIN url.votes vote WHERE evaluator_id is null or (evaluator_id is not null and evaluator_id != :evaluator) GROUP BY url HAVING count(evaluator_id) < 3 ORDER BY count(evaluator_id))")
    List<EvaluationUrl> findNextPart(@Param("evaluator") Long evaluator);
}
