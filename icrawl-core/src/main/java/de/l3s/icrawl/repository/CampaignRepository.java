package de.l3s.icrawl.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import de.l3s.icrawl.domain.specification.Campaign;

@Repository
public interface CampaignRepository extends CrudRepository<Campaign, Long> {

    @Query("SELECT c FROM Campaign c where owner = ?1 and archived IS NULL")
    List<Campaign> findActiveByOwner(String ownerName);

    @Query("SELECT c FROM Campaign c where owner = ?1 and archived IS NOT NULL")
    List<Campaign> findArchivedByOwner(String ownerName);

}
