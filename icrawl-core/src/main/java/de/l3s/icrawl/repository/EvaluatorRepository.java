package de.l3s.icrawl.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import de.l3s.icrawl.domain.evaluation.Evaluator;
import de.l3s.icrawl.domain.evaluation.EvaluatorScore;

@Repository
public interface EvaluatorRepository extends CrudRepository<Evaluator, Long> {
    Evaluator findByEmail(String email);

    @Query("SELECT new de.l3s.icrawl.domain.evaluation.EvaluatorScore(user, count(*) as score) from Evaluator user INNER JOIN user.votes GROUP BY user.id ORDER BY score DESC")
    List<EvaluatorScore> getRating();
}
