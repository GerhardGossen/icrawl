package de.l3s.icrawl.repository;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/** Marker class to be used with {@link EnableJpaRepositories}. */
public class RepositoriesBase {

    private RepositoriesBase() {}

}
