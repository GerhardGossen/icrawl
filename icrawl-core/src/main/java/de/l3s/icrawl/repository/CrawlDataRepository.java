package de.l3s.icrawl.repository;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;

import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.crawl.CrawlStatus;
import org.apache.nutch.protocol.ProtocolStatusUtils;
import org.apache.nutch.storage.ProtocolStatus;
import org.apache.nutch.storage.WebPage;
import org.apache.nutch.util.TableUtil;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.stereotype.Repository;

import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate.ResultMapper;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate.StatusFilterQuerySetup;

@Repository
@Lazy
public class CrawlDataRepository implements Closeable {
    private static final class WebPageResultMapper implements ResultMapper<WebPageInfo> {
        @Override
        public WebPageInfo map(String key, WebPage page) {
            return new WebPageInfo(key, page);
        }
    }

    public static class WebPageInfo {
        private static final Logger logger = LoggerFactory.getLogger(WebPageInfo.class);
        private static final DateTimeFormatter TS_FORMATTER = org.joda.time.format.DateTimeFormat.forPattern("yyyyMMddHHmmss");
        private final String url;
        private final int status;
        private final long fetchTime;
        private final String title;
        private final String key;
        private final ProtocolStatus protocolStatus;

        public WebPageInfo(String key, WebPage page) {
            this(key, getStringOrUrl(page.getBaseUrl(), key), page.getStatus(),
                page.getFetchTime(), getStringOrUrl(page.getTitle(), key), page.getProtocolStatus());
        }

        WebPageInfo(String key, String url, int status, long fetchTime, String title,
                ProtocolStatus protocolStatus) {
            this.key = key;
            this.url = url;
            this.status = status;
            this.fetchTime = fetchTime;
            this.title = title;
            this.protocolStatus = protocolStatus;
        }

        public String getKey() {
            return key;
        }

        public String getUrl() {
            return url;
        }

        public String getStatus() {
            logger.trace("Status={}, protocolStatus={}", status, protocolStatus);
            String s = CrawlStatus.getName((byte) status);
            String longStatus = s != null ? s.substring("status_".length()) : "unknown";
            if (protocolStatus != null && !protocolStatus.isSuccess()) {
                longStatus += " (" + ProtocolStatusUtils.getName(protocolStatus.getCode()) + ")";
            }
            return longStatus;
        }

        @DateTimeFormat(iso = ISO.DATE_TIME)
        public DateTime getFetchTime() {
            return fetchTime != 0 ? new DateTime(fetchTime) : null;
        }

        public String getFetchTimeTS() {
            return TS_FORMATTER.print(getFetchTime());
        }

        public String getTitle() {
            return title;
        }

        private static String getStringOrUrl(CharSequence utf8Url, String key) {
            if (utf8Url != null && utf8Url.length() > 0) {
                return utf8Url.toString();
            } else {
                return TableUtil.unreverseUrl(key);
            }
        }

        public boolean isFetched() {
            return ((byte) status) == CrawlStatus.STATUS_FETCHED;
        }
    }

    private static final String[] WEBPAGE_FIELDS = new String[] { "baseUrl", "status", "fetchTime",
            "title", "protocolStatus" };
    private final CrawlDataRepositoryTemplate template;

    @Inject
    public CrawlDataRepository(Configuration conf) {
        this.template = new CrawlDataRepositoryTemplate(conf);
    }

    public List<WebPageInfo> listWebpagesForCampaign(Campaign campaign, int maxItems, String startKey)
            throws IOException {
        return template.query(campaign, WEBPAGE_FIELDS, startKey, maxItems,
            new WebPageResultMapper());
    }

    public List<WebPageInfo> listFetchedWebpagesForCampaign(Campaign campaign, int maxItems, String startKey)
            throws IOException {
        return template.query(campaign, WEBPAGE_FIELDS, startKey, maxItems,
            new WebPageResultMapper(), new StatusFilterQuerySetup(CrawlStatus.STATUS_FETCHED));
    }

    public List<WebPageInfo> listUnfetchedWebpagesForCampaign(Campaign campaign, int maxItems,
            String startKey) throws IOException {
        return template.query(campaign, WEBPAGE_FIELDS, startKey, maxItems,
            new WebPageResultMapper(), new StatusFilterQuerySetup(CrawlStatus.STATUS_UNFETCHED));
    }

    public WebPage getWebPage(Campaign campaign, String url) throws IOException {
        return template.getWebPage(campaign, url);
    }

    @Override
    @PreDestroy
    public void close() throws IOException {
        template.close();
    }
}