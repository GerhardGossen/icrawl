package de.l3s.icrawl.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import de.l3s.icrawl.domain.crawl.TaskExecutionRecord;
import de.l3s.icrawl.domain.crawl.TaskExecutionRecord.ExecutionStatus;
import de.l3s.icrawl.domain.specification.Crawl;

public interface TaskExecutionRepository extends CrudRepository<TaskExecutionRecord<?>, Long> {

    List<TaskExecutionRecord<?>> findByCrawl(Crawl crawl);

    List<TaskExecutionRecord<?>> findByCrawl(Long crawlId);

    @Query("SELECT t FROM TaskExecutionRecord t WHERE t.executionStatus = de.l3s.icrawl.domain.crawl.TaskExecutionRecord$ExecutionStatus.SCHEDULED")
    List<TaskExecutionRecord<?>> findStartableTasks();

    @Query("SELECT t FROM TaskExecutionRecord t WHERE endTime is NULL")
    List<TaskExecutionRecord<?>> findScheduledTasks();

    /**
     * Marks all currently running tasks as {@link ExecutionStatus#ABORTED}
     *
     * @return the number of modified tasks
     */
    @Modifying
    @Transactional
    @Query("UPDATE TaskExecutionRecord t SET t.executionStatus = de.l3s.icrawl.domain.crawl.TaskExecutionRecord$ExecutionStatus.ABORTED "
            + "WHERE t.executionStatus = de.l3s.icrawl.domain.crawl.TaskExecutionRecord$ExecutionStatus.RUNNING ")
    public int stopRunningJobs();
}
