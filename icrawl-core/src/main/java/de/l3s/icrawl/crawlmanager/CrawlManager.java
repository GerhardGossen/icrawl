package de.l3s.icrawl.crawlmanager;

import java.util.List;

import de.l3s.icrawl.domain.crawl.CrawlStatistics;
import de.l3s.icrawl.domain.status.CrawlStatus;
import de.l3s.icrawl.domain.status.ExportStatus;
import de.l3s.icrawl.domain.status.RunningCrawlStatus;

/**
 * Manages the execution of crawls.
 */
public interface CrawlManager {

    /**
     * Get the current status of a crawl.
     *
     * @param crawlId
     *            the crawl to query for
     * @return the current status of the crawl
     */
    CrawlStatus getCrawlStatus(long crawlId);

    /**
     * Schedule a crawl for immediate execution.
     *
     * The crawl may be actually executed at a later point in time, e.g. when
     * the crawling machine(s) are overloaded, check the return value for the
     * actual status of the crawl.
     *
     * @param crawlId
     *            the crawl to start
     */
    void startCrawl(long crawlId);

    void updateCrawl(long crawl);

    /**
     * Schedule a graceful stop of a crawl.
     *
     * A currently running crawl batch is allowed to finish, but no new batches
     * are scheduled.
     *
     * @param crawlId
     *            the crawl to stop
     * @return the status of the crawl
     */
    CrawlStatus stopCrawl(long crawlId);

    /**
     * Immediately and forcefully stop a crawl.
     *
     * Kills all running jobs for a crawl.
     *
     * Warning: This method cannot guarantee that the crawl can continue without
     * any manual intervention or unexpected results. Use only as an emergency
     * measure!
     *
     * @param crawlId
     *            the crawl to kill
     * @return the status of the crawl
     */
    CrawlStatus killCrawl(long crawlId);

    void deleteCrawl(long crawlId);

    List<RunningCrawlStatus> getRunningCrawls();

    /**
     * Schedule the export of a campaign for immediate execution.
     *
     * The crawl may be executed at a later time, e.g. when the machine(s) are
     * overloaded. Check the returned value for the actual status.
     *
     * @param campaignId
     *            the campaign to export
     * @param path
     *            the filesystem path where the resulting files should be
     *            stored. It must start with "hdfs://" or "file:///", in the
     *            later case the files are exported to the given path on the
     *            machine running the crawl manager implementation.
     */
    void exportCampaign(long campaignId, String path);

    void stopJobsForCampaign(long campaignId);

    /**
     * Retrieve the current status of an export job.
     *
     * @param campaignId
     *            the campaign to query
     *
     * @return the current status of the export job
     */
    ExportStatus getExportStatus(long campaignId);

    /**
     * Abort an export job.
     *
     * @param campaignId
     *            the campaign being exported
     * @param removeFiles
     *            if true, all already created files are removed
     * @return the status of the export job
     */
    ExportStatus stopExport(long campaignId, boolean removeFiles);

    long getNumJobs();

    /** Get the status of the CrawlManager itself. */
    String getStatus();

    CrawlStatistics getCrawlStatistics(long campaignId, boolean includeDetails);

    void findStartableJobs();

}
