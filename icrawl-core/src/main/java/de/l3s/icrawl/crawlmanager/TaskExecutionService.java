package de.l3s.icrawl.crawlmanager;

import java.util.List;

import org.joda.time.DateTime;

import de.l3s.icrawl.domain.crawl.BatchRecord;
import de.l3s.icrawl.domain.crawl.CrawlExecutionRecord;
import de.l3s.icrawl.domain.crawl.StepExecutionRecord;
import de.l3s.icrawl.domain.crawl.TaskExecutionRecord;
import de.l3s.icrawl.domain.specification.Crawl;
import de.l3s.icrawl.domain.specification.TaskConfiguration;

public interface TaskExecutionService {
    <T extends TaskConfiguration> TaskExecutionRecord<T> scheduleTaskExecution(
            TaskExecutionRecord<T> execution, DateTime startTime);

    <T extends TaskConfiguration> TaskExecutionRecord<T> finishTaskExecution(
            TaskExecutionRecord<T> execution);

    List<TaskExecutionRecord<?>> getAllStartableJobs();

    List<TaskExecutionRecord<?>> getAllScheduledJobs();

    BatchRecord startBatch(TaskExecutionRecord<?> crawl);

    BatchRecord finishBatch(BatchRecord batch);

    StepExecutionRecord startStep(BatchRecord batch, String name);

    StepExecutionRecord startStep(TaskExecutionRecord<?> crawl, String name);

    StepExecutionRecord finishStep(StepExecutionRecord step);

    TaskExecutionRecord<?> getExecution(long executionId);

    List<BatchRecord> getBatchesForExecution(long executionId);

    BatchRecord getBatch(long executionId, int seqNumber);

    StepExecutionRecord getStep(long stepId);

    List<TaskExecutionRecord<?>> getExecutionsForCrawl(Crawl crawl);

    CrawlExecutionRecord scheduleNextCrawlExecution(Crawl crawl, CrawlExecutionRecord lastExecution);

    void deleteExecution(TaskExecutionRecord<?> execution);

    <T extends TaskConfiguration> TaskExecutionRecord<T> startExecution(
            TaskExecutionRecord<T> execution);

    TaskExecutionRecord<?> getLastExecutionForCrawl(long crawlId);
}
