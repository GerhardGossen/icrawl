package de.l3s.icrawl.crawlmanager;

import de.l3s.icrawl.domain.crawl.BatchRecord;
import de.l3s.icrawl.domain.crawl.StepExecutionRecord;
import de.l3s.icrawl.domain.crawl.TaskExecutionRecord;

public class TaskExecutionContext {
    private final TaskExecutionService repository;
    private final TaskExecutionRecord<?> taskExecution;

    public TaskExecutionContext(TaskExecutionService repository,
            TaskExecutionRecord<?> taskExecution) {
        this.repository = repository;
        this.taskExecution = taskExecution;
    }

    public BatchRecord startBatch() {
        return repository.startBatch(taskExecution);
    }

    public void finishBatch(BatchRecord batch) {
        repository.finishBatch(batch);
    }

    public StepExecutionRecord startStep(String name) {
        return repository.startStep(taskExecution, name);
    }

    public StepExecutionRecord startStep(BatchRecord batch, String name) {
        return repository.startStep(batch, name);
    }

    public void finishStep(StepExecutionRecord stepRecord) {
        repository.finishStep(stepRecord);
    }

}
