package de.l3s.icrawl.crawlmanager;

import de.l3s.icrawl.domain.status.TaskStatus;

public interface TaskExecution {
    enum State {
        STARTING, RUNNING, FINISHED, STOPPING, STOPPED, KILLING, KILLED, FAILED
    }

    void execute(TaskExecutionContext context);

    TaskStatus getStatus();

    void stop();

    void kill();

    /**
     * Return the job progress in percent.
     *
     * @return a value in [0.0, 1.0] or -1.0 if the job duration is
     *         indeterminate (e.g. for monitoring jobs)
     */
    double getProgress();

}
