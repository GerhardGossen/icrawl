package de.l3s.icrawl.process;

import java.io.IOException;

public interface ProcessManager {

    void start() throws IOException;

    void stop();

    boolean isRunning();

    String getStatus();

}
