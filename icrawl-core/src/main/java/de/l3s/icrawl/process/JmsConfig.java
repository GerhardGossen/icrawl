package de.l3s.icrawl.process;

import javax.jms.ConnectionFactory;

import org.fusesource.stomp.jms.StompJmsConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.listener.SimpleMessageListenerContainer;
import org.springframework.jms.remoting.JmsInvokerProxyFactoryBean;
import org.springframework.jms.remoting.JmsInvokerServiceExporter;

@Configuration
public class JmsConfig {
    public static final String CRAWL_MANAGER_QUEUE = "crawlManager";
    public static final String CRAWL_MANAGER_MANAGER_QUEUE = "crawlManagerManager";
    public static final String CRAWL_DB_MANAGER_QUEUE = "CrawlDbManager";
    public static final String METADATA_DB_MANAGER_QUEUE = "MetadataDbManager";
    public static final String MESSAGE_BUS_MANAGER_QUEUE = "MessageBusManager";

    @Value("${icrawl.messages.tcpPort:61613}")
    int jmsPort = 61613;

    @Bean
    public ConnectionFactory connectionFactory() {
        StompJmsConnectionFactory factory = new StompJmsConnectionFactory();
        factory.setBrokerURI("tcp://localhost:" + jmsPort);
        factory.setDisconnectTimeout(1000);
        return factory;
    }

    public <T> T createProxy(String queueName, Class<T> serviceInterface, boolean limitWaitTime) {
        JmsInvokerProxyFactoryBean factoryBean = new JmsInvokerProxyFactoryBean();
        factoryBean.setServiceInterface(serviceInterface);
        factoryBean.setConnectionFactory(connectionFactory());
        factoryBean.setQueueName(queueName);
        if (limitWaitTime) {
            factoryBean.setReceiveTimeout(5000);
        }
        factoryBean.afterPropertiesSet();
        return serviceInterface.cast(factoryBean.getObject());
    }

    public <T> SimpleMessageListenerContainer createServiceExporter(Class<T> serviceInterface, T service, String destinationName) {
        JmsInvokerServiceExporter exporter = new JmsInvokerServiceExporter();
        exporter.setServiceInterface(serviceInterface);
        exporter.setService(service);
        exporter.afterPropertiesSet();

        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setMessageListener(exporter);
        container.setConnectionFactory(connectionFactory());
        container.setDestinationName(destinationName);
        container.setConcurrency("1");
        return container;
    }

}
