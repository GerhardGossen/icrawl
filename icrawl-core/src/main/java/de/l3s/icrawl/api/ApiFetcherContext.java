package de.l3s.icrawl.api;

import java.util.Map;

import javax.annotation.Nullable;

import de.l3s.icrawl.domain.api.ApiFetcherDocument;
import de.l3s.icrawl.domain.status.ApiStatus;

/**
 * Callback interface for API fetchers.
 *
 * An API fetcher receives an instance of this interface together with each
 * source it should fetch and needs to return results through its methods. The
 * crawler can give several {@link ApiFetcherContext}s for the same source, in
 * that case the API fetcher should store all contexts and report results for
 * each of them (subscriber pattern).
 */
public interface ApiFetcherContext {
    /** Report a fetched document. */
    void writeDocument(ApiFetcherDocument doc);

    /**
     * Report a new link that should be crawled.
     *
     * @param uri
     *            the URI to crawl
     *
     * @param metadata
     *            additional metadata for the link
     */
    void writeOutlink(String uri, Map<String, String> metadata);

    /**
     * Report a resolved redirect, e.g. from an URL shortener service.
     *
     * The crawler MAY skip downloading the <tt>fromUri</tt> to save bandwidth.
     *
     * @param fromUri
     *            the original URI
     * @param toUri
     *            the URI that fromUri redirects to
     * @param metadata
     *            additional metadata for the source
     */
    void writeRedirect(String fromUri, String toUri, Map<String, String> metadata);

    /**
     * Report an error while fetching from the API.
     *
     * The ApiFetcher should do its best to discover the cause of the error and
     * find a reasonable strategy for its future behavior.
     *
     * @param type
     *            the error class
     * @param behavior
     *            how the ApiFetcher will continue for this URI
     * @param message
     *            a human-readable description of the error and/or the future
     *            behavior (optional)
     * @param e
     *            the actual exception, e.g. in case of parser errors (optional)
     */
    void reportError(ApiStatus.ErrorType type, ApiStatus.ErrorBehavior behavior, @Nullable String message,
            @Nullable Exception e);

    @Override
    boolean equals(Object obj);

    @Override
    int hashCode();

}
