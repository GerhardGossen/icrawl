package de.l3s.icrawl.api;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.apache.hadoop.conf.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import de.l3s.icrawl.domain.api.ApiFetcherDocument;
import de.l3s.icrawl.domain.api.ApiRequest;
import de.l3s.icrawl.domain.status.ApiStatus;

import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

/**
 * Base class for API fetcher implementations.
 *
 * Subclasses need to implement the methods startFollowingUri and
 * stopFollowingUri, and start/stop following the passed in URI. On receiving an
 * update through monitoring the URIs, the appropriate <tt>protected</tt> should
 * be called.
 *
 * This base class makes sure that updates are propagated to all registered
 * {@link ApiFetcherContext}s.
 */
public abstract class AbstractApiFetcher implements ApiFetcher {
    private static final Logger logger = LoggerFactory.getLogger(AbstractApiFetcher.class);
    protected Configuration conf;
    private final Multimap<ApiRequest, ApiFetcherContext> listeners = HashMultimap.create();
    private Status status = Status.INIT;

    @Override
    public void setConfiguration(Configuration conf) {
        this.conf = conf;
        this.status = Status.RUNNING;
    }

    @Override
    public void addRequests(Collection<ApiRequest> requests, ApiFetcherContext context) {
        addRequest(mergeRequests(requests), context);
    }

    private ApiRequest mergeRequests(Collection<ApiRequest> requests) {
        Set<String> allLocations = requests.stream().flatMap(r -> r.getLocations().stream()).collect(toSet());
        Set<String> allQueries = requests.stream().flatMap(r -> r.getQueries().stream()).collect(toSet());
        Set<String> allRelations = requests.stream().flatMap(r -> r.getRelations().stream()).collect(toSet());
        Set<String> allResources = requests.stream().flatMap(r -> r.getRelations().stream()).collect(toSet());
        Set<String> allUsers = requests.stream().flatMap(r -> r.getUsers().stream()).collect(toSet());
        Map<String, String> allParams = requests.stream().flatMap(r -> r.getParams().entrySet().stream()).collect(toMap(e -> e.getKey(), e -> e.getValue()));
        String host = requests.stream().map(r -> r.getHost()).findFirst().get();
        String api = requests.stream().map(r -> r.getApi()).findFirst().get();
        return new ApiRequest(api, host, allResources, allQueries, allUsers, allRelations, allLocations, allParams);
    }

    public void addRequest(ApiRequest apiRequest, ApiFetcherContext context) {
        synchronized (listeners) {
            listeners.put(apiRequest, context);
            if (listeners.get(apiRequest).size() == 1) {
                try {
                    startFollowing(apiRequest);
                } catch (RuntimeException e) {
                    logger.info("Could not follow '{}': ", apiRequest, e);
                    throw e;
                }
            }
        }
    }

    @Override
    public void removeRequests(Collection<ApiRequest> requests, ApiFetcherContext context) {
        removeRequest(mergeRequests(requests), context);
    }

    public void removeRequest(ApiRequest apiRequest, ApiFetcherContext context) {
        synchronized (listeners) {
            Collection<ApiFetcherContext> uriListeners = listeners.get(apiRequest);
            uriListeners.remove(context);
            if (uriListeners.isEmpty()) {
                stopFollowing(apiRequest);
            }
        }
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public void stop(ApiFetcherContext context) {
        synchronized (listeners) {
            for (ApiRequest apiRequest : listeners.keySet()) {
                removeRequest(apiRequest, context);
            }
        }
    }

    @Override
    public void close() throws IOException {
        stop();
    }

    @Override
    public void stop() {
        synchronized (listeners) {
            for (ApiRequest request : listeners.keySet()) {
                stopFollowing(request);
            }
            listeners.clear();
        }
        status = Status.STOPPED;
    }

    @Override
    public void kill() {
        stop();
        status = Status.KILLED;
    }

    protected void reportError(ApiRequest source, ApiStatus.ErrorType type,
            ApiStatus.ErrorBehavior behavior,
            String message, Exception exception) {
        synchronized (listeners) {
            for (ApiFetcherContext context : listeners.get(source)) {
                context.reportError(type, behavior, message, exception);
            }
        }
    }

    protected void writeDocument(ApiRequest source, ApiFetcherDocument document) {
        synchronized (listeners) {
            Preconditions.checkState(listeners.containsKey(source),
                "No listeners defined for request %s", source);
            for (ApiFetcherContext context : listeners.get(source)) {
                context.writeDocument(document);
            }
        }
    }

    protected void writeRedirect(ApiRequest source, String fromUri, String toUri,
            Map<String, String> metadata) {
        synchronized (listeners) {
            for (ApiFetcherContext context : listeners.get(source)) {
                context.writeRedirect(fromUri, toUri, metadata);
            }
        }
    }

    protected void writeOutlink(ApiRequest source, String uri, Map<String, String> metadata) {
        synchronized (listeners) {
            for (ApiFetcherContext context : listeners.get(source)) {
                context.writeOutlink(uri, metadata);
            }
        }
    }

    protected abstract void startFollowing(ApiRequest apiRequest);

    protected abstract void stopFollowing(ApiRequest apiRequest);

}
