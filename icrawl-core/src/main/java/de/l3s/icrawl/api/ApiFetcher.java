package de.l3s.icrawl.api;

import java.io.Closeable;
import java.util.Collection;

import org.apache.hadoop.conf.Configuration;

import de.l3s.icrawl.domain.api.ApiFetcherException;
import de.l3s.icrawl.domain.api.ApiRequest;

/**
 * Module to monitor streams of documents from an API.
 *
 * An API fetcher is responsible for retrieving data from a (social media)
 * platform API. It is given one or more URIs that define the data to be
 * retrieved. Each API fetcher is started as a singleton, it is responsible for
 * maintaining quota and politeness restrictions itself.
 *
 * The module needs to implement a Listener pattern, where other modules can
 * subscribe to updates from a URI by passing in a {@link ApiFetcherContext}.
 */
public interface ApiFetcher extends Closeable {
    // TODO boolean supportsUri(...)
    /**
     * Current runtime status of the fetcher.
     */
    enum Status {
        /**
         * The fetcher is waiting to be initialized
         */
        INIT, RUNNING, FAILED, STOPPED, KILLED
    }

    /**
     * Hook for injecting the system configuration.
     *
     * This method is called immediately after the fetcher is instantiated.
     * After this method returns the fetcher MUST be ready to handle fetch
     * requests.
     *
     * @param conf
     *            the system configuration
     * @throws IllegalArgumentException
     *             if required configuration parameters are missing
     */
    void setConfiguration(Configuration conf);

    /**
     * Return the URI prefix that this fetcher can handle.
     *
     * @return a string ending in ":" or "://", e.g. <code>twitter:</code> or
     *         <code>atom:</code>. May not return null or an empty string.
     */
    String getUriScheme();

    /**
     * Start following updates for the given URI
     *
     * The processing of the request SHOULD happen asynchronously. Retrieved
     * documents should be passed back using the methods on <code>context</code>
     *
     * @param requests
     *            the requests to monitor
     * @param context
     *            used to report updates
     * @throws IllegalArgumentException
     *             if the URI is not a valid fetch request for this parser
     * @throws IllegalStateException
     *             if the fetcher has been {@link #stop() stopped},
     *             {@link #kill() killed} or if there is a permanent problem
     *             with the API
     * @throws ApiFetcherException
     */
    void addRequests(Collection<ApiRequest> requests, ApiFetcherContext apiFetcherJob);

    /**
     * Stop following updates for the given URI
     *
     * @param uri
     *            the previously monitored requests
     * @param context
     *            the context to unsubscribe
     */
    void removeRequests(Collection<ApiRequest> requests, ApiFetcherContext context);

    /**
     * Stop monitoring all URIs for the given context.
     */
    void stop(ApiFetcherContext context);

    /**
     * Request that the fetcher should finish retrieving data in the near
     * future.
     *
     * The fetcher should shut itself down afterwards. Any further method calls
     * SHOULD cause an IllegalStateException.
     */
    void stop();

    /**
     * Stop the fetcher immediately, interrupting any ongoing transfers.
     *
     * The fetcher should shut itself down afterwards. Any further method calls
     * SHOULD cause an IllegalStateException.
     */
    void kill();

    /**
     * Get the current status of the fetcher.
     *
     * @return the status code
     */
    Status getStatus();

}
