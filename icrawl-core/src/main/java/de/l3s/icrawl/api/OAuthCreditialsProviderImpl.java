package de.l3s.icrawl.api;

import org.springframework.stereotype.Service;

import de.l3s.icrawl.domain.api.OAuthAccessToken;
import de.l3s.icrawl.domain.api.OAuthCredentials;
import de.l3s.icrawl.service.OAuthAccessTokenService;

@Service
public class OAuthCreditialsProviderImpl implements OAuthCredentialsProvider {

    private final OAuthAccessTokenService tokensService;
    private final String service;

    public OAuthCreditialsProviderImpl(OAuthAccessTokenService tokensService, String service) {
        this.tokensService = tokensService;
        this.service = service;
    }

    @Override
    public OAuthCredentials get() {
        return tokensService.acquireUnusedForService(service).orElse(null);
    }

    @Override
    public void release(OAuthCredentials credentials) {
        tokensService.release((OAuthAccessToken) credentials);
    }

}
