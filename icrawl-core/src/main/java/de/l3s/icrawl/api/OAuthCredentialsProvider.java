package de.l3s.icrawl.api;

import de.l3s.icrawl.domain.api.OAuthCredentials;

public interface OAuthCredentialsProvider {

    OAuthCredentials get();

    void release(OAuthCredentials credentials);

}
