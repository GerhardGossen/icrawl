package de.l3s.icrawl.api;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Splitter;

import de.l3s.icrawl.domain.api.ApiRequest;

public class ApiUrlParser {
    private static final Splitter LIST_SPLITTER = Splitter.on(",").trimResults().omitEmptyStrings();

    public ApiRequest parse(final String uri) {
        String[] split = checkedSplit(uri, "q/", 2);
        String scheme = split[0];
        String api;
        String host;
        int sepPos = scheme.indexOf("://");
        if (sepPos >= 0) {
            api = scheme.substring(0, sepPos);
            host = scheme.substring(sepPos + 3, scheme.length() - 1);
        } else {
            api = scheme.substring(0, scheme.indexOf(":"));
            host = null;
        }

        String query = split[1];
        String[] querySplit = checkedSplit(query, "/", 2);
        Set<String> resources = new HashSet<>(LIST_SPLITTER.splitToList(querySplit[0]));

        String[] sourcesParamSplit = checkedSplit(querySplit[1], "\\?|$", 2);
        Set<String> queries = new HashSet<>();
        Set<String> users = new HashSet<>();
        Set<String> relations = new HashSet<>();
        Set<String> locations = new HashSet<>();
        for (String source : LIST_SPLITTER.split(sourcesParamSplit[0])) {
            String[] kv = checkedSplit(source, "=", 2);
            if ("query".equals(kv[0])) {
                queries.add(kv[1]);
            } else if ("user".equals(kv[0])) {
               users.add(kv[1]);
            } else if ("relations".equals(kv[0])) {
                relations.add(kv[1]);
            } else if ("location".equals(kv[0])) {
                locations.add(kv[1]);
            } else {
                throw new IllegalArgumentException("Unknown source type: " + kv[0]);
            }
        }

        Map<String, String> params = new HashMap<>();
        for (String param : LIST_SPLITTER.split(sourcesParamSplit[1])) {
            int splitPos = param.indexOf("=");
            if (splitPos > 0) {
                params.put(param.substring(0, splitPos), param.substring(splitPos + 1));
            } else {
                params.put(param, param);
            }
        }
        return new ApiRequest(api, host, resources, queries, users, relations, locations, params);
    }

    private static String[] checkedSplit(final String uri, String regex, int parts) {
        String[] split = uri.split(regex, parts);
        if (split.length != parts) {
            throw new IllegalArgumentException("Invalid URI: " + uri);
        }
        return split;
    }
}
