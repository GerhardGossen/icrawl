package de.l3s.icrawl;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import de.l3s.icrawl.domain.EntitiesBase;
import de.l3s.icrawl.process.MetadataDbManager;
import de.l3s.icrawl.repository.RepositoriesBase;

@Configuration
@EntityScan(basePackageClasses = EntitiesBase.class)
@EnableJpaRepositories(basePackageClasses = RepositoriesBase.class)
public class DbConfig {
    @Value("${icrawl.db.port:-1}")
    int port = -1;

    @Inject
    // explicit dependency on DB manager to make sure it's started first
    @Autowired(required = false)
    MetadataDbManager metadataDbManager;

    @Bean
    DataSource dataSource() {
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName("org.hsqldb.jdbcDriver");
        ds.setUsername("sa");
        ds.setPassword("");
        // TODO read DB url from config
        ds.setUrl("jdbc:hsqldb:hsql://localhost:" + port + "/metadata");
        return ds;
    }

}
