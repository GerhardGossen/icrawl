package de.l3s.icrawl.service;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import de.l3s.icrawl.crawlmanager.TaskExecutionService;
import de.l3s.icrawl.domain.api.ApiRequest;
import de.l3s.icrawl.domain.crawl.ApiFetcherExecutionRecord;
import de.l3s.icrawl.domain.crawl.BatchRecord;
import de.l3s.icrawl.domain.crawl.BatchRecord.BatchRecordKey;
import de.l3s.icrawl.domain.crawl.CrawlExecutionRecord;
import de.l3s.icrawl.domain.crawl.StepExecutionRecord;
import de.l3s.icrawl.domain.crawl.TaskExecutionRecord;
import de.l3s.icrawl.domain.crawl.TaskExecutionRecord.ExecutionStatus;
import de.l3s.icrawl.domain.specification.Crawl;
import de.l3s.icrawl.domain.specification.CrawlSpecification;
import de.l3s.icrawl.domain.specification.CrawlUrl;
import de.l3s.icrawl.domain.specification.CrawlUrl.Type;
import de.l3s.icrawl.domain.specification.TaskConfiguration;
import de.l3s.icrawl.repository.BatchRecordRepository;
import de.l3s.icrawl.repository.StepExecutionRecordRepository;
import de.l3s.icrawl.repository.TaskExecutionRepository;
import de.l3s.icrawl.util.DateUtils;

@Transactional(readOnly = true)
@Repository
public class TaskExecutionServiceImpl implements TaskExecutionService {
    private static final Logger logger = LoggerFactory.getLogger(TaskExecutionServiceImpl.class);

    private final CampaignService metadataRepository;
    private final TaskExecutionRepository taskExecutionRepository;
    private final BatchRecordRepository batchRepository;
    private final StepExecutionRecordRepository stepRepository;

    @Inject
    public TaskExecutionServiceImpl(CampaignService metadataRepository,
            TaskExecutionRepository taskExecutionRepository, BatchRecordRepository batchRepository,
            StepExecutionRecordRepository stepRepository) {
        this.metadataRepository = metadataRepository;
        this.taskExecutionRepository = taskExecutionRepository;
        this.batchRepository = batchRepository;
        this.stepRepository = stepRepository;
    }

    @Transactional
    @PostConstruct
    public void startup() {
        int stoppedJobs = this.taskExecutionRepository.stopRunningJobs();
        if (stoppedJobs > 0) {
            logger.info("Marked {} tasks from previous executions as stopped", stoppedJobs);
        }
    }


    @Transactional
    @Override
    public <T extends TaskConfiguration> TaskExecutionRecord<T> scheduleTaskExecution(
            TaskExecutionRecord<T> execution, DateTime startTime) {
        execution.setStartTime(DateUtils.max(startTime, DateTime.now()));
        return taskExecutionRepository.save(execution);
    }


    @Override
    public List<TaskExecutionRecord<?>> getExecutionsForCrawl(Crawl crawl) {
        return taskExecutionRepository.findByCrawl(crawl);
    }

    @Transactional
    @Override
    public <T extends TaskConfiguration> TaskExecutionRecord<T> finishTaskExecution(
            TaskExecutionRecord<T> task) {
        task.setFinished();
        taskExecutionRepository.save(task);
        if (task instanceof CrawlExecutionRecord) {
            CrawlExecutionRecord crawlExecution = (CrawlExecutionRecord) task;
            Crawl crawl = metadataRepository.getCrawl(crawlExecution.getCrawl().getId());
            scheduleNextCrawlExecution(crawl, crawlExecution);
        }
        return task;
    }

    @Transactional
    @Override
    public CrawlExecutionRecord scheduleNextCrawlExecution(Crawl crawl,
            CrawlExecutionRecord lastExecution) {

        CrawlExecutionRecord scheduledNextExecution = null;
        if (lastExecution == null) {
            CrawlExecutionRecord nextExecution = new CrawlExecutionRecord(crawl);
            scheduledNextExecution = (CrawlExecutionRecord) scheduleTaskExecution(nextExecution, crawl.getSpecification().getStartTime());
        } else {
            DateTime lastCrawlStart = lastExecution.getStartTime();
            CrawlSpecification spec = lastExecution.getConfiguration();
            DateTime nextStart = spec.getCrawlSchedule().nextExecution(spec.getStartTime(),
                lastCrawlStart);
            logger.debug("crawl schedule for crawl {}, lastStart={}, nextStart={}",
                lastExecution.getCrawl(), lastCrawlStart, nextStart);
            if (nextStart != null) {
                logger.debug("Creating new crawl execution for crawl {}", lastExecution.getCrawl());

                CrawlExecutionRecord nextExecution = new CrawlExecutionRecord(crawl);
                scheduledNextExecution = (CrawlExecutionRecord) scheduleTaskExecution(nextExecution, nextStart);
            }
        }
        if (scheduledNextExecution != null) {
            Multimap<String, ApiRequest> apiSeeds = HashMultimap.create();
            for (Iterator<CrawlUrl> iterator = crawl.getSpecification().getSeeds().iterator(); iterator.hasNext();) {
                CrawlUrl seed = iterator.next();
                if (seed.getType() == Type.API) {
                    ApiRequest request = ApiRequest.fromUrl(seed.getUrl());
                    apiSeeds.put(request.getApi(), request);
                    iterator.remove();
                }
            }
            for (Entry<String, Collection<ApiRequest>> apiRequests : apiSeeds.asMap().entrySet()) {
                taskExecutionRepository.save(ApiFetcherExecutionRecord.forCrawl(
                    scheduledNextExecution, apiRequests.getKey(), apiRequests.getValue()));
            }

        }
        return scheduledNextExecution;
    }

    @Transactional
    @Override
    public void deleteExecution(TaskExecutionRecord<?> execution) {
        taskExecutionRepository.delete(execution);
    }

    @Override
    public TaskExecutionRecord<?> getExecution(long executionId) {
        return taskExecutionRepository.findOne(executionId);
    }

    @Transactional
    @Override
    public BatchRecord startBatch(TaskExecutionRecord<?> taskExecution) {
        BatchRecord oldBatch = batchRepository.findFirstByExecutionOrderByStartTimeDesc(taskExecution);
        BatchRecord startBatch = BatchRecord.startBatch(taskExecution, oldBatch);
        return batchRepository.save(startBatch);
    }

    @Transactional
    @Override
    public BatchRecord finishBatch(BatchRecord batch) {
        batch.setEndTime(DateTime.now());
        return batchRepository.save(batch);
    }

    @Transactional
    @Override
    public StepExecutionRecord startStep(BatchRecord batch, String name) {
        StepExecutionRecord step = StepExecutionRecord.startStep(batch, name);
        return stepRepository.save(step);
    }

    @Transactional
    @Override
    public StepExecutionRecord startStep(TaskExecutionRecord<?> taskExecution, String name) {
        StepExecutionRecord step = StepExecutionRecord.startStep(taskExecution, name);
        return stepRepository.save(step);
    }

    @Transactional
    @Override
    public StepExecutionRecord finishStep(StepExecutionRecord step) {
        step.setEndTime(DateTime.now());
        return stepRepository.save(step);
    }

    @Override
    public BatchRecord getBatch(long executionId, int seqNumber) {
        return batchRepository.findOne(new BatchRecordKey(executionId, seqNumber));
    }

    @Override
    public List<BatchRecord> getBatchesForExecution(long executionId) {
        return batchRepository.findByExecutionId(executionId);
    }

    @Override
    public StepExecutionRecord getStep(long stepId) {
        return stepRepository.findOne(stepId);
    }


    @Override
    public List<TaskExecutionRecord<?>> getAllStartableJobs() {
        return taskExecutionRepository.findStartableTasks();
    }

    @Override
    public List<TaskExecutionRecord<?>> getAllScheduledJobs() {
        return taskExecutionRepository.findScheduledTasks();
    }

    @Transactional
    @Override
    public <T extends TaskConfiguration> TaskExecutionRecord<T> startExecution(
            TaskExecutionRecord<T> execution) {
        execution.setStartTime(DateTime.now());
        execution.setExecutionStatus(ExecutionStatus.RUNNING);
        taskExecutionRepository.save(execution);
        return execution;
    }

    @Override
    public TaskExecutionRecord<?> getLastExecutionForCrawl(long crawlId) {
        List<TaskExecutionRecord<?>> executions = taskExecutionRepository.findByCrawl(crawlId);
        return executions.isEmpty() ? null : executions.get(executions.size() - 1);
    }
}