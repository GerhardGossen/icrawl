package de.l3s.icrawl.service;

import java.util.Collection;
import java.util.List;

import org.joda.time.DateTime;

import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.domain.specification.Crawl;

// TODO check & update javadoc
public interface CampaignService {

    /**
     * Add a new crawl campaign to the repository.
     *
     * The campaign will be persisted in the Metadata DB. All scheduled jobs
     * will be started automatically.
     *
     * @param campaign
     *            the new campaign
     * @return the stored campaign. In particular, the id of the campaign is set
     *         to an unique value.
     */
    Campaign addCampaign(Campaign campaign);

    /**
     * Persist all changes to the campaign to the database.
     *
     * Note: Currently running jobs are not affected by this operation.
     *
     * @param campaign
     *            an already stored campaign where some attributes may have been
     *            modified
     * @return the stored campaign
     */
    Campaign updateCampaign(Campaign campaign);

    /**
     * Mark a campaign as archived.
     *
     * Note: Currently running jobs are not affected by this operation. Jobs
     * scheduled for the future are however not executed.
     *
     * @param campaign
     *            the campaign to archive
     * @return the stored campaign
     */
    Campaign archiveCampaign(Campaign campaign);

    /**
     * Retrieve all un-archived campaigns by the given owner.
     *
     * @param ownerName
     *            a valid username
     * @return all stored campaigns that are not archived. May return an empty
     *         collection, but never null.
     */
    Collection<Campaign> getActiveCampaignsForUser(String ownerName);

    /**
     * Retrieve all archived campaigns by the given owner.
     *
     * @param ownerName
     *            a valid username
     * @return all stored campaigns that are marked as archived. May return an
     *         empty collection, but never null.
     */
    Collection<Campaign> getArchivedCampaignsForUser(String ownerName);

    Campaign getCampaign(long campaignId);

    Crawl addCrawl(Crawl crawl);

    Crawl updateCrawl(Crawl crawl);

    void deleteCrawl(long crawlId);

    Crawl getCrawl(long crawlId);

    List<Crawl> getModifiedCrawls(DateTime lastScheduling);

    Collection<Crawl> getUnscheduledCrawls();

    Crawl markAsScheduled(Crawl crawl);
}
