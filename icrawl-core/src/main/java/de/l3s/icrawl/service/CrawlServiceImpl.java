package de.l3s.icrawl.service;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.domain.specification.Crawl;
import de.l3s.icrawl.repository.CampaignRepository;
import de.l3s.icrawl.repository.CrawlRepository;

@Repository
@Transactional(readOnly = true)
public class CrawlServiceImpl implements CampaignService {

    private final CampaignRepository campaignRepository;
    private final CrawlRepository crawlRepository;

    @Inject
    public CrawlServiceImpl(CampaignRepository campaignRepository, CrawlRepository crawlRepository) {
        this.campaignRepository = campaignRepository;
        this.crawlRepository = crawlRepository;
    }

    @Override
    @Transactional
    public Campaign addCampaign(final Campaign campaign) {
        return campaignRepository.save(campaign);
    }

    @Override
    @Transactional
    public Campaign updateCampaign(Campaign campaign) {
        return campaignRepository.save(campaign);
    }

    @Override
    @Transactional
    public Campaign archiveCampaign(Campaign campaign) {
        campaign.setArchived(DateTime.now());
        return campaignRepository.save(campaign);
    }

    @Override
    public List<Campaign> getActiveCampaignsForUser(String ownerName) {
        return campaignRepository.findActiveByOwner(ownerName);
    }

    @Override
    public List<Campaign> getArchivedCampaignsForUser(String ownerName) {
        return campaignRepository.findArchivedByOwner(ownerName);
    }

    @Override
    public Campaign getCampaign(long id) {
        return campaignRepository.findOne(id);
    }

    @Override
    @Transactional
    public Crawl addCrawl(Crawl crawl) {
        crawl.setModified(DateTime.now());
        return crawlRepository.save(crawl);
    }

    @Override
    @Transactional
    public Crawl updateCrawl(Crawl crawl) {
        crawl.setModified(DateTime.now());
        crawl.setScheduled(false);
        return crawlRepository.save(crawl);
    }

    @Override
    @Transactional
    public void deleteCrawl(long crawlId) {
        crawlRepository.delete(crawlId);
    }

    @Override
    public Crawl getCrawl(long crawlId) {
        return crawlRepository.findOne(crawlId);
    }

    @Override
    public List<Crawl> getModifiedCrawls(DateTime since) {
        return crawlRepository.findByModifiedAfter(since);
    }

    @Override
    public Collection<Crawl> getUnscheduledCrawls() {
        return crawlRepository.findUnscheduledCrawls();
    }

    @Transactional
    @Override
    public Crawl markAsScheduled(Crawl crawl) {
        crawl.setScheduled(true);
        return crawlRepository.save(crawl);
    }
}
