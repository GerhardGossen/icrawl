package de.l3s.icrawl.service;

import java.util.Optional;
import java.util.stream.Stream;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Preconditions;

import de.l3s.icrawl.domain.api.OAuthAccessToken;
import de.l3s.icrawl.repository.OAuthAccessTokenRepository;

@Service
@Transactional
public class OAuthAccessTokenService {

    @Inject
    OAuthAccessTokenRepository repository;

    public OAuthAccessToken add(OAuthAccessToken accessToken) {
        return repository.save(accessToken);
    }

    public Optional<OAuthAccessToken> acquireUnusedForService(String service) {
        return repository.findFirstUnused(service).map(at -> {
            at.setUsed(true);
            return repository.save(at);
        });
    }

    public void release(OAuthAccessToken accessToken) {
        Preconditions.checkNotNull(accessToken);
        accessToken.setUsed(false);
        repository.save(accessToken);
    }

    public Stream<OAuthAccessToken> findByService(String service) {
        return repository.findByService(service);
    }

    public Iterable<OAuthAccessToken> findAll() {
        return repository.findAll();
    }

    public OAuthAccessToken enable(OAuthAccessToken token) {
        token.setEnabled(true);
        return repository.save(token);
    }

    public OAuthAccessToken disable(OAuthAccessToken token) {
        token.setEnabled(false);
        return repository.save(token);
    }
}
