package de.l3s.icrawl.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import de.l3s.icrawl.domain.evaluation.EvaluationVote;
import de.l3s.icrawl.domain.evaluation.Evaluator;
import de.l3s.icrawl.domain.evaluation.EvaluatorScore;
import de.l3s.icrawl.domain.evaluation.Label;
import de.l3s.icrawl.repository.EvaluationUrlRepository;
import de.l3s.icrawl.repository.EvaluationVoteRepository;
import de.l3s.icrawl.repository.EvaluatorRepository;
import de.l3s.icrawl.repository.LabelRepository;

@Service
public class EvaluationService {
    private final EvaluatorRepository evaluatorRepository;
    private final EvaluationVoteRepository voteRepository;
    private final EvaluationUrlRepository urlRepository;
    private final LabelRepository labelRepository;
    private final PasswordEncoder passwordEncoder;

    @Inject
    public EvaluationService(EvaluatorRepository evaluatorRepository,
            EvaluationVoteRepository voteRepository, EvaluationUrlRepository urlRepository,
            LabelRepository labelRepository, PasswordEncoder passwordEncoder) {
        this.evaluatorRepository = evaluatorRepository;
        this.voteRepository = voteRepository;
        this.urlRepository = urlRepository;
        this.labelRepository = labelRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public void addLabel(Label label) {
        labelRepository.save(label);
    }

    public void createUser(Evaluator evaluator) {
        evaluator.setPassword(passwordEncoder.encode(evaluator.getPassword()));
        evaluatorRepository.save(evaluator);
    }

    public void addVote(Evaluator evaluator, String url, int relevance, String comment) {
        EvaluationVote vote = new EvaluationVote();
        vote.setEvaluator(evaluator);
        vote.setUrl(urlRepository.findByUrl(url));
        vote.setComment(comment);
        vote.setRelevance(relevance);

        voteRepository.save(vote);
    }

    public List<String> getNextBatchUrls(Evaluator evaluator) {
        return urlRepository.findNextPart(evaluator.getId())
            .stream()
            .map(url -> url.getUrl())
            .collect(Collectors.toList());
    }

    public Evaluator getCurrentEvaluator(UserDetails userDetails) {
        return evaluatorRepository.findByEmail(userDetails.getUsername());
    }

    public List<EvaluatorScore> getEvaluatorScores() {
        return evaluatorRepository.getRating();
    }

}
