package de.l3s.icrawl.service;

import java.util.Collection;
import java.util.Collections;

import javax.inject.Inject;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import de.l3s.icrawl.domain.evaluation.Evaluator;
import de.l3s.icrawl.repository.EvaluatorRepository;

@Service
public class EvaluatorUserDetailsService implements UserDetailsService {
    public class EvaluatorUserDetails implements UserDetails {
        private static final long serialVersionUID = 1L;
        private final Evaluator evaluator;

        public EvaluatorUserDetails(Evaluator evaluator) {
            this.evaluator = evaluator;
        }

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            return Collections.emptySet();
        }

        @Override
        public String getPassword() {
            return evaluator.getPassword();
        }

        @Override
        public String getUsername() {
            return evaluator.getEmail();
        }

        @Override
        public boolean isAccountNonExpired() {
            return true;
        }

        @Override
        public boolean isAccountNonLocked() {
            return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return true;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }

    }

    private final EvaluatorRepository repository;

    @Inject
    public EvaluatorUserDetailsService(EvaluatorRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) {
        Evaluator evaluator = repository.findByEmail(email);
        if (evaluator == null) {
            throw new UsernameNotFoundException("No account for email " + email);
        }
        return new EvaluatorUserDetails(evaluator);
    }

}
