package de.l3s.icrawl;

import org.apache.avro.util.Utf8;

public final class FieldNames {
    public static final Utf8 DETECTED_KEYWORDS = new Utf8("i.detKeywords");
    public static final Utf8 KEYWORDS = new Utf8("i.extractedKeywords");
    public static final Utf8 DOC_LENGTH = new Utf8("i.docLength");
    public static final Utf8 ENTITIES = new Utf8("i.extractedEntities");
    public static final Utf8 LANGUAGE = new Utf8("language");
    public static final Utf8 CASH = new Utf8("i.cash");
    public static final Utf8 OUTLINK_SCORES = new Utf8("i.outlinkScores");
    public static final Utf8 WEIGHT = new Utf8("i.weight");
    public static final Utf8 TIME_RELEVANCE = new Utf8("i.timeRelevance");
    public static final Utf8 MODIFIED_DATE = new Utf8("i.modified");
    public static final Utf8 FETCH_TIME_KEY = new Utf8("fetch.time");
    public static final String SIMILARITY = "icrawl.similarity";
    public static final String WEIGHTING_METHOD_KEY = "icrawl.crawl.weightingMethod";
    public static final String WEIGHTS_KEY = "icrawl.focus.weights";
    public static final String CRAWL_SPECIFICATION_KEY = "icrawl.specification";
    public static final String SCHEMA_NAME_PROPERTY = "preferred.schema.name";
    public static final String MIN_SCORE_PROPERTY = "icrawl.min.score";
    public static final String SCORE_HISTOGRAM_KEY = "score";
    public static final String TIME_HISTOGRAM_KEY = "time";
    public static final String HISTOGRAMS_KEY = "histograms";

    private FieldNames() {
        // hide constructor to prevent instantiation
    }

}
