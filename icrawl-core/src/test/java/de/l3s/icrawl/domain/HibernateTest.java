package de.l3s.icrawl.domain;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.l3s.icrawl.repository.RepositoriesBase;
import de.l3s.icrawl.repository.StepExecutionRecordRepository;
import de.l3s.icrawl.service.CampaignService;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { HibernateTest.class })
@EnableAutoConfiguration
@Configuration
@EntityScan(basePackageClasses = EntitiesBase.class)
@EnableJpaRepositories(basePackageClasses = RepositoriesBase.class)
@ComponentScan(basePackageClasses = CampaignService.class)
public class HibernateTest {
    @Bean
    PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }

    @Inject
    CampaignService crawlService;
    @Inject
    StepExecutionRecordRepository stepRepository;


    @Test
    public void testSetup() throws Exception {
        assertThat(crawlService, is(notNullValue()));
        assertThat(stepRepository, is(notNullValue()));
    }
}
