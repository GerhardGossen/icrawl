package de.l3s.icrawl.domain.support;

import org.junit.Test;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;

import de.l3s.icrawl.domain.specification.NamedEntity;
import de.l3s.icrawl.domain.specification.NamedEntity.Type;
import de.l3s.icrawl.domain.support.CountedSet;
import static org.junit.Assert.assertEquals;

public class CountedSetTest {

    @Test
    public void testCountedSetFromMultiset() {
        Multiset<String> multiset = HashMultiset.create();
        multiset.add("foo", 3);
        multiset.add("bar");
        CountedSet<String> countedSet = new CountedSet<>(multiset);
        assertEquals(3, countedSet.get("foo"));
        assertEquals(1, countedSet.get("bar"));
        assertEquals(0, countedSet.get("baz"));
    }

    @Test
    public void testAsMultiset() {
        CountedSet<String> countedSet = new CountedSet<>();
        countedSet.add("foo", 3);
        countedSet.add("bar");
        Multiset<String> multiSet = countedSet.asMultiset();
        assertEquals(3, multiSet.count("foo"));
        assertEquals(1, multiSet.count("bar"));
        assertEquals(0, multiSet.count("baz"));
    }

    @Test
    public void testSerializeEntity() throws Exception {
        NamedEntity entity = createEntity();
        ObjectMapper jsonMapper = new ObjectMapper();
        String json = jsonMapper.writeValueAsString(entity);
        NamedEntity readEntity = jsonMapper.readValue(json, NamedEntity.class);
        assertEquals(entity, readEntity);
    }

    @Test
    public void testSerializeEntry() throws Exception {
        CountedSet.Entry<NamedEntity> entry = new CountedSet.Entry<NamedEntity>(createEntity(), 3);
        ObjectMapper jsonMapper = new ObjectMapper();
        String json = jsonMapper.writeValueAsString(entry);
        CountedSet.Entry<NamedEntity> readEntry = jsonMapper.readValue(json,
            new TypeReference<CountedSet.Entry<NamedEntity>>() {});
        assertEquals(entry.getPayload(), readEntry.getPayload());
        assertEquals(entry.getCount(), readEntry.getCount());
    }

    @Test
    public void testJsonSerializationComplex() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        CountedSet<NamedEntity> entities = new CountedSet<>();
        NamedEntity entity = createEntity();
        entities.add(entity, 5);
        String entitiesJson = mapper.writeValueAsString(entities);
        CountedSet<NamedEntity> readEntities = mapper.readValue(entitiesJson,
            new TypeReference<CountedSet<NamedEntity>>() {});
        assertEquals(5, readEntities.get(entity));
    }

    @Test
    public void testJsonSerializationSimple() throws Exception {
        CountedSet<String> countedSet = new CountedSet<>();
        countedSet.add("foo", 3);
        countedSet.add("bar");
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(countedSet);
        CountedSet<String> readSet = mapper.readValue(json,
            new TypeReference<CountedSet<String>>() {});
        assertEquals(3, readSet.get("foo"));
        assertEquals(1, readSet.get("bar"));
        assertEquals(0, readSet.get("baz"));

    }

    private NamedEntity createEntity() {
        return new NamedEntity(Type.ORGANIZATION, "L3S");
    }
}
