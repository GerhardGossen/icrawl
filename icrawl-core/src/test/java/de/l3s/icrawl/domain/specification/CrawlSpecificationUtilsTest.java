package de.l3s.icrawl.domain.specification;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class CrawlSpecificationUtilsTest {
    private final static String SPEC = "{\"type\":\"crawl\",\"startTime\":1423488600,\"seeds\":[{\"url\":\"http://www.kmu.gov.ua/control/en\"}],\"referenceDocuments\":[],\"entities\":[],\"keywords\":[],\"downloadResources\":false,\"weightingMethod\":\"DOCUMENT_VECTOR\",\"crawlSchedule\":{\"interval\":\"ONCE\",\"endTime\":0}}";
    @Test
    public void testParseString() {
        CrawlSpecification spec = CrawlSpecificationUtils.parse(SPEC);
        assertThat(spec.getStartTime(), is(new DateTime(1423488600, DateTimeZone.UTC)));
    }


}
