package de.l3s.icrawl.domain.specification;

import java.util.Map;

import org.hamcrest.Matchers;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import de.l3s.icrawl.domain.specification.WeightVector;
import static org.junit.Assert.assertThat;

public class WeightVectorTest {

    @Test
    public void testWeight() {
        WeightVector wv = WeightVector.uniform(ImmutableSet.of("bergwerk", "erdogan",
            "grubenunglück", "soma", "türkei", "bergwerksunglück"));
        Map<String, Double> features = ImmutableMap.of("erdogan", 0.05, "soma", 0.10, "türkei",
            0.03, "grubenunglück", 0.05);
        double weight = wv.weight(features);
        System.out.println(weight);
        assertThat(weight, Matchers.greaterThan(0.0));
    }

}
