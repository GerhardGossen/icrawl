package de.l3s.icrawl.domain.specification;

import org.junit.Test;

import de.l3s.icrawl.domain.specification.NamedEntity.Type;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class NamedEntityTest {

    @Test
    public void testHashCode() {
        NamedEntity ne1 = new NamedEntity(Type.ORGANIZATION, "Foo");
        NamedEntity ne2 = new NamedEntity(Type.ORGANIZATION, "Foo");
        assertThat(ne1.hashCode(), is(equalTo(ne2.hashCode())));
    }

}
