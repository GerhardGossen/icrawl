package de.l3s.icrawl.domain.specification;

import javax.inject.Inject;

import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.l3s.icrawl.domain.EntitiesBase;
import de.l3s.icrawl.domain.api.ApiRequest;
import de.l3s.icrawl.domain.crawl.ApiFetcherExecutionRecord;
import de.l3s.icrawl.domain.crawl.TaskExecutionRecord;
import de.l3s.icrawl.domain.specification.TaskExecutionRecordMappingTest.TestConfiguration;
import de.l3s.icrawl.repository.CampaignRepository;
import de.l3s.icrawl.repository.CrawlRepository;
import de.l3s.icrawl.repository.RepositoriesBase;
import de.l3s.icrawl.repository.TaskExecutionRepository;

import static java.util.Collections.singleton;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class TaskExecutionRecordMappingTest {
    @Configuration
    @EnableAutoConfiguration
    @EnableJpaRepositories(basePackageClasses = RepositoriesBase.class)
    @EntityScan(basePackageClasses = EntitiesBase.class)
    static class TestConfiguration {}

    private static final Logger logger = LoggerFactory.getLogger(TaskExecutionRecordMappingTest.class);

    @Inject
    TaskExecutionRepository repository;
    @Inject
    CampaignRepository campaignRepository;
    @Inject
    CrawlRepository crawlRepository;

    @Test
    public void testRoundtrip() {
        Campaign campaign = campaignRepository.save(new Campaign("foo"));
        Crawl crawl = new Crawl();
        crawl.setCampaign(campaign);
        crawl.setName("Foo-crawl");
        CrawlSpecification spec = new CrawlSpecification();
        spec.setStartTime(DateTime.now());
        spec.getSeeds().add(new CrawlUrl("http://example.org/"));
        crawl.setSpecification(spec);
        crawl = crawlRepository.save(crawl);
        ApiFetcherExecutionRecord record = new ApiFetcherExecutionRecord(null, campaign, crawl,
            DateTime.now(), null, "twitter", singleton(new ApiRequest("twitter", "", "#foo")));
        repository.save(record);
        Long recordId = record.getId();
        logger.debug("Stored new ApiFetcherExecutionRecord with id {}", recordId);
        TaskExecutionRecord<?> retrieved = repository.findOne(recordId);
        logger.debug("Retrieved: {}", retrieved);
        assertThat(retrieved, is(instanceOf(ApiFetcherExecutionRecord.class)));
    }

}
