package de.l3s.icrawl.domain.specification;

import org.joda.time.DateTime;
import org.junit.Test;

import de.l3s.icrawl.util.DateUtils;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class CrawlScheduleTest {

    @Test
    public void testMax() {
        DateTime d1 = DateTime.now();
        DateTime d2 = d1.plusHours(1);
        assertThat(DateUtils.max(d1, d2), is(d2));
    }

}
