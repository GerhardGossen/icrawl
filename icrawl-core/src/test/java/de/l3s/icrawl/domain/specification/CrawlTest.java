package de.l3s.icrawl.domain.specification;

import org.junit.Test;

import de.l3s.icrawl.domain.specification.Crawl;
import de.l3s.icrawl.domain.specification.CrawlSpecification;
import de.l3s.icrawl.domain.specification.CrawlUrl;
import de.l3s.icrawl.domain.specification.NamedEntity;
import de.l3s.icrawl.domain.specification.NamedEntity.Type;
import static java.util.Collections.singleton;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class CrawlTest {

    @Test
    public void createdTimeIsSet() {
        CrawlSpecification spec = new CrawlSpecification();
        spec.setSeeds(singleton(new CrawlUrl("http://www.l3s.de/")));
        spec.setKeywords(singleton("L3S"));
        spec.setEntities(singleton(new NamedEntity(Type.ORGANIZATION, "L3S")));
        spec.setDownloadResources(true);
        Crawl crawl = new Crawl();
        crawl.setSpecification(spec);
        assertThat(crawl.getCreated(), notNullValue());
    }

}
