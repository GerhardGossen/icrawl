package de.l3s.icrawl.domain.util;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;

import de.l3s.icrawl.domain.support.ByteBufferUtils;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

public class ByteBufferUtilsTest {

    @Test
    public void testMap() {
        Map<String, Double> values = new HashMap<>();
        values.put("foo", 1.0);
        values.put("bar", 2.0);
        values.put("baz", 3.0);
        ByteBuffer bb = ByteBufferUtils.asByteBuffer(values);
        Map<String, Double> recoveredMap = ByteBufferUtils.asMap(bb, String.class, Double.class);
        assertThat(recoveredMap.entrySet(), hasSize(3));
        assertThat(recoveredMap, hasEntry("foo", 1.0));
        assertThat(recoveredMap, hasEntry("bar", 2.0));
        assertThat(recoveredMap, hasEntry("baz", 3.0));
    }

    @Test
    public void testMapWithNaN() {
        Map<String, Double> values = new HashMap<>();
        values.put("nan", Double.NaN);
        values.put("inf", Double.POSITIVE_INFINITY);
        values.put("ninf", Double.NEGATIVE_INFINITY);
        ByteBuffer bb = ByteBufferUtils.asByteBuffer(values);
        Map<String, Double> recoveredMap = ByteBufferUtils.asMap(bb, String.class, Double.class);
        assertThat(recoveredMap.entrySet(), hasSize(3));
        assertThat(recoveredMap, hasEntry("nan", Double.NaN));
        assertThat(recoveredMap, hasEntry("inf", Double.POSITIVE_INFINITY));
        assertThat(recoveredMap, hasEntry("ninf", Double.NEGATIVE_INFINITY));
    }

    @Test
    public void testMultiSet() throws Exception {
        Multiset<String> original = HashMultiset.create();
        original.add("foo");
        original.add("bar");
        original.add("baz");
        ByteBuffer bb = ByteBufferUtils.stringify(original);
        assertThat(ByteBufferUtils.asMultiset(bb, String.class), is(equalTo(original)));
    }
}
