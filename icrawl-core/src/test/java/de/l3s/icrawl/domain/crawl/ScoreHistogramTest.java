package de.l3s.icrawl.domain.crawl;

import org.junit.Test;

import de.l3s.icrawl.domain.crawl.ScoreHistogram;

public class ScoreHistogramTest {

    @Test
    public void testAdd() {
        ScoreHistogram histogram = new ScoreHistogram();
        histogram.add(0.1);
        histogram.add(0.01);
        histogram.add(0.011);
        histogram.add(1.1);
        histogram.add(3);
        System.out.println(histogram);
    }

}
