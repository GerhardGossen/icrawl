package de.l3s.icrawl.api;

import org.junit.Test;

import de.l3s.icrawl.domain.api.ApiRequest;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class ApiUrlParserTest {

    @Test
    public void testParseTwitter() {
        ApiRequest apiRequest = new ApiUrlParser().parse("twitter:q/post/user=foo?limit=200");

        assertThat(apiRequest, is(notNullValue()));
        assertThat(apiRequest.getApi(), is("twitter"));
        assertThat(apiRequest.getResources(), hasItem("post"));
        assertThat(apiRequest.getUsers(), hasItem("foo"));
        assertThat(apiRequest.getParams(), hasEntry("limit", "200"));
    }

    @Test
    public void testParsePinterest() throws Exception {
        ApiRequest apiRequest = new ApiUrlParser().parse("pinterest:q/relations/user=foo,relations=foo");

        assertThat(apiRequest, is(notNullValue()));
        assertThat(apiRequest.getApi(), is("pinterest"));
        assertThat(apiRequest.getResources(), hasItem("relations"));
        assertThat(apiRequest.getUsers(), hasItem("foo"));
        assertThat(apiRequest.getRelations(), hasItem("foo"));
        assertTrue("is empty: " + apiRequest.getParams(), apiRequest.getParams().isEmpty());
    }

    @Test
    public void testParseDiaspora() throws Exception {
        ApiRequest apiRequest = new ApiUrlParser().parse("diaspora://pod.geraspora.de/q/post/user=foo");

        assertThat(apiRequest, is(notNullValue()));
        assertThat(apiRequest.getApi(), is("diaspora"));
        assertThat(apiRequest.getHost(), is("pod.geraspora.de"));
        assertThat(apiRequest.getResources(), hasItem("post"));
        assertThat(apiRequest.getUsers(), hasItem("foo"));
    }
}
