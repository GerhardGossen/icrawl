package de.l3s.icrawl.apifetcher;

import java.io.Closeable;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.apache.hadoop.conf.Configuration;
import org.springframework.stereotype.Component;

import de.l3s.icrawl.nutch.Injector;
import de.l3s.icrawl.nutch.InjectorSetupException;

@Component
public class InjectorFactory implements Closeable {
    private final Map<String, Injector> injectorsByCrawlId = new HashMap<>();
    private final Configuration conf;

    @Inject
    public InjectorFactory(Configuration conf) {
        this.conf = conf;
    }

    public Injector getInstance(String crawlId) throws InjectorSetupException {
        synchronized (injectorsByCrawlId) {
            Injector injector = injectorsByCrawlId.get(crawlId);
            if (injector == null) {
                injector = new Injector(conf, crawlId);
                injectorsByCrawlId.put(crawlId, injector);
            }
            return injector;
        }
    }

    @Override
    public void close() throws IOException {
        for (Map.Entry<String, Injector> injector : injectorsByCrawlId.entrySet()) {
            injector.getValue().close();
        }
    }
}
