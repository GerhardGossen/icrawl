/**
 * Retrieve data through Web platform APIs.
 *
 * The actual fetching is delegated to instances of
 * {@link de.l3s.icrawl.api.ApiFetcher}, classes in this package handle the
 * instatiation of ApiFetchers and the updating of the database.
 */
package de.l3s.icrawl.apifetcher;