package de.l3s.icrawl.apifetcher;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Objects;
import com.google.common.base.Throwables;
import com.google.common.collect.EnumMultiset;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Multiset;

import de.l3s.icrawl.api.ApiFetcher;
import de.l3s.icrawl.api.ApiFetcherContext;
import de.l3s.icrawl.crawlmanager.TaskExecution;
import de.l3s.icrawl.crawlmanager.TaskExecutionContext;
import de.l3s.icrawl.domain.api.ApiFetcherDocument;
import de.l3s.icrawl.domain.api.ApiRequest;
import de.l3s.icrawl.domain.specification.ApiFetcherConfig;
import de.l3s.icrawl.domain.specification.Crawl;
import de.l3s.icrawl.domain.status.ApiFetcherStatus;
import de.l3s.icrawl.domain.status.ApiStatus;
import de.l3s.icrawl.domain.status.TaskStatus;
import de.l3s.icrawl.nutch.Injector;
import de.l3s.icrawl.nutch.InjectorInjectionException;

public class ApiFetcherJob implements ApiFetcherContext, TaskExecution {
    public static final String API_BATCH_ID = "-2";
    private static final Logger logger = LoggerFactory.getLogger(ApiFetcherJob.class);
    private final Multiset<ApiStatus.ErrorType> errors = EnumMultiset.create(ApiStatus.ErrorType.class);
    private final AtomicLong injectedUrls = new AtomicLong();
    private final AtomicLong injectedRedirects = new AtomicLong();
    private final AtomicLong writtenDocuments = new AtomicLong();
    private final Injector injector;
    private final Crawl crawl;
    private final ApiFetcherConfig apiFetcherConfig;
    private final ApiFetcher fetcher;
    private State state = State.STARTING;
    private final String crawlKey;

    public ApiFetcherJob(ApiFetcher fetcher, Injector injector, Crawl crawl,
            ApiFetcherConfig apiFetcherConfig) {
        this.fetcher = fetcher;
        this.injector = injector;
        this.crawl = crawl;
        this.apiFetcherConfig = apiFetcherConfig;
        this.crawlKey = Crawl.crawlKey(crawl);
    }

    public String getApi() {
        return apiFetcherConfig.getApi();
    }

    public Collection<ApiRequest> getRequests() {
        return apiFetcherConfig.getRequests();
    }

    @Override
    public void writeDocument(ApiFetcherDocument doc) {
        byte[] content = doc.getContent();
        String contentType = doc.getContentType();
        Map<String, String> headers = doc.getHeaders();
        Map<String, String> metadata = ImmutableMap.of("source", doc.getSourceUrl());
        try {
            injector.writeDocument(doc.getUri(), content, contentType, headers, metadata,
                API_BATCH_ID);
            writtenDocuments.incrementAndGet();
        } catch (InjectorInjectionException e) {
            throw Throwables.propagate(e);
        }
    }

    @Override
    public void writeOutlink(String uri, Map<String, String> metadata) {
        try {
            injector.inject(uri, metadata);
            injectedUrls.incrementAndGet();
        } catch (InjectorInjectionException e) {
            logger.warn("Exception while inserting URL {} with metadata {}", uri, metadata, e);
        }
    }

    @Override
    public void writeRedirect(String fromUri, String toUri, Map<String, String> metadata) {
        try {
            logger.debug("Writting redirect {} -> {}", fromUri, toUri);
            injector.addRedirect(fromUri, toUri, metadata);
            injectedRedirects.incrementAndGet();
        } catch (InjectorInjectionException e) {
            logger.warn("Exception while inserting redirect from {} to {} with metadata {}",
                fromUri, toUri, metadata, e);
        }
    }

    @Override
    public void reportError(ApiStatus.ErrorType type, ApiStatus.ErrorBehavior behavior, String message, Exception e) {
        errors.add(type);
        if (behavior == ApiStatus.ErrorBehavior.STOP || behavior == ApiStatus.ErrorBehavior.STOP_AFTER_RETRY) {
            state = State.FAILED;
        }
        logger.warn("Error while fetching from API {}: {}/{}: {}", apiFetcherConfig, type,
            behavior, message, e);
    }

    ApiStatus getApiStatus() {
        return new ApiStatus(EnumMultiset.create(errors), injectedUrls.longValue(),
            writtenDocuments.longValue(), injectedRedirects.longValue());
    }

    @Override
    public void execute(TaskExecutionContext context) {
        fetcher.addRequests(apiFetcherConfig.getRequests(), this);
        state = State.RUNNING;
        logger.debug("Running ApiFetcherJob for {}", apiFetcherConfig);
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof ApiFetcherJob) {
            ApiFetcherJob impl = (ApiFetcherJob) other;
            return Objects.equal(crawl, impl.crawl)
                    && Objects.equal(apiFetcherConfig, impl.apiFetcherConfig);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(crawl, apiFetcherConfig);
    }

    @Override
    public TaskStatus getStatus() {
        switch (state) {
        case STARTING:
            return ApiFetcherStatus.starting(crawlKey, apiFetcherConfig);
        case RUNNING:
            return ApiFetcherStatus.running(crawlKey, apiFetcherConfig, getApiStatus());
        case STOPPING:
        case KILLING:
            return ApiFetcherStatus.stopping(crawlKey, apiFetcherConfig, getApiStatus());
        case STOPPED:
        case KILLED:
            return ApiFetcherStatus.stopped(crawlKey, apiFetcherConfig, getApiStatus());
        case FAILED:
            return ApiFetcherStatus.failed(crawlKey, apiFetcherConfig, getApiStatus());
        case FINISHED:
            return ApiFetcherStatus.finished(crawlKey, apiFetcherConfig, getApiStatus());
        default:
            throw new IllegalStateException(state.name());
        }
    }

    @Override
    public void stop() {
        state = State.STOPPING;
        fetcher.stop(this);
        state = State.STOPPED;
    }

    @Override
    public void kill() {
        state = State.KILLING;
        fetcher.stop(this);
        state = State.KILLED;
    }

    @Override
    public double getProgress() {
        return -1;
    }

    public long getCrawlId() {
        return crawl.getId();
    }
}
