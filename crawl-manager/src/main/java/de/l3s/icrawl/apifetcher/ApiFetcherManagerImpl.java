package de.l3s.icrawl.apifetcher;

import java.io.Closeable;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;

import de.l3s.icrawl.api.ApiFetcher;
import de.l3s.icrawl.crawlmanager.TaskExecution;
import de.l3s.icrawl.domain.api.ApiFetcherException;
import de.l3s.icrawl.domain.crawl.ApiFetcherExecutionRecord;
import de.l3s.icrawl.domain.specification.Crawl;
import de.l3s.icrawl.nutch.Injector;
import de.l3s.icrawl.nutch.InjectorSetupException;

// TODO fold into other class and remove interface
public class ApiFetcherManagerImpl implements ApiFetcherManager, Closeable {

    private static final Logger logger = LoggerFactory.getLogger(ApiFetcherManagerImpl.class);
    private final ImmutableMap<String, ApiFetcher> fetchersByKey;
    private final InjectorFactory injectorFactory;

    public ApiFetcherManagerImpl(Map<String, ApiFetcher> fetchers, InjectorFactory injectorFactory) {
        this.fetchersByKey = ImmutableMap.copyOf(fetchers);
        this.injectorFactory = injectorFactory;
    }

    @Override
    public void close() {
        logger.debug("Starting to shut down fetchers");
        for (Entry<String, ApiFetcher> fetcher : fetchersByKey.entrySet()) {
            logger.debug("Shutting down fetcher for API {}", fetcher.getKey());
            try {
                fetcher.getValue().close();
            } catch (IOException e) {
                logger.warn("Exception while shutting down fetcher for API {}", fetcher.getKey(), e);
            }
        }
        logger.debug("Finished ApiFetcherManager shutdown");
    }

    @Override
    public TaskExecution createJob(ApiFetcherExecutionRecord execution) {
        String api = execution.getConfiguration().getApi();
        ApiFetcher fetcher = fetchersByKey.get(api);
        if (fetcher == null) {
            throw new IllegalArgumentException("Unknown API key " + api);
        }
        try {
            String crawlKey = Crawl.crawlKey(execution.getCampaign());
            Injector injector = injectorFactory.getInstance(crawlKey);
            logger.debug("Starting new job using API {} for crawl {}", api, crawlKey);
            return new ApiFetcherJob(fetcher, injector, execution.getCrawl(), execution.getConfiguration());
        } catch (InjectorSetupException e) {
            throw new ApiFetcherException("Could not connect to data store", e);
        }
    }

}
