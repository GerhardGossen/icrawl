package de.l3s.icrawl.apifetcher;

import de.l3s.icrawl.crawlmanager.TaskExecution;
import de.l3s.icrawl.domain.crawl.ApiFetcherExecutionRecord;

public interface ApiFetcherManager {
    public TaskExecution createJob(ApiFetcherExecutionRecord execution);
}
