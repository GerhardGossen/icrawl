package de.l3s.icrawl.crawlmanager.step;

import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.parse.ParserJob;

import de.l3s.icrawl.domain.crawl.CrawlExecutionRecord;

public class ParseStep extends Step<ParserJob> {

    public ParseStep() {
        super("parse", StepType.PARSE);
    }

    @Override
    ParserJob start(CrawlExecutionRecord crawl, Configuration conf) {
        return new ParserJob(conf);
    }
    @Override
    boolean run() throws StepFailedException {
        try {
            // TODO maybe set Nutch.ALL_BATCH_ID_STR to fix unparsed rows
            job.parse(getBatchKey(), true, false);
            return true;
        } catch (Exception e) {
            throw new StepFailedException("Failed during parse step", e, crawl.getCrawl());
        }
    }
}