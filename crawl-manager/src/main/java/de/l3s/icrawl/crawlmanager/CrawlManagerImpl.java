package de.l3s.icrawl.crawlmanager;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.crawl.GeneratorJob;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Throwables;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import de.l3s.icrawl.apifetcher.ApiFetcherJob;
import de.l3s.icrawl.apifetcher.ApiFetcherManager;
import de.l3s.icrawl.domain.crawl.ApiFetcherExecutionRecord;
import de.l3s.icrawl.domain.crawl.CrawlExecutionRecord;
import de.l3s.icrawl.domain.crawl.CrawlStatistics;
import de.l3s.icrawl.domain.crawl.TaskExecutionRecord;
import de.l3s.icrawl.domain.crawl.WarcExporterRecord;
import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.domain.specification.Crawl;
import de.l3s.icrawl.domain.status.CompositeCrawlStatus;
import de.l3s.icrawl.domain.status.CrawlStatus;
import de.l3s.icrawl.domain.status.ExportStatus;
import de.l3s.icrawl.domain.status.RunningCrawlStatus;
import de.l3s.icrawl.domain.status.TaskStatus;
import de.l3s.icrawl.export.warc.WarcExporterJob;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate;
import de.l3s.icrawl.service.CampaignService;

@Service
@ManagedResource
public class CrawlManagerImpl implements CrawlManager, Closeable {

    final class JobFinishedQueueUpdater implements Runnable {
        private final TaskExecution task;
        private final long campaignId;

        private JobFinishedQueueUpdater(long campaignId, TaskExecution task) {
            this.task = task;
            this.campaignId = campaignId;
        }

        @Override
        public void run() {
            jobs.remove(campaignId, task);
            logger.debug("Removing crawl {} from running job list", campaignId);
        }
    }

    private static final Logger logger = LoggerFactory.getLogger(CrawlManagerImpl.class);
    private final LoadingCache<Long, CrawlStatistics> statisticsCache = CacheBuilder.newBuilder()
        .expireAfterWrite(5, TimeUnit.MINUTES)
        .build(new CacheLoader<Long, CrawlStatistics>() {
            @Override
            public CrawlStatistics load(Long key) throws Exception {
                Campaign campaign = repository.getCampaign(key);
                return CrawlStatisticsJob.run(template, campaign);
            }
        });

    private final CampaignService repository;
    private final TaskExecutionService executionsRepository;
    private final Configuration conf;
    protected final Multimap<Long, TaskExecution> jobs = ArrayListMultimap.create();
    protected final ListeningExecutorService executor;
    protected final CrawlDataRepositoryTemplate template;
    protected final ApiFetcherManager apiFetcherManager;
    private final long defaultbatchSize;
    @Value("${disable.crawl:false}")
    boolean disableCrawl = false;


    @Inject
    public CrawlManagerImpl(CampaignService metadataRepository,
            TaskExecutionService executionsRepository,
            CrawlDataRepositoryTemplate template, Configuration conf, ExecutorService executor,
            ApiFetcherManager apiFetcherManager) {
        this.repository = metadataRepository;
        this.executionsRepository = executionsRepository;
        this.template = template;
        this.conf = conf;
        this.apiFetcherManager = apiFetcherManager;
        this.executor = MoreExecutors.listeningDecorator(executor);
        this.defaultbatchSize = conf.getLong(GeneratorJob.GENERATOR_TOP_N, 1000);
    }

    @Override
    public void startCrawl(long crawlID) {
        Crawl crawl = repository.getCrawl(crawlID);
        crawl.getSpecification().setStartTime(DateTime.now());
        CrawlExecutionRecord crawlExecution = executionsRepository.scheduleNextCrawlExecution(
            crawl, null);
        logger.debug("Scheduled crawl job {}", crawlExecution);
    }


    @Override
    public void deleteCrawl(long crawlId) {
        Crawl crawl = repository.getCrawl(crawlId);
        for (TaskExecutionRecord<?> execution : executionsRepository.getExecutionsForCrawl(crawl)) {
            if (execution instanceof CrawlExecutionRecord) {
                stopCrawl(execution.getCrawl().getId());
                executionsRepository.deleteExecution(execution);
            }
        }
        repository.deleteCrawl(crawlId);
    }

    @Override
    public CrawlStatistics getCrawlStatistics(long campaignId, boolean includeDetails) {
        try {
            CrawlStatistics statistics = statisticsCache.get(campaignId);
            return includeDetails ? statistics : statistics.summarize();
        } catch (ExecutionException e) {
            logger.info("Crawl statistics failed with exception {}", e);
            throw Throwables.propagate(e);
        }
    }

    @Override
    public String getStatus() {
        return "Running " + getNumJobs() + " jobs";
    }

    @Override
    public void stopJobsForCampaign(long campaignId) {
        int stoppedJobs = 0;
        for (TaskExecution task : jobs.removeAll(campaignId)) {
            task.stop();
            stoppedJobs++;
        }
        logger.info("Stopped {} jobs for campaign {}", stoppedJobs, campaignId);
    }

    @Override
    public CrawlStatus getCrawlStatus(long crawlId) {
        Collection<TaskExecution> tasksForCrawl = new ArrayList<>();
        if (tasksForCrawl.isEmpty()) {
            Crawl crawl = repository.getCrawl(crawlId);
            return CrawlStatus.unknown(crawl.getCampaign(), crawl,
                "No jobs scheduled for this crawl");
        } else {
            return aggregateCrawlStatus(tasksForCrawl);
        }
    }

    public CrawlStatus aggregateCrawlStatus(Collection<? extends TaskExecution> tasks) {
        if (tasks.size() == 1 && Iterables.get(tasks, 0) instanceof CrawlJob) {
            return ((CrawlJob) Iterables.get(tasks, 0)).getStatus();
        } else {
            Collection<TaskStatus> jobStatuses = Lists.newArrayListWithExpectedSize(tasks.size());
            for (TaskExecution job : tasks) {
                jobStatuses.add(job.getStatus());
            }
            return new CompositeCrawlStatus(jobStatuses);
        }
    }

    @Override
    public CrawlStatus stopCrawl(long crawlId) {
        Collection<TaskExecution> tasksToStop = findAndRemoveTasksForCrawl(crawlId);
        if (tasksToStop.isEmpty()) {
            throw new IllegalStateException("Crawl " + crawlId + " is not running");
        }
        for (TaskExecution taskToStop : tasksToStop) {
            taskToStop.stop();
        }
        return aggregateCrawlStatus(tasksToStop);
    }

    @Override
    public CrawlStatus killCrawl(long crawlId) {
        Collection<TaskExecution> tasksToKill = findAndRemoveTasksForCrawl(crawlId);
        if (tasksToKill.isEmpty()) {
            throw new IllegalStateException("Crawl " + crawlId + " is not running");
        }
        for (TaskExecution taskToKill : tasksToKill) {
            taskToKill.kill();
        }
        return aggregateCrawlStatus(tasksToKill);
    }

    @Override
    @ManagedAttribute
    public List<RunningCrawlStatus> getRunningCrawls() {
        List<RunningCrawlStatus> ret = Lists.newArrayListWithExpectedSize(jobs.size());
        for (TaskExecution entry : jobs.values()) {
            TaskStatus jobStatus = entry.getStatus();
            if (jobStatus instanceof RunningCrawlStatus) {
                RunningCrawlStatus status = (RunningCrawlStatus) jobStatus;
                ret.add(status);
            } else {
                logger.debug("Unknown job status: {}", jobStatus);
            }
        }
        return ret;
    }

    private WarcExporterJob getExportJob(long campaign) {
        for (TaskExecution task : jobs.get(campaign)) {
            if (task instanceof WarcExporterJob) {
                return (WarcExporterJob) task;
            }
        }
        return null;
    }

    @Override
    public void exportCampaign(long campaignId, String path) {
        synchronized (jobs) {
            if (getExportJob(campaignId) != null) {
                logger.info("There is already an export job running for campaign {}", campaignId);
                return;
            }
            Campaign campaign = repository.getCampaign(campaignId);
            WarcExporterRecord taskRecord = new WarcExporterRecord(null, campaign,
                DateTime.now(), null, "user", path);
            executionsRepository.scheduleTaskExecution(taskRecord, DateTime.now());
        }
    }

    @Override
    public ExportStatus getExportStatus(long crawlId) {
        WarcExporterJob exportJob = getExportJob(crawlId);
        if (exportJob != null) {
            return exportJob.getStatus();
        } else {
            throw new IllegalStateException("No export job running for crawl " + crawlId);
        }
    }

    @Override
    public ExportStatus stopExport(long crawlId, boolean removeFiles) {
        WarcExporterJob job = getExportJob(crawlId);
        if (job != null) {
            job.stop();
            return job.getStatus();
        } else {
            throw new IllegalStateException("No export job running for crawl " + crawlId);
        }
    }

    @Override
    @PreDestroy
    public void close() throws IOException {
        for (TaskExecution task : jobs.values()) {
            task.kill();
        }
    }

    @ManagedAttribute
    @Override
    public long getNumJobs() {
        return jobs.size();
    }

    @Scheduled(fixedRate = 30 * 1000)
    @Transactional(isolation = Isolation.SERIALIZABLE)
    @Override
    public void findStartableJobs() {
        List<TaskExecutionRecord<?>> startableJobs = executionsRepository.getAllStartableJobs();
        logger.debug("Checked for startable tasks, {} found: {}", startableJobs.size(), startableJobs);
        for (TaskExecutionRecord<?> taskExecution : startableJobs) {
            startTask(taskExecution);
        }
    }

    void startTask(final TaskExecutionRecord<?> execution) {
        if (disableCrawl) {
            logger.info(
                "Skipping task execution because of InMemoryJobQueue.disableCrawl, task is: {}",
                execution);
            return;
        }
        synchronized (jobs) {
            if (taskAlreadyRunning(execution)) {
                logger.debug("Already running a task for {}, trying again later", execution);
                return;
            }
            if (execution.getEndTime() != null) {
                logger.debug("Starting task {} has already finished, skipping", execution.getId());
                return;
            }
            final TaskExecutionRecord<?> startedExecution = executionsRepository.startExecution(execution);
            final TaskExecution task = getTaskExecution(startedExecution);
            final TaskExecutionContext context = new TaskExecutionContext(executionsRepository,
                startedExecution);
            long campaignId = execution.getCampaign().getId();
            int expectedJobs = jobs.size() + 1;
            jobs.put(campaignId, task);
            if (jobs.size() != expectedJobs) {
                logger.warn("Failed to add taskExecution {} to jobs ({})", task, jobs);
            }
            logger.debug("Starting task execution {}", startedExecution);
            ListenableFuture<?> future = executor.submit(new Runnable() {
                @Override
                public void run() {
                    logger.debug("Executing task {} in thread pool", task);
                    task.execute(context);
                    executionsRepository.finishTaskExecution(startedExecution);
                }
            });
            future.addListener(new JobFinishedQueueUpdater(campaignId, task), executor);
        }
    }

    private TaskExecution getTaskExecution(TaskExecutionRecord<?> taskExecution) {
        logger.debug("Creating task execution for {}", taskExecution);
        if (taskExecution instanceof CrawlExecutionRecord) {
            return new CrawlJob((CrawlExecutionRecord) taskExecution,
                new ContinuousCrawlCondition(), conf, template, defaultbatchSize,
                executionsRepository);

        } else if (taskExecution instanceof ApiFetcherExecutionRecord) {
            return apiFetcherManager.createJob((ApiFetcherExecutionRecord) taskExecution);
        } else if (taskExecution instanceof WarcExporterRecord) {
            return new WarcExporterJob(template, (WarcExporterRecord) taskExecution);
        }
        throw new UnsupportedOperationException("Unknown type of scheduled job: " +taskExecution);
    }

    private boolean taskAlreadyRunning(TaskExecutionRecord<?> taskExecution) {
        Collection<TaskExecution> tasksForCampaign = jobs.get(taskExecution.getCampaign().getId());
        for (TaskExecution campaignTask : tasksForCampaign) {
            if (tasksConflict(campaignTask, taskExecution)) {
                return true;
            }
        }
        return false;
    }

    boolean tasksConflict(TaskExecution campaignTask, TaskExecutionRecord<?> startingTask) {
        if (campaignTask instanceof CrawlJob && startingTask instanceof CrawlExecutionRecord) {
            return ((CrawlJob) campaignTask).getCrawl() == ((CrawlExecutionRecord) startingTask).getCrawl();
        }
        return false;
    }

    Collection<CrawlJob> findTasksForCrawl(long crawlId) {
        Collection<CrawlJob> tasks = new ArrayList<>();
        for (TaskExecution task : jobs.values()) {
            if (task instanceof CrawlJob) {
                CrawlJob crawlTask = (CrawlJob) task;
                if (crawlTask.getCrawl().getId() == crawlId) {
                    tasks.add(crawlTask);
                }
            }
        }
        return tasks;
    }

    Collection<TaskExecution> findAndRemoveTasksForCrawl(long crawlId) {
        Collection<TaskExecution> tasks = new ArrayList<>();
        Iterator<TaskExecution> taskIter = jobs.values().iterator();
        while (taskIter.hasNext()) {
            TaskExecution task = taskIter.next();
            if (task instanceof CrawlJob) {
                CrawlJob crawlTask = (CrawlJob) task;
                if (crawlTask.getCrawl().getId() == crawlId) {
                    tasks.add(crawlTask);
                    taskIter.remove();
                }
            } else if (task instanceof ApiFetcherJob) {
                ApiFetcherJob apiFetch = (ApiFetcherJob) task;
                if (apiFetch.getCrawlId() == crawlId) {
                    tasks.add(apiFetch);
                    taskIter.remove();
                }
            } else {
                logger.info("Unknown task type while finding tasks to remove: {}", task);
            }
        }
        return tasks;
    }

    @Override
    public void updateCrawl(long crawl) {
        throw new UnsupportedOperationException("updateCrawl");
    }

}
