package de.l3s.icrawl.crawlmanager.step;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import org.apache.avro.util.Utf8;
import org.apache.gora.mapreduce.GoraMapper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.NullWritable;
import org.apache.nutch.crawl.GeneratorJob;
import org.apache.nutch.storage.Mark;
import org.apache.nutch.storage.StorageUtils;
import org.apache.nutch.storage.WebPage;
import org.apache.nutch.storage.WebPage.Field;
import org.apache.nutch.util.NutchJob;
import org.apache.nutch.util.NutchTool;
import org.apache.nutch.util.ToolUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableSet;

public class ClearMarksJob extends NutchTool {
    private static final Logger logger = LoggerFactory.getLogger(ClearMarksJob.class);

    private static class ClearMarksMapper extends
            GoraMapper<String, WebPage, NullWritable, NullWritable> {
        public enum Operation {
            CLEARED, PROCESSED
        }

        private Utf8 batchId;

        @Override
        protected void setup(Context context)
                throws IOException, InterruptedException {
            String sBatchId = context.getConfiguration().get(GeneratorJob.BATCH_ID);
            if (sBatchId == null) {
                throw new IllegalArgumentException("BatchId parameter was not set");
            }
            batchId = new Utf8(sBatchId);
        }

        @Override
        protected void map(String key, WebPage page, Context context) throws IOException,
                InterruptedException {
            Utf8 generateMark = Mark.GENERATE_MARK.checkMark(page);
            if (generateMark != null && Mark.FETCH_MARK.checkMark(page) == null
                    && generateMark.equals(batchId)) {
                Mark.GENERATE_MARK.removeMark(page);
                incrementCounter(context, Operation.CLEARED);
            }
            incrementCounter(context, Operation.PROCESSED);
        }

        private void incrementCounter(Context context, Enum<?> counter) {
            context.getCounter(counter).increment(1);
        }
    }

    public ClearMarksJob(Configuration conf) {
        setConf(conf);
    }

    @Override
    public Map<String, Object> run(Map<String, Object> args) throws Exception {
        String batchId = (String) args.get(GeneratorJob.BATCH_ID);
        clearGenerateMarks(batchId);
        ToolUtil.recordJobStatus(null, currentJob, results);
        return results;
    }

    public long clearGenerateMarks(String batchId) throws IOException {
        getConf().set(GeneratorJob.BATCH_ID, batchId);
        currentJob = new NutchJob(getConf(), "clearMarks: " + batchId);
        Collection<Field> fields = ImmutableSet.of(WebPage.Field.MARKERS);
        try {
            StorageUtils.initMapperJob(currentJob, fields, NullWritable.class, NullWritable.class,
                ClearMarksMapper.class);
            currentJob.waitForCompletion(true);
        } catch (InterruptedException e) {
            logger.warn("ClearMarksJob was interrupted, may not have finished");
        } catch (ClassNotFoundException e) {
            throw Throwables.propagate(e);
        }
        return currentJob.getCounters().findCounter(ClearMarksMapper.Operation.CLEARED).getValue();

    }
}
