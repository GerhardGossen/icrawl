package de.l3s.icrawl.crawlmanager;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.l3s.icrawl.domain.crawl.TaskExecutionRecord;
import de.l3s.icrawl.domain.specification.Crawl;
import de.l3s.icrawl.service.CampaignService;

@Service
public class CrawlUpdatesFinder {
    private static final Logger logger = LoggerFactory.getLogger(CrawlUpdatesFinder.class);

    @Inject
    CampaignService repository;

    @Inject
    TaskExecutionService executionsRepository;

    @Scheduled(fixedDelay = 30 * 1000, initialDelay = 180 * 1000)
    @Transactional
    public void loadCrawlUpdates() {
        logger.debug("Checking for updated crawls");
        Collection<Crawl> updatedCrawls = repository.getUnscheduledCrawls();
        for (Crawl crawl : updatedCrawls) {
            List<TaskExecutionRecord<?>> executions = executionsRepository.getExecutionsForCrawl(crawl);
            if (executions.isEmpty()) {
                executionsRepository.scheduleNextCrawlExecution(crawl, null);
            }
            repository.markAsScheduled(crawl);
        }
    }

}
