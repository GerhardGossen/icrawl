package de.l3s.icrawl.crawlmanager;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import de.l3s.icrawl.process.CrawlManagerManager;

@Service("cmCrawlManagerManager")
@Profile("crawl-manager")
public class CrawlManagerManagerImpl implements CrawlManagerManager {
    private static final Logger logger = LoggerFactory.getLogger(CrawlManagerManagerImpl.class);
    private final ExecutorService shutdownExecutor = Executors.newSingleThreadExecutor(new ThreadFactoryBuilder().setDaemon(true).build());
    @Inject
    CrawlManager crawlManager;

    @Inject
    ConfigurableApplicationContext context;

    private boolean isRunning = true;

    @Override
    public void start() throws IOException {
        isRunning = true;
    }

    @Override
    public void stop() {
        logger.info("got request to stop, calling System.exit()");
        shutdownExecutor.execute(new Runnable() {

                @Override
                public void run() {
                    context.stop();
                    System.exit(0);
                }
            });
    }

    @Override
    public boolean isRunning() {
        return isRunning;
    }

    @Override
    public String getStatus() {
        return crawlManager.getStatus();
    }

}
