package de.l3s.icrawl.crawlmanager;


public interface StoppingCondition {
    /**
     * Determnines whether a new crawl batch should be started.
     *
     * This method is called after each crawl batch is finished.
     *
     * @return true, iff the crawl should continue
     */
    boolean continueCrawling();
}
