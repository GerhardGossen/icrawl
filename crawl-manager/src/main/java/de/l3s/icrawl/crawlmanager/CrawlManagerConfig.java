package de.l3s.icrawl.crawlmanager;

import java.io.File;
import java.io.IOException;
import java.lang.Thread.UncaughtExceptionHandler;
import java.net.MalformedURLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jms.listener.SimpleMessageListenerContainer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.util.ErrorHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.google.common.collect.ImmutableMap;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

import de.l3s.icrawl.DbConfig;
import de.l3s.icrawl.api.ApiFetcher;
import de.l3s.icrawl.api.OAuthCredentialsProvider;
import de.l3s.icrawl.api.OAuthCreditialsProviderImpl;
import de.l3s.icrawl.apifetcher.ApiFetcherManager;
import de.l3s.icrawl.apifetcher.ApiFetcherManagerImpl;
import de.l3s.icrawl.apifetcher.InjectorFactory;
import de.l3s.icrawl.apifetcher.TwitterApiFetcher;
import de.l3s.icrawl.process.CrawlManagerManager;
import de.l3s.icrawl.process.JmsConfig;
import de.l3s.icrawl.process.MetadataDbManager;
import de.l3s.icrawl.repository.BatchRecordRepository;
import de.l3s.icrawl.repository.CampaignRepository;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate;
import de.l3s.icrawl.repository.CrawlRepository;
import de.l3s.icrawl.repository.OAuthAccessTokenRepository;
import de.l3s.icrawl.repository.StepExecutionRecordRepository;
import de.l3s.icrawl.repository.TaskExecutionRepository;
import de.l3s.icrawl.service.CampaignService;
import de.l3s.icrawl.service.CrawlServiceImpl;
import de.l3s.icrawl.service.EvaluationService;
import de.l3s.icrawl.service.OAuthAccessTokenService;
import de.l3s.icrawl.service.TaskExecutionServiceImpl;
import de.l3s.icrawl.util.HadoopUtils;

import static de.l3s.icrawl.process.JmsConfig.CRAWL_MANAGER_MANAGER_QUEUE;
import static de.l3s.icrawl.process.JmsConfig.CRAWL_MANAGER_QUEUE;

@Configuration
@EnableAutoConfiguration
@EnableScheduling
@Import({ DbConfig.class, JmsConfig.class })
@PropertySource({ "application.properties" })
@ComponentScan(basePackageClasses = { CrawlManagerConfig.class, OAuthAccessTokenRepository.class }, excludeFilters = @Filter(type = FilterType.ASSIGNABLE_TYPE, value = EvaluationService.class))
public class CrawlManagerConfig implements SchedulingConfigurer {
    private final class MockMetadataDbManager implements MetadataDbManager {
        @Override
        public void stop() {
            // nothing to do
        }

        @Override
        public void start() throws IOException {
            // nothing to do
        }

        @Override
        public boolean isRunning() {
            return true;
        }

        @Override
        public String getStatus() {
            return "Mock metadata DB manager";
        }
    }

    private static final Logger logger = LoggerFactory.getLogger(CrawlManagerConfig.class);

    @Value("${icrawl.directory}")
    File appDirectory;

    @Value("${icrawl.jms.port:61613}")
    int jmsPort = 61613;

    @Inject
    JmsConfig jmsConfig;

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        ThreadFactory tf = new ThreadFactoryBuilder().setNameFormat("crawl-scheduler-%d").build();
        taskRegistrar.setScheduler(Executors.newSingleThreadScheduledExecutor(tf));
    }

    @Bean
    org.apache.hadoop.conf.Configuration conf() throws MalformedURLException {
        org.apache.hadoop.conf.Configuration conf = HadoopUtils.createConf(appDirectory);
        logger.info("Nutch/Hadoop config: {}", conf);
        logger.info("Using {} store for Nutch", conf.get("storage.data.store.class"));
        logger.info("Value for 'io.serializations', should be non-null: {}",
            conf.get("io.serializations"));

        return conf;
    }

    @Bean
    ExecutorService executorService() {
        ThreadFactory tf = new ThreadFactoryBuilder().setNameFormat("crawl-manager-%d")
            .setUncaughtExceptionHandler(new UncaughtExceptionHandler() {

                @Override
                public void uncaughtException(Thread t, Throwable e) {
                    logger.warn("Got uncaught exception in thread {}: {}", t, e);
                }
            })
            .build();
        return Executors.newCachedThreadPool(tf);
    }

    @Bean
    CampaignService campaignService(CampaignRepository campaignRepository,
            CrawlRepository crawlRepository) {
        return new CrawlServiceImpl(campaignRepository, crawlRepository);
    }

    @Bean
    TaskExecutionService executionRepository(CampaignService crawlService,
            TaskExecutionRepository taskExecutionRepository, BatchRecordRepository batchRepository,
            StepExecutionRecordRepository stepRepository) {
        return new TaskExecutionServiceImpl(crawlService, taskExecutionRepository, batchRepository,
            stepRepository);
    }

    @Bean
    CrawlDataRepositoryTemplate crawlDataRepositoryTemplate() throws MalformedURLException {
        return new CrawlDataRepositoryTemplate(conf());
    }

    @Bean
    ApiFetcherManager apiFetcherManager(InjectorFactory injectorFactory) throws IOException {
        ImmutableMap<String, ApiFetcher> fetchers = ImmutableMap.of("twitter", twitterApiFetcher());
        return new ApiFetcherManagerImpl(fetchers, injectorFactory);
    }

    @Bean
    InjectorFactory injectorFactory(org.apache.hadoop.conf.Configuration conf) {
        return new InjectorFactory(conf);
    }

    @Bean
    ApiFetcher twitterApiFetcher() throws IOException {
        OAuthCredentialsProvider credentialsProvider = new OAuthCreditialsProviderImpl(
            tokenSerivce(), "twitter");
        TwitterApiFetcher fetcher = new TwitterApiFetcher(credentialsProvider);
        fetcher.setConfiguration(conf());
        return fetcher;
    }

    @Bean
    ObjectMapper jsonFactory() {
        return new ObjectMapper().registerModules(new JodaModule(), new GuavaModule(), new Hibernate4Module());
    }

    @Bean
    @Profile("crawl-manager")
    MetadataDbManager mockMetadataDbManager() {
        return new MockMetadataDbManager();
    }

    @Bean
    SimpleMessageListenerContainer managerListenerContainer(CrawlManager crawlManager) {
        return jmsConfig.createServiceExporter(CrawlManager.class, crawlManager, CRAWL_MANAGER_QUEUE);
    }

    @Bean
    SimpleMessageListenerContainer managerManagerListenerContainer(
            CrawlManagerManager managerManager, ErrorHandler jmsErrorHandler) {
        SimpleMessageListenerContainer serviceExporter = jmsConfig.createServiceExporter(
            CrawlManagerManager.class, managerManager, CRAWL_MANAGER_MANAGER_QUEUE);
        serviceExporter.setErrorHandler(jmsErrorHandler);
        return serviceExporter;
    }

    @Bean
    ErrorHandler jmsErrorHandler(ConfigurableApplicationContext context) {
        return new JmsErrorHandler(context);
    }

    @Bean
    OAuthAccessTokenService tokenSerivce() {
        return new OAuthAccessTokenService();
    }

}
