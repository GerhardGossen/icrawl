package de.l3s.icrawl.crawlmanager.step;

import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.fetcher.FetcherJob;
import org.apache.nutch.metadata.Nutch;

import de.l3s.icrawl.domain.crawl.CrawlExecutionRecord;

public class FetchStep extends Step<FetcherJob> {
    public FetchStep() {
        super("fetch", StepType.FETCH);
    }

    @Override
    FetcherJob start(CrawlExecutionRecord crawl, Configuration conf) {
        return new FetcherJob(conf);
    }

    @Override
    boolean run() throws StepFailedException {
        try {
            job.fetch(Nutch.ALL_BATCH_ID_STR, -1, true, -1);
            return true;
        } catch (Exception e) {
            throw new StepFailedException("Exception during fetch", e, crawl.getCrawl());
        }
    }
}