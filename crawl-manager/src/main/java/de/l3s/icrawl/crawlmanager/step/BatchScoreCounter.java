package de.l3s.icrawl.crawlmanager.step;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import org.apache.avro.util.Utf8;
import org.apache.gora.filter.FilterOp;
import org.apache.gora.filter.MapFieldValueFilter;
import org.apache.gora.mapreduce.GoraMapper;
import org.apache.gora.query.Query;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.nutch.crawl.GeneratorJob;
import org.apache.nutch.metadata.Nutch;
import org.apache.nutch.storage.Mark;
import org.apache.nutch.storage.WebPage;
import org.apache.nutch.storage.WebPage.Field;
import org.apache.nutch.util.NutchTool;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import de.l3s.icrawl.crawlmanager.TaskExecutionService;
import de.l3s.icrawl.domain.crawl.TaskExecutionRecord;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate.QuerySetup;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate.ResultLinesMapper;

import static org.apache.nutch.storage.WebPage.Field.MARKERS;
import static org.apache.nutch.storage.WebPage.Field.SCORE;

public class BatchScoreCounter extends NutchTool {
    enum Statistics {
        COUNTED, SKIPPED, ERRORS
    }

    private final class BatchScoreCounterResultMapper implements
            ResultLinesMapper<Text, DoubleWritable, Double> {
        private double sum = 0;
        private boolean gotInput = false;

        @Override
        public void processLine(Text key, DoubleWritable value) {
            gotInput = true;
            sum += value.get();
        }

        @Override
        public Double getResult() {
            return gotInput ? sum : null;
        }
    }


    public static class BatchScoreCounterReducer extends
            Reducer<Text, DoubleWritable, Text, DoubleWritable> {
        private final DoubleWritable out = new DoubleWritable();
        @Override
        protected void reduce(Text key, Iterable<DoubleWritable> values, Context context)
                throws IOException, InterruptedException {
            double sum = 0.0;
            long numValues = 0;
            long numErrors = 0;
            for (DoubleWritable val : values) {
                double value = val.get();
                if (Double.isFinite(value)) {
                    sum += value;
                    numValues++;
                } else {
                    numErrors++;
                }
            }
            context.getCounter(Statistics.COUNTED).increment(numValues);
            context.getCounter(Statistics.ERRORS).increment(numErrors);
            out.set(sum);
            context.write(key, out);
        }
    }


    public static class BatchScoreCounterMapper extends
            GoraMapper<String, WebPage, Text, DoubleWritable> {
        private final Text outKey = new Text("score");
        private final DoubleWritable out = new DoubleWritable();
        private String batchId;
        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            batchId = context.getConfiguration().get(GeneratorJob.BATCH_ID);
            Preconditions.checkArgument(batchId != null, "batchId is not set in configuration");
        }

        @Override
        protected void map(String key, WebPage page, Context context) throws IOException,
                InterruptedException {
            Utf8 generateMark = Mark.GENERATE_MARK.checkMark(page);
            if (generateMark != null && batchId.equals(generateMark.toString())) {
                context.getCounter(Statistics.COUNTED).increment(1);
                Float score = page.getScore();
                if (score != null) {
                    out.set(score);
                    context.write(outKey, out);
                }
            } else {
                context.getCounter(Statistics.SKIPPED).increment(1);
            }
        }

    }

    private final CrawlDataRepositoryTemplate template;
    private final TaskExecutionService executionsService;
    private final Configuration conf;

    public BatchScoreCounter(CrawlDataRepositoryTemplate template,
            TaskExecutionService executionsService, Configuration conf) {
        this.template = template;
        this.executionsService = executionsService;
        this.conf = conf;
    }


    @Override
    public Map<String, Object> run(Map<String, Object> args) throws Exception {
        long crawlId = Long.valueOf((String) args.get(Nutch.CRAWL_ID_KEY));
        final String batchKey = (String) args.get(GeneratorJob.BATCH_ID);
        TaskExecutionRecord<?> execution = executionsService.getLastExecutionForCrawl(crawlId);
        return ImmutableMap.of("RESULT", (Object) getScoreSum(execution, batchKey));
    }


    public Double getScoreSum(final TaskExecutionRecord<?> execution, final String batchKey)
            throws IOException {
        Set<Field> fields = ImmutableSet.of(SCORE, MARKERS);
        QuerySetup querySetup = new QuerySetup() {
            @Override
            public void setup(Query<String, WebPage> query) {
                MapFieldValueFilter<String, WebPage> filter = new MapFieldValueFilter<>();
                filter.setFieldName("markers");
                filter.setMapKey(new Utf8("_gnmrk_"));
                filter.setFilterOp(FilterOp.EQUALS);
                filter.getOperands().add(new Utf8(batchKey));
                filter.setFilterIfMissing(true);
                query.setFilter(filter);
            }
        };
        return template.jobBuilder(execution.getCampaign(),
            "BatchScoreCounter for batch " + batchKey)
            .setFields(fields)
            .setConf(conf)
            .setMapper(BatchScoreCounterMapper.class, Text.class, DoubleWritable.class)
            .setReducer(BatchScoreCounterReducer.class, Text.class, DoubleWritable.class)
            .setCombiner(BatchScoreCounterReducer.class)
            .setNumReducers(1)
            .setupQuery(querySetup)
            .executeAndGetResult(new BatchScoreCounterResultMapper());
    }
}
