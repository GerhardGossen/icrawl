package de.l3s.icrawl.crawlmanager.step;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.l3s.icrawl.domain.crawl.CrawlExecutionRecord;

// TODO check if killing a job during a batch still accumulates generated URLs
public class ClearGenerateMarksStep extends Step<ClearMarksJob> {
    private static final Logger logger = LoggerFactory.getLogger(ClearGenerateMarksStep.class);

    public ClearGenerateMarksStep() {
        super("clearGenerateMarks", StepType.GENERATE);
    }
    @Override
    ClearMarksJob start(CrawlExecutionRecord crawl, Configuration conf) {
        return new ClearMarksJob(conf);
    }

    @Override
    boolean run() throws StepFailedException {
        try {
            long cleared = job.clearGenerateMarks(getBatchKey());
            logger.info("Cleared {} old GENERATE marks", cleared);
            addMetadata("cleared", Long.toString(cleared));
            return true;
        } catch (IOException e) {
            throw new StepFailedException("Could not clear generate marks", e, crawl.getCrawl());
        }
    }
}