package de.l3s.icrawl.crawlmanager.step;

import de.l3s.icrawl.domain.specification.Crawl;

public class StepFailedException extends Exception {
    private static final long serialVersionUID = 1L;

    public StepFailedException(String message, Exception cause, Crawl crawl) {
        super(message + " for crawl " + crawl.getId(), cause);
    }
}
