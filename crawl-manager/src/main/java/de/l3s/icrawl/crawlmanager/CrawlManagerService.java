package de.l3s.icrawl.crawlmanager;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.google.common.base.Throwables;

import de.l3s.icrawl.process.CrawlManagerManager;

public class CrawlManagerService {
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(CrawlManagerConfig.class);
        app.setAdditionalProfiles("crawl-manager");
        app.setShowBanner(false);
        app.setWebEnvironment(false);
        ConfigurableApplicationContext context = app.run(args);
        context.registerShutdownHook();
        try {
            context.getBean("cmCrawlManagerManager", CrawlManagerManager.class).start();
        } catch (IOException e) {
            throw Throwables.propagate(e);
        }
    }
}
