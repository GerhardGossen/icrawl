package de.l3s.icrawl.crawlmanager.step;

import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.crawl.GeneratorJob;
import org.apache.nutch.crawl.GeneratorReducer;

import de.l3s.icrawl.domain.crawl.CrawlExecutionRecord;

public class GenerateStep extends Step<GeneratorJob> {
    /* GeneratorReducer has gobal variable that counts the number of generated URLs
     * and is never reset. This can create spurious stops of the crawl, when a generate
     * limit is set and the number of URLs generated so far exceeds that limit. */
    private static final class GeneratorReducerExtender extends GeneratorReducer {
        static void resetReducerCount() {
            GeneratorReducer.count = 0;
        }
    }
    private final long batchSize;
    private final boolean applyFilterPlugin;
    private final boolean applyNormPlugin;

    public GenerateStep(long batchSize, boolean applyFilterPlugin, boolean applyNormPlugin) {
        super("generate", StepType.GENERATE);
        this.batchSize = batchSize;
        this.applyFilterPlugin = applyFilterPlugin;
        this.applyNormPlugin = applyNormPlugin;
    }

    @Override
    GeneratorJob start(CrawlExecutionRecord crawl, Configuration conf) {
        GeneratorJob generatorJob = new GeneratorJob(conf);
        GeneratorReducerExtender.resetReducerCount();
        generatorJob.getConf().set(GeneratorJob.BATCH_ID, getBatchKey());
        return generatorJob;
    }

    @Override
    boolean run() throws StepFailedException {
        try {
            job.generate(batchSize, System.currentTimeMillis(), applyFilterPlugin, applyNormPlugin);
            return true;
        } catch (Exception e) {
            throw new StepFailedException("Failed during generate", e, crawl.getCrawl());
        }
    }
}