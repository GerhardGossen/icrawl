package de.l3s.icrawl.crawlmanager;

import java.io.IOException;

import javax.inject.Inject;
import javax.jms.JMSException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.util.ErrorHandler;

class JmsErrorHandler implements ErrorHandler {
    private static final Logger logger = LoggerFactory.getLogger(JmsErrorHandler.class);

    private final ConfigurableApplicationContext context;

    @Inject
    public JmsErrorHandler(ConfigurableApplicationContext context) {
        this.context = context;
    }

    @Override
    public void handleError(Throwable t) {
        if (t instanceof JMSException && t.getCause() != null
                && t.getCause() instanceof IOException) {
            logger.info("Caught IOException from message bus, shutting down CrawlManager", t);
            context.stop();
        } else {
            logger.debug("Unhandled exception in message bus communication: ", t);
        }
    }

}
