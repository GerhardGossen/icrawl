package de.l3s.icrawl.crawlmanager.step;

import java.io.IOException;
import java.util.EnumSet;
import java.util.Map;

import javax.inject.Inject;

import org.apache.gora.mapreduce.GoraMapper;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.nutch.crawl.GeneratorJob;
import org.apache.nutch.metadata.Nutch;
import org.apache.nutch.storage.WebPage;
import org.apache.nutch.util.NutchTool;

import com.google.common.collect.ImmutableMap;

import de.l3s.icrawl.FieldNames;
import de.l3s.icrawl.crawlmanager.TaskExecutionService;
import de.l3s.icrawl.domain.crawl.ScoreHistogram;
import de.l3s.icrawl.domain.crawl.TaskExecutionRecord;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate.ResultLinesMapper;

import static de.l3s.icrawl.domain.support.ByteBufferUtils.asDouble;
import static de.l3s.icrawl.domain.support.ByteBufferUtils.asMap;

public class ScoreHistogramJob extends NutchTool {

    private final static class OutputMapper implements
            ResultLinesMapper<Text, ScoreHistogram, Map<String, ScoreHistogram>> {
        private final ImmutableMap.Builder<String, ScoreHistogram> map = ImmutableMap.builder();

        @Override
        public void processLine(Text key, ScoreHistogram value) {
            map.put(key.toString(), value.copy());
        }

        @Override
        public Map<String, ScoreHistogram> getResult() {
            return map.build();
        }
    }

    public enum Status {
        PROCESSED
    }

    public static class ScoreHistogramMapper extends
            GoraMapper<String, WebPage, Text, ScoreHistogram> {
        private final static Text SCORE_KEY = new Text(FieldNames.SCORE_HISTOGRAM_KEY);
        private final static Text TIME_KEY = new Text(FieldNames.TIME_HISTOGRAM_KEY);
        private final ScoreHistogram histogram = new ScoreHistogram();

        @Override
        protected void map(String key, WebPage value, Context context) throws IOException,
                InterruptedException {
            histogram.clear();
            double timeRelevance = asDouble(value.getMetadata().get(FieldNames.TIME_RELEVANCE));
            histogram.add(timeRelevance);
            context.write(TIME_KEY, histogram);

            histogram.clear();
            Map<String, Double> scores = asMap(value.getMetadata().get(FieldNames.OUTLINK_SCORES),
                String.class, Double.class);
            histogram.addAll(scores.values());
            context.write(SCORE_KEY, histogram);

            context.getCounter(Status.PROCESSED).increment(1);
        }

    }

    public static class ScoreHistogramAggregator extends
            Reducer<Text, ScoreHistogram, Text, ScoreHistogram> {
        private final ScoreHistogram value = new ScoreHistogram();

        @Override
        protected void reduce(Text key, Iterable<ScoreHistogram> values, Context context)
                throws IOException, InterruptedException {
            value.clear();
            for (ScoreHistogram val : values) {
                value.add(val);
            }
            context.write(key, value);
        }
    }

    private final TaskExecutionService executionsService;
    private final CrawlDataRepositoryTemplate template;

    @Inject
    public ScoreHistogramJob(TaskExecutionService executionsService,
            CrawlDataRepositoryTemplate template) {
        this.executionsService = executionsService;
        this.template = template;
    }

    @Override
    public Map<String, Object> run(Map<String, Object> args) throws Exception {
        long crawlId = Long.valueOf((String) args.get(Nutch.CRAWL_ID_KEY));
        final String batchKey = (String) args.get(GeneratorJob.BATCH_ID);
        TaskExecutionRecord<?> execution = executionsService.getLastExecutionForCrawl(crawlId);
        return ImmutableMap.of("RESULT", (Object) getHistogram(execution, batchKey));
    }

    public Map<String, ScoreHistogram> getHistogram(TaskExecutionRecord<?> execution,
            String batchKey) throws IOException {
        return template.jobBuilder(execution.getCampaign(), "ScoreHistogram for batch " + batchKey)
            .setFields(EnumSet.of(WebPage.Field.METADATA, WebPage.Field.BATCH_ID))
            .setMapper(ScoreHistogramMapper.class, Text.class, ScoreHistogram.class)
            .setReducer(ScoreHistogramAggregator.class, Text.class, ScoreHistogram.class)
            .setCombiner(ScoreHistogramAggregator.class)
            .setNumReducers(1)
            .setupQuery(CrawlDataRepositoryTemplate.batchQuery(batchKey))
            .executeAndGetResult(new OutputMapper());
    }
}
