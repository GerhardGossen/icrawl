package de.l3s.icrawl.crawlmanager;

import java.time.Duration;

import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.crawl.AbstractFetchSchedule;
import org.apache.nutch.crawl.CrawlStatus;
import org.apache.nutch.storage.WebPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.l3s.icrawl.FieldNames;
import de.l3s.icrawl.domain.support.ByteBufferUtils;

public class DateStoringFetchSchedule extends AbstractFetchSchedule {
    private static final Logger logger = LoggerFactory.getLogger(DateStoringFetchSchedule.class);
    private float minScore = 0.0f;
    @Override
    public void setFetchSchedule(String url, WebPage page, long prevFetchTime,
            long prevModifiedTime, long fetchTime, long modifiedTime, int state) {
        super.setFetchSchedule(url, page, prevFetchTime, prevModifiedTime, fetchTime, modifiedTime,
            state);
        long now = System.currentTimeMillis();

        Integer fetchInterval = page.getFetchInterval();
        if (fetchInterval == null || fetchInterval <= 0) {
            fetchInterval = defaultInterval;
        }
        page.setFetchTime(now + fetchInterval * 1000L);
        logger.debug("Updated fetch time to now + {}s, {}", (page.getFetchTime() - now) / 1000,
            Duration.ofMillis(fetchInterval * 1000L));
        page.setPrevFetchTime(fetchTime);
        page.setModifiedTime(modifiedTime);
        page.setPrevModifiedTime(prevModifiedTime);

        page.getMetadata().put(FieldNames.FETCH_TIME_KEY, ByteBufferUtils.asByteBuffer(now));
    }

    @Override
    public void setConf(Configuration conf) {
        super.setConf(conf);
        if (conf == null) {
            return;
        }
        minScore = conf.getFloat(FieldNames.MIN_SCORE_PROPERTY, 0.0f);
    }

    @Override
    public boolean shouldFetch(String url, WebPage page, long curTime) {
        Float score = page.getScore();
        if (score != null && score < minScore) {
            logger.debug("Rejecting to fetch URL '{}' because of low score: {} < {}", url, score,
                minScore);
            return false;
        } else if (isAlreadyFetched(page)) {
            logger.debug("Already fetched: {}", url);
            return false;
        } else {
            boolean shouldFetch = super.shouldFetch(url, page, curTime);
            if (logger.isDebugEnabled() && shouldFetch) {
                logger.debug("shouldFetch prev={} fetch={} curr={} diff={} url={}",
                    page.getPrevFetchTime(), page.getFetchTime(), curTime,
                    curTime - page.getPrevFetchTime(), url);
            }
            return shouldFetch;
        }
    }

    boolean isAlreadyFetched(WebPage page) {
        return page.getStatus() == CrawlStatus.STATUS_FETCHED
                || page.getStatus() == CrawlStatus.STATUS_GONE
                || page.getStatus() == CrawlStatus.STATUS_REDIR_PERM
                || page.getStatus() == CrawlStatus.STATUS_NOTMODIFIED;

    }
}
