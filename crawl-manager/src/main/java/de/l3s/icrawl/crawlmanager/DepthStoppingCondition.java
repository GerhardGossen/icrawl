package de.l3s.icrawl.crawlmanager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DepthStoppingCondition implements StoppingCondition {
    private static final Logger logger = LoggerFactory.getLogger(DepthStoppingCondition.class);
    private int remainingBatches;

    public DepthStoppingCondition(int depth) {
        this.remainingBatches = depth;
    }

    @Override
    public boolean continueCrawling() {
        logger.debug("Remaining batches: {}", remainingBatches - 1);
        return --remainingBatches > 0;
    }

}
