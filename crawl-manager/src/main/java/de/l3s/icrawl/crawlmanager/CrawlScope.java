package de.l3s.icrawl.crawlmanager;

public interface CrawlScope {

    /**
     * Check if the given page is in the scope of the crawl.
     *
     * @param url
     *            the URL of the page
     * @param webpage
     *            the webpage (body of the HTTP request)
     * @return a {@link ScopeResult} describing the relevance
     */
    ScopeResult checkScope(String url, String webpage);

    static class ScopeResult {
        private final Double relevance;
        private final boolean skipFurtherTesting;

        ScopeResult(Double relevance, boolean skipFurtherTesting) {
            this.relevance = relevance;
            this.skipFurtherTesting = skipFurtherTesting;
        }

        public static ScopeResult result(double relevance) {
            return new ScopeResult(relevance, false);
        }

        public static ScopeResult noResult() {
            return new ScopeResult(null, false);
        }

        public static ScopeResult finalResult(double relevance) {
            return new ScopeResult(relevance, true);
        }

        /**
         * Get the relevance of the tested resource.
         *
         * @return a value in [0.0, 1.0], where perfect relevance is 1.0 and no
         *         relevance is 0.0 or null, if the CrawlScope could not be
         *         applied
         */
        public Double relevance() {
            return relevance;
        }

        /**
         * Check if this answer is a complete answer.
         *
         * @return true, if no further scope tests SHOULD be conducted
         */
        public boolean skipFurtherTesting() {
            return skipFurtherTesting;
        }

        @Override
        public String toString() {
            return String.format("ScopeResult [relevance=%s, skipFurtherTesting=%s]", relevance,
                skipFurtherTesting);
        }
    }
}
