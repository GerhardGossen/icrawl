package de.l3s.icrawl.crawlmanager.step;

import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;

import org.apache.hadoop.conf.Configuration;

import de.l3s.icrawl.FieldNames;
import de.l3s.icrawl.crawlmanager.TaskExecutionService;
import de.l3s.icrawl.domain.crawl.CrawlExecutionRecord;
import de.l3s.icrawl.domain.crawl.ScoreHistogram;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate;

public class ScoreHistogramStep extends Step<ScoreHistogramJob> {
    private final TaskExecutionService executionsService;
    private final CrawlDataRepositoryTemplate template;


    @Inject
    public ScoreHistogramStep(CrawlDataRepositoryTemplate template,
            TaskExecutionService executionsService) {
        super("scoreHistogram", StepType.NONE);
        this.executionsService = executionsService;
        this.template = template;
    }

    @Override
    ScoreHistogramJob start(CrawlExecutionRecord crawl, Configuration conf)
            throws StepFailedException {
        return new ScoreHistogramJob(executionsService, template);
    }

    @Override
    boolean run() throws StepFailedException {
        try {
            Map<String, ScoreHistogram> results = job.getHistogram(crawl, getBatchKey());
            addMetadata(FieldNames.HISTOGRAMS_KEY, results);
            addMetadata("batchId", batchId);
            return true;
        } catch (IOException e) {
            throw new StepFailedException("Exception while calculating score sum", e,
                crawl.getCrawl());
        }
    }

}
