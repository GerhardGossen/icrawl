package de.l3s.icrawl.crawlmanager;

import java.io.IOException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.avro.util.Utf8;
import org.apache.gora.mapreduce.GoraMapper;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.nutch.crawl.CrawlStatus;
import org.apache.nutch.crawl.DbUpdaterJob;
import org.apache.nutch.storage.WebPage;
import org.apache.nutch.storage.WebPage.Field;
import org.apache.nutch.util.TableUtil;
import org.apache.nutch.util.URLUtil;
import org.apache.nutch.util.domain.DomainSuffix;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multiset.Entry;
import com.google.common.collect.Multisets;
import com.google.common.net.HttpHeaders;

import static de.l3s.icrawl.domain.support.ByteBufferUtils.asList;

import de.l3s.icrawl.FieldNames;
import de.l3s.icrawl.domain.crawl.CrawlStatistic;
import de.l3s.icrawl.domain.crawl.CrawlStatistics;
import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.domain.support.ByteBufferUtils;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate.ResultLinesMapper;
import static org.apache.nutch.storage.WebPage.Field.*;

public class CrawlStatisticsJob {
    private static final Logger logger = LoggerFactory.getLogger(CrawlStatisticsJob.class);
    private static final Set<Field> FIELDS = ImmutableSet.of(CONTENT_TYPE, STATUS, METADATA, MARKERS, HEADERS);
    private static final Map<String, String> TYPE_MAPPING = ImmutableMap.<String, String> builder()
        .put("status", "pie")
        .put("domain", "pie")
        .put("tld", "pie")
        .put("type", "pie")
        .put("size", "column")
        .put("dist", "column")
        .put("meta", "column")
        .put("keyword", "column")
        .put("language", "pie")
        .build();

    public static class CrawlStatisticsReducer extends
            Reducer<Text, LongWritable, Text, LongWritable> {
        private final LongWritable sumWritable = new LongWritable();

        @Override
        protected void reduce(Text key, Iterable<LongWritable> values, Context ctx)
                throws IOException, InterruptedException {
            long sum = 0;
            for (LongWritable value : values) {
                sum += value.get();
            }
            sumWritable.set(sum);
            ctx.write(key, sumWritable);
        }
    }


    public static class CrawlStatisticsMapper extends
            GoraMapper<String, WebPage, Text, LongWritable> {
        private static final Utf8 CONTENT_LENGTH = new Utf8(HttpHeaders.CONTENT_LENGTH);
        private static final LongWritable ONE = new LongWritable(1);
        private final Text keyWritable = new Text();

        @Override
        protected void map(String key, WebPage page, Context context) throws IOException,
                InterruptedException {
            String status = CrawlStatus.getName(page.getStatus().byteValue());
            URL url = new URL(TableUtil.unreverseUrl(key));
            String domainName = URLUtil.getDomainName(url);
            DomainSuffix domainSuffix = URLUtil.getDomainSuffix(url);
            String tld = domainSuffix != null ? domainSuffix.getDomain() : "???";

            keyWritable.set("status." + status);
            context.write(keyWritable, ONE);
            keyWritable.set("domain." + domainName.trim());
            context.write(keyWritable, ONE);
            keyWritable.set("tld." + tld);
            context.write(keyWritable, ONE);
            if (page.getStatus() == CrawlStatus.STATUS_FETCHED) {
                CharSequence contentType = page.getContentType();
                keyWritable.set("type." + contentType.toString());
                context.write(keyWritable, ONE);

                long contentSize = getContentSize(page);
                if (contentSize >= 0) {
                    keyWritable.set("size." + contentSize);
                    context.write(keyWritable, ONE);
                } else {
                    keyWritable.set("size.unknown");
                    context.write(keyWritable, ONE);
                }

            }
            CharSequence distance = page.getMarkers().get(DbUpdaterJob.DISTANCE);
            if (distance != null) {
                keyWritable.set("dist." + distance);
                context.write(keyWritable, ONE);
            }
            ByteBuffer keywords = page.getMetadata().get(FieldNames.DETECTED_KEYWORDS);
            for (String keyword : asList(keywords, String.class)) {
                keyWritable.set("keyword." + keyword);
                context.write(keyWritable, ONE);
            }
            ByteBuffer language = page.getMetadata().get(FieldNames.LANGUAGE);
            if (language != null) {
                keyWritable.set("language." + ByteBufferUtils.asString(language));
                context.write(keyWritable, ONE);
            }
            for (CharSequence metadataKey : page.getMetadata().keySet()) {
                keyWritable.set("meta." + metadataKey);
                context.write(keyWritable, ONE);
            }
        }

        /** Get content size in KB, last digit always zero */
        private long getContentSize(WebPage page) {
            CharSequence contentLength = page.getHeaders().get(CONTENT_LENGTH);
            if (contentLength == null) {
                return -1;
            }
            try {
                long contentSize = Long.parseLong(contentLength.toString());
                return (contentSize / (1024 * 10)) * 10;
            } catch (NumberFormatException e) {
                logger.trace("Could not parse content-length header {}: ", contentLength, e);
                return -1;
            }
        }
    }

    private static class CrawlStatisticsOutputMapper implements
            ResultLinesMapper<Text, LongWritable, CrawlStatistics> {
        private final Map<String, Multiset<String>> statistics = new HashMap<>();

        @Override
        public void processLine(Text tKey, LongWritable value) {
            String key = tKey.toString();
            int val = (int) value.get();
            int delim = key.indexOf(".");
            if (delim < 0) {
                logger.warn("Invalid key: {}", key);
                return;
            }
            String group = key.substring(0, delim);
            String subKey = key.substring(delim + 1);
            add(group, subKey, val);
        }

        private void add(String group, String subKey, int val) {
            Multiset<String> counts = statistics.get(group);
            if (counts == null) {
                counts = HashMultiset.create();
                statistics.put(group, counts);
            }
            counts.add(subKey, val);
        }

        @Override
        public CrawlStatistics getResult() {
            Collection<CrawlStatistic> allStatistics = Lists.newArrayListWithExpectedSize(statistics.size());
            for (Map.Entry<String, Multiset<String>> statistic : statistics.entrySet()) {
                allStatistics.add(new CrawlStatistic(statistic.getKey(),
                    asMap(statistic.getValue()), TYPE_MAPPING.get(statistic.getKey())));
            }
            return new CrawlStatistics(allStatistics);
        }

    }

    private CrawlStatisticsJob() {}

    public static CrawlStatistics run(CrawlDataRepositoryTemplate template, Campaign campaign)
            throws IOException {
        String jobName = "Crawl statistics for campaign " + campaign.getId();

        return template.jobBuilder(campaign, jobName)
            .setFields(FIELDS)
            .setMapper(CrawlStatisticsMapper.class, Text.class, LongWritable.class)
            .setReducer(CrawlStatisticsReducer.class, Text.class, LongWritable.class)
            .setCombiner(CrawlStatisticsReducer.class)
            .setNumReducers(1)
            .executeAndGetResult(new CrawlStatisticsOutputMapper());
    }

    private static <T> Map<T, Long> asMap(Multiset<T> multiset) {
        ImmutableMap.Builder<T, Long> builder = ImmutableMap.builder();
        for (Entry<T> entry : Multisets.copyHighestCountFirst(multiset).entrySet()) {
            builder.put(entry.getElement(), Long.valueOf(entry.getCount()));
        }
        return builder.build();
    }

}
