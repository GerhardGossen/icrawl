package de.l3s.icrawl.crawlmanager.step;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.l3s.icrawl.crawlmanager.TaskExecutionService;
import de.l3s.icrawl.domain.crawl.CrawlExecutionRecord;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate;

public class ScoreCountStep extends Step<BatchScoreCounter> {
    public static final String NAME = "scoreCount";
    private static final Logger logger = LoggerFactory.getLogger(ScoreCountStep.class);
    private final CrawlDataRepositoryTemplate template;
    private final TaskExecutionService executionsService;

    public ScoreCountStep(CrawlDataRepositoryTemplate template,
            TaskExecutionService executionsService) {
        super(NAME, StepType.GENERATE);
        this.template = template;
        this.executionsService = executionsService;
    }

    @Override
    BatchScoreCounter start(CrawlExecutionRecord crawl, Configuration conf) {
        return new BatchScoreCounter(template, executionsService, conf);
    }

    @Override
    boolean run() throws StepFailedException {
        try {
            Double scoreSum = job.getScoreSum(crawl, getBatchKey());
            logger.info("ScoreSum: {}", scoreSum);
            addMetadata("scoreSum", scoreSum != null ? Double.toString(scoreSum) : null);
            addMetadata("batchId", batchId);
            // stop when no URLs are generated
            return scoreSum != null;
        } catch (IOException e) {
            throw new StepFailedException("Exception while calculating score sum", e,
                crawl.getCrawl());
        }
    }
}