package de.l3s.icrawl.crawlmanager.step;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.util.Map.Entry;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.nutch.crawl.InjectorJob;

import de.l3s.icrawl.domain.crawl.CrawlExecutionRecord;
import de.l3s.icrawl.domain.specification.CrawlUrl;
import de.l3s.icrawl.domain.specification.CrawlUrl.Type;

public class InjectStep extends Step<InjectorJob> {

    private File seedsDirectory;

    public InjectStep() {
        super("inject", StepType.INJECT);
    }

    @Override
    InjectorJob start(CrawlExecutionRecord crawl, Configuration conf) throws StepFailedException {
        try {
            InjectorJob injector = new InjectorJob(conf);
            seedsDirectory = createSeedsDirectory(crawl);
            return injector;
        } catch (IOException e) {
            throw new StepFailedException("Could not generate seeds directory", e, crawl.getCrawl());
        }
    }

    File createSeedsDirectory(CrawlExecutionRecord crawl) throws IOException {
        File dir = Files.createTempDirectory("inject-").toFile();
        try (Writer writer = new FileWriter(new File(dir, "seeds"))) {
            for (CrawlUrl url : crawl.getConfiguration().getSeeds()) {
                if (url.getType() != Type.WEB) {
                    continue;
                }
                writer.write(url.getUrl());
                for (Entry<String, Object> metadatum : url.getMetadata().entrySet()) {
                    writer.write("\t");
                    writer.write(metadatum.getKey());
                    writer.write("=");
                    writer.write(metadatum.getValue().toString());
                }
                writer.write('\n');
            }
        }
        return dir;
    }

    @Override
    boolean run() throws StepFailedException {
        try {
            job.inject(new Path(seedsDirectory.toURI()));
            return true;
        } catch (Exception e) {
            throw new StepFailedException("Failed during inject", e, crawl.getCrawl());
        }
    }

    @Override
    void cleanup() {
        if (seedsDirectory != null) {
            for (File seedFile : seedsDirectory.listFiles()) {
                seedFile.delete();
            }
            seedsDirectory.delete();
        }
    }
}