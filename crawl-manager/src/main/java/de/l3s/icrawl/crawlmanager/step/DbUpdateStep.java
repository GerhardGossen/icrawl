package de.l3s.icrawl.crawlmanager.step;

import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.crawl.DbUpdaterJob;
import org.apache.nutch.metadata.Nutch;
import org.apache.nutch.util.ToolUtil;

import de.l3s.icrawl.domain.crawl.CrawlExecutionRecord;

public class DbUpdateStep extends Step<DbUpdaterJob> {

    public DbUpdateStep() {
        super("updateDB", StepType.UPDATE_DB);
    }
    @Override
    DbUpdaterJob start(CrawlExecutionRecord crawl, Configuration conf) {
        return new DbUpdaterJob(conf);
    }

    @Override
    boolean run() throws StepFailedException {
        try {
            job.run(ToolUtil.toArgMap(Nutch.ARG_BATCH, Nutch.ALL_BATCH_ID_STR));
            return true;
        } catch (Exception e) {
            throw new StepFailedException("Failed to update DB", e, crawl.getCrawl());
        }
    }
}