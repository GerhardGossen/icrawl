package de.l3s.icrawl.crawlmanager;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.crawl.FetchSchedule;
import org.apache.nutch.crawl.GeneratorJob;
import org.apache.nutch.metadata.Nutch;
import org.apache.nutch.storage.WebPage;
import org.apache.nutch.util.TableUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableList;

import de.l3s.icrawl.FieldNames;
import de.l3s.icrawl.crawlmanager.step.DbUpdateStep;
import de.l3s.icrawl.crawlmanager.step.FetchStep;
import de.l3s.icrawl.crawlmanager.step.GenerateStep;
import de.l3s.icrawl.crawlmanager.step.InjectStep;
import de.l3s.icrawl.crawlmanager.step.ParseStep;
import de.l3s.icrawl.crawlmanager.step.ScoreCountStep;
import de.l3s.icrawl.crawlmanager.step.ScoreHistogramStep;
import de.l3s.icrawl.crawlmanager.step.Step;
import de.l3s.icrawl.crawlmanager.step.StepFailedException;
import de.l3s.icrawl.domain.crawl.BatchRecord;
import de.l3s.icrawl.domain.crawl.CrawlExecutionRecord;
import de.l3s.icrawl.domain.crawl.StepExecutionRecord;
import de.l3s.icrawl.domain.crawl.TaskExecutionRecord;
import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.domain.specification.Crawl;
import de.l3s.icrawl.domain.specification.CrawlSpecification;
import de.l3s.icrawl.domain.specification.CrawlSpecificationUtils;
import de.l3s.icrawl.domain.specification.CrawlUrl;
import de.l3s.icrawl.domain.specification.WeightVector;
import de.l3s.icrawl.domain.specification.WeightingMethod;
import de.l3s.icrawl.domain.status.CrawlStatus;
import de.l3s.icrawl.online.DocumentVectorSimilarity;
import de.l3s.icrawl.online.DocumentVectorUtils;
import de.l3s.icrawl.repository.CrawlDataRepositoryTemplate;

import static com.google.common.base.MoreObjects.firstNonNull;

public class CrawlJob implements TaskExecution {
    private static final Logger logger = LoggerFactory.getLogger(CrawlJob.class);
    private static final String RESOURCES_SIZE_LIMIT = String.valueOf(5 * 1024 * 1024); // 5 MByte

    private final CrawlExecutionRecord crawlRecord;
    private final StoppingCondition stoppingCondition;
    private Throwable exception;
    private boolean stopped = false;
    private boolean continueCrawl = true;
    private boolean retriedGenerateStep = false;
    private final Configuration conf;
    private final List<Step<?>> crawlSteps;
    private Step<?> runningStep = null;
    private final InjectStep injectStep;
    private State state = State.STARTING;
    private final CrawlDataRepositoryTemplate template;

    public CrawlJob(CrawlExecutionRecord crawl, StoppingCondition stoppingCondition,
            Configuration conf, CrawlDataRepositoryTemplate template, long batchSize,
            TaskExecutionService executionsService) {
        this.crawlRecord = crawl;
        this.stoppingCondition = stoppingCondition;
        this.template = template;
        this.conf = confForCrawl(conf, crawl);
        applyConfiguration();
        injectStep = new InjectStep();
        boolean applyFilterPlugin = conf.getBoolean(GeneratorJob.GENERATOR_FILTER, true);
        boolean applyNormPlugin = conf.getBoolean(GeneratorJob.GENERATOR_NORMALISE, true);
        crawlSteps = ImmutableList.of(
//            new ClearGenerateMarksStep(),
            new GenerateStep(batchSize, applyFilterPlugin, applyNormPlugin),
            new ScoreCountStep(template, executionsService),
            new FetchStep(),
            new ParseStep(),
            new ScoreHistogramStep(template, executionsService),
            new DbUpdateStep()
        );
    }

    public static Configuration confForCrawl(Configuration conf, CrawlExecutionRecord crawl) {
        Configuration newConf = new Configuration(conf);
        String crawlId = crawl.crawlKey();
        newConf.set(Nutch.CRAWL_ID_KEY, crawlId);
        return newConf;
    }

    protected void applyConfiguration() {
        conf.setBoolean("mapred.mapper.new-api", true);
        conf.setBoolean("mapred.reducer.new-api", true);
        CrawlSpecification spec = crawlRecord.getConfiguration();
        logger.trace("Got crawl specification for crawl {}: {}", crawlRecord.crawlKey(), spec);
        if (spec.isDownloadResources()) {
            conf.set("urlfilter.regex.file", "regex-urlfilter-withResources.txt");
            conf.set("http.content.limit", RESOURCES_SIZE_LIMIT);
        } else {
            conf.set("urlfilter.regex.file", "regex-urlfilter.txt");
        }
        conf.set(GeneratorJob.GENERATOR_MAX_COUNT, "100");
        conf.set(GeneratorJob.GENERATOR_COUNT_MODE, GeneratorJob.GENERATOR_COUNT_VALUE_DOMAIN);
        conf.set("fetcher.queue.depth.multiplier", "100");
        CrawlSpecificationUtils.set(conf, spec);
        if (conf.get(FieldNames.WEIGHTS_KEY, "").isEmpty() && !spec.getKeywords().isEmpty()) {
            WeightVector weightVector = WeightVector.uniform(spec.getKeywords());
            conf.set(FieldNames.WEIGHTS_KEY, CrawlSpecificationUtils.toString(weightVector));
        }
        WeightingMethod weightingMethod = firstNonNull(spec.getWeightingMethod(),
            WeightingMethod.DOCUMENT_VECTOR);
        if (weightingMethod.requiresKeywords() && spec.getKeywords().isEmpty()
                && spec.getEntities().isEmpty()) {
            logger.info("Missing keywords for crawl {}, reverting to GRAPH_ONLY", crawlRecord);
            weightingMethod = WeightingMethod.GRAPH_ONLY;
        }
        conf.setEnum(FieldNames.WEIGHTING_METHOD_KEY, weightingMethod);

        conf.setClass("db.fetch.schedule.class", DateStoringFetchSchedule.class,
            FetchSchedule.class);

        conf.setFloat(FieldNames.MIN_SCORE_PROPERTY,
            weightingMethod == WeightingMethod.GRAPH_ONLY ? 0f : 0.001f);

        if (!spec.getReferenceDocuments().isEmpty()) {
            try {
                DocumentVectorSimilarity referenceVector = DocumentVectorUtils.createWebReferenceVector(spec);
                DocumentVectorUtils.storeToConfiguration(conf, referenceVector);
                logger.info("Using reference vector {} for crawl {}", referenceVector, crawlRecord);
            } catch (IOException e) {
                logger.info("Exception while creating reference vector: ", e);
            }
        }
    }

    @Override
    public void execute(TaskExecutionContext context) {
        logger.info("Starting CrawlJob.run for execution {}", crawlRecord.getId());
        if (runningStep != null) {
            throw new IllegalStateException("CrawlJob is already running");
        }
        this.state = State.RUNNING;
        try {
            logger.debug("About to start batch loop, stopped={}, continueCrawl={}", stopped,
                continueCrawl);
            BatchRecord setupBatch = context.startBatch();
            runInject(context);
            context.finishBatch(setupBatch);
            boolean continueAfterBatch = true;
            boolean firstBatch = true;
            while (!stopped && continueCrawl && continueAfterBatch) {
                Stopwatch stopwatch = Stopwatch.createStarted();
                continueAfterBatch = runBatch(context);
                if (firstBatch) {
                    runSetupAfterFirstBatch();
                    firstBatch = false;
                }
                logger.info("Finished batch in {}", stopwatch.stop());
                logSeedStatus(crawlRecord.getCampaign(), crawlRecord.getConfiguration().getSeeds());
            }
            logger.info(
                "Crawl execution over: stopped={}, continueCrawl={}, continueAfterBatch={}",
                stopped, continueCrawl, continueAfterBatch);
            this.state = stopped ? State.STOPPED : State.FINISHED;
        } catch (StepFailedException e) {
            logger.info(e.getMessage(), e.getCause());
            this.exception = e.getCause();
            this.state = State.FAILED;
        } catch (Exception e) {
            logger.warn("Unexpected exception in CrawlJob", e);
        }
    }

    private void logSeedStatus(Campaign campaign, Set<CrawlUrl> seeds) throws IOException {
        for (CrawlUrl crawlUrl : seeds) {
            String url = crawlUrl.getUrl();
            WebPage webPage = template.getWebPage(campaign, TableUtil.reverseUrl(url));
            if (webPage != null) {
                String status = org.apache.nutch.crawl.CrawlStatus.getName(webPage.getStatus()
                    .byteValue());
                Long fetchTime = webPage.getFetchTime();
                logger.info("Seed {}: {}, {}", url, status, fetchTime);
            } else {
                logger.info("Seed {} not found in DB", url);
            }
        }
    }

    /**
     * Additional setup after first batch has been processed. Allows refinement
     * of configuration based on content of seed pages.
     */
    protected void runSetupAfterFirstBatch() {
        try {
            if (DocumentVectorUtils.readFromConfiguration(conf) != null) {
                logger.info("Found existing reference vector, not overwriting it");
                return;
            }
            DocumentVectorSimilarity referenceVector = DocumentVectorUtils.createCrawlReferenceVector(
                template, crawlRecord.getCampaign().getId(), crawlRecord.getConfiguration(), true, -1, false);
            DocumentVectorUtils.storeToConfiguration(conf, referenceVector);
            logger.info("Using reference vector {} for crawl {}", referenceVector, crawlRecord);
        } catch (IOException e) {
            logger.info("Could not create reference vector for crawl job {} because of exception ",
                crawlRecord, e);
        }
        logger.info("Finished setup after first batch");
    }

    private void runInject(TaskExecutionContext context) throws StepFailedException {
        StepExecutionRecord injectStepData = context.startStep(injectStep.getName());
        injectStep.execute(crawlRecord, conf, -1);
        context.finishStep(injectStepData);
        logger.debug("Finished inject step, stopped={}, continueCrawl={}", stopped, continueCrawl);
    }

    private boolean runBatch(TaskExecutionContext context) throws StepFailedException {
        BatchRecord batch = context.startBatch();
        logger.debug("Starting batch {}", batch);
        boolean continueAfterStep = true;
        try {
            for (Step<?> step : crawlSteps) {
                if (stopped) {
                    logger.info("Stopped before starting step {} on batch {}", step.getName(),
                        batch.getCrawlKey());
                    return false;
                }
                continueAfterStep = runStep(batch, step, context);
                if (!continueAfterStep && step.getName().equals(ScoreCountStep.NAME)
                        && !retriedGenerateStep) {
                    logger.info("Got empty Generate result, retrying once");
                    retriedGenerateStep = true;
                    return true;
                }
                if (!continueAfterStep) {
                    batch.addMetadata("failingStep", step.getName());
                    return false;
                }
            }
            continueCrawl = stoppingCondition.continueCrawling();
        } finally {
            batch.addMetadata("stopped", Boolean.toString(stopped));
            batch.addMetadata("continueCrawl", Boolean.toString(continueCrawl));
            context.finishBatch(batch);
        }
        return true;
    }

    private boolean runStep(BatchRecord batch, Step<?> step, TaskExecutionContext context)
            throws StepFailedException {
        StepFailedException thrownException = null;
        runningStep = step;
        StepExecutionRecord stepData = context.startStep(batch, step.getName());
        boolean continueCrawlAfterStep = false;
        try {
            continueCrawlAfterStep = step.execute(crawlRecord, conf, batch.getSeqNumber());
            template.getStore(crawlRecord.getCampaign().getId()).flush();
        } catch (StepFailedException e) {
            thrownException = e;
            throw e;
        } catch (IOException e) {
            StepFailedException sfe = new StepFailedException("Could not flush data store", e,
                getCrawl());
            thrownException = sfe;
            throw sfe;
        } finally {
            stepData.addMetadata(step.getMetadata());
            if (thrownException != null) {
                stepData.addMetadata("exception", thrownException.getCause().getMessage());
            }
            context.finishStep(stepData);
            runningStep = null;
        }
        printBatchSize(crawlRecord, batch.getSeqNumber(), step.getName());
        return continueCrawlAfterStep;
    }

    @Override
    public CrawlStatus getStatus() {
        switch (state) {
        case STARTING:
            return CrawlStatus.starting(crawlRecord.getCampaign(), crawlRecord.getCrawl());
        case RUNNING:
            return runningStep != null ? runningStep.getStatus() : CrawlStatus.unknown(
                crawlRecord.getCampaign(), crawlRecord.getCrawl(), "Current step is unknown");
        case FINISHED:
            return CrawlStatus.finished(crawlRecord.getCampaign(), crawlRecord.getCrawl());
        case FAILED:
            return CrawlStatus.failed(crawlRecord.getCampaign(), crawlRecord.getCrawl(),
                exception.getMessage(), 0.0);
        case STOPPING:
        case KILLING:
            return CrawlStatus.stopping(crawlRecord.getCampaign(), crawlRecord.getCrawl(), getProgress());
        case STOPPED:
        case KILLED:
            return CrawlStatus.stopped(crawlRecord.getCampaign(), crawlRecord.getCrawl());
        default:
            throw new IllegalStateException("Job is in unhandled state " + state);
        }
    }

    @Override
    public void stop() {
        this.state = State.STOPPING;
        this.stopped = true;
    }

    @Override
    public void kill() {
        this.state = State.KILLING;
        stop();
        if (runningStep != null) {
            try {
                runningStep.getJob().killJob();
            } catch (Exception e) {
                logger.info("Exception while killing job for crawl {}: ", crawlRecord.crawlKey(), e);
                this.exception = e;
            }
        }
        this.state = State.KILLED;
    }

    @Override
    public double getProgress() {
        switch (runningStep.getJobType()) {
        case NONE:
            return 0.0;
        case INJECT:
            return 0.1;
        case GENERATE:
            return 0.2;
        case FETCH:
            return 0.5;
        case PARSE:
            return 0.8;
        case UPDATE_DB:
            return 0.9;

        default:
            return -1;
        }
    }

    public Crawl getCrawl() {
        return crawlRecord.getCrawl();
    }

    private void printBatchSize(TaskExecutionRecord<?> execution, int currentBatch, String step) {
        if (currentBatch > 1) {
            try {
                String[] fields = new String[] { WebPage.Field.BATCH_ID.getName() };
                int count = template.query(crawlRecord.getCampaign(), fields, "", 1000,
                    (key, page) -> null,
                    CrawlDataRepositoryTemplate.batchQuery(execution, currentBatch - 1)).size();
                logger.info("#records in batch {} after step {} of batch {}: {}", currentBatch - 1,
                    step, currentBatch, count);
            } catch (IOException e) {
                logger.info("Exception while counting records in batch {}-1 after step {}: ",
                    currentBatch, step, e);
            }
        }

    }

}
