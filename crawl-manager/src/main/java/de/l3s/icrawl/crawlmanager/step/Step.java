package de.l3s.icrawl.crawlmanager.step;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.metadata.Nutch;
import org.apache.nutch.util.NutchTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.SimpleTimeLimiter;
import com.google.common.util.concurrent.TimeLimiter;

import de.l3s.icrawl.FieldNames;
import de.l3s.icrawl.domain.crawl.CrawlExecutionRecord;
import de.l3s.icrawl.domain.status.CrawlStatus;
import de.l3s.icrawl.domain.status.RunningCrawlStatus;

public abstract class Step<T extends NutchTool> {
    public enum StepType {
        NONE, INJECT, GENERATE, FETCH, PARSE, UPDATE_DB
    }

    private static final Logger logger = LoggerFactory.getLogger(Step.class);
    private static final Field RESULTS_FIELD = getResultsFieldAccessor();
    private static final long TIME_LIMIT = 60;
    private static final int MAX_FAILURE = 1;
    private final String name;
    private final StepType jobType;
    private final Map<String, Object> metadata = new HashMap<>();
    private final TimeLimiter timeLimiter = new SimpleTimeLimiter();
    protected T job;
    protected int batchId;
    protected CrawlExecutionRecord crawl;

    protected Step(String name, StepType jobType) {
        this.name = name;
        this.jobType = jobType;
    }

    private static Field getResultsFieldAccessor() {
        try {
            Field[] fields = NutchTool.class.getDeclaredFields();
            for (Field field : fields) {
                if ("results".equals(field.getName())) {
                    field.setAccessible(true);
                    return field;
                }
            }
            logger.info("Could not find field \"results\", got: {}", (Object) fields);
        } catch (SecurityException e) {
            logger.info("Could not access NutchTool field results: ", e);
        }
        return null;
    }

    /**
     * Perform setup of the step.
     *
     * @return the NutchTool instance for the actual processing
     */
    abstract T start(CrawlExecutionRecord crawl, Configuration conf) throws StepFailedException;

    /**
     * Actually run the tool.
     *
     * @return true, if the crawl should continue
     */
    abstract boolean run() throws StepFailedException;

    public String getName() {
        return name;
    }

    public StepType getJobType() {
        return jobType;
    }

    public NutchTool getJob() {
        return job;
    }

    void cleanup() {}

    public void addMetadata(String key, Object value) {
        metadata.put(key, value);
    }

    public void addMetadata(Map<String, Object> values) {
        metadata.putAll(values);
    }

    public boolean execute(CrawlExecutionRecord crawl, Configuration conf, int batchId)
            throws StepFailedException {
        this.crawl = crawl;
        boolean continueCrawl = false;
        logger.debug("Starting step {} for batch {} in schema {}", getName(), batchId,
            conf.get(FieldNames.SCHEMA_NAME_PROPERTY));
        int numFailures = 0;
        try {
            boolean ranSuccessfully = false;
            while (!ranSuccessfully && numFailures <= MAX_FAILURE + 1) {
                try {
                    this.batchId = batchId;
                    continueCrawl = timeLimiter.callWithTimeout(() -> {
                        job = start(crawl, conf);
                        return run();
                    }, TIME_LIMIT, TimeUnit.MINUTES, true);
                    ranSuccessfully = true;
                    logger.debug("Finished step {} for batch {}", getName(), batchId);
                } catch (Exception e) {
                    logger.info("Job {} failed, killing it and maybe retrying (numFailures = {})",
                        getName(), numFailures, e);
                    numFailures++;
                }
            }
        } catch (Exception e) {
            throw new StepFailedException(getName(), e, crawl.getCrawl());
        } finally {
            saveCounters();

            cleanup();
        }
        return continueCrawl;
    }

    private void saveCounters() {
        Map<String, Object> results = getResults(job);
        if (!results.isEmpty()) {
            addMetadata(results);
        }
    }

    private static Map<String, Object> getResults(NutchTool job) {
        if (RESULTS_FIELD == null) {
            return new HashMap<>();
        }
        Object result = null;
        try {
            result = RESULTS_FIELD.get(job);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            logger.info("Could not retrieve results from job {}: ", job, e);
        }
        if (result instanceof Map<?, ?>) {
            @SuppressWarnings("unchecked")
            Map<String, Object> resultMap = (Map<String, Object>) result;
            Object jobs = resultMap.get(Nutch.STAT_JOBS);
            if (jobs instanceof Map<?, ?>) {
                @SuppressWarnings("unchecked")
                Map<String, Object> jobsMap = (Map<String, Object>) jobs;
                if (!jobsMap.isEmpty()) {
                    @SuppressWarnings("unchecked")
                    Map<String, Object> currentJob = (Map<String, Object>) jobsMap.values().iterator().next();
                    logger.trace("Retrieved job results: {}", currentJob);
                    return currentJob;
                }
            }
        }
        return new HashMap<>();
    }

    public CrawlStatus getStatus() {
        return new RunningCrawlStatus(crawl.getCampaign(), crawl.getCrawl(), crawl.getId(),
            batchId, getName(), getJob().getProgress(), crawl.getStartTime(), (Long) null,
            "Running");
    }

    public Map<String, Object> getMetadata() {
        return Collections.unmodifiableMap(metadata);
    }

    protected String getBatchKey() {
        return crawl.getId() + "-" + batchId;
    }
}