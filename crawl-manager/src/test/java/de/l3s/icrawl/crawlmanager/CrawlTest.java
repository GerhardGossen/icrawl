package de.l3s.icrawl.crawlmanager;

import org.junit.Test;

import de.l3s.icrawl.domain.specification.Campaign;
import de.l3s.icrawl.domain.specification.Crawl;
import de.l3s.icrawl.domain.specification.CrawlSpecification;
import de.l3s.icrawl.domain.specification.NamedEntity;
import de.l3s.icrawl.domain.specification.NamedEntity.Type;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

public class CrawlTest {

    @Test
    public void testCrawlSetup() {
        Campaign campaign = new Campaign("test");
        Crawl crawl = createCrawl();
        campaign.addCrawl(crawl);

        assertThat(crawl.getName(), is("testCrawl"));
        assertThat(crawl.getSpecification().getKeywords(), hasSize(1));
    }

    Crawl createCrawl() {
        Crawl crawl = new Crawl();
        crawl.setName("testCrawl");
        CrawlSpecification spec = new CrawlSpecification();
        spec.setDownloadResources(true);
        spec.addKeyword("l3s");
        spec.addEntity(new NamedEntity(Type.ORGANIZATION, "L3S"));
        crawl.setSpecification(spec);
        return crawl;
    }

    //    @Test
    //    public void testStartCrawl() throws Exception {
    //        DataSource ds;
    //        ObjectMapper jsonFactory;
    //        CrawlManager manager = new CrawlManagerImpl(new InMemoryJobQueue(),
    //            new MetadataRepositoryImpl(ds, jsonFactory), ds, jsonFactory);
    //        Crawl crawl = createCrawl();
    //        JobStatus status = manager.startCrawl(crawl);
    //        assertThat(status.getStatus(), anyOf(equalTo(STARTING), equalTo(QUEUED), equalTo(RUNNING)));
    //    }
}
